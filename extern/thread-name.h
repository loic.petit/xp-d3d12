#pragma once

#include <thread>
#include <utils/utils.h>

#include <windows.h>
#include <processthreadsapi.h>

inline void SetThreadName(HANDLE handle, const wchar_t* threadName) {
    ThrowIfFailed(SetThreadDescription(
        handle,
        threadName
    ));
}

inline void SetThreadName(const wchar_t* threadName) {
    SetThreadName(GetCurrentThread(), threadName);
}

inline void SetThreadName(std::thread& thread, const wchar_t* threadName) {
    SetThreadName(thread.native_handle(), threadName);
}
