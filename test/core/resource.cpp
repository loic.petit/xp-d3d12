#include "core/resource.h"
#include <gtest/gtest.h>

TEST(ResourceTest, StateMachine) {
    Resource r;
    D3D12_RESOURCE_BARRIER barrier;
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_FALSE(r.commitTransition(barrier));

    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Flags, D3D12_RESOURCE_BARRIER_FLAG_BEGIN_ONLY);
    ASSERT_EQ(barrier.Transition.StateBefore, D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Transition.StateAfter, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    r.stateEnd();
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(barrier.Flags, D3D12_RESOURCE_BARRIER_FLAG_END_ONLY);
    ASSERT_EQ(barrier.Transition.StateBefore, D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Transition.StateAfter, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    r.state(D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Flags, D3D12_RESOURCE_BARRIER_FLAG_NONE);
    ASSERT_EQ(barrier.Transition.StateBefore, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(barrier.Transition.StateAfter, D3D12_RESOURCE_STATE_COMMON);

    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    r.stateEnd();
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(barrier.Flags, D3D12_RESOURCE_BARRIER_FLAG_NONE);
    ASSERT_EQ(barrier.Transition.StateBefore, D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Transition.StateAfter, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    r.state(D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));

    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    r.state(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(barrier.Flags, D3D12_RESOURCE_BARRIER_FLAG_NONE);
    ASSERT_EQ(barrier.Transition.StateBefore, D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Transition.StateAfter, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    r.state(D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));

    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_TRUE(r.commitTransition(barrier));
    r.state(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_EQ(barrier.Flags, D3D12_RESOURCE_BARRIER_FLAG_END_ONLY);
    ASSERT_EQ(barrier.Transition.StateBefore, D3D12_RESOURCE_STATE_COMMON);
    ASSERT_EQ(barrier.Transition.StateAfter, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    r.state(D3D12_RESOURCE_STATE_COMMON);
    ASSERT_TRUE(r.commitTransition(barrier));

    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_THROW(r.state(D3D12_RESOURCE_STATE_COMMON), std::invalid_argument);
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_THROW(r.state(D3D12_RESOURCE_STATE_COMMON), std::invalid_argument);
    r.stateEnd();
    ASSERT_TRUE(r.commitTransition(barrier));
    ASSERT_EQ(r.state(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    // Empty transitions
    r.stateBegin(D3D12_RESOURCE_STATE_COMMON);
    ASSERT_THROW(r.stateBegin(D3D12_RESOURCE_STATE_COMMON), std::invalid_argument);
    r.stateEnd();
    //ASSERT_THROW(r.stateEnd(), std::invalid_argument);
    r.state(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_FALSE(r.commitTransition(barrier));

    // empty transition
    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    r.stateEnd();
    ASSERT_FALSE(r.commitTransition(barrier));
    // empty transition
    r.stateBegin(D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    ASSERT_FALSE(r.commitTransition(barrier));
    r.stateEnd();
    ASSERT_FALSE(r.commitTransition(barrier));
}