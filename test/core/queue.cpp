#include "core/queue.h"
#include "core/device.h"
#include "core/commandlist.h"
#include <gtest/gtest.h>
#include <utils/service.h>

class CommandQueueTest : public ::testing::Test {
public:
    static void SetUpTestSuite() {
        Service<Device>::build();
        Get<Device>().init();
    }

    static void TearDownTestSuite() {
        Service<Device>::destroy();
    }
};

TEST_F(CommandQueueTest, Sync) {
    Queue queue(D3D12_COMMAND_LIST_TYPE_DIRECT);
    Queue syncQueue(D3D12_COMMAND_LIST_TYPE_DIRECT);

    queue.init(L"queue");
    syncQueue.init(L"syncQueue");

    std::shared_ptr<CommandList> commandList = queue.createCommandList();
    auto baseFence = queue.latestFenceValue();
    ASSERT_TRUE(commandList);

    {
        // this will be destroyed
        std::shared_ptr<CommandList> newCommandList = queue.createCommandList();
        ASSERT_TRUE(newCommandList);
        ASSERT_NE(commandList.get(), newCommandList.get());
    }

    ASSERT_EQ(queue.latestFenceValue(), baseFence);

    // block current queue with syncQueue
    queue->Wait(syncQueue.m_fence.Get(), syncQueue.m_fenceValue+1);

    // technically we have two command lists in preparation
    ASSERT_EQ(queue.m_inPreparationLists.size(), 2);
    // none in cache
    ASSERT_TRUE(queue.m_availableLists.empty());
    // none in gpu
    ASSERT_TRUE(queue.m_inFlightQueue.empty());

    auto reuseCheck = commandList;

    // execute
    queue.execute(commandList);
    ASSERT_FALSE(commandList);
    // the fence has advanced
    ASSERT_NE(queue.latestFenceValue(), baseFence);
    // but it has not been executed because we've blocked it to syncQueue
    ASSERT_FALSE(queue.isFenceReady(queue.latestFenceValue()));

    // we have now 0 in preparation because we have
    // 1. removed the one that was destroyed else where
    // 2. sent one in gpu
    ASSERT_TRUE(queue.m_inPreparationLists.empty());
    // none in cache
    ASSERT_TRUE(queue.m_availableLists.empty());
    // one in gpu
    ASSERT_EQ(queue.m_inFlightQueue.size(), 1);

    // getting a new CL will be new one, but it's still in flight
    {
        std::shared_ptr<CommandList> newCommandList = queue.createCommandList();
        ASSERT_TRUE(newCommandList);
        ASSERT_NE(reuseCheck.get(), newCommandList.get());
    }
    // now it believes a new preparation is in progress
    ASSERT_EQ(queue.m_inPreparationLists.size(), 1);

    // let's force advances
    syncQueue.signal();
    queue.waitForFence(queue.latestFenceValue());

    // by definition, we've waited for the fence to work
    ASSERT_TRUE(queue.isFenceReady(queue.latestFenceValue()));

    // now the original CL can be safely reused! (wee)
    {
        std::shared_ptr<CommandList> newCommandList = queue.createCommandList();
        ASSERT_TRUE(newCommandList);
        ASSERT_EQ(reuseCheck.get(), newCommandList.get());
    }

    // now it thinks there are more in prep
    ASSERT_EQ(queue.m_inPreparationLists.size(), 2);
    ASSERT_TRUE(queue.m_availableLists.empty());
    ASSERT_TRUE(queue.m_inFlightQueue.empty());

    // cleaning preparation, we should only have the local commandList
    queue.clearInPreparation();

    ASSERT_EQ(queue.m_inPreparationLists.size(), 1);
    ASSERT_TRUE(queue.m_availableLists.empty());
    ASSERT_TRUE(queue.m_inFlightQueue.empty());

    reuseCheck.reset();

    // no refs exist outside so we can clean it!
    queue.clearInPreparation();

    ASSERT_TRUE(queue.m_inPreparationLists.empty());
    ASSERT_TRUE(queue.m_availableLists.empty());
    ASSERT_TRUE(queue.m_inFlightQueue.empty());
}