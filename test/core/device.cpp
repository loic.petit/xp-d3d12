#include "core/queue.h"
#include "core/device.h"
#include "core/commandlist.h"
#include <gtest/gtest.h>
#include <utils/service.h>

class DeviceTest : public ::testing::Test {
public:
    static void SetUpTestSuite() {
        Service<Device>::build();
        Get<Device>().init();
    }

    static void TearDownTestSuite() {
        Service<Device>::destroy();
    }
};

TEST_F(DeviceTest, uploadAndDownload) {
    Queue queue(D3D12_COMMAND_LIST_TYPE_COPY);
    queue.init(L"queue");
    auto ptr = queue.createCommandList();
    CommandList& list = *ptr;

    uint8_t test[16] = {
            1, 2, 3, 4, 5, 6, 7, 8,
            9, 10, 11, 12, 13, 14, 15, 16
    };

    Resource resource = Get<Device>().uploadBuffer(list, test, sizeof(test));
    // we have a GPU address \o/
    ASSERT_TRUE(resource->GetGPUVirtualAddress());
    size_t size;
    Resource download = Get<Device>().downloadBufferInto(list, resource, size);
    ASSERT_TRUE(download->GetGPUVirtualAddress());
    ASSERT_NE(resource->GetGPUVirtualAddress(), download->GetGPUVirtualAddress());
    ASSERT_EQ(sizeof(test), size);
    queue.execute(ptr);
    queue.waitForFence(queue.latestFenceValue());

    void *mem;
    download->Map(0, nullptr, &mem);
    ASSERT_EQ(memcmp(mem, test, sizeof(test)), 0);
    download->Unmap(0, nullptr);
}

TEST_F(DeviceTest, uploadAndDownloadTexture) {
    Queue queue(D3D12_COMMAND_LIST_TYPE_COPY);
    queue.init(L"queue");
    auto ptr = queue.createCommandList();
    CommandList& list = *ptr;

    uint8_t test[9][4] = {
            {255, 254, 253, 252}, {251, 250, 249, 248}, {247, 246, 245, 244},
            {235, 234, 233, 232}, {231, 230, 229, 228}, {227, 226, 225, 224},
            {215, 214, 213, 212}, {211, 210, 209, 208}, {207, 206, 205, 204},
    };

    D3D12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM, 3, 3, 1, 1, 1);
    Resource resource = Get<Device>().uploadTexture(list, desc, test, sizeof(test));

    D3D12_SUBRESOURCE_FOOTPRINT footprint;
    Resource download = Get<Device>().downloadTextureInto(list, resource, 0, footprint);
    ASSERT_TRUE(download->GetGPUVirtualAddress());
    ASSERT_EQ(footprint.Width, desc.Width);
    ASSERT_EQ(footprint.Height, desc.Height);
    queue.execute(ptr);
    queue.waitForFence(queue.latestFenceValue());

    void *mem;
    download->Map(0, nullptr, &mem);
    for (uint32_t x = 0 ; x < footprint.Height ; x++) {
        ASSERT_EQ(memcmp(((uint8_t*) mem) + footprint.RowPitch * x, ((uint8_t*) test) + 3 * 4 * x, 3 * 4), 0);
    }
    download->Unmap(0, nullptr);
}