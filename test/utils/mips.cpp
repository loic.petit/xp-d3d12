#include "core/queue.h"
#include "core/device.h"
#include "core/commandlist.h"
#include "utils/mips.h"
#include <gtest/gtest.h>
#include <utils/service.h>

static void executeMips(Queue& copy, Resource& resource) {
    Queue compute(D3D12_COMMAND_LIST_TYPE_COMPUTE);
    compute.init(L"compute");
    Mips mips;
    mips.load();
    auto computeList = compute.createCommandList();
    compute.waitForQueue(copy);
    mips.use(*computeList);
    mips.execute(*computeList, resource);
    compute.execute(computeList);
    copy.waitForFence(copy.waitForQueue(compute));
}

struct Rgba {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;

    bool operator==(const Rgba& v) const {
        return v.r == r && v.g == g && v.b == b && v.a == a;
    };
};
std::ostream& operator<<(std::ostream& stream, struct Rgba const& rgba)
{
    return stream << "(r=" << (uint32_t)rgba.r << ", g="<< (uint32_t)rgba.g << ", b="<< (uint32_t)rgba.b << ", a=" << (uint32_t)rgba.a << ")";
}

struct MipsTestParams {
    std::vector<Rgba> data;
    uint32_t width;
    uint32_t height;
    std::vector<Rgba> pixels;
    const char* testName;
};

class MipsTest : public ::testing::TestWithParam<struct MipsTestParams> {
public:
    static void SetUpTestSuite() {
        Service<Device>::build();
        Get<Device>().init();
    }

    static void TearDownTestSuite() {
        Service<Device>::destroy();
    }
};

TEST_P(MipsTest, check) {
    auto& device = Get<Device>();
    Queue copy(D3D12_COMMAND_LIST_TYPE_COPY);
    copy.init(L"queue");

    auto list = copy.createCommandList();

    struct MipsTestParams params = GetParam();

    D3D12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM, params.width, params.height, 1, 0, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
    Resource resource = device.uploadTexture(*list, desc, params.data.data(), params.data.size()*sizeof(Rgba));
    ASSERT_GT(resource->GetDesc().MipLevels, 1);
    DescriptorHeap& heap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    D3D12_CPU_DESCRIPTOR_HANDLE descriptor = heap.cpu(heap.allocate());
    device->CreateShaderResourceView(resource.ptr(), nullptr, descriptor);
    resource.srv(descriptor);

    copy.execute(list);
    executeMips(copy, resource);

    list = copy.createCommandList();
    D3D12_SUBRESOURCE_FOOTPRINT footprint;
    Resource download = device.downloadTextureInto(*list, resource, 1, footprint);
    copy.execute(list);
    copy.waitForFence(copy.latestFenceValue());

    ASSERT_EQ(footprint.Height, std::max(params.height >> 1, 1u));
    ASSERT_EQ(footprint.Width, std::max(params.width >> 1, 1u));

    uint8_t* mem;
    download->Map(0, nullptr, (void**) &mem);
    std::vector<Rgba> downloadedData(params.pixels.size());
    memcpy(downloadedData.data(), mem, params.pixels.size()*sizeof(Rgba));
    ASSERT_EQ(params.pixels, downloadedData);
    download->Unmap(0, nullptr);
}

INSTANTIATE_TEST_SUITE_P(
        MipsTest,
        MipsTest,
        ::testing::Values(
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {50,  60,  70,  80},
                        },
                        2, 1,
                        {{30, 40, 50, 60}},
                        // multiple of 2, easy downscale
                        "2x1"
                },
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {50,  60,  70,  80},
                                {90,  100, 110, 120},
                                {170, 180, 190, 200},
                        },
                        2, 2,
                        // multiple of 2, easy downscale
                        {{80, 90, 100, 110}},
                        "2x2"
                },
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {50,  60,  70,  80},
                                {90,  100, 110, 120},
                                {170, 180, 190, 200},
                        },
                        4, 1,
                        {{30, 40, 50, 60}, {130, 140, 150, 160}},
                        // multiple of 2, easy downscale
                        "4x1"
                },
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {50,  60,  70,  80},
                                {130, 140, 150, 160},
                        },
                        3, 1,
                        //{{60, 70, 80, 90}},
                        {{65, 75, 85, 95}},
                        "3x1"
                },
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {50,  60,  70,  80},
                                {130, 140, 150, 160},
                                {170, 180, 190, 200},
                                {220, 230, 240, 250},
                        },
                        5, 1,
                        {
                        //    {60, 70, 80, 90},
                        //    {172, 182, 192, 202},
                                {47, 57, 67, 77},
                                {184, 194, 204, 214},
                        },
                        "5x1"
                },
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {30,  40,  50,  60},
                                {90,  100, 110, 120},
                                {130, 140, 150, 160},
                                {170, 180, 190, 200},
                                {210, 220, 230, 240},
                        },
                        3, 2,
                        //{{105, 115, 125, 135}},
                        {{107, 117, 127, 137}},
                        "3x2"
                },
                MipsTestParams {
                        {
                                {10,  20,  30,  40},
                                {30,  40,  50,  60},
                                {50,  60,  70,  80},
                                {70,  80,  90,  100},
                                {90,  100, 110, 120},
                                {130, 140, 150, 160},
                                {170, 180, 190, 200},
                                {190, 200, 210,  220},
                                {210, 220, 230, 240},
                        },
                        3, 3,
                        //{{102, 112, 122, 132}},
                        {{107, 117, 127, 137}},
                        "3x3"
                }),
        [](const ::testing::TestParamInfo<MipsTestParams>& info) { return info.param.testName; });