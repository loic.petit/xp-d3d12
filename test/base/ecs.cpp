#include "base/entity.h"
#include <gtest/gtest.h>

class TestComponent {
public:
    uint32_t a = 1;
};

TEST(ECS, Create) {
    World w;
    w.declare<TestComponent>();

    auto table = w.table<TestComponent>();
    table.add(5);

    const auto& test = table.get(5);
    // constructor was executed
    ASSERT_EQ(test.a, 1);
}

TEST(ECS, ThrowAccess) {
    World w;
    ASSERT_THROW(w.table<TestComponent>(), std::out_of_range);
}

TEST(ECS, Query) {
    World w;

    class TestComponent2 {
    public:
        uint32_t b = 2;
    };
    w.declare<TestComponent>();
    w.declare<TestComponent2>();

    const EntityId youpi = w.create("Youpi")
            .add<TestComponent>()
            .add<TestComponent2>()
            .id();

    const EntityId youpi2 = w.create("Youpi2")
            .add<TestComponent>()
            .id();

    ASSERT_EQ(w.table<TestComponent>().get(youpi2).a, 1);

    {
        Query q;
        q.add<TestComponent>();
        q.add<TestComponent2>();
        uint32_t count = 0;
        ASSERT_EQ(w.query(q, [&](const EntityId id, Table::Row** row) {
            count++;
            ASSERT_EQ(id, youpi);
            const auto* component = static_cast<TestComponent*>(row[0]->data);
            ASSERT_EQ(component->a, 1);
            const auto* component2 = static_cast<TestComponent2*>(row[1]->data);
            ASSERT_EQ(component2->b, 2);
        }), 1);
        ASSERT_EQ(count, 1);
    }

    {
        uint32_t count = 0;
        ASSERT_EQ(w.query([&](EntityId id, const TestComponent& c1, TestComponent2& c2) {
            count++;
            ASSERT_EQ(id, youpi);
            ASSERT_EQ(c1.a, 1);
            ASSERT_EQ(c2.b, 2);
            c2.b += c1.a;
        }), 1);
        ASSERT_EQ(count, 1);
        ASSERT_EQ(w.table<TestComponent2>().get(youpi).b, 3);
    }
}
