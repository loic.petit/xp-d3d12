import sys, os

from tools.ue4_material.parser import ParameterType
from tools.ue4_material.parser import parse_exported_material

sys.path.append(os.path.abspath(os.path.dirname("__file__")))

from ue4_material import Parser, Serializer, parse_exported_material

BASE_MATERIAL = "StreetFighterV/Content/Chara/CMN/BaseMaterial/"
CMY_017_MATERIAL = "StreetFighterV/Content/Chara/CMY/SkelMesh/17/Material/"
#
# p = Parser(CMY_017_MATERIAL+"CM_Z10_17_E05_Cloth")
# s = Serializer(p)
# s.debug = False
# for flag in ["UseThicknessMap", "UseSubsurfaceMap", "UseSubsurfaceColorM", "UseSSS", "UseSRMASpecular", "UseSRMARoughness", "UseSRMAMetallic", "UseMetallicMap", "UseSpecularMap", "UseNormalMap", "EnableColorCustomize", "UseAOMap", "UseSRMAAO", "CustomizeMask_B_UV_Disp"]:
#     # if flag not in SWITCHES:
#     # print("Kaboom", flag)
#     # continue
#     s.switches[flag] = True
# s.serialize()
# "CM_Z10_17_E00_Eye"
# "CM_Z10_17_E01_Eyebrow"
# "CM_Z10_17_E02_EyesAO"
# "CM_Z10_17_E03_EyeLens"
# "CM_Z10_17_E04_Skin"
# "CM_Z10_17_E05_Cloth"
# "CM_Z10_17_E06_Skintransition"
# "CM_Z10_17_E07_HatGloves"
# "CM_Z10_17_E08_Zipline"

blackClip = 0.0270874
whiteClip = 0.259612
slope = 0.773196
shoulder = 0.491064
toe = 0.112864

print((1 - slope * (16 - whiteClip) / (16 + shoulder))/(whiteClip - blackClip * blackClip / (blackClip + toe)))

slopeBlack = 1.03

parser = Parser(BASE_MATERIAL+"Function/MF_ColorFade")
# parser = Parser("StreetFighterV/Content/Common/Materials/Function/MF_GradientWave")
# parser = parse_exported_material(CMY_017_MATERIAL+"CM_Z10_17_E05_Cloth")
s = Serializer(parser)
# s.inline_parameters = True
s.serialize()

"""
BaseColor = mf_color_fade.Result

EmissiveColor = {
    mf_color_fade.Emission + mf_calc_chara_base_color.BaseColor * customize_mask.a * {s:EmissionScale:0.0}
}
Normal = NormalMap.Sample(uv).rgb
WorldPositionOffset = MF_EzFixProj().Result
AmbientOcclusion = {
    MF_CalcAO({s:AOMin:0.0}, {s:AOMax:1.0}, srma_map.a).AO
}
"""