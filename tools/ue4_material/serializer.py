import token

from .expressions import *
from enum import Enum


class Serializer:
    def __init__(self, parser):
        self.indent = 0
        self.indent_base = "    "
        self.switches = parser.switches
        self.debug = False
        self.declarations = []
        self.parser = parser
        self.declared = dict()
        self.usage = [0] * len(parser.parsed)
        self.parameters = parser.parameters
        self.inline_parameters = False

    def _open_block(self):
        self.indent += 1
        return self.indent_base * self.indent

    def _close_block(self):
        self.indent -= 1
        return self.indent_base * self.indent

    def _propagate_usage(self, param, parent=-1):
        self.usage[param] += 1
        elt = self.parser.parsed[param]
        args = self.parser.usage[param]
        if not self.debug and type(elt) is SwitchParameter:
            args = [elt.a.idx if self.switch(elt.flag) else elt.b.idx]
        for idx in args:
            if self.usage[idx] == 0 or (not self.debug and type(self.parser.parsed[idx]) is SwitchParameter):
                self._propagate_usage(idx, param)
            else:
                self.usage[idx] += 1
        # print("Parent %d, Node %d, Children %s, Usage: %s" % (parent, param, str(args), str([self.usage[i]+1 for i in args])))

    def serialize(self, expr=None):
        if expr is None:
            self._propagate_usage(self.parser.main)
            expr = self.parser.parsed[self.parser.main]
        result = "".join(self.serialize_yield(expr))
        print(result, end="")

    def serialize_yield(self, expr):
        self.indent = 0
        indent = ""
        for token in expr.serialize(self):
            if token is Token.OpenBlock:
                indent = self._open_block()
            elif token is Token.CloseBlock:
                indent = self._close_block()
            if type(token) is not str:
                yield "\n"
                yield indent
            else:
                yield token

    def serialize_parser(self, material):
        s = Serializer(material)
        s._propagate_usage(s.parser.main)
        expr = s.parser.parsed[s.parser.main]
        yield from expr.serialize(s)

    def switch(self, name):
        return self.switches.get(name, False)

    def parameter(self, name, type_, value):
        typed_parameter = self.parameters.get(type_)
        if typed_parameter is None:
            return value
        return typed_parameter.get(name, value)

    def try_inline(self, *args, sep=""):
        sep_size = len(sep)
        result = list(list(elt.serialize(self)) for elt in args)
        inline = True
        size = (len(result) - 1) * sep_size
        for elt in result:
            for content in elt:
                if type(content) is str:
                    size += len(content)
                else:
                    inline = False
                    break
            if size > 50:
                inline = False
            if not inline:
                break
        return result, inline

    def get_non_trivial_expression(self, expr):
        if type(expr) is not Expression:
            return expr
        parsed = self.parser.parsed[expr.idx]
        if not self.debug and type(parsed) is SwitchParameter:
            return self.get_non_trivial_expression(parsed.a if self.switch(parsed.flag) else parsed.b)
        return parsed

    def expression_reference(self, expr):
        parsed = self.parser.parsed[expr]
        inline = expr in self.declared
        non_trivial = self.get_non_trivial_expression(Expression(expr, (), 0))
        inline |= type(non_trivial) is Constant
        inline |= type(non_trivial) is Expression
        inline |= type(non_trivial) is FunctionInput
        inline |= non_trivial.id in self.declared
        usage = self.usage[non_trivial.id]
        inline |= self.usage[expr] < 2
        inline |= self.usage[non_trivial.id] < 2

        if not inline:
            keep_indent = self.indent
            evaluate = "".join(self.serialize_yield(parsed))
            self.indent = keep_indent
            # in the meantime, non_trivial became inline
            if non_trivial.id not in self.declared:
                var_name = non_trivial.guess_name()
                for d in self.declared.values():
                    if d == var_name:
                        var_name += "_" + str(expr)
                        break

                self.declarations.extend([var_name, " = "])
                self.declarations.extend(parsed.serialize(self))
                self.declarations.extend([";", Token.NewLine])
                self.declared[expr] = var_name
                yield var_name
                return
        if expr in self.declared:
            yield self.declared[expr]
            return
        # if expr == 55:
        # print("Decided to declare 55, usage %d, non_trivial %d (declared: %d)" % (usage, non_trivial.id, non_trivial.id in self.declared))
        yield from parsed.serialize(self)

    def output_name(self, idx, output_index):
        expr = self.parser.parsed[idx]
        if type(expr) is FunctionCall:
            if output_index >= len(expr.output):
                raise Exception(str(expr.output) + " on expression " + str(idx) + " tried to get " + str(output_index))
            if len(expr.output[output_index]) > 1:
                out = expr.output[output_index]
                if not out:
                    return "Default"
                return out
        return ""
