import re
from enum import Enum
from typing import List

Token = Enum('Token', ['OpenBlock', 'CloseBlock', 'NewLine'])
ParameterType = Enum('ParameterType', ['Scalar', 'Vector', 'Texture', 'Collection', 'FunctionInput'])


def camel_to_snake(name):
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()


class BinaryOp:
    def __init__(self, op, a, b=None):
        self.op = op
        self.a = a
        self.b = b

    def serialize(self, serializer):
        if self.b is None:
            if self.op == "-":
                yield self.op
            yield from self.a.serialize(serializer)
            return
        result, inline = serializer.try_inline(self.a)
        filter_op = set()
        if self.op == "*":
            filter_op = {"-", "+"}
        elif self.op == "/":
            filter_op = {"-", "+"}
        block = sum(1 for tok in result[0] if tok in filter_op) > 0
        if block:
            yield "("
            if not inline:
                yield Token.OpenBlock
        yield from result[0]
        if block:
            if not inline:
                yield Token.CloseBlock
            yield ")"
        yield from (" ", self.op, " ")
        result, inline = serializer.try_inline(self.b)
        filter_op = set()
        if self.op == "*":
            filter_op = {"-", "+"}
        elif self.op == "/":
            filter_op = {"-", "+", "*"}
        block = sum(1 for tok in result[0] if tok in filter_op) > 0
        if block:
            yield "("
            if not inline:
                yield Token.OpenBlock
        yield from result[0]
        if block:
            if not inline:
                yield Token.CloseBlock
            yield ")"

    def guess_name(self):
        return "expr_%d" % self.id


class FunctionCall:
    def __init__(self, func, *args, output=[""], input_ids=None):
        self.func = func
        self.args = args
        self.output = output
        self.input_ids = input_ids

    def serialize(self, serializer):
        yield self.func
        yield "("
        result, inline = serializer.try_inline(*self.args, sep=", ")
        if not inline:
            yield Token.OpenBlock
        first = True
        for i, elt in enumerate(result):
            if not first:
                yield ", "
                if not inline:
                    yield Token.NewLine
            first = False
            # if self.input_ids:
            #     yield from ("[", self.input_ids[i], "]=")
            yield from elt
        if not inline:
            yield Token.CloseBlock
        yield ")"

    def guess_name(self):
        name = camel_to_snake(self.func.split('.')[0]).replace('__', '_')
        if name == self.func:
            return "var_%d" % self.id
        return name


class Constant:
    def __init__(self, value):
        self.value = value

    def serialize(self, serializer):
        yield str(self.value)

    def guess_name(self):
        return "const"


class SwitchParameter:
    def __init__(self, flag, a, b):
        self.flag = flag
        self.a = a
        self.b = b

    def serialize(self, serializer):
        if serializer.debug:
            yield from ("switch[", self.flag, "]{", Token.OpenBlock)
            yield from self.a.serialize(serializer)
            yield ", "
            yield Token.NewLine
            yield from self.b.serialize(serializer)
            yield from (Token.CloseBlock, "}")
        else:
            yield from (self.a if serializer.switch(self.flag) else self.b).serialize(serializer)

    def guess_name(self):
        return camel_to_snake(self.flag)


class Expression:
    def __init__(self, idx, mask, output):
        self.idx = idx
        self.mask = mask
        self.output = output

    def serialize(self, serializer):
        non_trivial = serializer.get_non_trivial_expression(self)
        out = serializer.output_name(self.idx, self.output)
        brackets = type(non_trivial) is BinaryOp and (out or self.mask)
        if brackets:
            yield "("
        yield from serializer.expression_reference(self.idx)
        if brackets:
            yield ")"

        if out:
            yield "."
            yield out
        if self.mask:
            yield "."
            if self.mask[0]:
                yield "r"
            if self.mask[1]:
                yield "g"
            if self.mask[2]:
                yield "b"
            if self.mask[3]:
                yield "a"

    def guess_name(self):
        return "expr_%d" % self.idx


class AttributeSet:
    def __init__(self, name, attributes):
        self.name = name
        self.attributes = attributes

    def serialize(self, serializer):
        not_inline_attr = len(self.attributes) > 1
        if not_inline_attr:
            yield from (self.name + " " if self.name else "", "{", Token.OpenBlock)
        first = True
        for k, v in self.attributes:
            if not first:
                yield Token.NewLine
            else:
                first = False
            if not_inline_attr:
                yield from (k, " = ")
            value, inline = serializer.try_inline(v)
            if not inline:
                if not_inline_attr:
                    yield from ("{", Token.OpenBlock)
                yield from value[0]
                if not_inline_attr:
                    yield from (Token.CloseBlock, "}")
            else:
                yield from value[0]
        if not_inline_attr:
            yield from (Token.CloseBlock, "}")

    def guess_name(self):
        return camel_to_snake(self.name)


class Parameter:
    def __init__(self, type_, name, value=None):
        self.type = type_
        self.name = name
        self.value = value

    def serialize_constant(self, constant):
        if self.type == ParameterType.Vector:
            yield from (
                "float4(",
                str(constant.get("R", 0)), ",",
                str(constant.get("G", 0)), ",",
                str(constant.get("B", 0)), ",",
                str(constant.get("A", 0)),
                ")"
            )
        else:
            yield str(constant)

    def serialize(self, serializer):
        value = serializer.parameter(self.name, self.type, self.value)
        if self.type != ParameterType.Collection and serializer.inline_parameters:
            yield from self.serialize_constant(value)
        else:
            yield "{"
            yield ["s", "v", "t", "c", "i"][self.type.value - 1]
            yield ":"
            yield self.name
            if value is not None:
                yield ":"
                yield from self.serialize_constant(value)
            yield "}"

    def guess_name(self):
        return camel_to_snake(self.name)


class MaterialInstance:
    def __init__(self, material):
        self.material = material

    def serialize(self, serializer):
        yield from serializer.serialize_parser(self.material)

    def guess_name(self):
        return camel_to_snake(self.material.path)


class FunctionInput:
    def __init__(self, name, type_, sort, default_value, id_):
        self.name = name
        self.type = type_
        self.sort = sort
        self.default_value = default_value
        self.input_id = id_

    def serialize(self, serializer, decl=False):
        if decl:
            yield from (
                #"[", self.input_id, "] ",
                self.name, ": ", self.type
            )
            if self.default_value:
                yield " = "
                yield from self.default_value.serialize(serializer)
        else:
            yield from ("{", self.name, "}")

    def guess_name(self):
        return camel_to_snake(self.name)

class Material:
    def __init__(self, attributes):
        self.attributes = attributes

    def serialize(self, serializer):
        attributes = list(self.attributes.serialize(serializer))
        yield from serializer.declarations
        yield from attributes

    def guess_name(self):
        return "material"


class FunctionDefinition:
    def __init__(self, name, inputs: List[Expression], outputs: AttributeSet):
        self.name = name
        self.inputs = inputs
        self.outputs = outputs

    def serialize(self, serializer):
        yield from (self.name, "(")
        inputs = [list(input.serialize(serializer, decl=True)) for input in self.inputs]
        outputs = list(self.outputs.serialize(serializer))
        first = True
        if inputs:
            yield Token.OpenBlock
        for input in inputs:
            if not first:
                yield Token.NewLine
            else:
                first = False
            yield from input
            yield ","
        if inputs:
            yield Token.CloseBlock
        yield from (") {", Token.OpenBlock)
        yield from serializer.declarations
        yield "return "
        yield from outputs
        yield from (Token.CloseBlock, "}")

    def guess_name(self):
        return camel_to_snake(self.material.path)

