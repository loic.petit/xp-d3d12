import json

from .expressions import *

BASE_PATH = "D:/Downloads/net472/Output/Exports/"

def quicksort(arr, key=lambda x:x):
    if len(arr) <= 1:
        return arr
    if len(arr) == 2:
        return arr if key(arr[0]) <= key(arr[1]) else [arr[1], arr[0]]
    else:
        pivot = key(arr[0])
        left = [x for x in arr[1:] if key(x) <= pivot]
        right = [x for x in arr[1:] if key(x) > pivot]
        return quicksort(left, key=key) + [arr[0]] + quicksort(right, key=key)

BLEND_MODE_ATTRIBUTES = {
    "Opaque": [],
    "Masked": [],
    "Translucent": [],
    "Additive": [],
    "Modulate": [],
    "AlphaComposite": [],
    "AlphaHoldout": [],
}

MATERIAL_ATTRIBUTES = {
    "Unlit": [
        "EmissiveColor",
        "WorldPositionOffset",
        "PixelDepthOffset",
    ],
    "DefaultLit": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Normal",
        "WorldPositionOffset",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "Subsurface": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "SubsurfaceColor",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "PreintegratedSkin": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "SubsurfaceColor",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "ClearCoat": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Normal",
        "WorldPositionOffset",
        "ClearCoat",
        "ClearCoatRoughness",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "SubsurfaceProfile": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "TwoSidedFoliage": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Normal",
        "WorldPositionOffset",
        "SubsurfaceColor",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "Hair": [
        "BaseColor",
        "Scatter",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Tangent",
        "WorldPositionOffset",
        "Backlit",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "Cloth": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "FuzzColor",
        "Cloth",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "Eye": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "IrisMask",
        "IrisDistance",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "SingleLayerWater": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "AmbientOcclusion",
        "Refraction",
        "PixelDepthOffset",
    ],
    "ThinTranslucent": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Opacity",
        "Normal",
        "WorldPositionOffset",
        "AmbientOcclusion",
        "PixelDepthOffset",
    ],
    "FromMaterialExpression": [
        "BaseColor",
        "Metallic",
        "Specular",
        "Roughness",
        "EmissiveColor",
        "Normal",
        "WorldPositionOffset",
        "AmbientOcclusion",
        "PixelDepthOffset",
        "ShadingModel"
    ],
}

class Parser:
    def __init__(self, path, init=True):
        self.base = json.load(open(BASE_PATH + path + ".json", encoding="utf-8"))
        self.path = path
        self.switches = dict()
        self.usage = [[] for i in range(len(self.base))]
        self.parsed = [None] * len(self.base)
        self.main = None
        self.current_idx = 0
        self.current_switches = dict()
        self.material_override = dict()
        self.parameters = {
            ParameterType.Scalar: dict(),
            ParameterType.Vector: dict(),
            ParameterType.Texture: dict(),
            ParameterType.Collection: dict(),
        }
        self.material_attributes = []
        if init:
            self.init()

    def init(self):
        for i, b in enumerate(self.base):
            self.current_idx = i
            self.parse(b)

    def parse(self, expr):
        type_ = expr["Type"]
        method_name = type_
        # try:
        if type_.startswith("MaterialExpression"):
            method_name = type_[len("MaterialExpression"):]
        parser = getattr(self, "parse" + method_name)
        out = parser(expr)
        # except Exception as e:
        #     print("Failed to parse type %s" % method_name)
        #     traceback.print_exc()
        #     out = Constant(type_)
        out.id = self.current_idx
        self.parsed[self.current_idx] = out

    def expression(self, f, name=None, const=None):
        if name is not None:
            f = f["Properties"].get(name)
        if not f:
            return Constant(const) if const is not None else const
        if not f["Expression"]:
            return Constant("null")
        path, idx = f["Expression"]["ObjectPath"].split(".")
        if path != self.path:
            raise Exception("Calling expression from outside %s" % path)
        idx = int(idx)
        # current_flags = tuple(sorted(k for k, v in self.current_switches.items() if v))
        # self.usage[idx][current_flags] = 1 + self.usage[idx].get(current_flags, 0)
        self.usage[self.current_idx].append(idx)
        mask = None
        if f["Mask"]:
            mask = (f["MaskR"], f["MaskG"], f["MaskB"], f["MaskA"])
        return Expression(idx, mask, f["OutputIndex"])

    def parseLinearInterpolate(self, f):
        return FunctionCall(
            "lerp",
            self.expression(f, "A", f["Properties"].get("ConstA", 0.0)),
            self.expression(f, "B", f["Properties"].get("ConstB", 1.0)),
            self.expression(f, "Alpha", f["Properties"].get("ConstAlpha", 0.0))
        )

    def _parsePreviewValue(self, f, input_type):
        preview = f["Properties"]["PreviewValue"] if "PreviewValue" in f["Properties"] else {"X": 0.0, "Y": 0.0, "Z": 0.0, "W": 0.0}
        if input_type == "Scalar":
            return preview["X"]
        if input_type == "Vector2":
            return "float2(%f, %f)" % (preview["X"], preview["Y"])
        if input_type == "Vector3":
            return "float3(%f, %f, %f)" % (preview["X"], preview["Y"], preview["Z"])
        return "float4(%f, %f, %f, %f)" % (preview["X"], preview["Y"], preview["Z"], preview["W"])

    def parseFunctionInput(self, f):
        input_type = f["Properties"]["InputType"][14:] if "InputType" in f["Properties"] else "Any"
        return FunctionInput(
            f["Properties"]["InputName"],
            input_type,
            f["Properties"].get("SortPriority", 0),
            self.expression(f, "Preview", self._parsePreviewValue(f, input_type)) if "bUsePreviewValueAsDefault" in f["Properties"] else None,
            f["Properties"]["Id"][:4].lower()
        )
    def parseStaticBool(self, f):
        return Constant("true" if "Value" in f["Properties"] else "false")

    def parseStaticSwitch(self, f):
        return FunctionCall("switch", self.expression(f, "A"), self.expression(f, "B"), self.expression(f, "Value", f["Properties"].get("DefaultValue")))

    def parseFunctionOutput(self, f):
        name = f["Properties"].get("OutputName", "Result")
        attrSet = AttributeSet(name, [(name, self.expression(f, "A"))])
        attrSet.sort = f["Properties"].get("SortPriority", 0)
        return attrSet

    def parseMaterialFunction(self, f):
        self.main = self.current_idx
        outputs = []
        inputs = []
        for expr in f["Properties"]["FunctionExpressions"]:
            idx = int(expr["ObjectPath"].split('.')[1])
            if expr["ObjectName"].startswith("MaterialExpressionFunctionOutput"):
                outputs.append(self.parsed[idx])
                self.usage[self.current_idx].append(idx)
            elif expr["ObjectName"].startswith("MaterialExpressionFunctionInput"):
                inputs.append(self.parsed[idx])
                self.usage[self.current_idx].append(idx)

        inputs = quicksort(inputs, key=lambda x:x.sort)
        outputs = AttributeSet("", [out.attributes[0] for out in sorted(outputs, key=lambda x:x.sort)])
        return FunctionDefinition(f["Name"], inputs, outputs)

    def parseMaterialFunctionCall(self, f):
        func = f["Properties"]["MaterialFunction"]["ObjectName"].split("'")[1]
        inputs = [self.expression(i["Input"]) for i in f["Properties"].get("FunctionInputs", [])]
        input_ids = [i["ExpressionInputId"][:4].lower() for i in f["Properties"].get("FunctionInputs", [])]
        outputs = [i["Output"]["OutputName"] for i in f["Properties"].get("FunctionOutputs", [])]
        return FunctionCall(func, *inputs, output=outputs, input_ids=input_ids)

    def parseStaticSwitchParameter(self, f):
        flag = f["Properties"]["ParameterName"]
        if flag not in self.switches:
            self.switches[flag] = False
        if flag in self.current_switches:
            raise Exception("Conflicting flags?? %s" % flag)
        self.current_switches[flag] = True
        true_value = self.expression(f, "A")
        self.current_switches[flag] = False
        false_value = self.expression(f, "B")
        del self.current_switches[flag]

        return SwitchParameter(
            flag,
            true_value,
            false_value,
        )

    def _parseBinaryOp(self, f, op, def_a, def_b):
        return BinaryOp(
            op,
            self.expression(f, "A", f["Properties"].get("ConstA", def_a)),
            self.expression(f, "B", f["Properties"].get("ConstB", def_b))
        )

    def parseMultiply(self, f):
        return self._parseBinaryOp(f, "*", 0.0, 1.0)

    def parseSubtract(self, f):
        return self._parseBinaryOp(f, "-", 1.0, 1.0)

    def parseAdd(self, f):
        return self._parseBinaryOp(f, "+", 0.0, 1.0)

    def parseDivide(self, f):
        return self._parseBinaryOp(f, "/", 1.0, 2.0)

    def parseFrac(self, f):
        return FunctionCall("frac", self.expression(f, "Input"))

    def parseAppendVector(self, f):
        return FunctionCall(
            "floatN",
            self.expression(f, "A"),
            self.expression(f, "B"),
        )

    def _parseSample(self, f):
        return FunctionCall(
            f["Properties"]["ParameterName"] + ".Sample",
            self.expression(f, "Coordinates", "uv"),
            output=[
                "",  # RGB
                "",  # R
                "",  # G
                "",  # B
                ""   # A
            ]
        )

    def parseTextureSampleParameter2D(self, f):
        return self._parseSample(f)

    def parseTextureSampleParameterCube(self, f):
        return self._parseSample(f)

    def parseVectorParameter(self, f):
        return self._parseParameter(f, ParameterType.Vector)

    def parseScalarParameter(self, f):
        return self._parseParameter(f, ParameterType.Scalar)

    def parseTextureObjectParameter(self, f):
        return self._parseParameter(f, ParameterType.Texture)

    def parseComponentMask(self, f):
        expr = self.expression(f, "Input")
        mask = (
            f["Properties"].get("R", 0),
            f["Properties"].get("G", 0),
            f["Properties"].get("B", 0),
            f["Properties"].get("A", 0)
        )
        return Expression(expr.idx, mask, 0)

    def parseClamp(self, f):
        return FunctionCall("clamp", self.expression(f, "Input"))

    def parseCeil(self, f):
        return FunctionCall("ceil", self.expression(f, "Input"))

    def parseFloor(self, f):
        return FunctionCall("floor", self.expression(f, "Input"))

    def parseAbs(self, f):
        return FunctionCall("abs", self.expression(f, "Input"))

    def parseNormalize(self, f):
        return FunctionCall("normalize", self.expression(f, "Input") if "Input" in f["Properties"] else self.expression(f, "VectorInput"))

    def parseRotator(self, f):
        return FunctionCall("rotator", self.expression(f, "Coordinate", "uv"), self.expression(f, "Time", "time"))

    def parsePower(self, f):
        return FunctionCall("pow", self.expression(f, "Base"), self.expression(f, "Exp", f["Properties"].get("ConstExponent")))

    def parseMin(self, f):
        return FunctionCall(
            "min",
            self.expression(f, "A", f["Properties"].get("ConstA")),
            self.expression(f, "B", f["Properties"].get("ConstB"))
        )

    def parseMax(self, f):
        return FunctionCall(
            "max",
            self.expression(f, "A", f["Properties"].get("ConstA")),
            self.expression(f, "B", f["Properties"].get("ConstB"))
        )

    def parseTransform(self, f):
        return FunctionCall(f["Properties"].get("TransformType", f["Properties"].get("TransformSourceType")), self.expression(f, "Input"))

    def parseConstantBiasScale(self, f):
        return BinaryOp("+", Constant(f["Properties"]["Bias"]),
                        BinaryOp("*", Constant(f["Properties"]["Scale"]), self.expression(f, "Input")))

    def _parseParameter(self, f, t):
        default_value = None
        name = f["Properties"]["ParameterName"]
        if "DefaultValue" in f["Properties"]:
            default_value = f["Properties"]["DefaultValue"]
        if "Collection" in f["Properties"]:
            name = f["Properties"]["Collection"]["ObjectName"].split("'")[1]+"."+name
        return Parameter(t, name, default_value)

    def parseCollectionParameter(self, f):
        return self._parseParameter(f, ParameterType.Collection)

    def parseOneMinus(self, f):
        return BinaryOp("-", Constant(1), self.expression(f, "Input"))

    def parseVertexColor(self, f):
        return Constant("color")

    def parseConstant(self, f):
        return Constant(f["Properties"].get("R"))

    def parseConstant3Vector(self, f):
        constant = f["Properties"].get("Constant", {})
        return Constant("float3(" + str(constant.get("R", "0.0")) + "," + str(constant.get("G", "0.0")) + "," + str(
            constant.get("B", "0.0")) + ")")

    def parseConstant2Vector(self, f):
        return Constant(
            "float2(" + str(f["Properties"].get("R", "0.0")) + "," + str(f["Properties"].get("G", "0.0")) + ")")

    def parseFresnel(self, f):
        return FunctionCall(
            "fresnel",
            self.expression(f, "ExponentIn", f["Properties"].get("Exponent", 1)),
            self.expression(f, "BaseReflectFractionIn", f["Properties"].get("BaseReflectFraction", 0)),
            self.expression(f, "Normal", "normalWS")
        )

    def parseMakeMaterialAttributes(self, f):
        # return Constant("MakeMaterialAttributes")
        attributes = []
        for attr in self.material_attributes:
            if attr in f["Properties"]:
                attributes.append((attr, self.expression(f["Properties"][attr])))
        return AttributeSet("MaterialAttributes", attributes)

    def parseWorldPosition(self, f):
        return Constant("positionWS")

    def parseCameraPositionWS(self, f):
        return Constant("cameraPositionWS")

    def parseReflectionVectorWS(self, f):
        return Constant("reflectionVectorWS")

    def parseSceneTexelSize(self, f):
        return Constant("sceneTexelSize")

    def parseTextureCoordinate(self, f):
        return Constant("uv")

    def parseTime(self, f):
        return Constant("time")

    def parseMaterial(self, f):
        self.main = self.current_idx
        attributes = []
        prop = dict(f["Properties"])
        prop.update(self.material_override)
        default_values = {
            "BlendMode": "BLEND_Opaque",
            "ShadingModel": "MSM_DefaultLit",
            "TranslucencyLightingMode": "TLM_VolumetricNonDirectional",
        }
        for k, v in prop.get("BasePropertyOverrides", dict()).items():
            if k.startswith("bOverride") and v:
                k = k.split("_")[1]
                prop[k] = f["Properties"]["BasePropertyOverrides"].get(k, default_values.get(k, False))
        use_material_attributes = prop.get("bUseMaterialAttributes", False)
        for k, p in prop.items():
            if type(p) is not dict or "Expression" not in p:
                if type(p) is not list and "Editor" not in k and k not in {"StateId", "ParentLightingGuid", "BasePropertyOverrides"}:
                    attributes.append((k, Constant(p)))
        self.material_attributes = MATERIAL_ATTRIBUTES[prop["ShadingModel"][4:]]
        if prop["BlendMode"] == "BLEND_Masked":
            self.material_attributes.append("OpacityMask")
        elif prop["BlendMode"] != "BLEND_Opaque":
            if "Opacity" not in self.material_attributes:
                self.material_attributes.append("Opacity")
        for attr in (["MaterialAttributes"] if use_material_attributes else self.material_attributes):
            if attr in prop:
                attributes.append((attr, self.expression(prop[attr])))
        return Material(AttributeSet("Material", attributes))

    def parseMaterialInstanceConstant(self, f):
        self.main = self.current_idx
        path = f["Properties"]["Parent"]["ObjectPath"].split('.')
        default_values = {
            "BlendMode": "BLEND_Opaque",
            "ShadingModel": "MSM_DefaultLit",
            "TranslucencyLightingMode": "TLM_VolumetricNonDirectional",
        }
        material = Parser(path[0], init=False)
        for k, v in f["Properties"].get("BasePropertyOverrides", dict()).items():
            if k.startswith("bOverride") and v:
                k = k.split("_")[1]
                material.material_override[k] = f["Properties"]["BasePropertyOverrides"].get(k, default_values.get(k, False))
        material.init()
        material.main = int(path[1])
        return MaterialInstance(material)


def parse_exported_material(path):
    base = json.load(open(BASE_PATH + path + ".json"))
    material_path = base["Parameters"]["Properties"]["Expressions"][0]["ObjectPath"].split(".")[0]
    material = Parser(material_path, init=False)
    for m in material.base:
        if m["Type"] == "Material":
            m["Properties"] = base["Parameters"]["Properties"]
            break
    material.parameters[ParameterType.Scalar].update(base["Parameters"]["Scalars"])
    material.parameters[ParameterType.Vector].update(base["Parameters"]["Colors"])
    material.parameters[ParameterType.Texture].update(base["Textures"])
    material.switches.update(base["Parameters"]["Switches"])
    material.init()
    return material
