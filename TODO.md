# TODO

# Light
- [x] Diffuse shader
- [x] Specular shader
- [x] Normal mapping
- [x] Tone mapping
- [x] Shadow mapping
- [x] Soft shadow mapping (kinda)
- [x] PBR (kinda)
- [x] Subsurface scattering (kinda)

# Texturing
- [x] Texture loading
- [x] Colour masks
- [x] Alpha blending (e.g. eyebrows) (kinda)
- [x] Mip compute shader
- [ ] Eyes
- [ ] Eyebrows

# Mesh
- [x] Grid render
- [x] Mesh loading

# Animation
- [x] Skeleton node mapping
- [x] Animation loading
- [ ] Braid physics

# Misc
- [x] Grid texture
- [x] ImGUI
- [x] Material tester
- [x] Feature tester

# Weird materials
- [ ] Mirrors
- [ ] Glass
- [ ] Water
- [ ] Fog

# Plumbing
- [x] Full swap chain setup
- [x] Resize
- [x] MSAA
- [x] Render target abstraction
- [x] Command list allocation
- [ ] Support for arbitrary number of light sources
- [ ] Deferred Rendering pipeline
- [ ] Fullscreen toggle
- [x] Descriptor allocation for PSOs
- [x] Proper pipeline configuration
- [ ] HDR support

