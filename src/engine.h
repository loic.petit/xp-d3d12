#pragma once

#include "core/window.h"
#include "core/queue.h"
#include "utils/utils.h"
#include "scenes/context.h"
#include "scenes/training.h"
#include "scenes/cube.h"
#include "utils/gui.h"


class Engine : public EngineQueues, Provider<EngineQueues>, Provider<EngineInput>  {
public:
    explicit Engine(HINSTANCE hInstance);
    ~Engine() override;

    void init();

    bool start();

    void flush();

    inline Queue& directQueue() override { return m_directCommandQueue; }

    inline Queue& computeQueue() override { return m_computeCommandQueue; }

    inline Queue& copyQueue() override { return m_copyCommandQueue; }

private:
    void onWindowsMsg(UINT message, WPARAM wParam, LPARAM lParam, UINT& result);

    void onRender();

    void onUpdate();

    void updateFpsCounter();

    void updateInputs();

    Queue m_directCommandQueue;
    Queue m_computeCommandQueue;
    Queue m_copyCommandQueue;

    StoppableThread m_renderThread;

    Window m_window;
    DelegateHandle m_updateHandle;
    DelegateHandle m_resizeHandle;
    RenderTarget m_msaaRenderTarget;
    Training m_scene;
    DelegateHandle m_msgHandle;
    std::recursive_mutex m_mutex;

    bool m_isLoaded = false;
    struct EngineInput m_inputs;
    uint64_t m_frameId = 0;
    std::atomic_bool m_needsPing = false;

    struct EngineLocalInput {
        bool wasd[4] = {};

        bool mouseButton = false;
        int32_t mouseX = 0;
        int32_t mouseY = 0;
        bool guiIsCapturingKeyboard = false;
        bool guiIsCapturingMouse = false;
    };

    struct {
        int32_t status;
        uint32_t frameTime;
        uint32_t reflexSleep;
        uint32_t swapChainWait;
        uint32_t gameToRender;
        uint32_t sim;
        uint32_t renderSubmit;
        uint32_t present;
        uint32_t driver;
        uint32_t osRenderQueue;
        uint32_t gpuRender;;
    } m_reflexMeasures {};

    uint32_t m_frameTimes[64]{};
    uint32_t m_reflexSleep[64]{};
    uint32_t m_swapChainWait[64]{};

    struct {
        bool enabled;
        bool boost;
        uint32_t maxFps;
        bool useMarkers;
    } m_latencyMode = {true, true, 240, true};
    struct EngineLocalInput m_localInputs;

    void reflexSdkGui();
};