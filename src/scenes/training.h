#pragma once

#include "core/device.h"
#include "core/rendertarget.h"
#include "core/pipeline.h"
#include "utils/camera.h"
#include "utils/gui.h"
#include "utils/mips.h"
#include "gltf/load.h"

#include "context.h"
#include "shaders/gbuffer-render.h"
#include "shaders/deferred-lighting.h"
#include "shaders/deferred-ambient-lighting.h"
#include "shaders/tone-mapping.h"
#include "shaders/shadow.h"
#include "shaders/fxaa.h"
#include "shaders/ssao.h"
#include "lights/spotlight.h"
#include "materials/resource-manager.h"
#include "nodes/graph.h"
#include "shaders/material-render.h"

class Training {
public:
    explicit Training();

    void load(const RenderTarget::Desc& desc);

    void update();

    void render(CommandList& list, RenderTarget& target);

    void gui(bool mainControls, bool materialEditorView, bool gbufferView, bool fadeSelectorView);

    void move(int32_t pitch, int32_t yaw);

    void resize(uint32_t width, uint32_t height);

    void translate(int i, int i1, int i2, int i3);

private:
    MaterialRender m_testMaterialPipeline;
    GBufferRenderPipeline m_pipeline;
    RenderTarget m_gbuffers;

    DeferredLighting m_lighting;
    RenderTarget m_lightingBuffer;

    ToneMapping m_toneMapping;
    RenderTarget m_toneMappedBuffer;

    FxaaPipeline m_fxaa;

    ShadowPipeline m_shadow;

    SSAOPipeline m_ssao;
    RenderTarget m_ssaoBuffer;

    DeferredAmbientLighting m_ambientLighting;
    DirectX::XMFLOAT3 m_ambientColor;
    float m_ambientStrength;

    Object m_grid {};
    Camera m_camera {};
    Camera m_testCamera {};
    float m_yaw = 0;
    float m_pitch = 0;

    void renderMeshPipeline(CommandList& list, Object& entity, uint32_t featureFlag);

    void renderMeshShadow(CommandList& list, Object& entity);

    RenderTarget m_testRenderTarget;

    Object m_cammy;
    struct ResourceCache {
        ResourceManager::Id id;
        Resource* resource;
        ResourceManager::State state;
    };

    D3D12_CPU_DESCRIPTOR_HANDLE getResource(ResourceCache& cache, uint32_t& featureMask, uint32_t feature) const;

    Resource m_whitePixel;
    ResourceCache m_gridTexture{};
    ResourceCache m_cammyTexture{};
    ResourceCache m_cammyMaskTexture{};
    ResourceCache m_cammyNormalTexture{};
    ResourceCache m_cammySRMATexture{};
    ResourceCache m_cammySSSTexture{};
    Object m_testSphere;
    FadeSelect m_fadeSelect{};

    std::vector<std::vector<std::array<DirectX::XMFLOAT4, 3>>> m_customColors;
    int m_currentColor = 0;
    float m_angle = -90;
    uint32_t m_flagMask = 0xffffffff;

    void loadTexture(DescriptorHeap& heap, CommandList& copyList, CommandList& computeList, const std::wstring& name, const char* filename, Resource& resource);

    DirectX::XMFLOAT4 m_lightPositionTest {5, 5, -5, 1};
    std::vector<Spotlight> m_lights;
    std::vector<DirectX::XMMATRIX> m_animationBuffers;
    int m_currentAnimation = 9;
    int m_bufferSelector = 0;
    int m_aoKernelSize = 32;
    float m_aoPower = 1;
    float m_aoRadius = 0.05;
    float m_exposure = 1.0f;
    bool m_enableAOPass = true;
    bool m_enableAOMapping = true;
    bool m_enableFxaa = true;
    float m_time = 1.0;
    Graph m_testMaterial;

    void computeAnimationBuffers();

    void computeShadows(CommandList& list, const DirectX::XMMATRIX& cammyModelMatrix, const D3D12_GPU_VIRTUAL_ADDRESS& animationNodeMatricesPtr);

    void renderMainScene(CommandList& list, RenderTarget& renderTarget, const DirectX::XMMATRIX& cammyModelMatrix, const D3D12_GPU_VIRTUAL_ADDRESS& animationNodeMatricesPtr);

    void renderTestSphere(CommandList& list);

    void renderLights(CommandList& list, RenderTarget& gbuffers, RenderTarget& renderTarget);
};
