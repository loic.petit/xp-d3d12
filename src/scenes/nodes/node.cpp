#include "node.h"

#include "graph.h"
#include <utils/gui.h>

bool GraphNode::gui() {
    bool updated = false;

    ImGui::PushID(m_id);
    ed::BeginNode(m_id);
    float nodePadding = ed::GetStyle().NodePadding.x;
    float nodeWidth = ed::GetNodeSize(m_id).x - 2 * nodePadding;
    const ImVec2 pos = ed::GetNodePosition(m_id);
    ImGui::BeginHorizontal("header", ImVec2(nodeWidth, 20), 0);
    ImGui::TextColored(ImColor(24, 24, 24), "%s", m_title.c_str());
    ImGui::Spring(1.0);
    ImGui::EndHorizontal();
    auto headerMin = ImGui::GetItemRectMin();
    auto headerMax = ImGui::GetItemRectMax();
    ImGui::Spacing();

    if (pos.x != m_x) {
        m_x = pos.x;
        updated = true;
    }
    if (pos.y != m_y) {
        m_y = pos.y;
        updated = true;
    }

    auto binput = m_inputs.begin();
    auto boutput = m_outputs.begin();
    auto continueInput = binput < m_inputs.end();
    auto continueOutput = boutput < m_outputs.end();
    while (continueInput || continueOutput) {
        ImGui::BeginHorizontal(continueInput ? (void*) binput->get() : (void*) boutput->get(), ImVec2(nodeWidth, 0), 0.5);
        float spacing = continueInput && continueOutput ? 10 : 0;
        if (continueInput) {
            binput->get()->gui();
            ++binput;
            continueInput = binput < m_inputs.end();
        }
        ImGui::Spring(1.0, spacing);
        if (continueOutput) {
            boutput->get()->gui();
            ++boutput;
            continueOutput = boutput < m_outputs.end();
        }
        ImGui::EndHorizontal();
        ImGui::Spacing();
    }
    updated |= nodeGui();
    ed::EndNode();
    const auto halfBorderWidth = ed::GetStyle().NodeBorderWidth * 0.5f;
    float rounding = ed::GetStyle().NodeRounding;
    auto drawList = ed::GetNodeBackgroundDrawList(m_id);
    drawList->AddRectFilled(headerMin - ImVec2(nodePadding - halfBorderWidth, nodePadding - halfBorderWidth), headerMax + ImVec2(nodePadding - halfBorderWidth, 0), color(), rounding, ImDrawFlags_RoundCornersTop);
    ImGui::PopID();
    return updated;
}

nlohmann::json GraphNode::save() {
    nlohmann::json json = {
            {"id", m_id},
            {"type", type()},
            {"title", m_title},
            {"x", m_x},
            {"y", m_y},
            {"inputs", nlohmann::json::array()},
            {"outputs", nlohmann::json::array()}};
    auto& inputs = json["inputs"];
    for (auto& input: m_inputs) {
        inputs.emplace_back(nlohmann::json {
                {"id", input->m_id},
                {"type", input->m_type},
                {"label", input->m_label},
                {"link", input->m_link ? input->m_link->m_id : 0},
        });
    }
    auto& outputs = json["outputs"];
    for (auto& output: m_outputs) {
        outputs.emplace_back(nlohmann::json {
                {"id", output->m_id},
                {"type", output->m_type},
                {"label", output->m_label},
        });
    }
    return json;
}

bool GraphNode::load(const nlohmann::json& json) {
    bool ret = true;
    m_id = json["id"];
    m_x = json["x"];
    m_y = json["y"];
    if (json.contains("title")) {
        m_title = json["title"];
    }

    uint32_t i = 0;
    for (auto& input: json["inputs"]) {
        if (i >= m_inputs.size()) {
            m_inputs.emplace_back(std::make_unique<InputPort>(this));
        }
        m_inputs[i]->load(input["label"], input["type"], input["id"]);
        i++;
    }
    i = 0;
    for (auto& output: json["outputs"]) {
        if (i >= m_outputs.size()) {
            m_outputs.emplace_back(std::make_unique<OutputPort>(this));
        }
        m_outputs[i]->load(output["label"], output["type"], output["id"]);
        i++;
    }
    if (s_id <= m_id) {
        s_id = m_id + 1;
    }
    ed::SetNodePosition(m_id, {m_x, m_y});
    ret |= loadCustom(json);
    postLoad();
    return ret;
}

void GraphNode::addInput(const std::string& label, const std::string& type) {
    m_inputs.emplace_back(std::make_unique<InputPort>(this))->load(label, type);
}

void GraphNode::addOutput(const std::string& label, const std::string& type) {
    m_outputs.emplace_back(std::make_unique<OutputPort>(this))->load(label, type);
}
