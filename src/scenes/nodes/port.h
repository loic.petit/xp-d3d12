#pragma once

class GraphNode;
struct ImVec2;

class Port
{
public:
    Port(GraphNode* const nodeReference) : m_nodeReference(nodeReference)
    {
    }

    Port(const Port& other) = delete;

    Port(Port&& other) noexcept = default;

    Port& operator=(const Port& other) = delete;

    Port& operator=(Port&& other) noexcept = delete;

    virtual ~Port();

    void labelAndTypeGui();

    bool typeGui(const ImVec2& position);

    virtual void load(const std::string& label, const std::string& type, int32_t id = 0);

    std::string m_label;
    std::string m_type;
    GraphNode* const m_nodeReference;
    int32_t m_id;
    bool m_isInput = false;
};

class OutputPort : public Port
{
public:
    OutputPort(GraphNode* nodeReference) : Port(nodeReference)
    {
        m_isInput = false;
    }

    OutputPort(const OutputPort& other) = delete;

    OutputPort(OutputPort&& other) noexcept = delete;

    OutputPort& operator=(const OutputPort& other) = delete;

    OutputPort& operator=(OutputPort&& other) noexcept = delete;

    ~OutputPort() override;

    [[nodiscard]] std::string uniqueLabel() const
    {
        return m_uniqueLabel;
    }

    void load(const std::string& label, const std::string& type, int32_t id = 0) override;

    void declare(std::stringstream& out) const
    {
        out << m_type << " " << m_uniqueLabel << ";\n";
    }

    void declareLocal(std::stringstream& out) const
    {
        out << m_type << " " << m_label << ";\n";
    }

    void localToGlobal(std::stringstream& out) const
    {
        out << m_uniqueLabel << " = " << m_label << ";\n";
    }

    void gui();

    std::string m_uniqueLabel;
};

class InputPort : public Port
{
public:
    InputPort(GraphNode* nodeReference) : Port(nodeReference)
    {
        m_isInput = true;
    }

    InputPort(const InputPort& other) = delete;

    InputPort(InputPort&& other) noexcept = delete;

    InputPort& operator=(const InputPort& other) = delete;

    InputPort& operator=(InputPort&& other) noexcept = delete;

    void gui();

    void declareLinked(std::stringstream& out) const
    {
        out << m_type << " " << m_label << " = " << linkedValue() << ";\n";
    }

    const std::string& linkedValue() const
    {
        return m_link->m_uniqueLabel;
    }

    OutputPort* m_link = nullptr;
};
