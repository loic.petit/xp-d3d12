#pragma once


#include <json.hpp>
#include <core/pipeline.h>

#include "node.h"

class Port;

class Graph {
public:
    Graph();
    ~Graph() {
        m_nodes.clear();
    }

    bool gui();

    nlohmann::json save() const;

    template<typename T>
    T& append() {
        auto node = reinterpret_cast<T*>(m_nodes.emplace_back(std::make_unique<T>(this)).get());
        node->init();
        return *node;
    }

    bool refresh(ShaderBuilder& builder) const;

    void stage(CommandList& list, MaterialRender& pipeline) const {
        for (auto& node : m_nodes) {
            node->stage(list, pipeline);
        }
    }

    void load(const nlohmann::json& json);

    std::unordered_map<int32_t, Port*> m_parameterIdx;

    GraphNode& appendTyped(const std::string& type);

private:
    std::vector<std::unique_ptr<GraphNode>> m_nodes;
    std::map<std::string, std::function<GraphNode&()>> m_nodeTypes;
};
