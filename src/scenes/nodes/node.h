#pragma once

#include <core/commandlist.h>
#include <json.hpp>

#include "port.h"
#include "shader-builder.h"

struct ImColor;
class MaterialRender;
class Graph;

class GraphNode {
public:
    inline static uint32_t s_id = 1;

    explicit GraphNode(Graph* const graphReference, uint8_t maxInputs, uint8_t maxOutputs)
        : m_id(s_id++), m_graphReference(graphReference) {
        m_inputs.reserve(maxInputs);
        m_outputs.reserve(maxOutputs);
    }

    virtual ~GraphNode() = default;

protected:
    virtual bool loadCustom(const nlohmann::json& json) {
        return true;
    }

    virtual bool sidePanelGui() {
        return false;
    }

    virtual void defaultPorts() {
    }

    virtual void postLoad() {
    }

    virtual bool nodeGui() {
        return false;
    }

    virtual void buildShader(ShaderBuilder& builder) {
    }

    virtual const char* type() = 0;

    virtual const ImColor& color() = 0;

    virtual void stage(CommandList& list, MaterialRender& pipeline) const {}

    virtual nlohmann::json save();

    void addInput(const std::string& label, const std::string& type);

    void addOutput(const std::string& label, const std::string& type);

private:
    bool gui();

    void init() {
        m_title = type();
        defaultPorts();
        postLoad();
    }

    bool load(const nlohmann::json& json);

public:
    // GUI
    float m_x = 0;
    float m_y = 0;
    std::vector<std::unique_ptr<InputPort>> m_inputs;
    std::vector<std::unique_ptr<OutputPort>> m_outputs;
    std::string m_title;
    int32_t m_id;

    Graph* const m_graphReference;

    friend class Graph;
};
