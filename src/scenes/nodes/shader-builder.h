#pragma once

class ShaderBuilder {
public:
    static constexpr auto VECTOR_PARAMETER = "Vectors";
    /**
     * Require a new texture into the shader
     * @return the staging offset
     */
    uint32_t registerTexture(std::string& reference);
    /**
     * Require a new vector parameter
     * @return the staging offset
     */
    uint32_t registerVectorParameter(const std::string& name, std::string& reference);
    /**
     * Require a new vector parameter
     * @return the staging offset
     */
    uint32_t registerSampler(const CD3DX12_STATIC_SAMPLER_DESC& description, std::string& reference);

    std::stringstream& body() {
        return m_shaderBody;
    }

private:
    uint32_t m_uvs = 0;
    uint32_t m_vectorParameters = 0;
    std::vector<CD3DX12_STATIC_SAMPLER_DESC> m_samplers;
    std::stringstream m_shaderVectorParameters;
    std::stringstream m_shaderDeclaration;
    std::stringstream m_shaderBody;
    friend class MaterialRender;
};

