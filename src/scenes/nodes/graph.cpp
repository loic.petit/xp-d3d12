#include "graph.h"

#include <imgui_internal.h>
#include <unordered_set>
#include <utils/gui.h>

#include <scenes/materials/nodes/color-node.h>
#include <scenes/materials/nodes/normal-node.h>
#include <scenes/materials/nodes/renderer-node.h>
#include <scenes/materials/nodes/srgb-to-linear-node.h>
#include <scenes/materials/nodes/texcoord-node.h>
#include <scenes/materials/nodes/transform-node.h>
#include <scenes/materials/nodes/uv-node.h>

#define DECLARE_NODE_TYPE(clazz) \
    m_nodeTypes.emplace(clazz##Node::TYPE, [&]() -> GraphNode& { return append<clazz##Node>(); })

GraphNode& Graph::appendTyped(const std::string& type) {
    return m_nodeTypes[type]();
}

Graph::Graph() {
    DECLARE_NODE_TYPE(Uv);
    DECLARE_NODE_TYPE(TexCoord);
    DECLARE_NODE_TYPE(Renderer);
    DECLARE_NODE_TYPE(Transform);
    DECLARE_NODE_TYPE(Color);
    DECLARE_NODE_TYPE(Normal);
    DECLARE_NODE_TYPE(SrgbToLinear);
}

static bool Splitter(bool split_vertically, float thickness, float* size1, float* size2, float min_size1, float min_size2, float splitter_long_axis_size = -1.0f)
{
    using namespace ImGui;
    ImGuiContext& g = *GImGui;
    ImGuiWindow* window = g.CurrentWindow;
    ImGuiID id = window->GetID("##Splitter");
    ImRect bb;
    bb.Min = window->DC.CursorPos + (split_vertically ? ImVec2(*size1, 0.0f) : ImVec2(0.0f, *size1));
    bb.Max = bb.Min + CalcItemSize(split_vertically ? ImVec2(thickness, splitter_long_axis_size) : ImVec2(splitter_long_axis_size, thickness), 0.0f, 0.0f);
    return SplitterBehavior(bb, id, split_vertically ? ImGuiAxis_X : ImGuiAxis_Y, size1, size2, min_size1, min_size2, 0.0f);
}

bool Graph::gui() {
    bool updated = false;

    static float leftPaneWidth  = 400.0f;
    static float rightPaneWidth = 0;
    //if (!rightPaneWidth) ImGui::GetMainViewport()->WorkSize.x - leftPaneWidth;
    Splitter(true, 4.0f, &leftPaneWidth, &rightPaneWidth, 50.0f, 50.0f);
    ImGui::BeginChild("MaterialProperties", ImVec2(leftPaneWidth-4.0f, 0), true, ImGuiWindowFlags_AlwaysVerticalScrollbar);
    ed::NodeId selectIds[2];
    if (ed::GetSelectedNodes(selectIds, 2) == 1) {
        for (auto& node : m_nodes) {
            if (selectIds[0].Get() == node->m_id) {
                updated |= node->sidePanelGui();
                break;
            }
        }
    }
    ImGui::EndChild();
    ImGui::SameLine(0, 12.0f);
    ed::Begin("Material editor", ImVec2(rightPaneWidth, 0));
    const ImVec2 click_pos = ImGui::GetMousePos();
    ed::Suspend();
    if (ed::ShowBackgroundContextMenu()) {
        ImGui::OpenPopup("add node");
    }
    if (ImGui::BeginPopup("add node")) {
        for (const auto& item: m_nodeTypes) {
            auto& type = item.first;
            if (ImGui::MenuItem(type.c_str())) {
                auto& node = item.second();
                updated = true;
                ed::SetNodePosition(node.m_id, click_pos);
            }
        }
        ImGui::EndPopup();
    }
    ed::Resume();

    for (auto& node: m_nodes) {
        updated |= node->gui();
    }

    for (auto& node: m_nodes) {
        for (auto& input: node->m_inputs) {
            if (input->m_link) {
                ed::Link(input->m_id << 16 | input->m_link->m_id, input->m_link->m_id, input->m_id, ImVec4(1, 1, 1, 1),
                         2.0f);
            }
        }
    }

    if (ed::BeginCreate(ImColor(255, 255, 255), 2.0f)) {
        ed::PinId linkCreatedBegin = 0, linkCreatedEnd = 0;
        if (ed::QueryNewLink(&linkCreatedBegin, &linkCreatedEnd)) {
            auto startPin = m_parameterIdx[linkCreatedBegin.Get()];
            auto endPin = m_parameterIdx[linkCreatedEnd.Get()];

            if (startPin && startPin->m_isInput) {
                std::swap(startPin, endPin);
            }

            if (startPin && endPin) {
                if (endPin == startPin) {
                    ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
                } else if (!endPin->m_isInput || startPin->m_isInput) {
                    ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
                } else if (endPin->m_type != startPin->m_type) {
                    ed::RejectNewItem(ImColor(255, 0, 0), 2.0f);
                } else if (ed::AcceptNewItem(ImColor(128, 255, 128), 4.0f)) {
                    static_cast<InputPort*>(endPin)->m_link = static_cast<OutputPort*>(startPin);
                    updated = true;
                }
            }
        }
    }
    ed::EndCreate();
    if (ed::BeginDelete()) {
        ed::LinkId linkDestroyed = 0;
        ed::PinId startId = 0, endId = 0;
        while (ed::QueryDeletedLink(&linkDestroyed, &startId, &endId)) {
            auto startPin = m_parameterIdx[startId.Get()];
            auto endPin = m_parameterIdx[endId.Get()];
            if (startPin->m_isInput) {
                std::swap(startPin, endPin);
            }
            static_cast<InputPort*>(endPin)->m_link = nullptr;
            updated = true;
            ed::AcceptDeletedItem();
        }
        ed::NodeId deletedNode;
        while (ed::QueryDeletedNode(&deletedNode)) {
            for (auto it = m_nodes.begin() ; it < m_nodes.end() ;) {
                if (it->get()->m_id == deletedNode.Get()) {
                    it = m_nodes.erase(it);
                } else {
                    ++it;
                }
            }
            updated = true;
        }
    }
    ed::EndDelete();
    ed::End();
    return updated;
}

nlohmann::json Graph::save() const {
    nlohmann::json output;

    nlohmann::json nodesArray = nlohmann::json::array();
    for (auto& node: m_nodes) {
        nodesArray.emplace_back(node->save());
    }
    output["nodes"] = nodesArray;
    return output;
}


static bool sortNodes(std::deque<GraphNode*>& nodes, GraphNode* const current, std::unordered_set<uint32_t>& visitedIds) {
    if (!current->m_inputs.empty()) {
        for (auto& input: current->m_inputs) {
            if (!input->m_link) return false;
            sortNodes(nodes, input->m_link->m_nodeReference, visitedIds);
        }
    }
    if (visitedIds.find(current->m_id) == visitedIds.end()) {
        nodes.emplace_back(current);
        visitedIds.emplace(current->m_id);
    }
    return true;
}

bool Graph::refresh(ShaderBuilder& builder) const {

    std::deque<GraphNode*> queue;
    std::unordered_set<uint32_t> visitedIds;

    GraphNode* renderer = nullptr;
    for (auto& node : m_nodes) {
        if (node->m_outputs.empty()) {
            if (strcmp(node->type(), "Renderer") == 0) {
                renderer = node.get();
            } else {
                if (!sortNodes(queue, node.get(), visitedIds)) return false;
            }
        }
    }
    if (!sortNodes(queue, renderer, visitedIds)) return false;

    for (auto& node : queue) {
        node->buildShader(builder);
    }
    builder.body() << "return;\n";
    return true;
}

void Graph::load(const nlohmann::json& json) {
    m_parameterIdx.clear();
    m_nodes.clear();

    for (auto& node : json["nodes"]) {
        auto& graphNode = appendTyped(node["type"].get<std::string>());
        graphNode.load(node);
    }
    for (auto& node : json["nodes"]) {
        for (auto& input : node["inputs"]) {
            int32_t link = input["link"];
            if (link != 0) {
                static_cast<InputPort*>(m_parameterIdx[input["id"]])->m_link = static_cast<OutputPort*>(m_parameterIdx[link]);
            }
        }
    }
}
