#include "port.h"

#include "scenes/nodes/graph.h"
#include "scenes/nodes/node.h"
#include <utils/gui.h>

Port::~Port() {
    if (!m_type.empty()) {
        auto& pairs = m_nodeReference->m_graphReference->m_parameterIdx;
        // if type is empty = it has been moved so no need to change the refs
        pairs.erase(m_id);
    }
}

constexpr float PIN_SIZE = 13;

void Port::labelAndTypeGui() {
    ImGui::TextUnformatted(m_label.c_str());
}

bool Port::typeGui(const ImVec2& position) {
    constexpr int radius = 3;
    auto drawList = ImGui::GetWindowDrawList();
    auto positionWithOffset = position + ImVec2(3, 4);
    if (m_type == "float4") {
        drawList->AddCircleFilled(positionWithOffset + ImVec2(0, 0), radius, ImColor(255, 100, 100, 255));
        drawList->AddCircleFilled(positionWithOffset + ImVec2(PIN_SIZE / 2, 0), radius, ImColor(100, 255, 100, 255));
        drawList->AddCircleFilled(positionWithOffset + ImVec2(0, PIN_SIZE / 2), radius, ImColor(100, 100, 255, 255));
        drawList->AddCircleFilled(positionWithOffset + ImVec2(PIN_SIZE / 2, PIN_SIZE / 2), radius, ImColor(255, 255, 255, 255));
    } else if (m_type == "float3") {
        drawList->AddCircleFilled(positionWithOffset + ImVec2(PIN_SIZE / 4, 0), radius, ImColor(255, 100, 100, 255));
        drawList->AddCircleFilled(positionWithOffset + ImVec2(0, PIN_SIZE / 2), radius, ImColor(100, 100, 255, 255));
        drawList->AddCircleFilled(positionWithOffset + ImVec2(PIN_SIZE / 2, PIN_SIZE / 2), radius, ImColor(100, 255, 100, 255));
    } else if (m_type == "float2") {
        drawList->AddCircleFilled(positionWithOffset + ImVec2(0, PIN_SIZE / 4), radius, ImColor(100, 100, 100, 255));
        drawList->AddCircleFilled(positionWithOffset + ImVec2(PIN_SIZE / 2, PIN_SIZE / 4), radius, ImColor(255, 255, 255, 255));
    } else if (m_type == "float") {
        drawList->AddCircleFilled(position + ImVec2(PIN_SIZE / 2, PIN_SIZE / 2), radius, ImColor(100, 100, 100, 255));
    }
    return true;
}

void Port::load(const std::string& label, const std::string& type, int32_t id) {
    if (id == 0) {
        id = GraphNode::s_id++;
    }
    m_label = label;
    m_type = type;
    m_id = id;
    m_nodeReference->m_graphReference->m_parameterIdx[m_id] = this;
    if (GraphNode::s_id <= id) GraphNode::s_id = id + 1;
}

void OutputPort::gui() {
    ed::BeginPin(m_id, ed::PinKind::Output);
    ed::PinPivotAlignment(ImVec2(1.0f, 0.5f));
    ed::PinPivotSize(ImVec2(0, 0));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(2, 0));
    labelAndTypeGui();
    ImGui::Dummy(ImVec2(PIN_SIZE, PIN_SIZE));
    auto typeLocation = ImGui::GetItemRectMin();
    ImGui::Dummy(ImVec2(PIN_SIZE, PIN_SIZE));
    auto pinLocation = ImGui::GetItemRectMin();
    ImGui::PopStyleVar();
    ed::EndPin();
    auto drawList = ImGui::GetWindowDrawList();
    drawList->AddTriangleFilled(pinLocation, pinLocation + ImVec2(PIN_SIZE, PIN_SIZE / 2), pinLocation + ImVec2(0, PIN_SIZE), ImColor(255, 255, 255, 200));
    typeGui(typeLocation);
}

OutputPort::~OutputPort() {
    for (auto& entry: m_nodeReference->m_graphReference->m_parameterIdx) {
        if (entry.second->m_isInput) {
            auto& parameter = *static_cast<InputPort*>(entry.second);
            if (parameter.m_link == this) {
                parameter.m_link = nullptr;
            }
        }
    }
}

void OutputPort::load(const std::string& label, const std::string& type, int32_t id) {
    Port::load(label, type, id);
    m_uniqueLabel = "node" + std::to_string(m_nodeReference->m_id) + "_" + m_label;
}

void InputPort::gui() {
    ed::BeginPin(m_id, ed::PinKind::Input);
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(2, 0));
    ed::PinPivotAlignment(ImVec2(0.0f, 0.5f));
    ed::PinPivotSize(ImVec2(0, 0));
    ImGui::Dummy(ImVec2(PIN_SIZE, PIN_SIZE));
    auto pinLocation = ImGui::GetItemRectMin();
    ImGui::Dummy(ImVec2(PIN_SIZE, PIN_SIZE));
    auto typeLocation = ImGui::GetItemRectMin();
    labelAndTypeGui();
    ImGui::PopStyleVar();
    ed::EndPin();
    auto drawList = ImGui::GetWindowDrawList();
    if (m_link) {
        drawList->AddCircleFilled(pinLocation + ImVec2(PIN_SIZE / 2, PIN_SIZE / 2), PIN_SIZE / 2,
                                  ImColor(255, 255, 255, 200));
    } else {
        drawList->AddCircle(pinLocation + ImVec2(PIN_SIZE / 2, PIN_SIZE / 2), PIN_SIZE / 2, ImColor(255, 255, 255, 200),
                            0, 2);
    }
    typeGui(typeLocation);
}
