#include "shader-builder.h"

uint32_t ShaderBuilder::registerTexture(std::string& reference) {
    const auto offsetAsString = std::to_string(m_uvs);
    reference = "UV" + offsetAsString;
    m_shaderDeclaration << "Texture2D " << reference << " : register(t" << offsetAsString << ");\n";
    return m_uvs++;
}

uint32_t ShaderBuilder::registerVectorParameter(const std::string& name, std::string& reference) {

    m_shaderVectorParameters << "float4 " << name << ";\n";
    reference = std::string(VECTOR_PARAMETER) + "." + reference;
    return m_vectorParameters++;
}

uint32_t ShaderBuilder::registerSampler(const CD3DX12_STATIC_SAMPLER_DESC& description, std::string& reference) {
    uint32_t offset = m_samplers.size();
    auto offsetAsString = std::to_string(offset);
    reference = "SAMPLER" + offsetAsString;
    m_shaderDeclaration << "SamplerState " << reference << " : register(s" << offsetAsString << ");\n";
    m_samplers.emplace_back(description);
    return offset;
}
