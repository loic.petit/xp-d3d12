#include "core/commandlist.h"
#include "core/device.h"
#include "scenes/training.h"

void createGrid(Object& result, CommandList& copy, float w, float h, float d);

void createTestSample(Object& result, CommandList& copy, float w, float h);

void createSphere(Object& result, CommandList& copy, float radius, uint32_t tessellation);