#include "cube.h"
#include "utils/logger.h"
#include "utils/service.h"

#include "utils/shader-loader.h"
#include "utils/utils.h"

using namespace DirectX;

Cube::Cube() {
}

// Vertex data for a colored cube.
struct VertexPosColor {
    XMFLOAT3 Position;
    XMFLOAT3 Color;
};

static VertexPosColor g_Vertices[8] = {
        {XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f)},  // 0
        {XMFLOAT3(-1.0f, 1.0f, -1.0f),  XMFLOAT3(0.0f, 1.0f, 0.0f)},   // 1
        {XMFLOAT3(1.0f, 1.0f, -1.0f),   XMFLOAT3(1.0f, 1.0f, 0.0f)},    // 2
        {XMFLOAT3(1.0f, -1.0f, -1.0f),  XMFLOAT3(1.0f, 0.0f, 0.0f)},   // 3
        {XMFLOAT3(-1.0f, -1.0f, 1.0f),  XMFLOAT3(0.0f, 0.0f, 1.0f)},   // 4
        {XMFLOAT3(-1.0f, 1.0f, 1.0f),   XMFLOAT3(0.0f, 1.0f, 1.0f)},    // 5
        {XMFLOAT3(1.0f, 1.0f, 1.0f),    XMFLOAT3(1.0f, 1.0f, 1.0f)},     // 6
        {XMFLOAT3(1.0f, -1.0f, 1.0f),   XMFLOAT3(1.0f, 0.0f, 1.0f)}     // 7
};

static WORD g_Indicies[36] = {0, 1, 2, 0, 2, 3, 4, 6, 5, 4, 7, 6, 4, 5, 1, 4, 1, 0,
                              3, 2, 6, 3, 6, 7, 1, 5, 6, 1, 6, 2, 4, 0, 3, 4, 3, 7};

void Cube::load(const RenderTarget::Desc& desc) {
    if (m_loaded) return;
    m_loaded = true;

    // Use a copy queue to copy GPU resources.
    auto& copyQueue = Get<EngineQueues>().copyQueue();
    std::shared_ptr<CommandList> copyList = copyQueue.createCommandList();

    // Load vertex data:
    m_vertexBuffer = Get<Device>().uploadBuffer(*copyList, g_Vertices, _countof(g_Vertices) * sizeof(VertexPosColor));
    ThrowIfFailed(m_vertexBuffer->SetName(L"Vertex Buffer"));
    m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
    m_vertexBufferView.SizeInBytes = static_cast<UINT>(_countof(g_Vertices) * sizeof(VertexPosColor));
    m_vertexBufferView.StrideInBytes = static_cast<UINT>( sizeof(VertexPosColor));
    LOG_DEBUG("Loaded vertex buffer with %d entries and stride %d", _countof(g_Vertices), m_vertexBufferView.StrideInBytes);

    // Load index data:
    m_indexBuffer = Get<Device>().uploadBuffer(*copyList, g_Indicies, _countof(g_Indicies) * sizeof(g_Indicies[0]));
    ThrowIfFailed(m_vertexBuffer->SetName(L"Index Buffer"));
    m_indexBufferView.BufferLocation = m_indexBuffer->GetGPUVirtualAddress();
    m_indexBufferView.SizeInBytes = static_cast<UINT>(_countof(g_Indicies) * sizeof(g_Indicies[0]));
    m_indexBufferView.Format = DXGI_FORMAT_R16_UINT;
    LOG_DEBUG("Loaded index buffer with %d entries", _countof(g_Indicies));

    // Execute the command list to uploadBuffer the resources to the GPU.
    copyQueue.execute(copyList);

    // wait for the loading to be complete
    Get<EngineQueues>().directQueue().waitForQueue(Get<EngineQueues>().copyQueue());

    loadPSO(desc);
}


void Cube::loadPSO(const RenderTarget::Desc& renderTargetDesc) {
    // Create the vertex input layout
    D3D12_INPUT_ELEMENT_DESC inputLayout[] = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,
                    D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"COLOR",    0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,
                    D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
    };

    // Create the root signature.
    // Allow input layout and deny unnecessary access to certain pipeline stages.
    D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
                                                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                                                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                                                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
                                                    D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

    // A single 32-bit constant root parameter that is used by the vertex shader.
    CD3DX12_ROOT_PARAMETER1 rootParameters[1];
    const size_t values = sizeof(XMMATRIX) / 4;
    rootParameters[0].InitAsConstants(values, 0, 0, D3D12_SHADER_VISIBILITY_VERTEX);

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDescription(
                    _countof(rootParameters), rootParameters, 0,
                    nullptr, rootSignatureFlags
    );

    m_rootSignature = Get<Device>().createCompatibleRootSignature(rootSignatureDescription);

    // Create the pipeline state object.
    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
    } pipelineStateStream;

    pipelineStateStream.pRootSignature = m_rootSignature.Get();
    pipelineStateStream.InputLayout = {inputLayout, _countof(inputLayout)};
    pipelineStateStream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    pipelineStateStream.DSVFormat = renderTargetDesc.depthFormat;
    D3D12_RT_FORMAT_ARRAY rtvFormats = {{renderTargetDesc.pixelFormats[0]}, 1};
    pipelineStateStream.RTVFormats = rtvFormats;

    // Shaders
    ShaderBlob vs = COMPILE_PS("cube/vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_VS("cube/ps.hlsl", nullptr);
    pipelineStateStream.VS = vs;
    pipelineStateStream.PS = ps;

    D3D12_PIPELINE_STATE_STREAM_DESC desc = {sizeof(PipelineStateStream), &pipelineStateStream};
    ThrowIfFailed(Get<Device>()->CreatePipelineState(&desc, IID_PPV_ARGS(&m_pso)));
}

void Cube::render(CommandList& list, RenderTarget& renderTarget) {
    // Update the view matrix.
    const XMVECTOR eyePosition = XMVectorSet(0, 0, -10, 1);
    const XMVECTOR focusPoint = XMVectorSet(0, 0, 0, 1);
    const XMVECTOR upDirection = XMVectorSet(0, 1, 0, 0);
    XMMATRIX viewMatrix = XMMatrixLookAtLH(eyePosition, focusPoint, upDirection);

    // Update the projection matrix.
    float aspectRatio = renderTarget.desc().width * 1.0 / renderTarget.desc().height;
    XMMATRIX projectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0), aspectRatio, 0.1f, 100.0f);
    XMMATRIX mvpMatrix = m_modelMatrix * viewMatrix * projectionMatrix;

    // Set the pipeline state object
    list->SetPipelineState(m_pso.Get());
    // Set the root signatures.
    list->SetGraphicsRootSignature(m_rootSignature.Get());

    // Set root parameters
    list->SetGraphicsRoot32BitConstants(0, sizeof(mvpMatrix) / 4, &mvpMatrix, 0);

    renderTarget.use(list);

    // Render the cube.
    list->IASetVertexBuffers(0, 1, &m_vertexBufferView);
    list->IASetIndexBuffer(&m_indexBufferView);
    list->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    list->DrawIndexedInstanced(_countof(g_Indicies), 1, 0, 0, 0);
}

void Cube::move(int32_t pitch, int32_t yaw) {
    constexpr float mouseSpeed = 0.2f;

    m_modelMatrix *= XMMatrixRotationRollPitchYaw(XMConvertToRadians(-static_cast<float>(pitch) * mouseSpeed), XMConvertToRadians(-static_cast<float>(yaw) * mouseSpeed), 0.0f);
}

void Cube::update() {
    const auto& inputs = Get<EngineInput>();
    if (inputs.mouseButton & INPUT_PRESSED) {
        move(inputs.mouseDeltaY, inputs.mouseDeltaX);
    }
}
