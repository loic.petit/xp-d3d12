#pragma once

#include "core/commandlist.h"
#include "core/device.h"
#include "core/rendertarget.h"
#include "context.h"

class Cube {
public:
    explicit Cube();

    void load(const RenderTarget::Desc& desc);

    void render(CommandList& list, RenderTarget& target);

    void update();

    void move(int32_t pitch, int32_t yaw);

private:
    void loadPSO(const RenderTarget::Desc& desc);

    ComPtr<ID3D12RootSignature> m_rootSignature;
    ComPtr<ID3D12PipelineState> m_pso;
    Resource m_indexBuffer;
    Resource m_vertexBuffer;
    D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView {};
    D3D12_INDEX_BUFFER_VIEW m_indexBufferView {};
    bool m_loaded = false;

    DirectX::XMMATRIX m_modelMatrix = DirectX::XMMatrixRotationRollPitchYaw(0, 0, 0);
};