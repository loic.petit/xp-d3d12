#pragma once

#include "core/device.h"
#include "core/queue.h"
#include "utils/gui.h"
#include "utils/service.h"

constexpr uint8_t INPUT_PRESSED = 0x01;
constexpr uint8_t INPUT_POSITIVE_EDGE = 0x02;
constexpr uint8_t INPUT_NEGATIVE_EDGE = 0x04;

struct EngineInput {
    uint8_t wasd[4] = {};

    uint8_t trigger = false;
    uint8_t mouseButton = false;
    int32_t mouseX = 0;
    int32_t mouseY = 0;
    int32_t mouseDeltaX = 0;
    int32_t mouseDeltaY = 0;
};

class EngineQueues {
public:
    EngineQueues() = default;

    EngineQueues(const EngineQueues& other) = delete;

    EngineQueues(EngineQueues&& other) noexcept = delete;

    EngineQueues& operator=(const EngineQueues& other) = delete;

    EngineQueues& operator=(EngineQueues&& other) noexcept = delete;

    virtual ~EngineQueues() = default;
    virtual Queue& copyQueue() = 0;
    virtual Queue& directQueue() = 0;
    virtual Queue& computeQueue() = 0;
};