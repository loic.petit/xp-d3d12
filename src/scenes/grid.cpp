#include "grid.h"

using namespace DirectX;

#define offset(attr, base) (uint32_t)(reinterpret_cast<char const volatile*>(&(attr))-reinterpret_cast<const volatile char*>(&(base)))

void createGrid(Object& result, CommandList& copy, float w, float h, float d) {
    auto& device = Get<Device>();
    struct Grid {
        XMFLOAT3 vertices[20];
        XMFLOAT3 normals[20];
        XMFLOAT4 tangents[20];
        uint8_t color[20][4];
        uint16_t joints[20][4];
        uint8_t weights[20][4];
        XMFLOAT2 texCoord[20];
    };

    Grid rawData = {
        {
            {-w, 0, d}, // 0 back
            {+w, 0, d},
            {-w, h, d},
            {+w, h, d},

            {-w, 0, -d}, // 4 left
            {-w, 0, +d},
            {-w, h, -d},
            {-w, h, +d},

            {-w, 0, -d}, // 8 floor
            {+w, 0, -d},
            {-w, 0, +d},
            {+w, 0, +d},

            {+w, 0, +d}, // 12 right
            {+w, 0, -d},
            {+w, h, +d},
            {+w, h, -d},

            {-w, h, +d}, // 16 ceiling
            {+w, h, +d},
            {-w, h, -d},
            {+w, h, -d},
        },
        // normals
        {
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},

            {1, 0, 0},
            {1, 0, 0},
            {1, 0, 0},
            {1, 0, 0},

            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0},

            {-1, 0, 0},
            {-1, 0, 0},
            {-1, 0, 0},
            {-1, 0, 0},

            {0, -1, 0},
            {0, -1, 0},
            {0, -1, 0},
            {0, -1, 0},
        },
        // tangents
        {
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},

            {0, 0, -1, 1},
            {0, 0, -1, 1},
            {0, 0, -1, 1},
            {0, 0, -1, 1},

            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},

            {0, 0, 1, 1},
            {0, 0, 1, 1},
            {0, 0, 1, 1},
            {0, 0, 1, 1},

            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},
        },
        // colors
        {
            {255, 255, 255, 255},
            {127, 255, 127, 255},
            {127, 255, 127, 255},
            {0, 255, 0, 255},

            {255, 255, 255, 255},
            {127, 127, 255, 255},
            {127, 127, 255, 255},
            {0, 0, 255, 255},

            {255, 255, 255, 255},
            {255, 127, 127, 255},
            {255, 127, 127, 255},
            {255, 0, 0, 255},

            {255, 255, 255, 255},
            {127, 255, 255, 255},
            {127, 255, 255, 255},
            {0, 255, 255, 255},

            {255, 255, 255, 255},
            {0, 127, 255, 255},
            {0, 127, 255, 255},
            {255, 0, 255, 255},
        },
        // joints
        {},
        // weights
        {},
        // tex coords
        {
            {-w, 0}, // 0 back
            {+w, 0},
            {-w, h},
            {+w, h},

            {0, -d}, // 4 left
            {0, +d},
            {h, -d},
            {h, +d},

            {-w, -d}, // 8 floor
            {+w, -d},
            {-w, +d},
            {+w, +d},

            {0, +d}, // 12 right
            {0, -d},
            {h, +d},
            {h, -d},

            {-w, +d}, // 16 ceiling
            {+w, +d},
            {-w, -d},
            {+w, -d},
        }
    };

    result.buffers.emplace_back(device.uploadBuffer(copy, &rawData, sizeof(rawData)));
    D3D12_GPU_VIRTUAL_ADDRESS gpuPtr = result.buffers[0]->GetGPUVirtualAddress();
    MeshPrimitive& primitive = result.primitives.emplace_back();
    primitive.vertexViews = {
        {gpuPtr + offset(rawData.vertices, rawData), sizeof(rawData.vertices), sizeof(rawData.vertices[0])},
        {gpuPtr + offset(rawData.normals, rawData), sizeof(rawData.normals), sizeof(rawData.normals[0])},
        {gpuPtr + offset(rawData.tangents, rawData), sizeof(rawData.tangents), sizeof(rawData.tangents[0])},
        {gpuPtr + offset(rawData.color, rawData), sizeof(rawData.color), sizeof(rawData.color[0])},
        {gpuPtr + offset(rawData.joints, rawData), sizeof(rawData.joints), sizeof(rawData.joints[0])},
        {gpuPtr + offset(rawData.weights, rawData), sizeof(rawData.weights), sizeof(rawData.weights[0])},
        {gpuPtr + offset(rawData.texCoord, rawData), sizeof(rawData.texCoord), sizeof(rawData.texCoord[0])},
    };
    primitive.vertexCount = _countof(rawData.vertices);

    uint16_t indices[30] = {
        0, 2, 1,
        1, 2, 3,

        4, 6, 5,
        5, 6, 7,

        8, 10, 9,
        9, 10, 11,

        12, 14, 13,
        13, 14, 15,

        16, 18, 17,
        17, 18, 19,
    };
    result.buffers.emplace_back(device.uploadBuffer(copy, &indices, sizeof(indices)));
    gpuPtr = result.buffers[1]->GetGPUVirtualAddress();
    primitive.indexViews = {gpuPtr, sizeof(indices), DXGI_FORMAT_R16_UINT};
    primitive.indexCount = _countof(indices);
    primitive.materialId = 0;
    result.materials.push_back({
        {0.4, 0.4, 0.4, 1.0}, {0, 0.9, 0, 1}, {}, {}, 0,
        LIGHTING_ALBEDO_MAPPING | LIGHTING_HAS_SHADOWS
    });
}

void createTestSample(Object& result, CommandList& copy, float w, float h) {
    auto& device = Get<Device>();
    struct Grid {
        XMFLOAT3 vertices[12];
        XMFLOAT3 normals[12];
        XMFLOAT4 tangents[12];
        uint8_t color[12][4];
        uint16_t joints[12][4];
        uint8_t weights[12][4];
        XMFLOAT2 texCoord[12];
        float time[12];
        XMFLOAT3 translation[12];
        XMFLOAT4 rotation[12];
        XMFLOAT3 translation2[12];
        XMFLOAT4 rotation2[12];
    };

    Grid rawData = {
        {
            {-.5f * w, 0, 0}, // 0 back
            {.5f * w, 0, 0},
            {-.5f * w, .5f * h, 0},
            {.5f * w, .5f * h, 0},
            {-.5f * w, h, 0},
            {.5f * w, h, 0},
            {-.5f * w, 1.5f * h, 0},
            {.5f * w, 1.5f * h, 0},
            {-.5f * w, 2.0f * h, 0},
            {.5f * w, 2.0f * h, 0},
            {-.5f * w, 2.5f * h, 0},
            {.5f * w, 2.5f * h, 0},
        },
        // normals
        {
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
        },
        // tangents
        {
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
        },
        // colors
        {
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
            {255, 255, 255, 255},
        },
        // joints
        {
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
            {1, 2, 3, 0},
        },
        // weights
        {
            {255, 0, 0, 0},
            {255, 0, 0, 0},
            {191, 64, 0, 0},
            {191, 64, 0, 0},
            {128, 127, 0, 0},
            {128, 127, 0, 0},
            {64, 191, 0, 0},
            {64, 191, 0, 0},
            {0, 128, 127, 0},
            {0, 128, 127, 0},
            {0, 0, 255, 0},
            {0, 0, 255, 0},
        },
        // tex coords
        {
            {-.5f * w, 0}, // 0 back
            {.5f * w, 0},
            {-.5f * w, .5f * h},
            {.5f * w, .5f * h},
            {-.5f * w, h},
            {.5f * w, h},
            {-.5f * w, 1.5f * h},
            {.5f * w, 1.5f * h},
            {-.5f * w, 2.0f * h},
            {.5f * w, 2.0f * h},
            {-.5f * w, 2.5f * h},
            {.5f * w, 2.5f * h},
        },
        // time
        {
            0, 0.5f, 1.0f, 1.5f,
            2.0f, 2.5f, 3.0f, 3.5f,
            4.0f, 4.5f, 5.0f, 5.5f,
        },
        // translate
        {
            // same as node
            {0, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
        },
        // rotate
        {
            // same as node
            {0.0f, 0.0f, 0.0f, 1.0f},
            {0.0f, 0.0f, 0.383f, 0.924f},
            {0.0f, 0.0f, 0.707f, 0.707f},
            {0.0f, 0.0f, 0.707f, 0.707f},
            {0.0f, 0.0f, 0.383f, 0.924f},
            {0.0f, 0.0f, 0.0f, 1.0f},
            {0.0f, 0.0f, 0.0f, 1.0f},
            {0.0f, 0.0f, -0.383f, 0.924f},
            {0.0f, 0.0f, -0.707f, 0.707f},
            {0.0f, 0.0f, -0.707f, 0.707f},
            {0.0f, 0.0f, -0.383f, 0.924f},
            {0.0f, 0.0f, 0.0f, 1.0f},
        },
        // translate
        {
            // same as node
            {0, 1.0f * h, 0},
            {0.0f * w, 1.0f * h, 0},
            {0.0f * w, 1.2f * h, 0},
            {0.0f * w, 1.2f * h, 0},
            {0.0f * w, 1.4f * h, 0},
            {0.0f * w, 1.4f * h, 0},
            {0.0f * w, 1.6f * h, 0},
            {0.0f * w, 1.6f * h, 0},
            {0.0f * w, 1.8f * h, 0},
            {0.0f * w, 1.8f * h, 0},
            {0.0f * w, 2.0f * h, 0},
            {0.0f * w, 2.0f * h, 0},
        },
        // rotate
        {
            // same as node
            {0.0f, 0.0f, -0.0f, 1.0f},
            {0.0f, 0.0f, -0.383f, 0.924f},
            {0.0f, 0.0f, -0.707f, 0.707f},
            {0.0f, 0.0f, -0.707f, 0.707f},
            {0.0f, 0.0f, -0.383f, 0.924f},
            {0.0f, 0.0f, -0.0f, 1.0f},
            {0.0f, 0.0f, -0.0f, 1.0f},
            {0.0f, 0.0f, 0.383f, 0.924f},
            {0.0f, 0.0f, 0.707f, 0.707f},
            {0.0f, 0.0f, 0.707f, 0.707f},
            {0.0f, 0.0f, 0.383f, 0.924f},
            {0.0f, 0.0f, -0.0f, 1.0f},
        },
    };

    result.buffers.emplace_back(device.uploadBuffer(copy, &rawData, sizeof(rawData)));
    D3D12_GPU_VIRTUAL_ADDRESS gpuPtr = result.buffers[0]->GetGPUVirtualAddress();
    MeshPrimitive& primitive = result.primitives.emplace_back();
    primitive.vertexViews = {
        {gpuPtr + offset(rawData.vertices, rawData), sizeof(rawData.vertices), sizeof(rawData.vertices[0])},
        {gpuPtr + offset(rawData.normals, rawData), sizeof(rawData.normals), sizeof(rawData.normals[0])},
        {gpuPtr + offset(rawData.tangents, rawData), sizeof(rawData.tangents), sizeof(rawData.tangents[0])},
        {gpuPtr + offset(rawData.color, rawData), sizeof(rawData.color), sizeof(rawData.color[0])},
        {gpuPtr + offset(rawData.joints, rawData), sizeof(rawData.joints), sizeof(rawData.joints[0])},
        {gpuPtr + offset(rawData.weights, rawData), sizeof(rawData.weights), sizeof(rawData.weights[0])},
        {gpuPtr + offset(rawData.texCoord, rawData), sizeof(rawData.texCoord), sizeof(rawData.texCoord[0])},
    };
    primitive.vertexCount = _countof(rawData.vertices);

    uint16_t indices[30] = {
        0, 3, 1,
        0, 2, 3,

        2, 5, 3,
        2, 4, 5,

        4, 7, 5,
        4, 6, 7,

        6, 9, 7,
        6, 8, 9,

        8, 11, 9,
        8, 10, 11,
    };

    result.buffers.emplace_back(device.uploadBuffer(copy, &indices, sizeof(indices)));
    gpuPtr = result.buffers[1]->GetGPUVirtualAddress();
    primitive.indexViews = {gpuPtr, sizeof(indices), DXGI_FORMAT_R16_UINT};
    primitive.indexCount = _countof(indices);
    primitive.materialId = 0;

    result.nodes.emplace_back(Node{"root", XMMatrixIdentity(), XMMatrixIdentity(), XMMatrixIdentity(), 0});
    result.nodes.emplace_back(Node{"base", XMMatrixIdentity(), XMMatrixIdentity(), XMMatrixIdentity(), 0, 0});
    result.nodes.emplace_back(Node{
        "head", XMMatrixTranslation(0, 1.0f * h, 0), XMMatrixTranslation(0, 1.0f * h, 0),
        XMMatrixInverse(nullptr, XMMatrixTranslation(0, 1.0f * h, 0)), 1, 1
    });
    result.nodes.emplace_back(Node{
        "head", XMMatrixTranslation(0, 1.0f * h, 0), XMMatrixTranslation(0, 2.0f * h, 0),
        XMMatrixInverse(nullptr, XMMatrixTranslation(0, 2.0f * h, 0)), 2, 2
    });
    result.joints = {1, 2, 3};

    auto& buffer = result.buffersData.emplace_back();
    buffer.resize(sizeof(rawData));
    memcpy(buffer.data(), &rawData, buffer.size());

    auto& rawDataStored = *reinterpret_cast<Grid*>(buffer.data());
    result.animations.emplace_back(Animation{
        "Boop", {
            NodeAnimation{},
            NodeAnimation{},
            NodeAnimation{
                Timeline(rawDataStored.time, 11, rawDataStored.translation),
                Timeline(rawDataStored.time, 11, rawDataStored.rotation)
            },
            NodeAnimation{
                Timeline(rawDataStored.time, 11, rawDataStored.translation2),
                Timeline(rawDataStored.time, 11, rawDataStored.rotation2)
            },
        },
        5.5f
    });
}

void createSphere(Object& result, CommandList& copy, float radius, uint32_t tessellation) {
    if (tessellation < 3)
        throw std::out_of_range("tessellation parameter out of range");

    auto& device = Get<Device>();
    std::vector<XMFLOAT3> vertices;
    std::vector<XMFLOAT3> normals;
    std::vector<XMFLOAT4> tangents;
    std::vector<std::array<uint8_t, 4>> color;
    std::vector<std::array<uint16_t, 4>> joints;
    std::vector<std::array<uint8_t, 4>> weights;
    std::vector<XMFLOAT2> texCoord;

    size_t verticalSegments = tessellation;
    size_t horizontalSegments = tessellation * 2;

    // Create rings of vertices at progressively higher latitudes.
    for (size_t i = 0; i <= verticalSegments; i++) {
        float v = 1 - (float) i / (float) verticalSegments;

        float latitude = ((float) i * XM_PI / (float) verticalSegments) - XM_PIDIV2;
        float dy, dxz;

        XMScalarSinCos(&dy, &dxz, latitude);

        // Create a single ring of vertices at this latitude.
        for (size_t j = 0; j <= horizontalSegments; j++) {
            float u = (float) j / (float) horizontalSegments;

            float longitude = (float) j * XM_2PI / (float) horizontalSegments;
            float dx, dz;

            XMScalarSinCos(&dx, &dz, longitude);

            dx *= dxz;
            dz *= dxz;

            vertices.emplace_back(radius * dx, radius * dy, radius * dz);
            normals.emplace_back(dx, dy, dz);
            // may be the other way around
            tangents.emplace_back(dy, dz, dx, 0.0f);
            texCoord.emplace_back(1 - u, v);
            color.push_back({127, 127, 127, 255});
            // useless (no skeleton)
            joints.push_back({0, 0, 0, 0});
            weights.push_back({0, 0, 0, 0});
        }
    }

    result.buffers.emplace_back(device.uploadBuffer(copy, vertices.data(), sizeof(vertices[0]) * vertices.size()));
    result.buffers.emplace_back(device.uploadBuffer(copy, normals.data(), sizeof(normals[0]) * normals.size()));
    result.buffers.emplace_back(device.uploadBuffer(copy, tangents.data(), sizeof(tangents[0]) * tangents.size()));
    result.buffers.emplace_back(device.uploadBuffer(copy, color.data(), sizeof(color[0]) * color.size()));
    result.buffers.emplace_back(device.uploadBuffer(copy, joints.data(), sizeof(joints[0]) * joints.size()));
    result.buffers.emplace_back(device.uploadBuffer(copy, weights.data(), sizeof(weights[0]) * weights.size()));
    result.buffers.emplace_back(device.uploadBuffer(copy, texCoord.data(), sizeof(texCoord[0]) * texCoord.size()));

    auto& primitive = result.primitives.emplace_back();
    primitive.vertexViews = {
        {
            result.buffers[0]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(vertices[0]) * vertices.size()),
            sizeof(vertices[0])
        },
        {
            result.buffers[1]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(normals[0]) * normals.size()),
            sizeof(normals[0])
        },
        {
            result.buffers[2]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(tangents[0]) * tangents.size()),
            sizeof(tangents[0])
        },
        {
            result.buffers[3]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(color[0]) * color.size()),
            sizeof(color[0])
        },
        {
            result.buffers[4]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(joints[0]) * joints.size()),
            sizeof(joints[0])
        },
        {
            result.buffers[5]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(weights[0]) * weights.size()),
            sizeof(weights[0])
        },
        {
            result.buffers[6]->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(texCoord[0]) * texCoord.size()),
            sizeof(texCoord[0])
        },
    };
    primitive.vertexCount = vertices.size();

    // Fill the index buffer with triangles joining each pair of latitude rings.
    size_t stride = horizontalSegments + 1;
    std::vector<uint32_t> indices;
    for (size_t i = 0; i < verticalSegments; i++) {
        for (size_t j = 0; j <= horizontalSegments; j++) {
            size_t nextI = i + 1;
            size_t nextJ = (j + 1) % stride;

            indices.push_back(i * stride + nextJ);
            indices.push_back(nextI * stride + j);
            indices.push_back(i * stride + j);

            indices.push_back(nextI * stride + nextJ);
            indices.push_back(nextI * stride + j);
            indices.push_back(i * stride + nextJ);
        }
    }
    auto& indicesGPU = result.buffers.emplace_back(
        device.uploadBuffer(copy, indices.data(), sizeof(indices[0]) * indices.size()));
    primitive.indexViews = {
        indicesGPU->GetGPUVirtualAddress(), static_cast<UINT>(sizeof(indices[0]) * indices.size()), DXGI_FORMAT_R32_UINT
    };
    primitive.indexCount = indices.size();
    primitive.materialId = 0;
    result.materials.push_back({
        {1.0, 0.0, 0.0, 1.0}, {0.5, 0.34, 0.5, 1}, {0.45, 0.03, 0, 0.34}, {},
        LIGHTING_ENABLE_SSS
    });
}
