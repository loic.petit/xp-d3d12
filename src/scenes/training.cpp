#include "training.h"

#include "core/commandlist.h"
#include <imgui/imgui.h>

#include "grid.h"
#include "utils/logger.h"
#include "utils/markers.h"
#include "imgui/imgui_internal.h"

#define STB_IMAGE_IMPLEMENTATION

#include <stb_image.h>

#include <scenes/materials/base.h>

#include "materials/resource-manager.h"
#include "materials/nodes/texcoord-node.h"
#include "materials/nodes/uv-node.h"
#include "materials/nodes/renderer-node.h"
#include "utils/service.h"

using namespace DirectX;

constexpr DXGI_FORMAT SHADOWMAP_FORMAT = DXGI_FORMAT_D16_UNORM;

Training::Training():
        m_testRenderTarget(L"TestRender"),
        m_gbuffers(L"GBuffer"),
        m_lightingBuffer(L"LightingBuffer"),
        m_toneMappedBuffer(L"ToneMappedBuffer"),
        m_ssaoBuffer(L"SSAOBuffer"),
        m_ambientColor{1, 1, 1},
        m_ambientStrength{0.05} {
    m_customColors = {
            {
                    {
                            XMFLOAT4 {0.25526800751686096, 0.8849070072174072, 1.0, 1.0},
                            XMFLOAT4 {0.25526800751686096, 0.8849070072174072, 1.0, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0.7400000095367432, 0.3799999952316284, 0.07999999821186066, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0.8349999785423279, 0.5302029848098755, 0.48429998755455017, 1.0},
                            XMFLOAT4 {0.675000011920929, 0.35196900367736816, 0.07720600068569183, 1.0},
                            XMFLOAT4 {1.0, 0.536952018737793, 0.48500001430511475, 1.0},
                    },
                    {
                            XMFLOAT4 {0.020124999806284904, 0.05637900158762932, 0.17499999701976776, 1.0},
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                            XMFLOAT4 {1.0, 0.0, 0.03671000152826309, 1.0},
                    },
                    {
                            XMFLOAT4 {0.8349999785423279, 0.5302029848098755, 0.48429998755455017, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0.8349999785423279, 0.5302029848098755, 0.48429998755455017, 1.0},
                    },
                    {
                            XMFLOAT4 {0.020124999806284904, 0.05637900158762932, 0.17499999701976776, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {1.0, 0.0, 0.03671000152826309, 1.0},
                    },
                    {
                            XMFLOAT4 {0.020124999806284904, 0.05637900158762932, 0.17499999701976776, 1.0},
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                    },
            },
            {
                    {
                            XMFLOAT4 {0.25526800751686096, 0.8849070072174072, 1.0, 1.0},
                            XMFLOAT4 {0.25526800751686096, 0.8849070072174072, 1.0, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0.7400000095367432, 0.3799999952316284, 0.07999999821186066, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0, 0, 0, 0},
                    },
                    {
                            XMFLOAT4 {0.8349999785423279, 0.5302029848098755, 0.48429998755455017, 1.0},
                            XMFLOAT4 {0.675000011920929, 0.35196900367736816, 0.07720600068569183, 1.0},
                            XMFLOAT4 {1.0, 0.536952018737793, 0.48500001430511475, 1.0},
                    },
                    {
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                            XMFLOAT4 {0.8399999737739563, 0.0, 0.0, 1.0},
                            XMFLOAT4 {0.04542500153183937, 0.12725499272346497, 0.39500001072883606, 1.0},
                    },
                    {
                            XMFLOAT4 {0.8349999785423279, 0.5302029848098755, 0.48429998755455017, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0.8349999785423279, 0.5302029848098755, 0.48429998755455017, 1.0},
                    },
                    {
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                            XMFLOAT4 {0, 0, 0, 0},
                            XMFLOAT4 {0.028750000521540642, 0.08054099977016449, 0.25, 1.0},
                    },
                    {
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                            XMFLOAT4 {0.04542500153183937, 0.12725499272346497, 0.39500001072883606, 1.0},
                            XMFLOAT4 {1.5099999904632568, 1.772171974182129, 2.0, 1.0},
                    },
            },
    };
    m_lights.reserve(8);
    m_lights.emplace_back(L"ShadowMap#0")
            .setParams(
                    {5, 4, -10},
                    {0, 1.2, 0},
                    {1, 1, 1},
                    5,
                    30,
                    25);
    m_lights.emplace_back(L"ShadowMap#1")
            .setParams(
                    {-5, 3, -10},
                    {0, 1.2, 0},
                    {1, 1, 1},
                    3,
                    30,
                    25);
    m_lights.emplace_back(L"ShadowMap#2")
            .setParams(
                    {4, -1.6, 5.2},
                    {0, 1.2, 0},
                    {1, 1, 1},
                    2,
                    30,
                    25);
}

void Training::load(const RenderTarget::Desc& desc) {
    auto& resourceManager = Get<ResourceManager>();

    LOG_DEBUG("Before load image");

    m_gridTexture.id = resourceManager.loadImage(L"Grid Texture", ASSETS_DIR L"textures/grid.png", DXGI_FORMAT_R8G8B8A8_UNORM_SRGB);
    m_cammyTexture.id = resourceManager.loadImage(L"Cammy Albedo Texture", ASSETS_DIR L"textures/CT_Z10_17_COLOR_01.dds", DXGI_FORMAT_BC3_UNORM_SRGB, false);
    m_cammyMaskTexture.id = resourceManager.loadImage(L"Cammy Masking Texture", ASSETS_DIR L"textures/CT_Z10_17_MASK_01.dds", DXGI_FORMAT_BC1_UNORM, false);
    m_cammyNormalTexture.id = resourceManager.loadImage(L"Cammy Normal Texture", ASSETS_DIR L"textures/CT_Z10_17_NORMAL_01.dds", DXGI_FORMAT_BC5_UNORM, false);
    m_cammySRMATexture.id = resourceManager.loadImage(L"Cammy SRMA Texture", ASSETS_DIR L"textures/CT_Z10_17_SRMA_01.dds", DXGI_FORMAT_BC3_UNORM, false);
    m_cammySSSTexture.id = resourceManager.loadImage(L"Cammy SSS Texture", ASSETS_DIR L"textures/CT_Z10_17_SSS_01.dds", DXGI_FORMAT_BC3_UNORM_SRGB, false);

    LOG_DEBUG("After load image");
    m_pipeline.load();
    m_toneMapping.load();
    m_lighting.load();
    m_ambientLighting.load();
    m_fxaa.load();
    m_ssao.load();
    m_shadow.load(SHADOWMAP_FORMAT);

    m_toneMappedBuffer.init(desc.width, desc.height, 1);
    m_gbuffers.init(desc.width, desc.height, 1);
    m_lightingBuffer.init(desc.width, desc.height, 1);
    m_ssaoBuffer.init(desc.width, desc.height, 1);
    GBufferRenderPipeline::createBuffers(m_gbuffers);
    DeferredLighting::createBuffers(m_lightingBuffer);
    m_ssaoBuffer.createBuffer(L"Buffer", DXGI_FORMAT_R32_FLOAT, true);
    m_toneMappedBuffer.createBuffer(L"Buffer", desc.pixelFormats[0], true);

    LOG_DEBUG("After pipe creation");

    auto& queues = Get<EngineQueues>();
    auto& device = Get<Device>();
    auto& heap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    Queue& copyQueue = queues.copyQueue();
    auto copyList = copyQueue.createCommandList();
    {
        uint32_t white = 0xffffffff;
        m_whitePixel = device.uploadTexture(*copyList, CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_R8G8B8A8_UNORM, 1, 1), &white, sizeof(white));
        D3D12_CPU_DESCRIPTOR_HANDLE descriptor = heap.cpu(heap.allocate());
        device->CreateShaderResourceView(m_whitePixel.ptr(), nullptr, descriptor);
        m_whitePixel.srv(descriptor);
    }
    createGrid(m_grid, *copyList, 8, 10, 5);

    for (auto & light : m_lights) {
        light.load(*copyList);
    }

    //
    // loadTexture(heap, *copyList, *computeList, L"Grid Texture", ASSETS_DIR "textures/grid.png", m_gridTexture);
    // loadTexture(heap, *copyList, *computeList, L"Cammy Albedo Texture", ASSETS_DIR "textures/CT_Z10_17_COLOR_01.png", m_cammyTexture);
    // loadTexture(heap, *copyList, *computeList, L"Cammy Masking Texture", ASSETS_DIR "textures/CT_Z10_17_MASK_01.png", m_cammyMaskTexture);
    // loadTexture(heap, *copyList, *computeList, L"Cammy Normal Texture", ASSETS_DIR "textures/CT_Z10_17_NORMAL_01.png", m_cammyNormalTexture);
    // loadTexture(heap, *copyList, *computeList, L"Cammy SRMA Texture", ASSETS_DIR "textures/CT_Z10_17_SRMA_01.png", m_cammySRMATexture);
    // loadTexture(heap, *copyList, *computeList, L"Cammy SSS Texture", ASSETS_DIR "textures/CT_Z10_17_SSS_01.png", m_cammySSSTexture);

    load_gltf(*copyList, m_pipeline.layout(), ASSETS_DIR L"model/Z10_17.gltf", m_cammy);
    // force eye-brows to alpha clip
    m_cammy.materials[1].alphaClip = 0.4;

    createSphere(m_testSphere, *copyList, 1, 32);
    for (auto& anim: m_cammy.animations) {
        if (m_animationBuffers.size() < anim.nodes.size())
            m_animationBuffers.resize(anim.nodes.size());
    }

    copyQueue.execute(copyList);

    LOG_DEBUG("After copy");

    // init with the same level
    uint32_t testWidth = 300, testHeight = 300;
    m_testRenderTarget.init(testWidth, testHeight, desc.samples);
    m_testRenderTarget.createBuffer(L"Buffer", desc.pixelFormats[0], true);
    m_testRenderTarget.createDepthStencil(L"DepthStencil", DXGI_FORMAT_D32_FLOAT, false);

    m_camera.set_LookAt(XMVectorSet(0, 1.125, -2.1, 1), XMVectorSet(0, 0, 5, 1), XMVectorSet(0, 1, 0, 0));
    m_testCamera.set_LookAt(XMVectorSet(0, 0, -3, 1), XMVectorSet(0, 0, 0, 1), XMVectorSet(0, 1, 0, 0));
    m_testCamera.set_Projection(45, m_testRenderTarget.desc().width * 1.0 / m_testRenderTarget.desc().height, 0.1, 100);

    LOG_DEBUG("End load");

    {
        std::ifstream fis("material-test.json");
        if (fis.good()) {
            nlohmann::json json;
            fis >> json;
            m_testMaterial.load(json);
        } else {
            m_testMaterial.append<UvNode>().setResourceId(m_cammyTexture.id);
            m_testMaterial.append<UvNode>().setResourceId(m_cammyMaskTexture.id);
            m_testMaterial.append<UvNode>().setResourceId(m_cammyNormalTexture.id);
            m_testMaterial.append<UvNode>().setResourceId(m_cammySRMATexture.id);
            m_testMaterial.append<UvNode>().setResourceId(m_cammySSSTexture.id);
            m_testMaterial.append<TexCoordNode>();
            m_testMaterial.append<RendererNode>();
        }
    }
}

void Training::render(CommandList& list, RenderTarget& renderTarget) {
    PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(0), "render()");

    D3D12_GPU_VIRTUAL_ADDRESS animationNodeMatricesPtr = m_pipeline.prepareNodeMatrices(list, m_animationBuffers);

    XMMATRIX cammyModelMatrix = XMMatrixRotationY(XMConvertToRadians(m_angle));

    // Prepare Shadows
    computeShadows(list, cammyModelMatrix, animationNodeMatricesPtr);

    // Render the main scene
    renderMainScene(list, m_gbuffers, cammyModelMatrix, animationNodeMatricesPtr);

    if (m_enableAOPass) {
        PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "SSAO");
        list.state(m_ssaoBuffer.getBuffer(0), D3D12_RESOURCE_STATE_RENDER_TARGET)
                .stateEnd(m_gbuffers.getBuffer(3))
                .stateEnd(m_gbuffers.getDepthStencil());
        m_ssao.use(list);
        m_ssao.setParameters(list, m_camera, m_aoKernelSize, m_aoRadius);
        m_ssao.execute(list, m_gbuffers.getDepthStencil(), m_gbuffers.getBuffer(3), m_ssaoBuffer);
        list.stateBegin(m_ssaoBuffer.getBuffer(0), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }

    // Render lights
    renderLights(list, m_gbuffers, m_lightingBuffer);

    {
        PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "toneMapping");
        m_toneMapping.use(list);
        m_toneMapping.execute(list, m_exposure, m_lightingBuffer.getBuffer(0), m_enableFxaa ? m_toneMappedBuffer : renderTarget);
    }

    if (m_enableFxaa) {
        PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "fxaa");
        m_fxaa.use(list);
        m_fxaa.execute(list, m_toneMappedBuffer.getBuffer(0), renderTarget);
    }

    // Render the test sphere
    {
//    PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "testScene");
//    renderTestSphere(list);
    }
}

void Training::renderTestSphere(CommandList& list) {
    /*
    m_pipeline.setPoV(m_testCamera.get_Translation(), m_testCamera.get_ViewMatrix(), m_testCamera.get_ProjectionMatrix());
    m_testRenderTarget.clear(list);
    m_testRenderTarget.use(list);

    m_pipeline.submitLights(list, {
            asLight(m_lightPositionTest, m_lights.front().color, XMVectorSet(0, 0, 0, 1), m_lights.front().strength)
    });
    m_pipeline.submitModelMatrices(list, XMMatrixIdentity());
    renderMeshPipeline(list, m_testSphere);

    if (m_testRenderTarget.desc().samples > 1) {
        // need to resolve the MSAA buffer for the gui
        m_testRenderTarget.resolveInto(list, m_testRenderTargetResolved);
    }
     */
}

D3D12_CPU_DESCRIPTOR_HANDLE Training::getResource(ResourceCache& cache, uint32_t& featureMask, uint32_t feature) const {
    auto& resourceManager = Get<ResourceManager>();
    if (cache.state != ResourceManager::State::READY) {
        cache.state = resourceManager.state(cache.id);
        if (cache.state != ResourceManager::State::READY) {
            // disabling feature
            featureMask &= ~feature;
            return m_whitePixel.srv();
        }
        cache.resource = &resourceManager.gpu(cache.id);
    }
    return cache.resource->srv();
}

void Training::renderMainScene(CommandList& list, RenderTarget& renderTarget, const XMMATRIX& cammyModelMatrix, const D3D12_GPU_VIRTUAL_ADDRESS& animationNodeMatricesPtr) {
    PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "gBuffers");
    renderTarget.transitionWrite(list);
    m_lightingBuffer.transitionWrite(list);
    // common attributes
    m_pipeline.use(list);
    if (!m_testMaterialPipeline.isLoaded()) {
        m_pipeline.submitNodeMatrices(list, animationNodeMatricesPtr, m_animationBuffers.size());
    }

    renderTarget.clear(list);
    m_lightingBuffer.clear(list);
    const Resource* mainSceneRT[] = {
        &renderTarget.getBuffer(0),
        &renderTarget.getBuffer(1),
        &renderTarget.getBuffer(2),
        &renderTarget.getBuffer(3),
        &m_lightingBuffer.getBuffer(0),
    };
    RenderTarget::setRenderTarget(list, renderTarget.desc().width, renderTarget.desc().height, _countof(mainSceneRT), mainSceneRT, &m_gbuffers.getDepthStencil());
    // renderTarget.use(list);
    m_camera.set_Projection(45, static_cast<float>(renderTarget.desc().width * 1.0 / renderTarget.desc().height), 0.1, 100);
    m_pipeline.setPoV(m_camera.get_Translation(), m_camera.get_ViewMatrix(), m_camera.get_ProjectionMatrix());

    // grid
    uint32_t featureMask = 0xffffffff;
    D3D12_CPU_DESCRIPTOR_HANDLE gridMaterialTextures[5]{};
    gridMaterialTextures[0] = getResource(m_gridTexture, featureMask, LIGHTING_ALBEDO_MAPPING);
    gridMaterialTextures[1] = gridMaterialTextures[0];
    gridMaterialTextures[2] = gridMaterialTextures[0];
    gridMaterialTextures[3] = gridMaterialTextures[0];
    gridMaterialTextures[4] = gridMaterialTextures[0];
    m_pipeline.submitModelMatrices(list, XMMatrixTranslation(0, 0, 0));
    m_pipeline.submitTextures(list, gridMaterialTextures, 5);
    renderMeshPipeline(list, m_grid, featureMask);

    // cammy
    featureMask = 0xffffffff;
    D3D12_CPU_DESCRIPTOR_HANDLE cammyMaterialTextures[5]{};
    cammyMaterialTextures[0] = getResource(m_cammyTexture, featureMask, LIGHTING_ALBEDO_MAPPING);
    cammyMaterialTextures[1] = getResource(m_cammyMaskTexture, featureMask, LIGHTING_MASK_MAPPING);
    cammyMaterialTextures[2] = getResource(m_cammyNormalTexture, featureMask, LIGHTING_NORMAL_MAPPING);
    cammyMaterialTextures[3] = getResource(m_cammySRMATexture, featureMask, LIGHTING_SRMA_MAPPING);
    cammyMaterialTextures[4] = getResource(m_cammySSSTexture, featureMask, LIGHTING_SSS_MAPPING);
    if (!m_testMaterialPipeline.isLoaded()) {
        m_pipeline.submitModelMatrices(list, cammyModelMatrix);
        m_pipeline.submitTextures(list, cammyMaterialTextures, 5);
        renderMeshPipeline(list, m_cammy, featureMask);
    } else {
        m_testMaterialPipeline.use(list);
        m_testMaterialPipeline.setPoV(m_camera.get_Translation(), m_camera.get_ViewMatrix(), m_camera.get_ProjectionMatrix());
        m_testMaterialPipeline.submitModelMatrices(list, cammyModelMatrix);
        m_testMaterialPipeline.submitNodeMatrices(list, animationNodeMatricesPtr, m_animationBuffers.size());
        m_testMaterial.stage(list, m_testMaterialPipeline);
        m_testMaterialPipeline.commitDescriptors(list);
        for (auto it = m_cammy.primitives.rbegin(); it != m_cammy.primitives.rend(); ++it) {
            auto& primitive = *it;
            if (primitive.materialId == 3 || primitive.materialId == 2) {
                // we don't support material 3 yet
                continue;
            }
            primitive.draw(list.ptr());
        }
    }

    for (auto& buffer: renderTarget.getBuffers()) {
        list.stateBegin(buffer, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
    list.stateBegin(renderTarget.getDepthStencil(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE | D3D12_RESOURCE_STATE_DEPTH_READ);
}

void Training::renderLights(CommandList& list, RenderTarget& gbuffers, RenderTarget& renderTarget) {
    PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "lighting");
    list.stateEnd(m_gbuffers.getDepthStencil());
    for (auto & buffer : m_gbuffers.getBuffers())
        list.stateEnd(buffer);
    for (auto & light : m_lights) {
        list.stateEnd(light.m_shadow.getDepthStencil());
    }
//    renderTarget.clear(list);
    RenderTarget::setRenderTarget(list, renderTarget.desc().width, renderTarget.desc().height, 1, &renderTarget.getBuffer(0), &gbuffers.getDepthStencil());
    // use depth stencil on top
    m_lighting.use(list);
    m_lighting.setPoV(m_camera.get_Translation(), m_camera.get_InverseViewMatrix(), m_camera.get_InverseProjectionMatrix(), m_camera.get_ViewMatrix(), m_camera.get_ProjectionMatrix());
    m_lighting.setFeatures(list, m_flagMask);
    m_lighting.submitGBuffers(list, gbuffers);
    for (auto& light: m_lights) {
        m_lighting.execute(list, light);
    }

    if (m_enableAOPass) {
        list.stateEnd(m_ssaoBuffer.getBuffer(0)).commitTransitions();
    }
    m_ambientLighting.use(list);
    m_ambientLighting.submitGBuffers(list, gbuffers, m_ssaoBuffer.getBuffer(0));
    m_ambientLighting.execute(list, XMVectorScale(XMLoadFloat3(&m_ambientColor), m_ambientStrength), m_enableAOPass, m_enableAOMapping, m_aoPower);
}

void Training::computeShadows(CommandList& list, const XMMATRIX& cammyModelMatrix, const D3D12_GPU_VIRTUAL_ADDRESS& animationNodeMatricesPtr) {
    PIXScopedEvent(list.ptr(), PIX_COLOR_INDEX(1), "computeShadows");
    for (auto &light : m_lights) {
        list.state(light.m_shadow.getDepthStencil(), D3D12_RESOURCE_STATE_DEPTH_WRITE);
    }
    m_shadow.use(list);
    m_shadow.submitNodeMatrices(list, animationNodeMatricesPtr, m_animationBuffers.size());
    Camera camera;
    // used on shadows & main
    for (auto& light: m_lights) {
        light.m_shadow.clear(list);
        light.m_shadow.use(list);

        m_shadow.setPoV(light.m_mvpMatrix);

        // render only cammy
        m_shadow.submitMatrices(list, cammyModelMatrix);
        renderMeshShadow(list, m_cammy);
    }
    for (auto & light : m_lights) {
        list.stateBegin(light.m_shadow.getDepthStencil(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    }
}

void Training::computeAnimationBuffers() {
    // update animations
    // compute current node transform
    if (m_cammy.animations.empty()) return;
    const std::vector<NodeAnimation>& animNodes = m_cammy.animations[m_currentAnimation].nodes;
    for (int j = 0; j < m_cammy.joints.size(); j++) {
        auto& buf = m_animationBuffers[j];
        auto nodeId = m_cammy.joints[j];
        if (animNodes[nodeId].translation.empty()) {
            buf = m_cammy.nodes[nodeId].localBindMatrix;
        } else {
            auto& anim = animNodes[nodeId];
            auto translation = anim.translation.lerp(m_time);
            auto rotation = anim.rotation.slerp(m_time);
            // not TRS because DX is stupid
            buf = XMMatrixRotationQuaternion(rotation) * XMMatrixTranslationFromVector(translation);
        }
        if (m_cammy.nodes[nodeId].parent) {
            buf = buf * m_animationBuffers[m_cammy.nodes[m_cammy.nodes[nodeId].parent].joint];
        }
    }
    // compute inverse + node
    auto gltfTransform = XMMatrixScaling(1, 1, -1);
    for (int j = 0; j < m_cammy.joints.size(); j++) {
        auto& buf = m_animationBuffers[j];
        auto nodeId = m_cammy.joints[j];
        buf = m_cammy.nodes[nodeId].inverseBindMatrix * buf * gltfTransform;
    }
}

void Training::renderMeshPipeline(CommandList& list, Object& entity, uint32_t featureFlag) {
    m_pipeline.commitDescriptors(list);
    for (auto it = entity.primitives.rbegin(); it != entity.primitives.rend(); ++it) {
        auto& primitive = *it;
        if (primitive.materialId == 3 || primitive.materialId == 2) {
            // we don't support material 3 yet
            continue;
        }

        LegacyMaterial material = entity.materials[it->materialId];
        material.features &= m_flagMask & featureFlag;
        if (material.features & LIGHTING_MASK_MAPPING) {
            memcpy(material.customColor, m_customColors[m_currentColor][primitive.materialId].data(), sizeof(material.customColor));
        }
        m_pipeline.submitMaterial(list, material, m_fadeSelect);
        primitive.draw(list.ptr());
    }
}

void Training::renderMeshShadow(CommandList& list, Object& entity) {
    m_shadow.commitDescriptors(list);
    for (auto it = entity.primitives.rbegin(); it != entity.primitives.rend(); ++it) {
        auto& primitive = *it;
        if (primitive.materialId == 3 || primitive.materialId == 2) {
            // we don't support material 3 yet
            continue;
        }
        primitive.draw(list.ptr());
    }
}

void Training::move(int32_t pitch, int32_t yaw) {
    constexpr float mouseSpeed = 0.1f;

    m_pitch -= pitch * mouseSpeed;
    m_pitch = std::clamp(m_pitch, -90.0f, 90.0f);
    m_yaw -= yaw * mouseSpeed;

    XMVECTOR cameraRotation =
            XMQuaternionRotationRollPitchYaw(XMConvertToRadians(m_pitch), XMConvertToRadians(m_yaw), 0.0f);
    m_camera.set_Rotation(cameraRotation);
}

void Training::translate(int w, int a, int s, int d) {
    XMVECTOR cameraPan = XMVectorScale(XMVectorSet(d - a, 0, w - s, 1.0f), 0.02f);
    m_camera.Translate(cameraPan);
}

void Training::gui(bool mainControls, bool materialEditorView, bool gbufferView, bool fadeSelectorView) {
    auto& gui = Get<GUI>();
    if (mainControls) {
        ImGui::SetNextWindowSize(ImVec2(0, 0));
        ImGui::SetNextWindowPos(ImGui::GetMainViewport()->WorkPos);
        ImGui::Begin("Training Stage");
        if (ImGui::Button("Load Material")) {
            m_testMaterialPipeline.load(m_testMaterial);
        }

        ImGui::Combo("Skin Color", &m_currentColor, [](void* training, int item, const char** out) -> bool {
            static char buffer[32];
            sprintf(buffer, "Color #%d", item);
            *out = buffer;
            return true;
        }, this, 2);
        ImGui::DragFloat("Orientation", &m_angle, 1, -180, 180);

        if (ImGui::CollapsingHeader("Lighting")) {
            if (ImGui::Button("Add light")) {
                auto & light = m_lights.emplace_back(std::wstring(L"ShadowMap#") + std::to_wstring(m_lights.size()));
                light.load(gui.stagingList());
                light.setParams(
                        {0, 0, 0},
                        {0, 1.2, 0},
                        {1, 1, 1},
                        1,
                        30,
                        25);
            }
            int i = 0;
            auto to_remove = m_lights.end();
            for (auto it = m_lights.begin(); it != m_lights.end(); ++it) {
                auto& light = *it;
                std::string node = "Light #" + std::to_string(i++);
                if (ImGui::TreeNode(node.c_str())) {
                    bool updateLight = ImGui::ColorEdit3("Light color", (float*) &light.m_params.color, ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_Float);
                    updateLight = ImGui::DragFloat("Light strength", (float*) &light.m_params.strength, 0.1, 0) || updateLight;
                    updateLight = ImGui::DragFloat3("Light position", (float*) &light.m_params.position, 0.05, -10, 10) || updateLight;
                    if (updateLight) {
                        light.setParams(light.m_params.position, {0, 1.2, 0}, light.m_params.color, light.m_params.strength, 30, 25);
                    }
                    if (ImGui::Button("Remove")) {
                        to_remove = it;
                    }
                    ImGui::TreePop();
                    ImGui::Separator();
                }
            }
            if (to_remove != m_lights.end()) {
                m_lights.erase(to_remove, to_remove+1);
            }
            if (ImGui::TreeNode("Ambient Light")) {
                ImGui::ColorEdit3("Light color", (float*) &m_ambientColor, ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_Float);
                ImGui::DragFloat("Light strength", (float*) &m_ambientStrength, 0.1, 0);
                ImGui::Checkbox("AO Pass", &m_enableAOPass);
                ImGui::SameLine();
                ImGui::Checkbox("AO Mapping", &m_enableAOMapping);
                ImGui::DragFloat("AO radius", (float*) &m_aoRadius, 0.01, 0.01, 1.0);
                ImGui::DragInt("AO kernel size", &m_aoKernelSize, 1, 1, 128);
                ImGui::DragFloat("AO power", &m_aoPower, 0.1, 0.1, 64);
                ImGui::TreePop();
            }
            ImGui::DragFloat("Exposure", (float*) &m_exposure, 0.01, 0.0, 5.0);
            ImGui::Separator();
        }
        ImGui::CheckboxFlagsT("Albedo mapping", &m_flagMask, (uint32_t) LIGHTING_ALBEDO_MAPPING);
        ImGui::CheckboxFlagsT("Normal mapping", &m_flagMask, (uint32_t) LIGHTING_NORMAL_MAPPING);
        ImGui::CheckboxFlagsT("SRMA mapping", &m_flagMask, (uint32_t) LIGHTING_SRMA_MAPPING);
        ImGui::CheckboxFlagsT("SSS mapping", &m_flagMask, (uint32_t) LIGHTING_SSS_MAPPING);
        ImGui::CheckboxFlagsT("Mask mapping", &m_flagMask, (uint32_t) LIGHTING_MASK_MAPPING);
        ImGui::CheckboxFlagsT("Saturate diffuse", &m_flagMask, (uint32_t) LIGHTING_SATURATE_DIFFUSE);
        ImGui::CheckboxFlagsT("Enable SSS", &m_flagMask, (uint32_t) LIGHTING_ENABLE_SSS);
        ImGui::CheckboxFlagsT("Shadow mapping", &m_flagMask, (uint32_t) LIGHTING_HAS_SHADOWS);
        ImGui::Checkbox("FXAA", &m_enableFxaa);

        if (!m_cammy.animations.empty()) {
            ImGui::Combo("Animation", &m_currentAnimation, [](void* animations, int item, const char** out) -> bool {
                auto& anim = *static_cast<std::vector<Animation>*>(animations);
                *out = anim[item].name.c_str();
                return true;
            }, &m_cammy.animations, (int) m_cammy.animations.size());
            ImGui::SliderFloat("Timeline", &m_time, 0, m_cammy.animations[m_currentAnimation].time);
            static bool s_playAnimation = false;
            if (s_playAnimation) {
                m_time += 1.0f / 170 / 2;
                if (m_time > m_cammy.animations[m_currentAnimation].time)
                    m_time = 0;
            }
            if (ImGui::Button(s_playAnimation ? "Pause" : "Play")) {
                s_playAnimation = !s_playAnimation;
            }
        }
        ImGui::End();
    }

    /*
    // material tester
    ImGui::SetNextWindowSize(ImVec2(0, 0));
    ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x, 0.0f), 0, ImVec2(1.0f, 0.0f));
    ImGui::Begin("Material Tester");
    ImGui::DragFloat3("Light Pos", &m_lightPositionTest.x, 0.05, -10, 10);
    ImGui::ColorEdit3("Albedo", (float*) &m_testSphere.materials[0].albedo, ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_Float);
    ImGui::DragFloat4("SRMA", &m_testSphere.materials[0].srma.x, 0.01, 0, 1.0);
    ImGui::DragFloat4("SSS", &m_testSphere.materials[0].sss.x, 0.01, 0, 1.0);

    D3D12_GPU_DESCRIPTOR_HANDLE testImage = gui.stageTexture((m_testRenderTarget.desc().samples > 1 ? m_testRenderTargetResolved : m_testRenderTarget).getBuffer(0));
    auto& desc = m_testRenderTarget.desc();
    ImGui::Image((ImTextureID) testImage.ptr, ImVec2(desc.width, desc.height));
    ImGui::End();
    */

    if (materialEditorView) {
        ImGui::SetNextWindowSize(ImGui::GetMainViewport()->WorkSize);
        ImGui::SetNextWindowPos(ImGui::GetMainViewport()->WorkPos);
        ImGui::Begin("Material Editor", nullptr, ImGuiWindowFlags_NoDecoration);
        ImGui::PushFont(Get<GUI>().getPrettyFont());
        if (m_testMaterial.gui()) {
            // updated
            std::ofstream fos("material-test.json", std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);
            fos << m_testMaterial.save();
        }
        ImGui::PopFont();
        ImGui::End();
    }
    /*
    ImNodes::BeginNode(1);
    ImNodes::BeginNodeTitleBar();
    ImGui::TextUnformatted("material");
    ImNodes::EndNodeTitleBar();
    ImNodes::BeginInputAttribute(2, ImNodesPinShape_Circle);
    ImGui::Text("albedo");
    ImNodes::EndInputAttribute();
    ImGui::Dummy(ImVec2(80.0f, 45.0f));
    ImNodes::EndNode();


    ImNodes::BeginNode(3);
    ImNodes::BeginNodeTitleBar();
    ImGui::TextUnformatted("source");
    ImNodes::EndNodeTitleBar();
    ImNodes::BeginOutputAttribute(4, ImNodesPinShape_Circle);
    ImGui::Text("albedo");
    ImNodes::EndOutputAttribute();

    ImGui::Dummy(ImVec2(80.0f, 45.0f));
    ImNodes::EndNode();
    ImNodes::Link(5, 2, 4);
    */

    if (gbufferView) {
        ImGui::SetNextWindowSize(ImVec2(0, 0));
        ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x, ImGui::GetMainViewport()->WorkPos.y), 0, ImVec2(1.0f, 0.0f));
        ImGui::Begin("GBuffer");
        ImGui::Combo("Selector", &m_bufferSelector, [](void* training, int item, const char** out) -> bool {
            static const char* label[] = {"Albedo", "SRMA", "SSS", "Normal", "Lighting", "AO"};
            if (item >= _countof(label)) return false;
            *out = label[item];
            return true;
        }, this, 6);
        Resource* r;
        if (m_bufferSelector < 4)
            r = &m_gbuffers.getBuffer(m_bufferSelector);
        else if (m_bufferSelector == 4)
            r = &m_lightingBuffer.getBuffer(0);
        else
            r = &m_ssaoBuffer.getBuffer(0);
        auto width = (*r)->GetDesc().Width;
        auto height = (*r)->GetDesc().Height;
        gui.image(*r, ImVec2(width / 2, height / 2), ImVec2(0.25, 0.25), ImVec2(0.75, 0.75));
        ImGui::End();
    }

    struct FadePreset {
        FadeSelect selector;
        const char* label;
    };
    FadePreset presets[] = {
            {
                    {
                            {},
                            {},
                            {0, 1,   1, 0},
                    },
                    "None",
            },
            {
                    {
                            {1,        0,        1,         0.5},
                            {0,       1,         0,        1},
                            {2, 1,   1, 1},},
                    "Test",
            },
            {
                    {
                            {},
                            {1,       0.0448045, 0,        14.1738},
                            {2, 1,   1, 1},},
                    "Ex Move",
            },
            {
                    {
                            {0.629325, 0.455,    3.5,       0.125},
                            {-0.05,   -0.1,      0.02,     0.6},
                            {2, 0.5, 1, 1},},
                    "Fang Poison",
            },
            {
                    {
                            {-0.2,     -0.6,     0.2,       0.3},
                            {2.00001, 0.600003,  30,       0.1},
                            {0, 3,   1, 1}
                    },
                    "Psycho Power",
            },
            {
                    {
                            {0.889896, 0.483515, -0.21787,  0.933334},
                            {13.242,  3.90477,   0.105216, 1.24333},
                            {2, 2.2, 1, 1},
                    },
                    "Blanka Electricity",
            },
            {
                    {
                            {0,        0.211821, 1,         0.85},
                            {0,       0.211821,  1,        0.85},
                            {2, 2.2, 1, 1},
                    },
                    "P1 Selector",
            },
            {
                    {
                            {0.64,     0.048,    0.048,     0.85},
                            {0.64,    0.048,     0.048,    0.85},
                            {2, 2.2, 1, 1},
                    },
                    "P2 Selector",
            },
            {
                    {
                            {0.682632, 0.451396, -0.870413, 0.983333},
                            {9.67104, 2.05003,   0.245785, 1.47222},
                            {2, 2.2, 1, 1},
                    },
                    "Ryu Electricity",
            },
            {
                    {
                            {-0.6,     -0.6,     -0.3,      0.26},
                            {60,      8,         1,        0.5},
                            {0, 2.2, 1, 1},
                    },
                    "Ken Fire",
            }
    };

    if (fadeSelectorView) {
        ImGui::SetNextWindowSize(ImVec2(300, 300));
        ImGui::SetNextWindowPos(ImVec2(0, ImGui::GetMainViewport()->Size.y-300));
        ImGui::Begin("Fade Tester", nullptr, ImGuiWindowFlags_NoDecoration);
//        ImGui::PushFont(gui.getLargeFont());
        static int32_t currentPreset = 0;
        ImGui::PushItemWidth(ImGui::GetWindowContentRegionWidth());
        if (ImGui::Combo("##Presets", &currentPreset, [](void* presets, int item, const char** out) -> bool {
            *out = ((struct FadePreset*) presets)[item].label;
            return true;
        }, presets, IM_ARRAYSIZE(presets))) {
            m_fadeSelect = presets[currentPreset].selector;
        }
        ImGui::SeparatorText("Inner");
        ImGui::ColorEdit4("##inner", (float*) &m_fadeSelect.innerColor, ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_Float | ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_AlphaBar);
        ImGui::SeparatorText("Outer");
        ImGui::ColorEdit4("##outer", (float*) &m_fadeSelect.outerColor, ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_Float | ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_AlphaBar);
        ImGui::SeparatorText("Params");
        ImGui::PopItemWidth();
        ImGui::DragFloat("Type", &m_fadeSelect.params[0], 1.0, 0, 2, "%.0f");
        ImGui::DragFloat("Exponent", &m_fadeSelect.params[1], 0.1, 0, 5);
        ImGui::DragFloat("Emission", &m_fadeSelect.params[2], 0.1, 0, 2);
        ImGui::DragFloat("Specular-", &m_fadeSelect.params[3], 0.1, 0, 1);
        ImGui::End();
    }
}

void Training::update() {
    PIXScopedEvent(PIX_COLOR_INDEX(0), "Scene::update()");
    const auto& inputs = Get<EngineInput>();
    auto w = inputs.wasd[0] & INPUT_PRESSED;
    auto a = inputs.wasd[1] & INPUT_PRESSED;
    auto s = inputs.wasd[2] & INPUT_PRESSED;
    auto d = inputs.wasd[3] & INPUT_PRESSED;
    if (w || a || s || d) {
        translate(w, a, s, d);
    }
    if (inputs.mouseButton & INPUT_PRESSED) {
        move(inputs.mouseDeltaY, inputs.mouseDeltaX);
    }
    computeAnimationBuffers();
}

void Training::resize(uint32_t width, uint32_t height) {
    m_gbuffers.resize(width, height);
    m_lightingBuffer.resize(width, height);
    m_toneMappedBuffer.resize(width, height);
}
