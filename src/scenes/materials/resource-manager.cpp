#include "resource-manager.h"

#include <future>
#include <utility>
#include <thread-name.h>
#include <core/commandlist.h>
#include <scenes/context.h>
#include <utils/logger.h>
#include <utils/markers.h>
#include "utils/utils.h"

#include "stb_image.h"
#include "DDSTextureLoader12.h"
#include "utils/service.h"

class ResourceManager::LoadingJob {
public:
    LoadingJob(State state, std::wstring name, std::wstring filename, bool computeLod, DXGI_FORMAT format): //
        m_state(state), m_name(std::move(name)), m_filename(std::move(filename)), m_computeLod(computeLod),
        m_format(format) {
    }

    virtual ~LoadingJob() = default;

    virtual bool load() = 0;

    virtual void gpu(Mips& mips, DescriptorHeap& heap, CommandList& copyList, CommandList& computeList) = 0;

    virtual void clean() = 0;

    State m_state;
    std::wstring m_name;
    const std::wstring m_filename;
    Resource m_resource;
    const bool m_computeLod;
    DXGI_FORMAT m_format;
    uint64_t m_copySignal = 0;
    uint64_t m_computeSignal = 0;
};

class ResourceManager::DDSLoadingJob : public LoadingJob {
public:
    DDSLoadingJob(State state, std::wstring name, std::wstring filename, bool computeLod, DXGI_FORMAT format)
        : LoadingJob(state, std::move(name), std::move(filename), computeLod, format) {
    }

    bool load() override {
        ComPtr<ID3D12Resource> resource;
        auto loadFlags = DirectX::DDS_LOADER_DEFAULT;
        if (Resource::isSRGB(m_format))
            loadFlags |= DirectX::DDS_LOADER_FORCE_SRGB;
        if (m_computeLod)
            loadFlags |= DirectX::DDS_LOADER_MIP_RESERVE;
        HRESULT hr = DirectX::LoadDDSTextureFromFileEx(Get<Device>().ptr(), m_filename.c_str(), 0, m_computeLod ? D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS : D3D12_RESOURCE_FLAG_NONE, loadFlags, &resource, m_data, m_subresources);
        if (FAILED(hr)) return false;
        m_resource = Resource(resource);
        m_format = resource->GetDesc().Format;
        return true;
    }

    void gpu(Mips& mips, DescriptorHeap& heap, CommandList& copyList, CommandList& computeList) override {
        auto& device = Get<Device>();
        UINT64 textureUploadBufferSize;
        const size_t numSubresources = m_subresources.size();
        const D3D12_RESOURCE_DESC desc = m_resource->GetDesc();
        std::vector<D3D12_PLACED_SUBRESOURCE_FOOTPRINT> footprints;
        std::vector<UINT> rows;
        std::vector<UINT64> rowSize;
        footprints.resize(numSubresources);
        rows.resize(numSubresources);
        rowSize.resize(numSubresources);
        device->GetCopyableFootprints(&desc, 0, desc.MipLevels, 0, footprints.data(), rows.data(), rowSize.data(), &textureUploadBufferSize);
        Resource upload(CD3DX12_RESOURCE_DESC::Buffer(textureUploadBufferSize), D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, D3D12_HEAP_TYPE_UPLOAD);
        ThrowIfFailed(upload->SetName(L"Upload texture buffer"));
        UpdateSubresources(copyList.ptr(), m_resource.ptr(), upload.ptr(), 0, numSubresources, textureUploadBufferSize,
                           footprints.data(), rows.data(), rowSize.data(), m_subresources.data());

        copyList.track(upload.d3d12());

        ThrowIfFailed(m_resource->SetName(m_name.c_str()));

        D3D12_CPU_DESCRIPTOR_HANDLE descriptor = heap.cpu(heap.allocate());
        device->CreateShaderResourceView(m_resource.ptr(), nullptr, descriptor);
        m_resource.srv(descriptor);

        if (m_computeLod) {
            mips.execute(computeList, m_resource);
        }
    }

    void clean() override {
        m_data.reset();
        m_subresources.clear();
    }

    std::unique_ptr<uint8_t[]> m_data;
    std::vector<D3D12_SUBRESOURCE_DATA> m_subresources;
};

class ResourceManager::StbLoadingJob : public LoadingJob {
public:
    StbLoadingJob(State state, std::wstring name, std::wstring filename,
                  bool computeLod, DXGI_FORMAT format)
        : LoadingJob(state, std::move(name), std::move(filename), computeLod, format) {
    }

    ~StbLoadingJob() override {
        if (m_data) {
            stbi_image_free(m_data);
            m_data = nullptr;
        }
    }

    bool load() override {
        FILE* f{nullptr};
        const auto err = _wfopen_s(&f, m_filename.c_str(), L"rb");
        if (err) {
            LOG_ERROR("Failed to load image %ls error: %d", m_filename.c_str(), err);
            return false;
        }
        m_data = stbi_load_from_file(f, &width, &height, &channels, Resource::getComponents(m_format));
        fclose(f);
        return m_data != nullptr;
    }

    void gpu(Mips& mips, DescriptorHeap& heap, CommandList& copyList, CommandList& computeList) override {
        CD3DX12_RESOURCE_DESC textureDesc = CD3DX12_RESOURCE_DESC::Tex2D(
            m_format, static_cast<UINT64>(width),
            static_cast<UINT>(height), 1, m_computeLod ? 0 : 1, 1, 0,
            m_computeLod ? D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS : D3D12_RESOURCE_FLAG_NONE);

        auto& device = Get<Device>();
        m_resource = device.uploadTexture(copyList, textureDesc, m_data,
                                          width * height * Resource::getComponents(m_format));
        ThrowIfFailed(m_resource->SetName(m_name.c_str()));

        D3D12_CPU_DESCRIPTOR_HANDLE descriptor = heap.cpu(heap.allocate());
        device->CreateShaderResourceView(m_resource.ptr(), nullptr, descriptor);
        m_resource.srv(descriptor);

        if (m_computeLod) {
            mips.execute(computeList, m_resource);
        }
    }

    void clean() override {
        stbi_image_free(m_data);
        m_data = nullptr;
    }

    int width = 0, height = 0, channels = 0;
    void* m_data = nullptr;
};

ResourceManager::ResourceManager(): //
    m_cpuThread(RESOURCE_MANAGER_CPU, this, &ResourceManager::cpuTick),
    m_gpuThread(RESOURCE_MANAGER_GPU, this, &ResourceManager::gpuTick),
    m_copyQueue(D3D12_COMMAND_LIST_TYPE_COPY),
    m_computeQueue(D3D12_COMMAND_LIST_TYPE_COMPUTE) {
}

ResourceManager::~ResourceManager() {
    m_cpuThread.stop();
    m_gpuThread.stop();
    m_loadingQueueCv.notify_all();
    m_gpuQueueCv.notify_all();
}

static bool hasDDSExtension(const std::wstring& filename) {
    if (filename.length() < 4) {
        return false; // Filename is too short to have a ".dds" extension
    }

    std::wstring extension = filename.substr(filename.length() - 4);

    // Convert extension to lowercase for case-insensitive comparison
    std::transform(extension.begin(), extension.end(), extension.begin(),
                   [](wchar_t c) { return std::tolower(c, std::locale::classic()); });

    return extension == L".dds";
}

ResourceManager::Id ResourceManager::loadImage(const std::wstring& name, const std::wstring& filename,
                                               DXGI_FORMAT format,
                                               bool computeLod) {
    static uint32_t s_id = 1;
    std::unique_lock lock(m_mutex);
    for (auto& entry: m_entries) {
        if (entry.second->m_filename == filename) {
            return entry.first;
        }
    }
    const uint32_t entityId = s_id++;
    std::unique_ptr<LoadingJob> job;
    if (hasDDSExtension(filename)) {
        job = std::make_unique<DDSLoadingJob>(QUEUED, name, filename, computeLod, format);
    } else {
        job = std::make_unique<StbLoadingJob>(QUEUED, name, filename, computeLod, format);
    }
    m_entries[entityId] = std::move(job);
    m_loadingQueue.push(entityId);
    m_loadingQueueCv.notify_one();
    return entityId;
}

ResourceManager::State ResourceManager::state(Id id) {
    std::unique_lock lock(m_mutex);
    return m_entries[id]->m_state;
}

Resource& ResourceManager::gpu(Id id) {
    std::unique_lock lock(m_mutex);
    return m_entries[id]->m_resource;
}

const std::wstring& ResourceManager::name(Id id) {
    std::unique_lock lock(m_mutex);
    return m_entries[id]->m_name;
}

nlohmann::json ResourceManager::save(Id id) {
    std::unique_lock lock(m_mutex);
    auto& job = *m_entries[id];
    return nlohmann::json({
        {"name", wstringToUtf8(job.m_name)},
        {"filename", wstringToUtf8(job.m_filename)},
        {"format", job.m_format},
        {"computeLod", job.m_computeLod},
    });
}

ResourceManager::Id ResourceManager::load(const nlohmann::json& json) {
    return loadImage(utf8ToWstring(json["name"]), utf8ToWstring(json["filename"]), json["format"],
                     json["computeLod"]);
}

void ResourceManager::cpuTick() {
    std::unique_lock lock(m_mutex);
    m_loadingQueueCv.wait(lock, [&]() {
        return !m_loadingQueue.empty() || !m_cpuThread.isAlive();
    });
    if (!m_cpuThread.isAlive()) return;
    Id id = m_loadingQueue.front();
    const auto job = m_entries[id].get();
    job->m_state = LOADING;
    LOG_DEBUG("<%ls>: loading using file %ls", job->m_name.c_str(), job->m_filename.c_str());
    lock.unlock();
    const bool res = job->load();
    lock.lock();
    LOG_DEBUG("<%ls>: finished cpu loading, result %s", job->m_name.c_str(), res ? "success" : "failed");
    if (res) {
        job->m_state = LOADED;
        m_gpuQueue.push_back(job);
        m_gpuQueueCv.notify_one();
    } else {
        job->m_state = FAILED;
    }
    m_loadingQueue.pop();
}

void ResourceManager::gpuTick() {
    std::unique_lock lock(m_mutex);
    m_gpuQueueCv.wait(lock, [&]() {
        return !m_gpuQueue.empty() || !m_gpuThread.isAlive();
    });
    if (!m_gpuThread.isAlive()) return;
    auto& heap = Get<Device>().getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    auto copyList = m_copyQueue.createCommandList();
    auto computeList = m_computeQueue.createCommandList();
    const std::vector<LoadingJob*> gpuQueue = m_gpuQueue;
    m_gpuQueue.clear();
    lock.unlock();

    m_mips.use(*computeList);
    // execute outside locks
    for (auto& job: gpuQueue) {
        LOG_DEBUG("<%ls>: Start gpu upload", job->m_name.c_str());
        job->gpu(m_mips, heap, *copyList, *computeList);
        LOG_DEBUG("<%ls>: Staging done", job->m_name.c_str());
    }
    const uint64_t signalCopy = m_copyQueue.execute(copyList);
    m_computeQueue.waitForQueue(m_copyQueue);
    const uint64_t signalCompute = m_computeQueue.execute(computeList);

    lock.lock();
    for (auto& job: gpuQueue) {
        job->m_copySignal = signalCopy;
        job->m_computeSignal = signalCompute;
        job->m_state = UPLOADING;
        LOG_DEBUG("<%ls>: Waiting for gpu execution", job->m_name.c_str());
        m_gpuWaitingQueue.push_back(job);
    }
}

void ResourceManager::executeOnGpu() {
    PIXScopedEvent(PIX_COLOR_INDEX(0), "ResourceManager::executeOnGpu()");
    std::unique_lock lock(m_mutex);
    if (m_gpuQueue.empty() && m_gpuWaitingQueue.empty()) return;
    for (auto it = m_gpuWaitingQueue.begin(); it != m_gpuWaitingQueue.end();) {
        auto job = *it;
        bool isCopyFinished = m_copyQueue.isFenceReady(job->m_copySignal);
        if (isCopyFinished) {
            job->m_state = PREPARING;
            LOG_TRACE("<%ls>: Copy is finished", job->m_name.c_str());
        }
        if (job->m_computeLod && m_computeQueue.isFenceReady(job->m_computeSignal) ||
            !job->m_computeLod && isCopyFinished) {
            job->m_state = READY;
            job->clean();
            auto desc = job->m_resource->GetDesc();
            LOG_INFO("<%ls>: Ready! %d x %d %.02f MB", job->m_name.c_str(), desc.Width, desc.Height,
                     desc.Width * desc.Height * 4.0 / 1024 / 1024);
            it = m_gpuWaitingQueue.erase(it);
            continue;
        }
        ++it;
    }
}

void ResourceManager::init() {
    m_mips.load();
    m_computeQueue.init(L"Queue: ResourceManager Compute");
    m_copyQueue.init(L"Queue: ResourceManager Copy");

    m_cpuThread.start();
    m_gpuThread.start();
}

const char* g_stateAsStr[] = {
    "UNKNOWN",
    "QUEUED",
    "LOADING",
    "LOADED",
    "UPLOADING",
    "PREPARING",
    "READY",
    "FAILED"
};

void ResourceManager::gui() {
    ImGui::Begin("Resources");

    for (auto& entry: m_entries) {
        ImGui::Text("%d", entry.first);
        ImGui::SameLine();
        ImGui::Text("%ls %s", entry.second->m_name.c_str(), g_stateAsStr[entry.second->m_state]);
    }
    ImGui::End();
}
