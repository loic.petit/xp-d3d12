#pragma once

#include <json.hpp>
#include <base/base-thread.h>
#include <utils/mips.h>
#include <core/queue.h>

class Resource;

class ResourceManager {
public:
    using Id = uint32_t;

    enum State: uint32_t {
        UNKNOWN [[maybe_unused]],
        QUEUED,
        LOADING,
        LOADED,
        UPLOADING,
        PREPARING,
        READY,
        FAILED
    };

    ResourceManager();

    ~ResourceManager();

    Id loadImage(const std::wstring& name, const std::wstring& filename, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, bool computeLod = true);

    State state(Id id);

    Resource& gpu(Id id);

    void executeOnGpu();

    void init();

    void gui();

    const std::wstring& name(Id id);

    nlohmann::json save(Id id);

    Id load(const nlohmann::json& json);

private:
    void cpuTick();
    void gpuTick();

    class StbLoadingJob;
    class DDSLoadingJob;
    class LoadingJob;
    std::mutex m_mutex;
    StoppableThread m_cpuThread;
    StoppableThread m_gpuThread;
    std::unordered_map<Id, std::unique_ptr<LoadingJob>> m_entries;
    std::queue<Id> m_loadingQueue;
    std::condition_variable m_loadingQueueCv;
    std::vector<LoadingJob*> m_gpuQueue;
    std::condition_variable m_gpuQueueCv;
    std::list<LoadingJob*> m_gpuWaitingQueue;

    Mips m_mips;

    Queue m_copyQueue;
    Queue m_computeQueue;
};
