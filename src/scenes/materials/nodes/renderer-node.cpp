#include "scenes/materials/nodes/renderer-node.h"

void RendererNode::defaultPorts() {
    addInput("alpha", "float");
    addInput("albedo", "float3");
    addInput("normal", "float3");
    addInput("sss", "float4");
    addInput("specular", "float");
    addInput("roughness", "float");
    addInput("metallic", "float");
    addInput("occlusion", "float");
}

void RendererNode::buildShader(ShaderBuilder& builder) {
    builder.body()
            << "OUT.albedo = float4(" << m_inputs[1]->m_link->uniqueLabel() << ", " << m_inputs[0]->m_link->uniqueLabel() << ");\n"
            << "OUT.normal = float4(" << m_inputs[2]->m_link->uniqueLabel() << ", 1.0);\n"
            << "OUT.sss = " << m_inputs[3]->m_link->uniqueLabel() << ";\n"
            << "OUT.srma = float4("
            << m_inputs[4]->m_link->uniqueLabel() << ", "
            << m_inputs[5]->m_link->uniqueLabel() << ", "
            << m_inputs[6]->m_link->uniqueLabel() << ", "
            << m_inputs[7]->m_link->uniqueLabel() << ");\n";
}