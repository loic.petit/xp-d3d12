#pragma once

#include "scenes/materials/base.h"

class TexCoordNode : public GraphNode {
public:
    GraphNodeType(TexCoord, COLORS_PISTACHIO);
    TexCoordNode(Graph* graph) : GraphNode(graph, 0, 1) {
    }

    void defaultPorts() override;

protected:
    void postLoad() override;

    void buildShader(ShaderBuilder& builder) override;
};
