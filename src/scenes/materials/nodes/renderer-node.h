#pragma once

#include "scenes/materials/base.h"

class RendererNode : public GraphNode {
public:
    GraphNodeType(Renderer, COLORS_ORANGE);
    RendererNode(Graph* graph) : GraphNode(graph, 8, 0) {
    }

    void defaultPorts() override;
    void buildShader(ShaderBuilder& builder) override;
};