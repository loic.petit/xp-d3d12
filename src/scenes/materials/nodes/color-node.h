#pragma once

#include "scenes/materials/base.h"

class ColorNode : public GraphNode {
public:
    GraphNodeType(Color, COLORS_RED);
    ColorNode(Graph* graph) : GraphNode(graph, 0, 1) {
    }

    void defaultPorts() override;

    nlohmann::json save() override;

    bool loadCustom(const nlohmann::json& json) override;

    bool sidePanelGui() override;

    void buildShader(ShaderBuilder& builder) override;

private:
    float m_color[4] {};
};
