#include "scenes/materials/nodes/texcoord-node.h"

void TexCoordNode::defaultPorts() {
    addOutput("uv", "float2");
}

void TexCoordNode::postLoad() {
    m_outputs[0]->m_uniqueLabel = "IN.texCoord";
}

void TexCoordNode::buildShader(ShaderBuilder& builder) {

}
