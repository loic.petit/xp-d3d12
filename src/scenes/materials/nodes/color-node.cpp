#include "scenes/materials/nodes/color-node.h"

void ColorNode::defaultPorts() {
    addOutput("rgba", "float4");
}

nlohmann::json ColorNode::save() {
    auto json = GraphNode::save();
    json["rgba"] = nlohmann::json::array({m_color[0], m_color[1], m_color[2], m_color[3]});
    return json;
}

bool ColorNode::loadCustom(const nlohmann::json& json) {
    if (json.contains("rgba")) {
        auto& rgba = json["rgba"];
        m_color[0] = rgba[0];
        m_color[1] = rgba[1];
        m_color[2] = rgba[2];
        m_color[3] = rgba[3];
    }
    return true;
}

bool ColorNode::sidePanelGui() {
    bool updated = false;
    ImGui::PushItemWidth(200);
    if (ImGui::ColorEdit4("##color", m_color, ImGuiColorEditFlags_Float)) {
        updated = true;
    }
    ImGui::PopItemWidth();
    return updated;
}

void ColorNode::buildShader(ShaderBuilder& builder) {
    m_outputs[0]->declare(builder.body());
    builder.body() << m_outputs[0]->uniqueLabel() << " = float4(" << m_color[0] << ", " << m_color[1] << ", " << m_color[2] << ", " << m_color[3] << ");\n";
}
