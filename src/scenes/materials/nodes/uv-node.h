#pragma once

#include "scenes/materials/base.h"

class UvNode : public GraphNode {
public:
    GraphNodeType(Uv, COLORS_SPRING_GREEN);
    explicit UvNode(Graph* graph) : GraphNode(graph, 1, 6) {
    }

    void defaultPorts() override;

    bool loadCustom(const nlohmann::json& json) override;

    nlohmann::json save() override;

    void setResourceId(ResourceManager::Id id) {
        m_resourceId = id;
        if (m_resourceId) {
            m_title = wstringToUtf8(Get<ResourceManager>().name(m_resourceId));
        }
    }

    void buildShader(ShaderBuilder& builder) override;

    bool sidePanelGui() override;

    void stage(CommandList& list, MaterialRender& pipeline) const override;

protected:
    void postLoad() override;

private:
    ResourceManager::Id m_resourceId = 0;
    uint32_t m_stageIndex = 0;
};
