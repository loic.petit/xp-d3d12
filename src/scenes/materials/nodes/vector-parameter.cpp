#include "vector-parameter.h"

#include "shaders/material-render.h"

void VectorParameter::postLoad() {
    m_outputs[1]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".rgb";
    m_outputs[2]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".r";
    m_outputs[3]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".g";
    m_outputs[4]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".b";
    m_outputs[5]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".a";
}

void VectorParameter::defaultPorts() {
    addOutput("rgba", "float4");
    addOutput("rgb", "float3");
    addOutput("r", "float");
    addOutput("g", "float");
    addOutput("b", "float");
    addOutput("a", "float");
}

bool VectorParameter::loadCustom(const nlohmann::json& json) {
    if (!json.contains("parameterName") || !json.contains("defaultValue") || json["defaultValue"].type() != nlohmann::detail::value_t::array || json["defaultValue"].size() != 4) {
        return false;
    }
    m_name = json["parameterName"];
    auto& defaultValue = json["defaultValue"];
    m_defaultValue.f[0] = defaultValue[0].get<float>();
    m_defaultValue.f[1] = defaultValue[1].get<float>();
    m_defaultValue.f[2] = defaultValue[2].get<float>();
    m_defaultValue.f[3] = defaultValue[3].get<float>();
    return true;
}

nlohmann::json VectorParameter::save() {
    auto json = GraphNode::save();
    json["parameterName"] = m_name;
    json["defaultValue"] = nlohmann::json::array({m_defaultValue.f[0], m_defaultValue.f[1], m_defaultValue.f[2], m_defaultValue.f[3]});
    return json;
}

void VectorParameter::buildShader(ShaderBuilder& builder) {
    builder.registerVectorParameter(m_outputs[0]->m_label, m_outputs[0]->m_uniqueLabel);
    postLoad();
}

void VectorParameter::stage(CommandList& list, MaterialRender& pipeline) const {

}