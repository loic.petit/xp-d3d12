#include "scenes/materials/nodes/transform-node.h"

#include <scenes/nodes/graph.h>

void ParameterEdit::select(Port& p) {
    pinId = p.m_id;
    parameter = &p;
    strncpy_s(buffer, p.m_label.c_str(), sizeof(buffer));
    for (int i = 0; i < _countof(PARAMETER_TYPES); i++) {
        auto& type = PARAMETER_TYPES[i];
        if (p.m_type == type) {
            typeSelection = i;
        }
    }
}

bool ParameterEdit::save() {
    bool changed = false;
    if (parameter->m_label != buffer) {
        parameter->m_label = buffer;
        changed = true;
    }
    if (parameter->m_type != PARAMETER_TYPES[typeSelection]) {
        parameter->m_type = PARAMETER_TYPES[typeSelection];
        changed = true;
    }
    pinId = 0;
    return changed;
}

bool TransformNode::loadCustom(const nlohmann::json& json) {
    if (json.contains("source")) {
        auto src = json["source"].get<std::string>();
        memcpy(m_buffer, src.data(), src.size());
    }
    m_inputEdits.reserve(m_inputs.size());
    for (auto& input : m_inputs) {
        m_inputEdits.emplace_back().select(*input);
    }
    m_outputEdits.reserve(m_outputs.size());
    for (auto& output : m_outputs) {
        m_outputEdits.emplace_back().select(*output);
    }
    return true;
}

nlohmann::json TransformNode::save() {
    auto json = GraphNode::save();
    json["source"] = m_buffer;
    return json;
}

bool TransformNode::sidePanelGui() {
    static char popupBuffer[1024] {};
    bool updated = false;
    ImGui::TextWrapped("%s", m_buffer);
    if (ImGui::Button("Edit")) {
        ImGui::OpenPopup("edit-source");
        strncpy_s(popupBuffer, m_buffer, sizeof(popupBuffer));
    }
    if (ImGui::BeginPopupModal("edit-source", nullptr, ImGuiWindowFlags_NoSavedSettings)) {
        ImGui::InputTextMultiline("##source", popupBuffer, sizeof(popupBuffer), ImVec2(500, 300), ImGuiInputTextFlags_AllowTabInput);
        if (ImGui::Button("Save", ImVec2(120, 0))) {
            strncpy_s(m_buffer, popupBuffer, sizeof(popupBuffer));
            updated = true;
            ImGui::CloseCurrentPopup();
        }
        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button("Cancel", ImVec2(120, 0))) {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }

    ImGui::PushItemWidth(100.0f);
    if (ImGui::CollapsingHeader("Inputs", ImGuiTreeNodeFlags_DefaultOpen)) {
        for (auto& input : m_inputEdits) {
            char buf[256];
            sprintf_s(buf, "##type%d", input.pinId);
            if (ImGui::Combo(buf, &input.typeSelection, PARAMETER_TYPES, _countof(PARAMETER_TYPES))) {
                updated |= input.save();
            }
            ImGui::SameLine();
            sprintf_s(buf, "##label%d", input.pinId);
            if (ImGui::InputText(buf, input.buffer, sizeof(input.buffer),
                                 ImGuiInputTextFlags_EnterReturnsTrue)) {
                updated |= input.save();
            }
        }
        if (ImGui::Button("Add input")) {
            addInput("input_" + std::to_string(m_inputs.size()), "float4");
            m_inputEdits.emplace_back().select(*m_inputs[m_inputs.size()-1]);
            updated = true;
        }
    }
    if (ImGui::CollapsingHeader("Outputs", ImGuiTreeNodeFlags_DefaultOpen)) {
        for (auto& output : m_outputEdits) {
            char buf[256];
            sprintf_s(buf, "##type%d", output.pinId);
            if (ImGui::Combo(buf, &output.typeSelection, PARAMETER_TYPES, _countof(PARAMETER_TYPES))) {
                updated |= output.save();
            }
            ImGui::SameLine();
            sprintf_s(buf, "##label%d", output.pinId);
            if (ImGui::InputText(buf, output.buffer, sizeof(output.buffer),
                                 ImGuiInputTextFlags_EnterReturnsTrue)) {
                updated |= output.save();
                                 }
        }
        if (ImGui::Button("Add output")) {
            addOutput("output_" + std::to_string(m_outputs.size()), "float4");
            m_outputEdits.emplace_back().select(*m_outputs[m_outputs.size()-1]);
            updated = true;
        }
    }
    ImGui::PopItemWidth();

    return updated;
}

void TransformNode::buildShader(ShaderBuilder& builder) {
    auto& body = builder.body();
    for (auto& out: m_outputs) {
        out->declare(body);
    }
    body << "{\n";
    for (auto& out: m_outputs) {
        out->declareLocal(body);
    }
    for (auto& in: m_inputs) {
        in->declareLinked(body);
    }
    body << m_buffer << "\n";
    for (auto& out: m_outputs) {
        out->localToGlobal(body);
    }
    body << "}\n";
}