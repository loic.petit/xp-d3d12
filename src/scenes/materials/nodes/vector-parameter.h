#pragma once

#include "scenes/materials/base.h"

class VectorParameter : public GraphNode {
public:
    GraphNodeType(VectorParameter, COLORS_PINK);

    VectorParameter(Graph* graphReference) : GraphNode(graphReference, 0, 6) {}

    void postLoad() override;

    void defaultPorts() override;

protected:
    bool loadCustom(const nlohmann::json& json) override;
    nlohmann::json save() override;

    void buildShader(ShaderBuilder& builder) override;

    void stage(CommandList& list, MaterialRender& pipeline) const override;

    std::string m_name;

    DirectX::XMVECTORF32 m_defaultValue{};
};
