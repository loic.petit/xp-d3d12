#include "scenes/materials/nodes/uv-node.h"
#include "shaders/material-render.h"

void UvNode::stage(CommandList& list, MaterialRender& pipeline) const {
    pipeline.submitTexture(list, Get<ResourceManager>().gpu(m_resourceId).srv(), m_stageIndex);
}

void UvNode::postLoad() {
    m_outputs[1]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".rgb";
    m_outputs[2]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".r";
    m_outputs[3]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".g";
    m_outputs[4]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".b";
    m_outputs[5]->m_uniqueLabel = m_outputs[0]->m_uniqueLabel + ".a";
}

void UvNode::defaultPorts() {
    addInput("uv", "float2");
    addOutput("rgba", "float4");
    addOutput("rgb", "float3");
    addOutput("r", "float");
    addOutput("g", "float");
    addOutput("b", "float");
    addOutput("a", "float");
}

bool UvNode::loadCustom(const nlohmann::json& json) {
    if (json.contains("resource")) {
        auto& rm = Get<ResourceManager>();
        setResourceId(rm.load(json["resource"]));
    }
    return true;
}

nlohmann::json UvNode::save() {
    auto json = GraphNode::save();
    auto& rm = Get<ResourceManager>();
    if (m_resourceId) {
        json["resource"] = rm.save(m_resourceId);
    }
    return json;
}

bool UvNode::sidePanelGui() {
    ImGui::Text("Resource: #%d", m_resourceId);

    if (m_resourceId) {
        auto& rm = Get<ResourceManager>();
        if (rm.state(m_resourceId) == ResourceManager::READY) {
            Get<GUI>().image(rm.gpu(m_resourceId), ImVec2(300, 300));
        }
    }

    ImGui::Button("Load file");
    return false;
}

void UvNode::buildShader(ShaderBuilder& builder) {
    std::string ref;
    m_stageIndex = builder.registerTexture(ref);
    m_outputs[0]->declare(builder.body());
    builder.body() << m_outputs[0]->uniqueLabel() << " = " << ref << ".Sample(SAMPLER, " << m_inputs[0]->m_link->uniqueLabel() << ");\n";
}
