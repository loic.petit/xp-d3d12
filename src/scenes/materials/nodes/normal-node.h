#pragma once

#include "scenes/materials/base.h"

class NormalNode : public GraphNode {
public:
    GraphNodeType(Normal, COLORS_DARK_MAGENTA);
    NormalNode(Graph* graph) : GraphNode(graph, 0, 2) {
    }

    void defaultPorts() override;

protected:
    void postLoad() override;
};
