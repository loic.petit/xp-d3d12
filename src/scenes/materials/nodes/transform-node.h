#pragma once

#include "scenes/materials/base.h"

static constexpr const char* PARAMETER_TYPES[] = {
    "float",
    "float2",
    "float3",
    "float4",
    "int",
    "int2",
    "int3",
    "int4",
};

struct ParameterEdit {
    uint32_t pinId = 0;
    char buffer[32] {};
    int typeSelection = 0;
    Port* parameter;

    void select(Port& p);

    bool save();
};

class TransformNode : public GraphNode {
public:
    GraphNodeType(Transform, COLORS_LIGHT_BLUE);
    TransformNode(Graph* graph) : GraphNode(graph, 8, 8) {
    }

    bool loadCustom(const nlohmann::json& json) override;

    nlohmann::json save() override;

    bool sidePanelGui() override;

    void buildShader(ShaderBuilder& builder) override;

private:
    char m_buffer[1024] {};

    std::vector<ParameterEdit> m_inputEdits;
    std::vector<ParameterEdit> m_outputEdits;
};
