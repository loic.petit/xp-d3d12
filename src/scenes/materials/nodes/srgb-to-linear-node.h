#pragma once

#include "scenes/materials/base.h"

class SrgbToLinearNode : public GraphNode {
public:
    GraphNodeType(SrgbToLinear, COLORS_DARK_EMERALD);
    SrgbToLinearNode(Graph* graph) : GraphNode(graph, 1, 1) {
    }

    void defaultPorts() override;

    void buildShader(ShaderBuilder& builder) override;
};