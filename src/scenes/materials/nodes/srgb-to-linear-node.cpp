#include "scenes/materials/nodes/srgb-to-linear-node.h"

void SrgbToLinearNode::defaultPorts() {
    addInput("srgb", "float3");
    addOutput("linear", "float3");
}

void SrgbToLinearNode::buildShader(ShaderBuilder& builder) {
    auto& body = builder.body();
    m_outputs[0]->declare(body);
    body << "{\n\t";
    m_inputs[0]->declareLinked(body);
    body << "\t" << m_outputs[0]->uniqueLabel() << " = srgb < 0.04045f ? srgb / 12.92 : pow(abs(srgb + 0.055) / 1.055, 2.4);\n";
    body << "}\n";
}
