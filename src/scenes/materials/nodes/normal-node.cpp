#include "scenes/materials/nodes/normal-node.h"

void NormalNode::defaultPorts() {
    addOutput("normal", "float3");
    addOutput("tangent", "float4");
}

void NormalNode::postLoad() {
    m_outputs[0]->m_uniqueLabel = "IN.normalWS";
    m_outputs[1]->m_uniqueLabel = "IN.tangentWS";
}
