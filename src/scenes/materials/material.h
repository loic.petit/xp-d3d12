#pragma once

#include <DirectXMath.h>
#include <json.hpp>

#include "scenes/materials/resource-manager.h"

template<typename T>
class ParameterValue {
public:
    const T& get() {
        if (m_value) {
            return m_value.value();
        }
        return m_defaultValue;
    }

    nlohmann::json save() const {
        auto json = nlohmann::json();
        json["default"] = saveValue(m_defaultValue);
        if (m_value.has_value()) {
            json["value"] = saveValue(m_value.value());
        }
        return json;
    }
    static nlohmann::json saveValue(const T& value) {
        return value;
    }
    static void loadValue(const nlohmann::json& json, T& value) {
        value = json;
    }
    void load(const nlohmann::json& json) {
        loadValue(json["default"], m_defaultValue);
        if (json.contains("value")) {
            m_value = {};
            loadValue(json["value"], m_value.value());
        } else {
            m_value.reset();
        }
    }
    
    T m_defaultValue;
    std::optional<T> m_value;
};

template<>
nlohmann::json ParameterValue<DirectX::XMFLOAT4>::saveValue(const DirectX::XMFLOAT4& value) {
    return {
            {"r", value.x},
            {"g", value.y},
            {"b", value.z},
            {"a", value.w},
    };
}
template<>
nlohmann::json ParameterValue<CD3DX12_STATIC_SAMPLER_DESC>::saveValue(const CD3DX12_STATIC_SAMPLER_DESC& value) {
    return {
            {"Filter", value.Filter},
            {"AddressU", value.AddressU},
            {"AddressV", value.AddressV},
            {"AddressW", value.AddressW},
            {"MipLODBias", value.MipLODBias},
            {"MaxAnisotropy", value.MaxAnisotropy},
            {"ComparisonFunc", value.ComparisonFunc},
            {"BorderColor", value.BorderColor},
            {"MinLOD", value.MinLOD},
            {"MaxLOD", value.MaxLOD},
    };
}

template<>
void ParameterValue<DirectX::XMFLOAT4>::loadValue(const nlohmann::json& json, DirectX::XMFLOAT4& value) {
    value.x = json["r"];
    value.y = json["g"];
    value.z = json["b"];
    value.w = json["a"];
}
template<>
void ParameterValue<CD3DX12_STATIC_SAMPLER_DESC>::loadValue(const nlohmann::json& json, CD3DX12_STATIC_SAMPLER_DESC& value) {
    value.Filter = json["Filter"];
    value.AddressU = json["AddressU"];
    value.AddressV = json["AddressV"];
    value.AddressW = json["AddressW"];
    value.MipLODBias = json["MipLODBias"];
    value.MaxAnisotropy = json["MaxAnisotropy"];
    value.ComparisonFunc = json["ComparisonFunc"];
    value.BorderColor = json["BorderColor"];
    value.MinLOD = json["MinLOD"];
    value.MaxLOD = json["MaxLOD"];
}

class Graph;
class Material {
public:
    using ParameterId = std::string;
    nlohmann::json save() const;
    void load(const nlohmann::json& json);
private:
    std::unique_ptr<Graph> m_graph;
    std::unordered_map<ParameterId, ParameterValue<float>> m_scalarParameters;
    std::unordered_map<ParameterId, ParameterValue<DirectX::XMFLOAT4>> m_vectorParameters;
    std::unordered_map<ParameterId, ParameterValue<CD3DX12_STATIC_SAMPLER_DESC>> m_samplerParameters;
    std::unordered_map<ParameterId, ParameterValue<ResourceManager::Id>> m_textureParameters;
    std::unordered_map<ParameterId, ParameterValue<float>> m_staticParameters;
};
