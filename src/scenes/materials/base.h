#pragma once

#include <utils/gui.h>

#include "resource-manager.h"
#include "scenes/nodes/node.h"
#include "utils/service.h"
#include "utils/utils.h"

constexpr ImColor COLORS_RED(0.77f, 0.12f, 0.23f);
constexpr ImColor COLORS_DARK_MAGENTA(0.64f, 0.19f, 0.79f);
constexpr ImColor COLORS_ORANGE(1.00f, 0.49f, 0.04f);
constexpr ImColor COLORS_DARK_EMERALD(0.20f, 0.58f, 0.50f);
constexpr ImColor COLORS_PISTACHIO(0.67f, 0.83f, 0.45f);
constexpr ImColor COLORS_LIGHT_BLUE(0.25f, 0.78f, 0.92f);
constexpr ImColor COLORS_SPRING_GREEN(0.00f, 1.00f, 0.60f);
constexpr ImColor COLORS_PINK(0.96f, 0.55f, 0.73f);
constexpr ImColor COLORS_WHITE(1.00f, 1.00f, 1.00f);
constexpr ImColor COLORS_YELLOW(1.00f, 0.96f, 0.41f);
constexpr ImColor COLORS_BLUE(0.00f, 0.44f, 0.87f);
constexpr ImColor COLORS_PURPLE(0.53f, 0.53f, 0.93f);
constexpr ImColor COLORS_TAN(0.78f, 0.61f, 0.43f);

#define GraphNodeType(typeName, colorName) \
    static constexpr const char* TYPE = #typeName; \
    const char* type() override { return TYPE; } \
    const ImColor& color() override { return colorName; }
