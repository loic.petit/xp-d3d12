#include "material.h"
#include "scenes/nodes/graph.h"

void Material::load(const nlohmann::json& json) {
}

template<typename T>
static nlohmann::json saveParameters(const std::unordered_map<Material::ParameterId, ParameterValue<T>>& parametersMap) {
    nlohmann::json json;
    for (const auto& item: parametersMap) {
        json[item.first] = item.second.save();
    }
    return json;
}

nlohmann::json Material::save() const {
    auto json = nlohmann::json();
    auto& parameters = json["parameters"];
    parameters["scalar"] = saveParameters(m_scalarParameters);
    parameters["vector"] = saveParameters(m_vectorParameters);
    parameters["sampler"] = saveParameters(m_samplerParameters);
    parameters["texture"] = saveParameters(m_textureParameters);
    parameters["static"] = saveParameters(m_staticParameters);
    json["graph"] = m_graph->save();
    return json;
}
