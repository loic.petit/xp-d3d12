#include "spotlight.h"
#include <DirectXMath.h>
#include "core/commandlist.h"
#include "utils/service.h"

using namespace Microsoft::WRL;
using namespace DirectX;

constexpr uint32_t TESSELATION = 16;
constexpr uint32_t SHADOWMAP_WIDTH = 4096;
constexpr uint32_t SHADOWMAP_HEIGHT = 4096;
const XMVECTOR CAMERA_UP = XMVectorSet(0, 1, 0, 0);

Spotlight::Spotlight(const std::wstring& name) : m_shadow(name + L"SpotLight Depth Buffer") {
}

void Spotlight::load(CommandList& list) {
    createMesh(list, TESSELATION);
    m_shadow.init(SHADOWMAP_WIDTH, SHADOWMAP_HEIGHT, 1);
    m_shadow.createDepthStencil(L"DepthStencil", DXGI_FORMAT_D16_UNORM, true);
}

void Spotlight::createMesh(CommandList& list, uint32_t tessellation) {
    if (tessellation < 3)
        throw std::out_of_range("tessellation parameter out of range");

    std::vector<XMFLOAT3> vertices;
    std::vector<uint32_t> indices;

    XMFLOAT3 topVertex{0, 0, 0};

    for ( size_t i = 0; i < tessellation; i++ )
    {
        XMFLOAT3 vertex{0, 0, 1};
        XMScalarSinCos( &vertex.x, &vertex.y, float( i ) * DirectX::XM_2PI / float( tessellation ) );

        vertices.emplace_back(vertex);

        indices.push_back(i == 0 ? tessellation-1 : i-1);
        indices.push_back(i);
        indices.push_back(tessellation);

        if (i > 1) {
            indices.push_back(0);
            indices.push_back(i);
            indices.push_back(i-1);
        }
    }
    vertices.emplace_back(topVertex);

    const uint32_t indicesSize = sizeof(indices[0]) * indices.size();
    const uint32_t verticesSize = sizeof(vertices[0]) * vertices.size();
    auto& device = Get<Device>();
    m_indices = device.uploadBuffer(list, indices.data(), indicesSize);
    m_vertices = device.uploadBuffer(list, vertices.data(), verticesSize);
    m_vertexView = {
            m_vertices->GetGPUVirtualAddress(),
            verticesSize,
            sizeof(vertices[0])
    };
    m_indexView = {
            m_indices->GetGPUVirtualAddress(),
            indicesSize,
            DXGI_FORMAT_R32_UINT
    };
    m_indexCount = indices.size();
}

void Spotlight::drawMesh(CommandList& list) const {
    list->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    list->IASetVertexBuffers(0, 1, &m_vertexView);
    list->IASetIndexBuffer(&m_indexView);
    list->DrawIndexedInstanced(m_indexCount, 1, 0, 0, 0);
    list.track(m_indices.d3d12());
    list.track(m_vertices.d3d12());
}

void Spotlight::setParams(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 lookAt, DirectX::XMFLOAT3 color, float strength, float fov, float depth) {
    m_params.strength = strength;
    m_params.position = position;
    m_params.color = color;

    m_viewPoint.set_LookAt(
            DirectX::XMVectorSet(position.x, position.y, position.z, 1.0),
            DirectX::XMVectorSet(lookAt.x, lookAt.y, lookAt.z, 1.0),
            CAMERA_UP);
    m_viewPoint.set_Projection(fov, 1, 0.1, depth);
    m_position = DirectX::XMFLOAT4(position.x, position.y, position.z, 1.0);
    m_color = DirectX::XMFLOAT4(color.x * strength, color.y * strength, color.z * strength, 1.0);

    float scale = tan(fov * DirectX::XM_PI / 360) * depth;
    // make the volume larger to make sure that the volume with tesselation encloses the theoretical volume
    // The scaling factor is 1/cos(alpha/2) with alpha the angle between two edges
    // (trace a unit circle on a draft, you should be convinced)
    scale /= cos(DirectX::XM_PI / TESSELATION);
    m_modelMatrix = DirectX::XMMatrixScaling(scale, scale, depth) * m_viewPoint.get_InverseViewMatrix();
    m_mvpMatrix = m_viewPoint.get_ViewMatrix() * m_viewPoint.get_ProjectionMatrix() * DirectX::XMMatrixScaling(1, 1, 1 / depth);
}
