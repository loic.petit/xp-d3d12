#pragma once

#include <DirectXMath.h>
#include "core/device.h"
#include "core/rendertarget.h"
#include "utils/camera.h"

class Spotlight {
public:
    Spotlight(const std::wstring& name);
    void setParams(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 lookAt, DirectX::XMFLOAT3 color, float strength, float fov, float depth);
    void load(CommandList& list);
    void drawMesh(CommandList& list) const;

    DirectX::XMFLOAT4 m_position;
    DirectX::XMFLOAT4 m_color;
    DirectX::XMMATRIX m_modelMatrix;
    DirectX::XMMATRIX m_mvpMatrix;
    Camera m_viewPoint;

    RenderTarget m_shadow;
    struct {
        DirectX::XMFLOAT3 color;
        DirectX::XMFLOAT3 position;
        float strength;
    } m_params{};
private:
    void createMesh(CommandList& list, uint32_t tessellation);
    Resource m_vertices;
    Resource m_indices;
    uint32_t m_indexCount;

    D3D12_VERTEX_BUFFER_VIEW m_vertexView;
    D3D12_INDEX_BUFFER_VIEW m_indexView;
};