#include "resource.h"

#include "device.h"
#include "utils/service.h"
#include "utils/utils.h"

Resource::Resource(ComPtr<ID3D12Resource>& resource, D3D12_RESOURCE_STATES state) : m_state(state) {
    m_native = resource;
}

Resource::Resource(const D3D12_RESOURCE_DESC& resourceDesc, D3D12_RESOURCE_STATES state, const D3D12_CLEAR_VALUE* clearValue, D3D12_HEAP_TYPE heapType) : m_state(state), m_statePending(state) {
    auto desc = CD3DX12_HEAP_PROPERTIES(heapType);
    ThrowIfFailed(Get<Device>()->CreateCommittedResource(
            &desc, D3D12_HEAP_FLAG_NONE, &resourceDesc,
            state, clearValue, IID_PPV_ARGS(&m_native)));
}

void Resource::clear() {
    m_native.Reset();
    m_state = D3D12_RESOURCE_STATE_COMMON;
    m_statePending = D3D12_RESOURCE_STATE_COMMON;
}

bool Resource::isSRGB(DXGI_FORMAT format) {
    return getUnormFromSRGB(format) != format;
}

DXGI_FORMAT Resource::getUnormFromSRGB(DXGI_FORMAT format) {
    switch (format) {
        case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
            return DXGI_FORMAT_R8G8B8A8_UNORM;
        case DXGI_FORMAT_BC1_UNORM_SRGB:
            return DXGI_FORMAT_BC1_UNORM;
        case DXGI_FORMAT_BC2_UNORM_SRGB:
            return DXGI_FORMAT_BC2_UNORM;
        case DXGI_FORMAT_BC3_UNORM_SRGB:
            return DXGI_FORMAT_BC3_UNORM;
        case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
            return DXGI_FORMAT_B8G8R8A8_UNORM;
        case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
            return DXGI_FORMAT_B8G8R8X8_UNORM;
        case DXGI_FORMAT_BC7_UNORM_SRGB:
            return DXGI_FORMAT_BC7_UNORM;
        default:
            return format;
    }
}

bool Resource::isCompressed(DXGI_FORMAT format) {
    switch (format) {
        case DXGI_FORMAT_BC1_TYPELESS:
        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC1_UNORM_SRGB:
        case DXGI_FORMAT_BC2_TYPELESS:
        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
        case DXGI_FORMAT_BC3_TYPELESS:
        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
        case DXGI_FORMAT_BC4_TYPELESS:
        case DXGI_FORMAT_BC4_UNORM:
        case DXGI_FORMAT_BC4_SNORM:
        case DXGI_FORMAT_BC5_TYPELESS:
        case DXGI_FORMAT_BC5_UNORM:
        case DXGI_FORMAT_BC5_SNORM:
        case DXGI_FORMAT_BC6H_TYPELESS:
        case DXGI_FORMAT_BC6H_UF16:
        case DXGI_FORMAT_BC6H_SF16:
        case DXGI_FORMAT_BC7_TYPELESS:
        case DXGI_FORMAT_BC7_UNORM:
        case DXGI_FORMAT_BC7_UNORM_SRGB:
            return true;
        default:
            return false;
    }
}

uint8_t Resource::getComponents(DXGI_FORMAT format) {
    switch (format) {
        case DXGI_FORMAT_BC4_UNORM:
        case DXGI_FORMAT_BC4_SNORM:
        case DXGI_FORMAT_D16_UNORM:
        case DXGI_FORMAT_R16_UNORM:
        case DXGI_FORMAT_R8_UNORM:
        case DXGI_FORMAT_A8_UNORM:
        case DXGI_FORMAT_R1_UNORM:
            return 1;
        case DXGI_FORMAT_BC5_UNORM:
        case DXGI_FORMAT_BC5_SNORM:
        case DXGI_FORMAT_R8G8_UNORM:
        case DXGI_FORMAT_R16G16_UNORM:
        case DXGI_FORMAT_D24_UNORM_S8_UINT:
        case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
            return 2;
        case DXGI_FORMAT_BC6H_UF16:
        case DXGI_FORMAT_BC6H_SF16:
        case DXGI_FORMAT_B5G6R5_UNORM:
        case DXGI_FORMAT_R8G8_B8G8_UNORM:
        case DXGI_FORMAT_G8R8_G8B8_UNORM:
            return 3;
        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC1_UNORM_SRGB:
        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
        case DXGI_FORMAT_BC7_UNORM:
        case DXGI_FORMAT_BC7_UNORM_SRGB:
        case DXGI_FORMAT_R16G16B16A16_UNORM:
        case DXGI_FORMAT_R10G10B10A2_UNORM:
        case DXGI_FORMAT_R8G8B8A8_UNORM:
        case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
        case DXGI_FORMAT_B5G5R5A1_UNORM:
        case DXGI_FORMAT_B8G8R8A8_UNORM:
        case DXGI_FORMAT_B8G8R8X8_UNORM:
        case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
        case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
        case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
        case DXGI_FORMAT_B4G4R4A4_UNORM:
            return 4;
        default:
            return 0;
    }
}

DXGI_FORMAT Resource::getStagingTextureFormat(DXGI_FORMAT format) {
    switch (format) {
        case DXGI_FORMAT_BC1_TYPELESS:
        case DXGI_FORMAT_BC2_TYPELESS:
        case DXGI_FORMAT_BC3_TYPELESS:
        case DXGI_FORMAT_BC6H_TYPELESS:
        case DXGI_FORMAT_BC7_TYPELESS:
            return DXGI_FORMAT_R8G8B8A8_TYPELESS;
        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC7_UNORM:
            return DXGI_FORMAT_R32G32B32A32_FLOAT;
        case DXGI_FORMAT_BC1_UNORM_SRGB:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
        case DXGI_FORMAT_BC7_UNORM_SRGB:
            return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
        case DXGI_FORMAT_BC4_TYPELESS:
            return DXGI_FORMAT_R8_TYPELESS;
        case DXGI_FORMAT_BC4_UNORM:
            return DXGI_FORMAT_R8_UNORM;
        case DXGI_FORMAT_BC4_SNORM:
            return DXGI_FORMAT_R8_SNORM;
        case DXGI_FORMAT_BC5_TYPELESS:
            return DXGI_FORMAT_R8G8_TYPELESS;
        case DXGI_FORMAT_BC5_UNORM:
            return DXGI_FORMAT_R8G8_UNORM;
        case DXGI_FORMAT_BC5_SNORM:
            return DXGI_FORMAT_R8G8_SNORM;
        case DXGI_FORMAT_BC6H_SF16:
            return DXGI_FORMAT_R16_SINT;
        case DXGI_FORMAT_BC6H_UF16:
            return DXGI_FORMAT_R16_UINT;
        default:
            return format;
    }
}

/*
void Resource::transition(const std::function<void(D3D12_RESOURCE_BARRIER&&)>& barrier, D3D12_RESOURCE_STATES newState, UINT subresource) {
    if (subresource == D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES) {
        if (m_subresourceState.empty()) {
            if (m_state != newState) {
                barrier(CD3DX12_RESOURCE_BARRIER::Transition(m_native.Get(), m_state, newState, subresource));
            }
        } else {
            for (auto& sub: m_subresourceState) {
                if (sub.second != newState) {
                    barrier(CD3DX12_RESOURCE_BARRIER::Transition(m_native.Get(), sub.second, newState, sub.first));
                }
            }
            // maybe add also the barrier for the non-tracked sub-resources?
            m_subresourceState.clear();
        }
        m_state = newState;
    } else {
        D3D12_RESOURCE_STATES previous = state(subresource);
        if (previous != newState) {
            barrier(CD3DX12_RESOURCE_BARRIER::Transition(m_native.Get(), previous, newState, subresource));
        }
        m_subresourceState[subresource] = newState;
    }
}
*/
