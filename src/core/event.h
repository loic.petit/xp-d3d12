#pragma once

template<typename H, typename Func>
class Delegate;

class DelegateHandle {
public:
    DelegateHandle() : m_release(_do_nothing) {}

    DelegateHandle(DelegateHandle&& handle) noexcept: m_release(std::move(handle.m_release)) {
        handle.m_release = _do_nothing;
    }

    DelegateHandle& operator=(DelegateHandle&& other) noexcept {
        m_release = std::move(other.m_release);
        other.m_release = _do_nothing;
        return *this;
    }

    DelegateHandle(const DelegateHandle& handle) = delete;

    explicit DelegateHandle(std::function<void()>&& release) : m_release(release) {}

    ~DelegateHandle() {
        m_release();
    }

private:
    std::function<void()> m_release;

    static void _do_nothing() {}
};

template<typename H, typename... Args>
class Delegate<H, void(Args...)> {
public:
    using callback = std::function<void(Args...)>;

    class CallbackCollection : public std::vector<std::optional<callback>> {
    public:
        mutable std::shared_mutex m_lock;
    };

    Delegate() : m_callbacks(std::make_shared<CallbackCollection>()) {}

    [[nodiscard]] DelegateHandle connect(callback&& f) {
        std::unique_lock lock(m_callbacks->m_lock);
        for (int i = 0; i < m_callbacks->size(); i++) {
            if (!m_callbacks->at(i).has_value()) {
                m_callbacks->at(i) = f;
                return createHandle(i);
            }
        }
        m_callbacks->emplace_back(f);
        return createHandle(m_callbacks->size() - 1);
    }

    template<typename C>
    [[nodiscard]] DelegateHandle connect(C* instance, void (C::*method)(Args...)) {
        return connect([instance, method](Args... args) {
            (instance->*method)(std::forward<Args>(args)...);
        });
    }

    void operator()(Args... args) {
        std::shared_lock lock(m_callbacks->m_lock);
        for (const std::optional<callback>& item: *m_callbacks) {
            if (item) (*item)(std::forward<Args>(args)...);
        }
    }

protected:
    friend H;

private:
    inline DelegateHandle createHandle(int idx) {
        return DelegateHandle([=]() {
            std::unique_lock lock(m_callbacks->m_lock);
            m_callbacks->at(idx).reset();
        });
    }

    std::shared_ptr<CallbackCollection> m_callbacks;
};
