#pragma once

#include <wrl/client.h>

template<typename T>
class DxWrapper {
public:
    T* operator->() const {
        return m_native.Get();
    }

    inline const ComPtr<T>& d3d12() const { return m_native; }

    inline T* ptr() const { return m_native.Get(); }

protected:
    ComPtr<T> m_native;
};