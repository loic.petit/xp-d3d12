#include "rendertarget.h"

#include "commandlist.h"
#include "device.h"
#include "utils/service.h"
#include "utils/utils.h"

static DXGI_FORMAT asTypeless(DXGI_FORMAT format);

static DXGI_FORMAT asResourceFormat(DXGI_FORMAT format);

static const D3D12_RECT DEFAULT_SCISSORS = CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);

RenderTarget::RenderTarget(std::wstring name) : m_name(std::move(name)) {
}

void RenderTarget::setRenderTarget(CommandList& list, uint32_t width, uint32_t height, size_t bufferCount, const Resource* buffers, const Resource* depthStencil) {
    D3D12_CPU_DESCRIPTOR_HANDLE handles[16];
    D3D12_VIEWPORT viewports[16];
    D3D12_RECT scissorRects[16];
    size_t viewportCount = bufferCount;

    for(int i = 0 ; i < bufferCount ; i++) {
        handles[i] = buffers[i].rtv();
        viewports[i] = CD3DX12_VIEWPORT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        scissorRects[i] = CD3DX12_RECT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        list.track(buffers[i].d3d12());
    }
    if (bufferCount == 0) {
        viewports[0] = CD3DX12_VIEWPORT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        scissorRects[0] = CD3DX12_RECT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        viewportCount = 1;
    }

    D3D12_CPU_DESCRIPTOR_HANDLE depthHandle{0};
    if (depthStencil != nullptr) {
        depthHandle = depthStencil->dsv();
        list.track(depthStencil->d3d12());
    }
    list->OMSetRenderTargets(bufferCount, handles, FALSE, depthStencil != nullptr ? &depthHandle : nullptr);
    list->RSSetViewports(viewportCount, viewports);
    list->RSSetScissorRects(viewportCount, scissorRects);
}
void RenderTarget::setRenderTarget(CommandList& list, uint32_t width, uint32_t height, size_t bufferCount, const Resource** buffers, const Resource* depthStencil) {
    D3D12_CPU_DESCRIPTOR_HANDLE handles[16];
    D3D12_VIEWPORT viewports[16];
    D3D12_RECT scissorRects[16];
    size_t viewportCount = bufferCount;

    for(int i = 0 ; i < bufferCount ; i++) {
        handles[i] = buffers[i]->rtv();
        viewports[i] = CD3DX12_VIEWPORT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        scissorRects[i] = CD3DX12_RECT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        list.track(buffers[i]->d3d12());
    }
    if (bufferCount == 0) {
        viewports[0] = CD3DX12_VIEWPORT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        scissorRects[0] = CD3DX12_RECT(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height));
        viewportCount = 1;
    }

    D3D12_CPU_DESCRIPTOR_HANDLE depthHandle{0};
    if (depthStencil != nullptr) {
        depthHandle = depthStencil->dsv();
        list.track(depthStencil->d3d12());
    }
    list->OMSetRenderTargets(bufferCount, handles, FALSE, depthStencil != nullptr ? &depthHandle : nullptr);
    list->RSSetViewports(viewportCount, viewports);
    list->RSSetScissorRects(viewportCount, scissorRects);
}

void RenderTarget::transition(CommandList& list, D3D12_RESOURCE_STATES renderTarget, D3D12_RESOURCE_STATES depthStencil) {
    for (auto& buffer: m_buffers) {
        list.state(buffer, renderTarget);
    }
    if (m_depthStencil.ptr() != nullptr) {
        list.state(m_depthStencil, depthStencil);
    }
}

uint32_t RenderTarget::createBuffer(const std::wstring& name, DXGI_FORMAT format, bool allowAsShaderResource) {
    auto& device = Get<Device>();
    uint32_t result = m_buffers.size();
    m_desc.pixelFormats[result] = format;
    // create the msaa buffer
    D3D12_RESOURCE_DESC textureDesc = CD3DX12_RESOURCE_DESC::Tex2D(format, m_desc.width, m_desc.height, 1, 1, m_desc.samples);
    textureDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

    D3D12_CLEAR_VALUE& clearValue = m_clearValuePixels[result];
    clearValue.Format = format;
    clearValue.Color[0] = 0.0f;
    clearValue.Color[1] = 0.0f;
    clearValue.Color[2] = 0.0f;
    clearValue.Color[3] = 0.0f;

    std::wstring bufferName = m_name + L": " + name;
    Resource& r = m_buffers.emplace_back(textureDesc, D3D12_RESOURCE_STATE_COMMON, &clearValue);
    ThrowIfFailed(r->SetName(bufferName.c_str()));

    DescriptorHeap& rtvHeap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    r.rtv(rtvHeap.cpu(rtvHeap.allocate()));
    device->CreateRenderTargetView(r.ptr(), nullptr, r.rtv());

    if (allowAsShaderResource) {
        DescriptorHeap& srvHeap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
        r.srv(srvHeap.cpu(srvHeap.allocate()));
        createSRV(r);
    }

    return result;
}

void RenderTarget::createDepthStencil(const std::wstring& name, DXGI_FORMAT format, bool allowAsShaderResource) {
    auto& device = Get<Device>();
    m_desc.depthFormat = format;
    // create a depth stencil
    D3D12_RESOURCE_DESC depthTextureDesc = CD3DX12_RESOURCE_DESC::Tex2D(m_desc.depthFormat, m_desc.width, m_desc.height, 1, 1, m_desc.samples);
    depthTextureDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
    if (!allowAsShaderResource) {
        depthTextureDesc.Flags |= D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;
    } else {
        depthTextureDesc.Format = asTypeless(m_desc.depthFormat);
    }

    m_clearValueDepth.Format = format;
    m_clearValueDepth.DepthStencil.Depth = 1.0f;
    m_clearValueDepth.DepthStencil.Stencil = 0;
    m_depthStencil = Resource(depthTextureDesc, D3D12_RESOURCE_STATE_COMMON, &m_clearValueDepth);
    std::wstring const bufferName = m_name + L": " + name;
    ThrowIfFailed(m_depthStencil->SetName(bufferName.c_str()));

    // allocate a dsv
    DescriptorHeap& dsvHeap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
    m_depthStencil.dsv(dsvHeap.cpu(dsvHeap.allocate()));

    createDSV(m_depthStencil);

    if (allowAsShaderResource) {
        DescriptorHeap& srvHeap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
        m_depthStencil.srv(srvHeap.cpu(srvHeap.allocate()));
        createSRV(m_depthStencil);
    }
}

void RenderTarget::createSRV(Resource& resource) {
    D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
    srvDesc.Format = asResourceFormat(resource->GetDesc().Format);
    srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
    if (m_desc.samples > 1) {
        srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DMS;
    } else {
        srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
        srvDesc.Texture2D.MipLevels = 1;
        srvDesc.Texture2D.MostDetailedMip = 0;
    }
    Get<Device>()->CreateShaderResourceView(resource.ptr(), &srvDesc, resource.srv());
}

void RenderTarget::clear(CommandList& list) {
    int i = 0;
    list.commitTransitions();
    for (auto& buffer: m_buffers) {
        list->ClearRenderTargetView(buffer.rtv(), m_clearValuePixels[i++].Color, 0, nullptr);
    }
    if (m_depthStencil.ptr() != nullptr) {
        list->ClearDepthStencilView(m_depthStencil.dsv(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, m_clearValueDepth.DepthStencil.Depth, m_clearValueDepth.DepthStencil.Stencil, 0, nullptr);
    }
}

void RenderTarget::copyInto(CommandList& list, RenderTarget& dest) {
    // copy
    const bool resolveDepthStencil = m_depthStencil.ptr() != nullptr && dest.m_depthStencil.ptr() != nullptr;
    if (resolveDepthStencil) {
        list.state(m_depthStencil, D3D12_RESOURCE_STATE_COPY_SOURCE)
            .state(dest.m_depthStencil, D3D12_RESOURCE_STATE_COPY_DEST);
    }
    for (auto & buffer : m_buffers) {
        list.state(buffer, D3D12_RESOURCE_STATE_COPY_SOURCE);
    }
    for (auto & buffer : dest.m_buffers) {
        list.state(buffer, D3D12_RESOURCE_STATE_COPY_DEST);
    }
    list.commitTransitions();
    for (uint32_t i = 0; i < m_buffers.size(); i++) {
        list->CopyResource(dest.m_buffers[i].ptr(), m_buffers[i].ptr());
    }
    if (resolveDepthStencil) {
        list->CopyResource(dest.m_depthStencil.ptr(), m_depthStencil.ptr());
    }
}

void RenderTarget::resolveInto(CommandList& list, RenderTarget& dest) {
    if (m_buffers.size() != dest.m_buffers.size() || m_desc.width != dest.m_desc.width || m_desc.height != dest.m_desc.height || dest.m_desc.samples > 1) {
        throw std::exception("KABOOM");
    }

    if (m_desc.samples <= 1) {
        copyInto(list, dest);
        return;
    }

    const bool resolveDepthStencil = m_depthStencil.ptr() != nullptr && dest.m_depthStencil.ptr() != nullptr && m_desc.samples == 1;
    if (resolveDepthStencil) {
        list.state(m_depthStencil, D3D12_RESOURCE_STATE_RESOLVE_SOURCE)
                .state(dest.m_depthStencil, D3D12_RESOURCE_STATE_RESOLVE_DEST);
    }
    for (auto & buffer : m_buffers) {
        list.state(buffer, D3D12_RESOURCE_STATE_RESOLVE_SOURCE);
    }
    for (auto & buffer : dest.m_buffers) {
        list.state(buffer, D3D12_RESOURCE_STATE_RESOLVE_DEST);
    }
    for (uint32_t i = 0; i < m_buffers.size(); i++) {
        list->ResolveSubresource(dest.m_buffers[i].ptr(), 0, m_buffers[i].ptr(), 0, m_desc.pixelFormats[i]);
    }
    if (resolveDepthStencil) {

        DXGI_FORMAT format = m_desc.depthFormat;
        if (format == DXGI_FORMAT_D24_UNORM_S8_UINT)
            format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
        list->ResolveSubresource(dest.m_depthStencil.ptr(), 0, m_depthStencil.ptr(), 0, format);
    }
}

void RenderTarget::prepareToPresent(CommandList& list, uint32_t idx) {
    list.state(getBuffer(idx), D3D12_RESOURCE_STATE_PRESENT);
}

void RenderTarget::init(uint32_t width, uint32_t height, uint32_t samples) {
    m_desc = {
            {},
            DXGI_FORMAT_UNKNOWN,
            width,
            height,
            samples
    };
    reset();
}

Resource& RenderTarget::getBuffer(uint32_t idx) {
    if (idx >= m_buffers.size())
        throw std::exception("KABOOM");
    return m_buffers[idx];
}

Resource& RenderTarget::getDepthStencil() {
    if (m_depthStencil.ptr() == nullptr)
        throw std::exception("KABOOM");
    return m_depthStencil;
}

void RenderTarget::reset() {
    m_buffers.clear();
    m_depthStencil.clear();
}

void RenderTarget::addExistingBuffer(ComPtr<ID3D12Resource>& buffer, D3D12_CPU_DESCRIPTOR_HANDLE& handle) {
    uint32_t id = m_buffers.size();
    m_desc.pixelFormats[id] = buffer->GetDesc().Format;
    Resource& r = m_buffers.emplace_back(buffer);
    r.rtv(handle);
    ThrowIfFailed(r->SetName(m_name.c_str()));
    createRTV(r);
}

void RenderTarget::resize(uint32_t width, uint32_t height) {
    m_desc.width = width;
    m_desc.height = height;

    for (int i = 0 ; i < m_buffers.size() ; i++) {
        resizeResource(m_buffers[i], m_clearValuePixels[i]);
    }
    if (m_depthStencil.ptr() != nullptr) {
        resizeResource(m_depthStencil, m_clearValueDepth);
    }
}

void RenderTarget::resizeResource(Resource& resource, const D3D12_CLEAR_VALUE& clearValue) {
    D3D12_RESOURCE_DESC desc = resource->GetDesc();
    desc.Width = m_desc.width;
    desc.Height = m_desc.height;
    const D3D12_CPU_DESCRIPTOR_HANDLE dsv = resource.dsv();
    const D3D12_CPU_DESCRIPTOR_HANDLE srv = resource.srv();
    const D3D12_CPU_DESCRIPTOR_HANDLE rtv = resource.rtv();
    wchar_t name[128] = {};
    UINT size = sizeof(name);
    ThrowIfFailed(resource->GetPrivateData(WKPDID_D3DDebugObjectNameW, &size, name));
    resource = Resource(desc, D3D12_RESOURCE_STATE_COMMON, &clearValue);
    ThrowIfFailed(resource->SetName(name));
    resource.srv(srv);
    resource.dsv(dsv);
    resource.rtv(rtv);
    if (dsv.ptr != 0) {
        createDSV(resource);
    }
    if (srv.ptr != 0) {
        createSRV(resource);
    }
    if (rtv.ptr != 0) {
        createRTV(resource);
    }
}

void RenderTarget::createDSV(Resource& resource) {
    D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
    dsvDesc.Format = m_desc.depthFormat;
    if (m_desc.samples > 1) {
        dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2DMS;
    } else {
        dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
        dsvDesc.Texture2D.MipSlice = 0;
    }
    Get<Device>()->CreateDepthStencilView(resource.ptr(), &dsvDesc, resource.dsv());
}

void RenderTarget::createRTV(Resource& resource) {
    Get<Device>()->CreateRenderTargetView(resource.ptr(), nullptr, resource.rtv());
}

static DXGI_FORMAT asResourceFormat(DXGI_FORMAT format) {
    DXGI_FORMAT resourceFormat = format;

    switch (format) {
        case DXGI_FORMAT_R16_TYPELESS:
        case DXGI_FORMAT_R16_UNORM:
            resourceFormat = DXGI_FORMAT_R16_UNORM;
        break;
        case DXGI_FORMAT_R32_TYPELESS:
        case DXGI_FORMAT_D32_FLOAT:
            resourceFormat = DXGI_FORMAT_R32_FLOAT;
            break;
        case DXGI_FORMAT_R24G8_TYPELESS:
            resourceFormat = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
            break;
        default:
            break;
    }

    return resourceFormat;
}

static DXGI_FORMAT asTypeless(DXGI_FORMAT format) {
    DXGI_FORMAT typelessFormat = format;

    switch (format) {
        case DXGI_FORMAT_R32G32B32A32_FLOAT:
        case DXGI_FORMAT_R32G32B32A32_UINT:
        case DXGI_FORMAT_R32G32B32A32_SINT:
            typelessFormat = DXGI_FORMAT_R32G32B32A32_TYPELESS;
            break;
        case DXGI_FORMAT_R32G32B32_FLOAT:
        case DXGI_FORMAT_R32G32B32_UINT:
        case DXGI_FORMAT_R32G32B32_SINT:
            typelessFormat = DXGI_FORMAT_R32G32B32_TYPELESS;
            break;
        case DXGI_FORMAT_R16G16B16A16_FLOAT:
        case DXGI_FORMAT_R16G16B16A16_UNORM:
        case DXGI_FORMAT_R16G16B16A16_UINT:
        case DXGI_FORMAT_R16G16B16A16_SNORM:
        case DXGI_FORMAT_R16G16B16A16_SINT:
            typelessFormat = DXGI_FORMAT_R16G16B16A16_TYPELESS;
            break;
        case DXGI_FORMAT_R32G32_FLOAT:
        case DXGI_FORMAT_R32G32_UINT:
        case DXGI_FORMAT_R32G32_SINT:
            typelessFormat = DXGI_FORMAT_R32G32_TYPELESS;
            break;
        case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
            typelessFormat = DXGI_FORMAT_R32G8X24_TYPELESS;
            break;
        case DXGI_FORMAT_R10G10B10A2_UNORM:
        case DXGI_FORMAT_R10G10B10A2_UINT:
            typelessFormat = DXGI_FORMAT_R10G10B10A2_TYPELESS;
            break;
        case DXGI_FORMAT_R8G8B8A8_UNORM:
        case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
        case DXGI_FORMAT_R8G8B8A8_UINT:
        case DXGI_FORMAT_R8G8B8A8_SNORM:
        case DXGI_FORMAT_R8G8B8A8_SINT:
            typelessFormat = DXGI_FORMAT_R8G8B8A8_TYPELESS;
            break;
        case DXGI_FORMAT_R16G16_FLOAT:
        case DXGI_FORMAT_R16G16_UNORM:
        case DXGI_FORMAT_R16G16_UINT:
        case DXGI_FORMAT_R16G16_SNORM:
        case DXGI_FORMAT_R16G16_SINT:
            typelessFormat = DXGI_FORMAT_R16G16_TYPELESS;
            break;
        case DXGI_FORMAT_D32_FLOAT:
        case DXGI_FORMAT_R32_FLOAT:
        case DXGI_FORMAT_R32_UINT:
        case DXGI_FORMAT_R32_SINT:
            typelessFormat = DXGI_FORMAT_R32_TYPELESS;
            break;
        case DXGI_FORMAT_R8G8_UNORM:
        case DXGI_FORMAT_R8G8_UINT:
        case DXGI_FORMAT_R8G8_SNORM:
        case DXGI_FORMAT_R8G8_SINT:
            typelessFormat = DXGI_FORMAT_R8G8_TYPELESS;
            break;
        case DXGI_FORMAT_R16_FLOAT:
        case DXGI_FORMAT_D16_UNORM:
        case DXGI_FORMAT_R16_UNORM:
        case DXGI_FORMAT_R16_UINT:
        case DXGI_FORMAT_R16_SNORM:
        case DXGI_FORMAT_R16_SINT:
            typelessFormat = DXGI_FORMAT_R16_TYPELESS;
            break;
        case DXGI_FORMAT_R8_UNORM:
        case DXGI_FORMAT_R8_UINT:
        case DXGI_FORMAT_R8_SNORM:
        case DXGI_FORMAT_R8_SINT:
            typelessFormat = DXGI_FORMAT_R8_TYPELESS;
            break;
        case DXGI_FORMAT_D24_UNORM_S8_UINT:
            typelessFormat = DXGI_FORMAT_R24G8_TYPELESS;
            break;
        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC1_UNORM_SRGB:
            typelessFormat = DXGI_FORMAT_BC1_TYPELESS;
            break;
        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
            typelessFormat = DXGI_FORMAT_BC2_TYPELESS;
            break;
        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
            typelessFormat = DXGI_FORMAT_BC3_TYPELESS;
            break;
        case DXGI_FORMAT_BC4_UNORM:
        case DXGI_FORMAT_BC4_SNORM:
            typelessFormat = DXGI_FORMAT_BC4_TYPELESS;
            break;
        case DXGI_FORMAT_BC5_UNORM:
        case DXGI_FORMAT_BC5_SNORM:
            typelessFormat = DXGI_FORMAT_BC5_TYPELESS;
            break;
        case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
            typelessFormat = DXGI_FORMAT_B8G8R8A8_TYPELESS;
            break;
        case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
            typelessFormat = DXGI_FORMAT_B8G8R8X8_TYPELESS;
            break;
        case DXGI_FORMAT_BC6H_UF16:
        case DXGI_FORMAT_BC6H_SF16:
            typelessFormat = DXGI_FORMAT_BC6H_TYPELESS;
            break;
        case DXGI_FORMAT_BC7_UNORM:
        case DXGI_FORMAT_BC7_UNORM_SRGB:
            typelessFormat = DXGI_FORMAT_BC7_TYPELESS;
            break;
        default:
            break;
    }

    return typelessFormat;
}