#include "queue.h"
#include "device.h"
#include "utils/logger.h"
#include "commandlist.h"
#include "utils/service.h"
#include "utils/utils.h"

Queue::Queue(D3D12_COMMAND_LIST_TYPE type, int32_t priority, D3D12_COMMAND_QUEUE_FLAGS flags, uint32_t nodeMask) :
        m_queueDescription({type, priority, flags, nodeMask}){
    switch (type) {
        case D3D12_COMMAND_LIST_TYPE_DIRECT:
            m_typeAsString = L"Direct";
            break;
        case D3D12_COMMAND_LIST_TYPE_BUNDLE:
            m_typeAsString = L"Bundle";
            break;
        case D3D12_COMMAND_LIST_TYPE_COMPUTE:
            m_typeAsString = L"Compute";
            break;
        case D3D12_COMMAND_LIST_TYPE_COPY:
            m_typeAsString = L"Copy";
            break;
        case D3D12_COMMAND_LIST_TYPE_VIDEO_DECODE:
            m_typeAsString = L"Decode";
            break;
        case D3D12_COMMAND_LIST_TYPE_VIDEO_PROCESS:
            m_typeAsString = L"Video Process";
            break;
        case D3D12_COMMAND_LIST_TYPE_VIDEO_ENCODE:
            m_typeAsString = L"Video Encode";
            break;
    }
}

void Queue::init(const std::wstring& name) {
    auto& device = Get<Device>();
    ThrowIfFailed(device->CreateCommandQueue(&m_queueDescription, IID_PPV_ARGS(&m_native)));
    ThrowIfFailed(m_native->SetName(name.c_str()));
    ThrowIfFailed(device->CreateFence(m_fenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence)));
    ThrowIfFailed(m_fence->SetName((name + L" Fence").c_str()));
    m_beginFlush = device.beginFlush.connect(this, &Queue::flush);
    m_endFlush = device.endFlush.connect(this, &Queue::resume);
}

std::shared_ptr<CommandList> Queue::createCommandList(ID3D12PipelineState* initPipeline) {
    std::unique_lock lock(m_mutex);

    m_flushing.wait(lock, [&]() { return !m_isFlushing || m_isClosed; });
    if (m_isClosed) {
        throw std::exception("You're trying to create a command list on a queue that is closed... kaboom!");
    }

    // process flight queue
    if (!m_inFlightQueue.empty()) {
        std::pair<std::shared_ptr<CommandList>, uint64_t>* entry = &m_inFlightQueue.front();
        const uint64_t currentFenceValue = m_fence->GetCompletedValue();
        while (currentFenceValue >= entry->second) {
            entry->first->reset(initPipeline);
            m_availableLists.push(entry->first);
            m_inFlightQueue.pop();
            if (m_inFlightQueue.empty()) break;
            entry = &m_inFlightQueue.front();
        }
    }

    std::shared_ptr<CommandList> result;
    // If there is a command list on the queue.
    if (!m_availableLists.empty()) {
        result = m_availableLists.front();
        m_availableLists.pop();
    } else {
        auto& device = Get<Device>();
        ComPtr<ID3D12CommandAllocator> allocator;
        ComPtr<ID3D12GraphicsCommandList> commandList;
        ThrowIfFailed(device->CreateCommandAllocator(m_queueDescription.Type, IID_PPV_ARGS(&allocator)));
        ThrowIfFailed(device->CreateCommandList(m_queueDescription.NodeMask, m_queueDescription.Type, allocator.Get(), initPipeline, IID_PPV_ARGS(&commandList)));
        result = std::make_shared<CommandList>(commandList, allocator, m_typeAsString + L" #" + std::to_wstring(m_lists++));
    }
    m_inPreparationLists.emplace_back(result);
    return result;
}

bool Queue::isFenceReady(uint64_t fenceValue) {
    return m_fence->GetCompletedValue() >= fenceValue;
}

void Queue::waitForFence(uint64_t fenceValue) {
    if (!isFenceReady(fenceValue)) {
        auto event = ::CreateEvent(nullptr, false, false, nullptr);
        if (event) {
            // Is this function thread safe?
            ThrowIfFailed(m_fence->SetEventOnCompletion(fenceValue, event));
            ::WaitForSingleObject(event, UINT32_MAX);

            ::CloseHandle(event);
        }
    }
}


void Queue::flush(bool isFinished) {
    std::unique_lock lock(m_mutex);
    if (!m_fence) return;
    m_isFlushing = true;
    if (!m_inPreparationLists.empty())
        clearInPreparation();
    while(!m_inPreparationCondition.wait_for(lock, std::chrono::milliseconds(20), [&]() {
        return m_inPreparationLists.empty();
    })) {
        clearInPreparation();
    }

    // wait for the latest fence to be submitted
    waitForFence(m_fenceValue);
    // all in flights are clear
    while (!m_inFlightQueue.empty()) {
        m_inFlightQueue.front().first->reset();
        m_availableLists.push(m_inFlightQueue.front().first);
        m_inFlightQueue.pop();
    }
    m_isClosed = isFinished;
    m_isFlushing = false;
    m_flushing.notify_all();
}

uint64_t Queue::signal() {
    ThrowIfFailed(m_native->Signal(m_fence.Get(), ++m_fenceValue));
    return m_fenceValue;
}

uint64_t Queue::waitForQueue(const Queue& queue) {
    ThrowIfFailed(m_native->Wait(queue.m_fence.Get(), queue.m_fenceValue));
    return signal();
}
template <typename T, typename Predicate>
static size_t removeIf(std::vector<T>& vec, Predicate pred)
{
    auto newEnd = std::remove_if(vec.begin(), vec.end(), pred);
    size_t removedCount = std::distance(newEnd, vec.end());
    vec.erase(newEnd, vec.end());
    return removedCount;
}

uint64_t Queue::execute(uint32_t n, std::shared_ptr<CommandList>* list) {
    if (n == 1) {
        (*list)->close();
        ID3D12CommandList* array = (*list)->ptr();
        m_native->ExecuteCommandLists(1, &array);
    } else if (n > 1) {
        std::vector<ID3D12CommandList*> array(n);
        for (int i = 0; i < n; i++) {
            list[i]->close();
            array[i] = list[i]->ptr();
        }
        m_native->ExecuteCommandLists(n, array.data());
    }
    uint64_t targetFence = signal();
    {
        std::unique_lock lock(m_mutex);
#ifdef _DEBUG
        clearInPreparation();
#endif
        for (int i = 0; i < n; i++) {
            const std::shared_ptr<CommandList>& val = list[i];
            removeIf(m_inPreparationLists, [val](auto& weak) {
                return weak.expired() || weak.lock() == val;
            });
            m_inFlightQueue.emplace(std::move(list[i]), targetFence);
            m_inPreparationCondition.notify_all();
        }
    }
    return targetFence;
}

void Queue::clearInPreparation() {
    auto erased = removeIf(m_inPreparationLists, [](auto& weak) {
        return weak.expired();
    });
    if (erased) {
        LOG_WARN("You had %u orphaned command list, avoid fetching a command list to do nothing with it!", erased);
    }
}

Queue::~Queue() {
    flush(true);
}

void Queue::resume() {
    std::unique_lock lock(m_mutex);
    m_isFlushing = false;
    m_flushing.notify_all();
}
