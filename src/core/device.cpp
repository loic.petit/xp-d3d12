#include "device.h"
#include "utils/logger.h"
#include "commandlist.h"

#if defined(_DEBUG)

#include "dxgidebug.h"

#endif

#include "utils/markers.h"
#include "utils/utils.h"

#ifdef _DEBUG
#define ENABLE_DX_DEBUG
#endif

constexpr D3D_FEATURE_LEVEL MINIMUM_FEATURE_LEVEL = D3D_FEATURE_LEVEL_12_0;

constexpr uint32_t DESCRIPTOR_DEFAULT_HEAP_SIZE = 128;

void Device::enableDebugLayer() {
#if defined(ENABLE_DX_DEBUG)
    ComPtr<ID3D12Debug> debugInterface;
    ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)));
    debugInterface->EnableDebugLayer();

    ComPtr<ID3D12Debug1> spDebugController1;
    ThrowIfFailed(debugInterface->QueryInterface(IID_PPV_ARGS(&spDebugController1)));
    spDebugController1->SetEnableGPUBasedValidation(true);
#endif
}

void Device::reportLiveObjects() {
#if defined(ENABLE_DX_DEBUG)
    IDXGIDebug1* dxgiDebug;
    ThrowIfFailed(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiDebug)));

    ThrowIfFailed(dxgiDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_IGNORE_INTERNAL));
    dxgiDebug->Release();
#endif
}

/**
 * Util function to get the best adapter to create a D3D12 device on
 *
 * @param dxgiFactory The factory to build upon (cannot be null)
 * @return An adapter (cannot be null otherwise a runtime exception is thrown)
 */
ComPtr<IDXGIAdapter4> getAdapter(const ComPtr<IDXGIFactory4>& dxgiFactory) {
    ComPtr<IDXGIAdapter4> dxgiAdapter4;
    // fetch the adapter with the largest memory
    {
        ComPtr<IDXGIAdapter1> dxgiAdapter1;
        SIZE_T maxDedicatedVideoMemory = 0;
        for (UINT i = 0; dxgiFactory->EnumAdapters1(i, &dxgiAdapter1) != DXGI_ERROR_NOT_FOUND; ++i) {
            DXGI_ADAPTER_DESC1 desc;
            ThrowIfFailed(dxgiAdapter1->GetDesc1(&desc));

            if ((desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0 &&
                SUCCEEDED(D3D12CreateDevice(dxgiAdapter1.Get(),
                                            MINIMUM_FEATURE_LEVEL, __uuidof(ID3D12Device), nullptr)) &&
                desc.DedicatedVideoMemory > maxDedicatedVideoMemory) {
                maxDedicatedVideoMemory = desc.DedicatedVideoMemory;
                ThrowIfFailed(dxgiAdapter1.As(&dxgiAdapter4));
            }
        }
    }
    if (!dxgiAdapter4) {
        LOG_INFO("Using a WARP adapter");
        // fallback to warp
        ComPtr<IDXGIAdapter1> dxgiAdapter1;
        ThrowIfFailed(dxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&dxgiAdapter1)));
        ThrowIfFailed(dxgiAdapter1.As(&dxgiAdapter4));
    } else {
        DXGI_ADAPTER_DESC1 desc;
        ThrowIfFailed(dxgiAdapter4->GetDesc1(&desc));
        LOG_INFO("Using adapter [ %S ]", desc.Description);
    }

    return dxgiAdapter4;
}

Device::Device() : m_baseDescriptorHeaps(D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES), m_rootSignatureVersion() {
}

void Device::init() {
    enableDebugLayer();
    // create dxgi factory
    UINT createFactoryFlags = 0;
#if defined(ENABLE_DX_DEBUG)
    createFactoryFlags = DXGI_CREATE_FACTORY_DEBUG;
#endif
    ThrowIfFailed(CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(&m_dxgiFactory)));

    {
        // create device
        const auto adapter = getAdapter(m_dxgiFactory);
        ThrowIfFailed(D3D12CreateDevice(adapter.Get(), MINIMUM_FEATURE_LEVEL, IID_PPV_ARGS(&m_native)));
        ThrowIfFailed(m_native->SetName(L"Main D3D12 Device"));
        LOG_INFO("Created D3D12Device");
    }

    // check root signature version
    {
        D3D12_FEATURE_DATA_ROOT_SIGNATURE feature;
        feature.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
        if (FAILED(m_native->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &feature,
                                                 sizeof(D3D12_FEATURE_DATA_ROOT_SIGNATURE)))) {
            feature.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
        }
        m_rootSignatureVersion = feature.HighestVersion;
        LOG_INFO("Best root signature version: 1.%d", m_rootSignatureVersion - 1);
    }

    // create the base descriptor heaps
    {
        for (D3D12_DESCRIPTOR_HEAP_TYPE type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV; type < D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES; type = static_cast<D3D12_DESCRIPTOR_HEAP_TYPE>(type + 1)) {
            std::wstring name = L"Base descriptor heap: ";
            switch (type) {
                case D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV:
                    name += L"CBV/SRV/UAV";
                    break;
                case D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER:
                    name += L"Sampler";
                    break;
                case D3D12_DESCRIPTOR_HEAP_TYPE_RTV:
                    name += L"RTV";
                    break;
                case D3D12_DESCRIPTOR_HEAP_TYPE_DSV:
                    name += L"DSV";
                    break;
                default:
                    throw std::exception("KABOOM!");
            }
            m_baseDescriptorHeaps.emplace_back(name, type, DESCRIPTOR_DEFAULT_HEAP_SIZE);
        }
        LOG_INFO("Created base descriptor heaps");
    }
}

void Device::setLowLatencyMode(bool lowLatencyMode, bool lowLatencyBoost, uint16_t maxFps, bool useMarkers) const {
#ifdef HAS_REFLEX_MARKERS
    NvAPI_Status status = NVAPI_OK;
    NV_SET_SLEEP_MODE_PARAMS params = {};
    params.version = NV_SET_SLEEP_MODE_PARAMS_VER1;
    params.bLowLatencyMode = lowLatencyMode;
    params.bLowLatencyBoost = lowLatencyBoost;
    params.minimumIntervalUs = maxFps ? 1000000/maxFps : 0;
    params.bUseMarkersToOptimize = useMarkers;
    status = NvAPI_D3D_SetSleepMode(ptr(), &params);
    if (status != NVAPI_OK) {
        LOG_ERROR("LowLatencyMode: Failed to apply new sleep mode %d", status);
    }
#endif
}

ComPtr<ID3D12RootSignature> Device::createCompatibleRootSignature(CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& description) {
    ComPtr<ID3D12RootSignature> rootSignature;
    // Serialize the root signature.
    ComPtr<ID3DBlob> rootSignatureBlob;
    ComPtr<ID3DBlob> errorBlob;
    ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&description, m_rootSignatureVersion, &rootSignatureBlob, &errorBlob));

    if (errorBlob.Get() != nullptr) {
        // kaboom
        char* const message = (char*) errorBlob->GetBufferPointer();
        throw std::exception(message);
    }

    // Create the root signature.
    ThrowIfFailed(m_native->CreateRootSignature(0, rootSignatureBlob->GetBufferPointer(),
                                                rootSignatureBlob->GetBufferSize(),
                                                IID_PPV_ARGS(&rootSignature)));
    return rootSignature;
}

Resource Device::uploadBuffer(CommandList& copyList, const void* ptr, size_t size) {
    Resource r(CD3DX12_RESOURCE_DESC::Buffer(size));
    Resource upload(CD3DX12_RESOURCE_DESC::Buffer(size), D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, D3D12_HEAP_TYPE_UPLOAD);
    ThrowIfFailed(upload->SetName(L"Upload buffer"));
    void* mem;
    D3D12_RANGE emptyRange{0, 0};
    ThrowIfFailed(upload->Map(0, &emptyRange, &mem));
    memcpy(mem, ptr, size);
    upload->Unmap(0, nullptr);

    copyList->CopyBufferRegion(r.ptr(), 0, upload.ptr(), 0, size);

    copyList.track(upload.d3d12());

    return r;
}

Resource Device::downloadBufferInto(CommandList& copyList, Resource& resource, size_t& size) {
    D3D12_RESOURCE_DESC desc = resource->GetDesc();
    Resource download(CD3DX12_RESOURCE_DESC::Buffer(desc.Width), D3D12_RESOURCE_STATE_COPY_DEST, nullptr, D3D12_HEAP_TYPE_READBACK);
    ThrowIfFailed(download->SetName(L"Download buffer"));

    copyList.state(resource, D3D12_RESOURCE_STATE_COPY_SOURCE).commitTransitions();
    copyList->CopyBufferRegion(download.ptr(), 0, resource.ptr(), 0, desc.Width);
    size = desc.Width;
    copyList.track(download.d3d12());

    return download;
}

Resource Device::downloadTextureInto(CommandList& copyList, Resource& resource, uint32_t sub, D3D12_SUBRESOURCE_FOOTPRINT& footprintOut) {
    UINT64 textureDownloadBufferSize;
    const D3D12_RESOURCE_DESC& desc = resource->GetDesc();
    std::vector<D3D12_PLACED_SUBRESOURCE_FOOTPRINT> footprints(desc.MipLevels);
    m_native->GetCopyableFootprints(&desc, 0, desc.MipLevels, 0, footprints.data(), nullptr, nullptr, &textureDownloadBufferSize);
    Resource download(CD3DX12_RESOURCE_DESC::Buffer(textureDownloadBufferSize), D3D12_RESOURCE_STATE_COPY_DEST, nullptr, D3D12_HEAP_TYPE_READBACK);
    ThrowIfFailed(download->SetName(L"Download buffer"));

    footprints[sub].Offset = 0;
    CD3DX12_TEXTURE_COPY_LOCATION dst(download.ptr(), footprints[sub]);
    CD3DX12_TEXTURE_COPY_LOCATION src(resource.ptr(), sub);
    copyList->CopyTextureRegion(&dst, 0, 0, 0, &src, nullptr);
    footprintOut = footprints[sub].Footprint;
    copyList.track(download.d3d12());

    return download;
}

Resource Device::uploadTexture(CommandList& copyList, const D3D12_RESOURCE_DESC& desc, const void* ptr, size_t size) {
    Resource r(desc);
    UINT64 textureUploadBufferSize;
    D3D12_PLACED_SUBRESOURCE_FOOTPRINT footprint;
    m_native->GetCopyableFootprints(&desc, 0, 1, 0, &footprint, nullptr, nullptr, &textureUploadBufferSize);
    Resource upload(CD3DX12_RESOURCE_DESC::Buffer(textureUploadBufferSize), D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, D3D12_HEAP_TYPE_UPLOAD);
    ThrowIfFailed(upload->SetName(L"Upload texture buffer"));
    void* mem;

    D3D12_RANGE emptyRange{0, 0};
    ThrowIfFailed(upload->Map(0, &emptyRange, &mem));

    size_t rowPitchSrc = (size / footprint.Footprint.Height);
    for (UINT y = 0; y < footprint.Footprint.Height; ++y) {
        memcpy(reinterpret_cast<char*>(mem) + footprint.Footprint.RowPitch * y,
               reinterpret_cast<const char*>(ptr) + rowPitchSrc * y,
               rowPitchSrc);
    }

    upload->Unmap(0, nullptr);

    CD3DX12_TEXTURE_COPY_LOCATION dst(r.ptr(), 0);
    CD3DX12_TEXTURE_COPY_LOCATION src(upload.ptr(), footprint);
    copyList->CopyTextureRegion(&dst, 0, 0, 0, &src, nullptr);

    copyList.track(upload.d3d12());

    return r;
}
