#pragma once

#include "descriptorheap.h"
#include "resource.h"
#include "utils/nmvector.h"

class Resource;
class BufferPage : public DxWrapper<ID3D12Resource> {
public:
    explicit BufferPage(const ComPtr<ID3D12Resource>& buffer);

    ~BufferPage();

    void reset();

    D3D12_GPU_VIRTUAL_ADDRESS upload(const void* ptr, size_t size, size_t i);

private:
    uint8_t* m_cpu;
    uint32_t m_allocated;
};

class CommandList : public DxWrapper<ID3D12GraphicsCommandList> {
public:
    explicit CommandList(const ComPtr<ID3D12GraphicsCommandList>& list, const ComPtr<ID3D12CommandAllocator>& allocator, const std::wstring& name);

    CommandList(const CommandList& o) = delete;

    CommandList(CommandList&& o) noexcept = delete;

    CommandList& operator=(const CommandList& o) noexcept = delete;

    CommandList& operator=(CommandList&& o) noexcept = delete;

    void reset(ID3D12PipelineState* initPipeline = nullptr);

    DescriptorHeap& getHeap(D3D12_DESCRIPTOR_HEAP_TYPE type);

    void close();

    inline CommandList& stateEnd(Resource& resource) {
        resource.stateEnd();
        m_trackedTransitions.emplace_back(&resource);
        return *this;
    }

    inline CommandList& stateBegin(Resource& resource, D3D12_RESOURCE_STATES state) {
        resource.stateBegin(state);
        m_trackedTransitions.emplace_back(&resource);
        return *this;
    }

    inline CommandList& state(Resource& resource, D3D12_RESOURCE_STATES state) {
        resource.state(state);
        m_trackedTransitions.emplace_back(&resource);
        return *this;
    }

    template<typename RT>
    inline CommandList& renderTargetWrite(RT& renderTarget) {
        renderTarget.transitionWrite(*this);
        return *this;
    }

    void track(const ComPtr<ID3D12Resource>& resource);

    void track(Resource& resource);

    void commitTransitions();

    D3D12_GPU_VIRTUAL_ADDRESS uploadTemporary(const void* ptr, size_t size);

    template<typename E>
    inline D3D12_GPU_VIRTUAL_ADDRESS uploadTemporary(const E& e) {
        return uploadTemporary(&e, sizeof(E));
    }

    BufferPage& getCurrentBuffer() const {
        return *m_availableBuffers.front();
    }

private:
    ComPtr<ID3D12CommandAllocator> m_allocator;
    std::vector<ComPtr<ID3D12Resource>> m_trackedResources;
    std::vector<Resource*> m_trackedTransitions;
    NmVector<DescriptorHeap> m_descriptorHeaps;

    std::queue<std::unique_ptr<BufferPage>> m_fullBuffers;
    std::queue<std::unique_ptr<BufferPage>> m_availableBuffers;
    std::wstring m_baseName;
};