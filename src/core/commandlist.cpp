#include "commandlist.h"

#include "device.h"
#include "resource.h"
#include "utils/logger.h"
#include "utils/utils.h"

using namespace Microsoft::WRL;

constexpr size_t UPLOAD_BUFFER_SIZE = 2 * 1024 * 1024;
constexpr uint32_t CBV_SRV_UAV_HEAP_SIZE = 128;
constexpr uint32_t SAMPLER_HEAP_SIZE = 16;

void CommandList::reset(ID3D12PipelineState* initPipeline) {
    ThrowIfFailed(m_allocator->Reset());
    ThrowIfFailed(m_native->Reset(m_allocator.Get(), initPipeline));
    m_trackedResources.clear();
    m_trackedTransitions.clear();

    if (!m_availableBuffers.empty()) {
        m_availableBuffers.front()->reset();
    }
    while (!m_fullBuffers.empty()) {
        std::unique_ptr<BufferPage>& buf = m_fullBuffers.front();
        buf->reset();
        m_availableBuffers.emplace(std::move(buf));
        m_fullBuffers.pop();
    }
    if (!m_descriptorHeaps.empty()) {
        m_descriptorHeaps[D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV].reset();
        m_descriptorHeaps[D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER].reset();
    }
}

CommandList::CommandList(const ComPtr<ID3D12GraphicsCommandList>& list, const ComPtr<ID3D12CommandAllocator>& allocator, const std::wstring& name) :
        m_allocator(allocator), m_baseName(name), m_descriptorHeaps(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER+1) {
    ThrowIfFailed(allocator->SetName((L"Allocator " + name).c_str()));
    ThrowIfFailed(list->SetName((L"List " + name).c_str()));
    m_native = list;
    LOG_INFO("Creating a new command list %ls", name.c_str());
}

void CommandList::track(const ComPtr<ID3D12Resource>& resource) {
    m_trackedResources.emplace_back(resource);
}

void CommandList::track(Resource& resource) {
    track(resource.d3d12());
    m_trackedTransitions.emplace_back(&resource);
}

D3D12_GPU_VIRTUAL_ADDRESS CommandList::uploadTemporary(const void* ptr, size_t size) {
    // align size to 256
    size_t allocatedSize = (size + 255) & (~255);
    if (!m_availableBuffers.empty()) {
        D3D12_GPU_VIRTUAL_ADDRESS result = m_availableBuffers.front()->upload(ptr, size, allocatedSize);
        if (result != 0) {
            return result;
        }
        // buffer is full
        m_fullBuffers.emplace(std::move(m_availableBuffers.front()));
        m_availableBuffers.pop();
    }
    if (m_availableBuffers.empty()) {
        Resource newBuffer(CD3DX12_RESOURCE_DESC::Buffer(UPLOAD_BUFFER_SIZE), D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, D3D12_HEAP_TYPE_UPLOAD);
        LOG_INFO("Allocating a new temporary buffer (this should be limited!)");
        m_availableBuffers.emplace(std::make_unique<BufferPage>(newBuffer.d3d12()));
    }
    return m_availableBuffers.front()->upload(ptr, size, allocatedSize);
}

DescriptorHeap& CommandList::getHeap(D3D12_DESCRIPTOR_HEAP_TYPE type) {
    if (type > D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER) {
        throw std::exception("Allocating an unknown descriptor!");
    }
    if (m_descriptorHeaps.empty()) {
        m_descriptorHeaps.emplace_back(L"Heap CBV/SRV/UAV " + m_baseName, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, CBV_SRV_UAV_HEAP_SIZE, true);
        m_descriptorHeaps.emplace_back(L"Heap Sampler " + m_baseName, D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER, SAMPLER_HEAP_SIZE, true);
    }
    return m_descriptorHeaps[type];
}

void CommandList::close() {
    commitTransitions();
    ThrowIfFailed(m_native->Close());
}

void CommandList::commitTransitions() {
    if (m_trackedTransitions.empty()) return;
    std::vector<D3D12_RESOURCE_BARRIER> barrier(m_trackedTransitions.size());
    int size = 0;
    for (auto & resource : m_trackedTransitions) {
        if (resource->commitTransition(barrier[size])) {
            size++;
        }
    }
    if (size != 0) {
        m_native->ResourceBarrier(size, barrier.data());
    }
    m_trackedTransitions.clear();
}

BufferPage::BufferPage(const ComPtr<ID3D12Resource>& buffer) : m_allocated(0), m_cpu(nullptr) {
    D3D12_RANGE range {0, 0};
    ThrowIfFailed(buffer->Map(0, &range, (void**) &m_cpu));
    m_native = buffer;
}

BufferPage::~BufferPage() {
    D3D12_RANGE range {0, m_allocated};
    m_native->Unmap(0, &range);
}

D3D12_GPU_VIRTUAL_ADDRESS BufferPage::upload(const void* ptr, size_t size, size_t allocatedSize) {
    if ((m_allocated + size) > UPLOAD_BUFFER_SIZE) return 0;
    memcpy(m_cpu + m_allocated, ptr, size);
    const D3D12_GPU_VIRTUAL_ADDRESS gpu = m_native->GetGPUVirtualAddress() + m_allocated;
    m_allocated += allocatedSize;
    return gpu;
}

void BufferPage::reset() {
    m_allocated = 0;
}
