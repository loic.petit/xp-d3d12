#pragma once

#include <dxgi1_6.h>

#include "dxwrapper.h"
#include "event.h"
#include "resource.h"
#include "descriptorheap.h"
#include "utils/nmvector.h"

class CommandList;

// MUST BE THREAD SAFE
class Device : public DxWrapper<ID3D12Device2> {
public:
    using BeginFlushEvent = Delegate<Device, void(bool isFinished)>;
    using EndFlushEvent = Delegate<Device, void()>;

    explicit Device();

    ~Device() {
        beginFlush(true);
    }

    void init();

    static void enableDebugLayer();
    static void reportLiveObjects();

    BeginFlushEvent beginFlush;
    EndFlushEvent endFlush;

    DescriptorHeap& getHeap(D3D12_DESCRIPTOR_HEAP_TYPE type) {
        return m_baseDescriptorHeaps[type];
    }

    Resource uploadBuffer(CommandList& copyList, const void* ptr, size_t size);

    Resource uploadTexture(CommandList& copyList, const D3D12_RESOURCE_DESC& desc, const void* ptr, size_t size);

    Resource downloadBufferInto(CommandList& copyList, Resource& resource, size_t& size);

    Resource downloadTextureInto(CommandList& copyList, Resource& resource, uint32_t sub, D3D12_SUBRESOURCE_FOOTPRINT& footprintOut);

    ComPtr<ID3D12RootSignature> createCompatibleRootSignature(CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& description);

    [[nodiscard]] inline const ComPtr<IDXGIFactory4>& factory() const { return m_dxgiFactory; }

    void setLowLatencyMode(bool lowLatencyMode, bool lowLatencyBoost, uint16_t maxFps, bool useMarkers) const;

private:
    // DirectX 12 Objects
    NmVector<DescriptorHeap> m_baseDescriptorHeaps;
    D3D_ROOT_SIGNATURE_VERSION m_rootSignatureVersion;
    ComPtr<IDXGIFactory4> m_dxgiFactory;
};
