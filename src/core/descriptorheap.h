#pragma once

#include "dxwrapper.h"

// THREAD SAFE
class DescriptorHeap : public DxWrapper<ID3D12DescriptorHeap> {
public:
    DescriptorHeap(const DescriptorHeap& other) = delete;

    DescriptorHeap(DescriptorHeap&& other) noexcept = delete;

    DescriptorHeap& operator=(const DescriptorHeap& other) = delete;

    DescriptorHeap& operator=(DescriptorHeap&& other) noexcept = delete;

    explicit DescriptorHeap(const std::wstring& name, D3D12_DESCRIPTOR_HEAP_TYPE type, uint32_t numDescriptors, bool shaderVisible = false);

    uint32_t allocate(uint32_t numDescriptors = 1);

    D3D12_CPU_DESCRIPTOR_HANDLE cpu(uint32_t offset) const;

    D3D12_GPU_DESCRIPTOR_HANDLE gpu(uint32_t offset) const;

    void reset();

private:
    std::atomic_uint32_t m_allocated = 0;
    const uint32_t m_incrementSize;
};
