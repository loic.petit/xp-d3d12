#include "descriptorheap.h"

#include "device.h"
#include "utils/logger.h"
#include "utils/service.h"
#include "utils/utils.h"

DescriptorHeap::DescriptorHeap(const std::wstring& name, D3D12_DESCRIPTOR_HEAP_TYPE type, uint32_t numDescriptors, bool shaderVisible):
    m_incrementSize(Get<Device>()->GetDescriptorHandleIncrementSize(type)) {
    D3D12_DESCRIPTOR_HEAP_DESC desc {
            type, numDescriptors, shaderVisible ? D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE : D3D12_DESCRIPTOR_HEAP_FLAG_NONE
    };
    ThrowIfFailed(Get<Device>()->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&m_native)));
    ThrowIfFailed(m_native->SetName(name.c_str()));
    LOG_INFO("Created: %ls", name.c_str());
}

uint32_t DescriptorHeap::allocate(uint32_t numDescriptors) {
    const uint32_t allocated = m_allocated.fetch_add(numDescriptors);
    if (allocated + numDescriptors >= m_native->GetDesc().NumDescriptors) {
        throw std::exception("NOT ENOUGH DESCRIPTORS!!");
    }
    return allocated;
}

void DescriptorHeap::reset() {
    m_allocated = 0;
}

D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeap::cpu(uint32_t offset) const {
    return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_native->GetCPUDescriptorHandleForHeapStart(), offset, m_incrementSize);
}

D3D12_GPU_DESCRIPTOR_HANDLE DescriptorHeap::gpu(uint32_t offset) const {
    const D3D12_GPU_DESCRIPTOR_HANDLE handle = m_native->GetGPUDescriptorHandleForHeapStart();
    if (handle.ptr == 0) {
        throw std::exception("Trying to use a gpu descriptor handle without being shader visible");
    }
    return CD3DX12_GPU_DESCRIPTOR_HANDLE(handle, offset, m_incrementSize);
}


