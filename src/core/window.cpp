#include "window.h"
#include "thread-name.h"

#include "utils/markers.h"

#ifdef HAS_REFLEX_MARKERS
PCLSTATS_DEFINE()
#else
#define PCLSTATS_INIT(flag)
#define PCLSTATS_SHUTDOWN()
#endif

static const wchar_t* s_windowClassName = L"D3D12XP";

static std::vector<Window*> g_instances;

Window::Window(HINSTANCE hInst, const wchar_t* windowTitle, uint32_t width, uint32_t height) : //
    m_fullscreen(false) {
    Window::registerWindowClass((HINSTANCE) hInst);
    int screenWidth = ::GetSystemMetrics(SM_CXSCREEN);
    int screenHeight = ::GetSystemMetrics(SM_CYSCREEN);

    RECT windowRect = {0, 0, static_cast<LONG>(width), static_cast<LONG>(height)};
    ::AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

    int windowWidth = windowRect.right - windowRect.left;
    int windowHeight = windowRect.bottom - windowRect.top;

    // Center the window within the screen. Clamp to 0, 0 for the top-left corner.
    int windowX = std::max<int>(0, (screenWidth - windowWidth) / 2);
    int windowY = std::max<int>(0, (screenHeight - windowHeight) / 2);
    m_hWnd = ::CreateWindowExW(
        0,
        s_windowClassName,
        windowTitle,
        WS_OVERLAPPEDWINDOW,
        windowX,
        windowY,
        windowWidth,
        windowHeight,
        nullptr,
        nullptr,
        (HINSTANCE) hInst,
        nullptr
    );

    assert(m_hWnd && "Failed to create window");

    ::GetWindowRect((HWND) m_hWnd, &m_windowRect);

    g_instances.push_back(this);
}

void Window::setFullscreen(bool fullscreen) {
    if (m_fullscreen == fullscreen) return;
    m_fullscreen = fullscreen;

    if (m_fullscreen) // Switching to fullscreen.
    {
        // Store the current window dimensions so they can be restored
        // when switching out of fullscreen state.
        ::GetWindowRect(m_hWnd, &m_windowRect);
        // Set the window style to a borderless window so the client area fills
        // the entire screen.
        UINT windowStyle =
                WS_OVERLAPPEDWINDOW & ~(WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);

        ::SetWindowLongW(m_hWnd, GWL_STYLE, windowStyle);
        // Query the name of the nearest display device for the window.
        // This is required to set the fullscreen dimensions of the window
        // when using a multi-monitor setup.
        HMONITOR hMonitor = ::MonitorFromWindow(m_hWnd, MONITOR_DEFAULTTONEAREST);
        MONITORINFOEX monitorInfo = {};
        monitorInfo.cbSize = sizeof(MONITORINFOEX);
        ::GetMonitorInfo(hMonitor, &monitorInfo);
        ::SetWindowPos(m_hWnd, HWND_TOP,
                       monitorInfo.rcMonitor.left,
                       monitorInfo.rcMonitor.top,
                       monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left,
                       monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top,
                       SWP_FRAMECHANGED | SWP_NOACTIVATE);

        ::ShowWindow(m_hWnd, SW_MAXIMIZE);
    } else {
        // Restore all the window decorators.
        ::SetWindowLong(m_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

        ::SetWindowPos(m_hWnd, HWND_NOTOPMOST,
                       m_windowRect.left,
                       m_windowRect.top,
                       m_windowRect.right - m_windowRect.left,
                       m_windowRect.bottom - m_windowRect.top,
                       SWP_FRAMECHANGED | SWP_NOACTIVATE);

        ::ShowWindow(m_hWnd, SW_NORMAL);
    }
}

LRESULT Window::wndProc(UINT message, WPARAM wParam, LPARAM lParam) {
    PIXScopedEvent(PIX_COLOR_INDEX(1), "Window::wndProc()");
    UINT result = 0;
    switch (message) {
        case WM_PAINT:
            return ::DefWindowProcW(m_hWnd, message, wParam, lParam);
        case WM_SYSKEYDOWN:
        case WM_KEYDOWN: {
            bool alt = (::GetAsyncKeyState(VK_MENU) & 0x8000) != 0;
            switch (wParam) {
                case VK_ESCAPE:
                    ::PostQuitMessage(0);
                    return 0;
                case VK_RETURN:
                    if (alt) {
                    case VK_F11:
                        setFullscreen(!m_fullscreen);
                    }
                    break;
            }
        }
        break;
        // The default window procedure will play a system notification sound
        // when pressing the Alt+Enter keyboard combination if this message is
        // not handled.
        case WM_SYSCHAR:
            break;
        case WM_SIZE: {
            if (m_swapChain)
                m_swapChain->fitToWindow();
            return 0;
        }
        case WM_DESTROY:
            ::PostQuitMessage(0);
            return 0;
    }
    onMsg(message, wParam, lParam, result);
    if (result != 0)
        return result;
    return ::DefWindowProcW(m_hWnd, message, wParam, lParam);
}

LRESULT CALLBACK Window::globalWinProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    for (Window* w: g_instances) {
        if (w->hwnd() == hwnd) {
            return w->wndProc(message, wParam, lParam);
        }
    }
    return ::DefWindowProcW(hwnd, message, wParam, lParam);
}

void Window::registerWindowClass(HINSTANCE hInst) {
    // Register a window class for creating our render window with.
    WNDCLASSEXW windowClass = {};

    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = &Window::globalWinProc;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = hInst;
    windowClass.hIcon = ::LoadIcon(hInst, nullptr);
    windowClass.hCursor = ::LoadCursor(nullptr, IDC_ARROW);
    windowClass.hbrBackground = CreateSolidBrush(0x00000000);
    windowClass.lpszMenuName = nullptr;
    windowClass.lpszClassName = s_windowClassName;
    windowClass.hIconSm = ::LoadIcon(hInst, nullptr);

    static ATOM atom = ::RegisterClassExW(&windowClass);
    assert(atom > 0);
}

Window::~Window() {
    g_instances.erase(std::find(g_instances.begin(), g_instances.end(), this));
}

void Window::show() {
    ::ShowWindow(m_hWnd, SW_SHOW);
}

void Window::hide() {
    ::ShowWindow(m_hWnd, SW_HIDE);
}

void Window::winRun() {
    PCLSTATS_INIT(0);
    MSG msg = {};
    while (msg.message != WM_QUIT) {
        if (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
        }
    }
    PCLSTATS_SHUTDOWN();
}

void Window::setTitle(wchar_t* title) {
    SetWindowTextW(m_hWnd, title);
}

SwapChain& Window::emplaceSwapChain() {
    m_swapChain = std::make_unique<SwapChain>(m_hWnd);
    return *m_swapChain;
}
