#include "pipeline.h"
#include "device.h"
#include "commandlist.h"
#include "utils/service.h"
#include "utils/utils.h"

void Pipeline::init(CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& rootSignature, D3D12_PIPELINE_STATE_STREAM_DESC& pipelineDescription, CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE& pipelineRootSignature) {
    auto& device = Get<Device>();
    m_rootSignature = device.createCompatibleRootSignature(rootSignature);
    pipelineRootSignature = m_rootSignature.Get();
    ThrowIfFailed(device->CreatePipelineState(&pipelineDescription, IID_PPV_ARGS(&m_pso)));

    uint32_t cbvSrvUavOffset = 0;
    uint32_t samplerOffset = 0;
    for (int i = 0; i < rootSignature.Desc_1_1.NumParameters; i++) {
        const D3D12_ROOT_PARAMETER1& parameter = rootSignature.Desc_1_1.pParameters[i];
        if (parameter.ParameterType == D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE) {
            uint32_t offset = 0;
            uint32_t maxOffset = 0;
            bool isSampler = false;
            for (int r = 0; r < parameter.DescriptorTable.NumDescriptorRanges; r++) {
                const D3D12_DESCRIPTOR_RANGE1& range = parameter.DescriptorTable.pDescriptorRanges[r];
                if (range.RangeType == D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER) {
                    isSampler = true;
                }
                if (range.OffsetInDescriptorsFromTableStart != D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND) {
                    offset = range.OffsetInDescriptorsFromTableStart;
                }
                if (range.NumDescriptors != UINT32_MAX) {
                    offset += range.NumDescriptors;
                    if (offset > maxOffset) maxOffset = offset;
                }
            }

            uint32_t& targetOffset = isSampler ? samplerOffset : cbvSrvUavOffset;
            m_allocations.emplace(static_cast<uint32_t>(i), Alloc {targetOffset, maxOffset, isSampler, false});
            targetOffset += maxOffset;
        }
    }
}

Pipeline::Pipeline(bool isCompute) : m_isCompute(isCompute) {
}

D3D12_CPU_DESCRIPTOR_HANDLE Pipeline::stageAllocated(CommandList& list, uint32_t rootIndex) {
    auto& alloc = m_allocations[rootIndex];
    D3D12_DESCRIPTOR_HEAP_TYPE heapType = alloc.isSampler ? D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER : D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    DescriptorHeap& heap = list.getHeap(heapType);
    if (!alloc.isStaged) {
        alloc.targetOffset = heap.allocate(alloc.size);
    }
    alloc.isStaged = true;
    return heap.cpu(alloc.targetOffset);
}

void Pipeline::stageDescriptors(CommandList& list, uint32_t rootIndex, D3D12_CPU_DESCRIPTOR_HANDLE start, uint32_t numDescriptors, uint32_t offset) {
    auto& alloc = m_allocations[rootIndex];
    D3D12_DESCRIPTOR_HEAP_TYPE heapType = alloc.isSampler ? D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER : D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    DescriptorHeap& heap = list.getHeap(heapType);
    if (!alloc.isStaged) {
        alloc.targetOffset = heap.allocate(alloc.size ? alloc.size : numDescriptors);
    }
    alloc.isStaged = true;
    if (!numDescriptors) numDescriptors = alloc.size;
    Get<Device>()->CopyDescriptorsSimple(
            numDescriptors,
            heap.cpu(alloc.targetOffset + offset),
            start,
            heapType
    );
}

void Pipeline::stageDescriptors(CommandList& list, uint32_t rootIndex, D3D12_CPU_DESCRIPTOR_HANDLE* start, uint32_t numDescriptors, uint32_t offset) {
    auto& alloc = m_allocations[rootIndex];
    D3D12_DESCRIPTOR_HEAP_TYPE heapType = alloc.isSampler ? D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER : D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    DescriptorHeap& heap = list.getHeap(heapType);
    if (!alloc.isStaged) {
        alloc.targetOffset = heap.allocate(alloc.size ? alloc.size : numDescriptors);
    }
    alloc.isStaged = true;
    for (int i = 0; i < numDescriptors; i++) {
        D3D12_CPU_DESCRIPTOR_HANDLE baseHandle = heap.cpu(alloc.targetOffset + offset + i);
        Get<Device>()->CopyDescriptorsSimple(1, baseHandle, start[i], heapType);
    }
}

void Pipeline::markStaged(uint32_t rootIndex, uint32_t offset) {
    auto& alloc = m_allocations[rootIndex];
    alloc.targetOffset = offset;
    alloc.isStaged = true;
}

void Pipeline::commitDescriptors(CommandList& list) {
    list.commitTransitions();
    for (uint32_t i = 0; i < m_allocations.size(); i++) {
        auto& alloc = m_allocations[i];
        if (!alloc.isStaged) {
            continue;
        }
        alloc.isStaged = false;
        D3D12_DESCRIPTOR_HEAP_TYPE heapType = alloc.isSampler ? D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER : D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        DescriptorHeap& heap = list.getHeap(heapType);
        if (m_isCompute) {
            list->SetComputeRootDescriptorTable(i, heap.gpu(alloc.targetOffset));
        } else {
            list->SetGraphicsRootDescriptorTable(i, heap.gpu(alloc.targetOffset));
        }
    }
}

void Pipeline::use(CommandList& list) const {
    if (m_isCompute) {
        list->SetComputeRootSignature(m_rootSignature.Get());
    } else {
        list->SetGraphicsRootSignature(m_rootSignature.Get());
    }
    list->SetPipelineState(m_pso.Get());
    if (!m_allocations.empty()) {
        ID3D12DescriptorHeap* heap[2] {
                list.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr(),
                list.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER).ptr()
        };
        list->SetDescriptorHeaps(2, heap);
    }
}
