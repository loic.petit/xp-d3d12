#pragma once

#include "dxwrapper.h"
#include "event.h"

class CommandList;

class Queue : public DxWrapper<ID3D12CommandQueue> {
public:
    Queue(D3D12_COMMAND_LIST_TYPE type, int32_t priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL, D3D12_COMMAND_QUEUE_FLAGS flags = D3D12_COMMAND_QUEUE_FLAG_NONE, uint32_t nodeMask = 0);

    ~Queue();

    void init(const std::wstring& name);

    void flush(bool isFinished);
    void resume();

    std::shared_ptr<CommandList> createCommandList(ID3D12PipelineState* initPipeline = nullptr);

    uint64_t signal();

    uint64_t execute(uint32_t n, std::shared_ptr<CommandList>* list);

    inline uint64_t execute(std::shared_ptr<CommandList>& list) {
        return execute(1, &list);
    }

    bool isFenceReady(uint64_t fenceValue);

    void waitForFence(uint64_t fenceValue);

    [[nodiscard]] uint64_t latestFenceValue() const {
        return m_fenceValue;
    }

    uint64_t waitForQueue(const Queue& queue);

private:
    void clearInPreparation();

    D3D12_COMMAND_QUEUE_DESC m_queueDescription;

    ComPtr<ID3D12Fence> m_fence;
    std::wstring m_typeAsString;
    uint64_t m_fenceValue = 0;
    uint32_t m_lists = 0;
    DelegateHandle m_beginFlush;
    DelegateHandle m_endFlush;

    std::queue<std::shared_ptr<CommandList>> m_availableLists;
    std::vector<std::weak_ptr<CommandList>> m_inPreparationLists;
    std::queue<std::pair<std::shared_ptr<CommandList>, uint64_t>> m_inFlightQueue;
    std::mutex m_mutex;
    std::condition_variable m_inPreparationCondition;
    std::condition_variable m_flushing;
    bool m_isFlushing = false;
    bool m_isClosed = false;

    friend class CommandQueueTest_Sync_Test;
};