#pragma once

class CommandList;

class Pipeline {
public:
    explicit Pipeline(bool isCompute);

    void init(CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& rootSignature, D3D12_PIPELINE_STATE_STREAM_DESC& pipelineDescription, CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE& pipelineRootSignature);

    template<typename E>
    void init(CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& rootSignature, E& e, CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE& pipelineRootSignature) {
        D3D12_PIPELINE_STATE_STREAM_DESC desc = {sizeof(E), &e};
        init(rootSignature, desc, pipelineRootSignature);
    }

    D3D12_CPU_DESCRIPTOR_HANDLE stageAllocated(CommandList& list, uint32_t rootIndex);

    void stageDescriptors(CommandList& list, uint32_t rootIndex, D3D12_CPU_DESCRIPTOR_HANDLE start, uint32_t numDescriptors = 0, uint32_t offset = 0);

    void stageDescriptors(CommandList& list, uint32_t rootIndex, D3D12_CPU_DESCRIPTOR_HANDLE* start, uint32_t numDescriptors, uint32_t offset = 0);

    void markStaged(uint32_t rootIndex, uint32_t offset);

    void commitDescriptors(CommandList& list);

    void use(CommandList& list) const;

    [[nodiscard]] bool isLoaded() const {
        return m_pso.Get() != nullptr;
    }
private:
    bool m_isCompute;

    struct Alloc {
        uint32_t targetOffset;
        uint32_t size;
        bool isSampler;
        bool isStaged;
    };

    std::unordered_map<uint32_t, struct Alloc> m_allocations;

    ComPtr<ID3D12RootSignature> m_rootSignature;
    ComPtr<ID3D12PipelineState> m_pso;
};