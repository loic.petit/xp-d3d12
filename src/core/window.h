#pragma once

#include <base/base-thread.h>

#include "event.h"
#include "swapchain.h"

class Engine;

class Window {
public:
    using UpdateEvent = Delegate<Window, void()>;
    using MsgEvent = Delegate<Window, void(UINT message, WPARAM wParam, LPARAM lParam, UINT& result)>;

    static void registerWindowClass(HINSTANCE hInst);

    explicit Window(HINSTANCE hInst, const wchar_t* windowTitle, uint32_t width, uint32_t height);

    virtual ~Window();

    void show();

    void hide();

    static void winRun();

    void setFullscreen(bool fullscreen);

    void setTitle(wchar_t* title);

    const HWND& hwnd() { return m_hWnd; }

    UpdateEvent onUpdate;
    MsgEvent onMsg;

    SwapChain& getSwapChain() {
        return *m_swapChain;
    }

    SwapChain& emplaceSwapChain();
private:
    LRESULT wndProc(UINT message, WPARAM wParam, LPARAM lParam);

    static LRESULT CALLBACK globalWinProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

    std::unique_ptr<SwapChain> m_swapChain;

    // Window handle.
    HWND m_hWnd;
    // Window rectangle (used to toggle fullscreen state).
    RECT m_windowRect {};

    bool m_fullscreen;
};