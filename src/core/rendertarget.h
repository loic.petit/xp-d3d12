#pragma once

#include "resource.h"

class CommandList;

class RenderTarget {
public:
    typedef struct {
        DXGI_FORMAT pixelFormats[8];
        DXGI_FORMAT depthFormat;
        uint32_t width;
        uint32_t height;
        uint32_t samples;
    } Desc;

    explicit RenderTarget(std::wstring name);

    RenderTarget(RenderTarget&& o) = default;

    RenderTarget& operator=(RenderTarget&& o) = default;

    ~RenderTarget() = default;

    const Desc& desc() {
        return m_desc;
    }

    void clear(CommandList& list);

    inline void use(CommandList& list) {
        RenderTarget::setRenderTarget(list, m_desc.width, m_desc.height, m_buffers.size(), m_buffers.data(), m_depthStencil.ptr() != nullptr ? &m_depthStencil : nullptr);
    }

    static void setRenderTarget(CommandList& list, uint32_t width, uint32_t height, size_t bufferCount, const Resource* buffers, const Resource* depthStencil);
    static void setRenderTarget(CommandList& list, uint32_t width, uint32_t height, size_t bufferCount, const Resource** buffers, const Resource* depthStencil);

    void transition(CommandList& list, D3D12_RESOURCE_STATES renderTarget, D3D12_RESOURCE_STATES depthStencil);

    void transitionWrite(CommandList& list) {
        transition(list, D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_DEPTH_WRITE);
    }

    Resource& getDepthStencil();

    Resource& getBuffer(uint32_t idx);

    std::vector<Resource>& getBuffers() {
        return m_buffers;
    }

    void init(uint32_t width, uint32_t height, uint32_t samples);

    void addExistingBuffer(ComPtr<ID3D12Resource>& buffer, D3D12_CPU_DESCRIPTOR_HANDLE& handle);

    uint32_t createBuffer(const std::wstring& name, DXGI_FORMAT format, bool allowAsShaderResource = false);

    void createDepthStencil(const std::wstring& name, DXGI_FORMAT format, bool allowAsShaderResource = false);

    void resize(uint32_t width, uint32_t height);

    void reset();
    // common util commands

    void resolveInto(CommandList& list, RenderTarget& dest);

    void copyInto(CommandList& list, RenderTarget& dest);

    void prepareToPresent(CommandList& list, uint32_t idx);

private:
    void createDSV(Resource& resource);

    void createSRV(Resource& resource);

    void resizeResource(Resource& resource, const D3D12_CLEAR_VALUE& clearValue);

    void createRTV(Resource& resource);

    Desc m_desc;
    Resource m_depthStencil;
    std::vector<Resource> m_buffers;
    D3D12_CLEAR_VALUE m_clearValuePixels[8];
    D3D12_CLEAR_VALUE m_clearValueDepth;
    std::wstring m_name;
};
