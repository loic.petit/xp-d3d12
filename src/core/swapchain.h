#pragma once

#include "device.h"
#include "rendertarget.h"
#include "event.h"
#include "queue.h"

class Window;

class SwapChain : public DxWrapper<IDXGISwapChain4> {
public:
    explicit SwapChain(HWND hwnd);

    using ResizeEvent = Delegate<SwapChain, void(uint32_t width, uint32_t height)>;

    ~SwapChain() = default;

    void waitForBackBufferToBeReady();

    void fitToWindow();

    void present();

    inline RenderTarget& getBackBuffer() {
        return m_backBuffers[m_currentBackBufferIndex];
    }

    ResizeEvent onResize;
protected:
    void applyResize();

    void setRenderTargets();

private:
    HWND m_hWnd;
    std::vector<RenderTarget> m_backBuffers;
    std::vector<uint64_t> m_frameFenceValues;
    uint32_t m_backBufferHandleBase;
    UINT m_currentBackBufferIndex {};
    BOOL m_allowTearing = false;
    uint32_t m_width;
    uint32_t m_height;
    HANDLE m_frameLatencyWaitableObject;
    std::mutex m_mutex;
    uint32_t m_desiredWidth;
    uint32_t m_desiredHeight;

    bool m_vsync = true;
};