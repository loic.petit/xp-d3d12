#include "swapchain.h"

#include <scenes/context.h>

#include "commandlist.h"
#include "utils/logger.h"
#include "utils/service.h"
#include "utils/markers.h"
#include "utils/utils.h"

constexpr int SWAP_CHAIN_BUFFERS = 2;
constexpr DXGI_FORMAT PIXEL_FORMAT = DXGI_FORMAT_R8G8B8A8_UNORM;

SwapChain::SwapChain(HWND hwnd) : m_hWnd(hwnd) {
    auto& device = Get<Device>();
//    m_vsync = false;
    ComPtr<IDXGIFactory4> factory = device.factory();

    // check tearing support
    {
        ComPtr<IDXGIFactory5> factory5;
        if (SUCCEEDED(factory.As(&factory5))) {
            if (FAILED(factory5->CheckFeatureSupport(
                    DXGI_FEATURE_PRESENT_ALLOW_TEARING,
                    &m_allowTearing, sizeof(m_allowTearing)))) {
                m_allowTearing = FALSE;
            }
        }
        LOG_INFO("Tearing support = %s", BOOL_STR(m_allowTearing));
    }
//    m_allowTearing = false;

    // create the swap chain
    {
        // compute the desired width / height
        fitToWindow();
        m_width = m_desiredWidth;
        m_height = m_desiredHeight;

        DXGI_SWAP_CHAIN_DESC1 desc {
                m_width,
                m_height,
                PIXEL_FORMAT,
                false,
                {1, 0}, // useless in flip mode
                DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT, // allow to read the render target
                SWAP_CHAIN_BUFFERS,
                DXGI_SCALING_STRETCH,
                DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL, // flip mode
                DXGI_ALPHA_MODE_UNSPECIFIED,
                DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT
        };

        if (m_allowTearing)
            desc.Flags |= DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING;
//        DXGI_SWAP_CHAIN_FULLSCREEN_DESC fsDesc {
//                {170, 1},
//                DXGI_MODE_SCANLINE_ORDER::DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE,
//                DXGI_MODE_SCALING_CENTERED,
//                false
//        };
        ComPtr<IDXGISwapChain1> dxgiSwapChain1;
        ThrowIfFailed(factory->CreateSwapChainForHwnd(Get<EngineQueues>().directQueue().ptr(), hwnd, &desc, nullptr, nullptr, &dxgiSwapChain1));
        ThrowIfFailed(dxgiSwapChain1.As(&m_native));
        // for waitForBackBufferToBeReady
        m_frameLatencyWaitableObject = m_native->GetFrameLatencyWaitableObject();
        m_currentBackBufferIndex = m_native->GetCurrentBackBufferIndex();
        // maximum frame latency
        ThrowIfFailed(m_native->SetMaximumFrameLatency(1));

        LOG_INFO("Created SwapChain with %d buffers with resolution %dx%d", desc.BufferCount, desc.Width, desc.Height);
    }

    // disable alt+enter
    ThrowIfFailed(factory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_WINDOW_CHANGES));

    // create the render targets & fence values for each frame
    {
        // Allocate the RTV handles (should be 0)
        m_backBufferHandleBase = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_RTV).allocate(SWAP_CHAIN_BUFFERS);
        m_backBuffers.reserve(SWAP_CHAIN_BUFFERS);
        for (int i = 0; i < SWAP_CHAIN_BUFFERS; ++i) {
            m_backBuffers.emplace_back(L"BackBuffer#" + std::to_wstring(i));
        }
        setRenderTargets();
        m_frameFenceValues.resize(SWAP_CHAIN_BUFFERS, 0);
        LOG_INFO("Created render targets");
    }
    LOG_INFO("DX12 Device ready!");
}

void SwapChain::waitForBackBufferToBeReady() {
    applyResize();
    {
        PIXScopedEvent(PIX_COLOR_INDEX(0), "SwapChain::waitForLatency()");
        // wait for the swap chain to be ready for another frame
        ::WaitForSingleObjectEx(m_frameLatencyWaitableObject, 1000, TRUE);
    }
    // ensures that the fence is ready (it should be)
    {
        PIXScopedEvent(PIX_COLOR_INDEX(0), "SwapChain::waitForFence()");
        Get<EngineQueues>().directQueue().waitForFence(m_frameFenceValues[m_currentBackBufferIndex]);
    }
}

void SwapChain::fitToWindow() {
    RECT clientRect = {};
    ::GetClientRect(m_hWnd, &clientRect);

    std::lock_guard lock(m_mutex);
    m_desiredWidth = std::max(1u, (uint32_t)(clientRect.right - clientRect.left));
    m_desiredHeight = std::max(1u, (uint32_t)(clientRect.bottom - clientRect.top));
}

void SwapChain::applyResize() {
    auto& device = Get<Device>();
    PIXScopedEvent(PIX_COLOR_INDEX(0), "SwapChain::applyResize()");
    {
        std::lock_guard lock(m_mutex);
        if (m_desiredWidth == m_width && m_desiredHeight == m_height) return;
        m_width = m_desiredWidth;
        m_height = m_desiredHeight;
    }

    auto windowStyle = ::GetWindowLongW(m_hWnd, GWL_STYLE);
//    if (windowStyle == WS_OVERLAPPEDWINDOW) {
//        m_native->SetFullscreenState(false, nullptr);
//    } else {
//        m_native->SetFullscreenState(true, nullptr);
//    }
    LOG_INFO("Resizing swap chain to %dx%d, flushing...", m_width, m_height);
    device.beginFlush(false);
    LOG_INFO("Resizing swap chain: flushed");

    for (int i = 0; i < SWAP_CHAIN_BUFFERS; ++i) {
        m_frameFenceValues[i] = m_frameFenceValues[m_currentBackBufferIndex];
        m_backBuffers[i].reset();
    }

    DXGI_SWAP_CHAIN_DESC desc;
    ThrowIfFailed(m_native->GetDesc(&desc));
    ThrowIfFailed(m_native->ResizeBuffers(SWAP_CHAIN_BUFFERS, m_width, m_height, desc.BufferDesc.Format, desc.Flags));

    m_currentBackBufferIndex = m_native->GetCurrentBackBufferIndex();

    setRenderTargets();
    onResize(m_width, m_height);
    device.endFlush();
    LOG_INFO("Resize swap chain: done");
}

void SwapChain::setRenderTargets() {
    auto& device = Get<Device>();
    DescriptorHeap& heap = device.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    for (int i = 0; i < SWAP_CHAIN_BUFFERS; ++i) {
        ComPtr<ID3D12Resource> backBuffer;
        ThrowIfFailed(m_native->GetBuffer(i, IID_PPV_ARGS(&backBuffer)));
        D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = heap.cpu(m_backBufferHandleBase + i);
        m_backBuffers[i].init(m_width, m_height, 1);
        m_backBuffers[i].addExistingBuffer(backBuffer, rtvHandle);
    }
}

void SwapChain::present() {
    UINT syncInterval = m_vsync ? 1 : 0;
    UINT presentFlags = m_allowTearing && !m_vsync ? DXGI_PRESENT_ALLOW_TEARING : 0;
    ThrowIfFailed(m_native->Present(syncInterval, presentFlags));
    // signal after the present
    m_frameFenceValues[m_currentBackBufferIndex] = Get<EngineQueues>().directQueue().signal();

    m_currentBackBufferIndex = m_native->GetCurrentBackBufferIndex();
}
