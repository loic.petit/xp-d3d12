#pragma once

#include "dxwrapper.h"

class Resource : public DxWrapper<ID3D12Resource> {
public:
    Resource() = default;

    explicit Resource(ComPtr<ID3D12Resource>& resource, D3D12_RESOURCE_STATES state = D3D12_RESOURCE_STATE_COMMON);

    explicit Resource(const D3D12_RESOURCE_DESC& resourceDesc, D3D12_RESOURCE_STATES state = D3D12_RESOURCE_STATE_COMMON, const D3D12_CLEAR_VALUE* clearValue = nullptr,
                      D3D12_HEAP_TYPE heapType = D3D12_HEAP_TYPE_DEFAULT);

    Resource(const Resource& o) = delete;

    Resource(Resource&& o) noexcept = default;

    Resource& operator=(const Resource& o) noexcept = delete;

    Resource& operator=(Resource&& o) noexcept = default;

    D3D12_RESOURCE_STATES state() { return m_state; }

    void reset(D3D12_RESOURCE_STATES state) {
        m_state = m_stateSplit = m_statePending = state;
        m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    }

    inline Resource& stateEnd() {
        if (m_state != m_stateSplit) {
//#ifdef _DEBUG
//            if (m_flagsPending == D3D12_RESOURCE_BARRIER_FLAG_END_ONLY) {
//                throw std::invalid_argument("Asking to end a split state but it was already asked");
//            }
//#endif
            m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_END_ONLY;
        } else {
//#ifdef _DEBUG
//            if (m_flagsPending != D3D12_RESOURCE_BARRIER_FLAG_BEGIN_ONLY) {
//                throw std::invalid_argument("Asking a split state end but none were in progress");
//            }
//#endif
            m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_NONE;
        }
        return *this;
    }

    inline Resource& stateBegin(D3D12_RESOURCE_STATES state) {
#ifdef _DEBUG
        if (m_state != m_stateSplit || m_flagsPending == D3D12_RESOURCE_BARRIER_FLAG_BEGIN_ONLY) {
            throw std::invalid_argument("There is already a split barrier in progress and you're asking for a new one");
        }
#endif
        m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_BEGIN_ONLY;
        m_statePending = state;
        return *this;
    }

    inline Resource& state(D3D12_RESOURCE_STATES state) {
        if (m_state != m_stateSplit) {
            // we have a split barrier
#ifdef _DEBUG
            if (m_stateSplit != state) {
                throw std::invalid_argument("Illegal split state current committed split {:#08x}, trying to {:#08x}");
            }
#endif
            m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_END_ONLY;
        } else {
#ifdef _DEBUG
            if (m_flagsPending == D3D12_RESOURCE_BARRIER_FLAG_BEGIN_ONLY && m_statePending != state) {
                throw std::invalid_argument("Illegal split state current non-committed split {:#08x}, trying to {:#08x}");
            }
#endif
            m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_NONE;
        }
        m_statePending = state;
        return *this;
    }

    inline bool commitTransition(D3D12_RESOURCE_BARRIER& transition) {
        if (m_state == m_statePending) {
            // nothing to commit
            return false;
        }
        transition = CD3DX12_RESOURCE_BARRIER::Transition(m_native.Get(), m_state, m_statePending, D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES, m_flagsPending);
        if (m_flagsPending == D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_BEGIN_ONLY) {
            m_stateSplit = m_statePending;
        } else {
            m_state = m_statePending;
            m_stateSplit = m_state;
        }
        m_flagsPending = D3D12_RESOURCE_BARRIER_FLAGS::D3D12_RESOURCE_BARRIER_FLAG_NONE;
        return true;
    }

    void clear();

    inline void srv(D3D12_CPU_DESCRIPTOR_HANDLE handle) {
        m_handles[SRV] = handle;
    }

    [[nodiscard]] inline const D3D12_CPU_DESCRIPTOR_HANDLE& srv() const {
        return m_handles[SRV];
    }

    inline void rtv(D3D12_CPU_DESCRIPTOR_HANDLE handle) {
        m_handles[RTV] = handle;
    }

    [[nodiscard]] inline const D3D12_CPU_DESCRIPTOR_HANDLE& rtv() const {
        return m_handles[RTV];
    }

    inline void dsv(D3D12_CPU_DESCRIPTOR_HANDLE handle) {
        m_handles[DSV] = handle;
    }

    [[nodiscard]] inline const D3D12_CPU_DESCRIPTOR_HANDLE& dsv() const {
        return m_handles[DSV];
    }

    static bool isSRGB(DXGI_FORMAT format);
    static DXGI_FORMAT getUnormFromSRGB(DXGI_FORMAT format);
    static bool isCompressed(DXGI_FORMAT format);
    static uint8_t getComponents(DXGI_FORMAT format);
    static DXGI_FORMAT getStagingTextureFormat(DXGI_FORMAT format);

private:
    enum ViewType {
        SRV = 0,
        RTV = (SRV + 1),
        DSV = (RTV + 1),
        _TOTAL = (DSV + 1)
    };
    // We do not track subresource state, it's up to the implementation to have this level of granularity
    D3D12_RESOURCE_STATES m_state = D3D12_RESOURCE_STATE_COMMON;
    D3D12_RESOURCE_STATES m_stateSplit = D3D12_RESOURCE_STATE_COMMON;
    D3D12_RESOURCE_STATES m_statePending = D3D12_RESOURCE_STATE_COMMON;
    D3D12_RESOURCE_BARRIER_FLAGS m_flagsPending = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    D3D12_CPU_DESCRIPTOR_HANDLE m_handles[ViewType::_TOTAL] {};
};
