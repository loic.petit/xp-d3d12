#include "timeline.h"

static DirectX::XMVECTOR DEFAULT_TRANSLATION = DirectX::XMQuaternionIdentity();
static DirectX::XMVECTOR DEFAULT_ROTATION = DirectX::XMQuaternionRotationRollPitchYaw(0, 0, 0);

Timeline::Timeline(float* time, size_t lastEntry, void* vectors) : m_time(time), m_last_entry(lastEntry), m_vectors(vectors) {}

std::pair<size_t, float> Timeline::interpolationTime(float currentTime) const {
    if (m_last_entry == 0) return std::make_pair(0, 0.0f);
    if (m_time[m_last_entry] <= currentTime) return std::make_pair(m_last_entry - 1, 1.0f);
    if (m_time[0] >= currentTime) return std::make_pair(0, 0.0f);
    size_t begin = 0;
    size_t end = m_last_entry;
    size_t find = m_last_entry / 2;

    while (m_time[find] > currentTime || currentTime >= m_time[find + 1]) {
        if (m_time[find] > currentTime) {
            end = find;
        } else {
            begin = find;
        }
        find = (begin + end) / 2;
    }

    return std::make_pair(find, (currentTime - m_time[find]) / (m_time[find + 1] - m_time[find]));
}

DirectX::XMVECTOR Timeline::lerp(float currentTime) const {
    if (m_time == nullptr) return DEFAULT_TRANSLATION;
    auto vector = static_cast<DirectX::XMFLOAT3*>(m_vectors);
    if (m_last_entry == 0) return DirectX::XMLoadFloat3(&vector[0]);
    auto [idx, interpolationValue] = interpolationTime(currentTime);
    return DirectX::XMVectorLerp(DirectX::XMLoadFloat3(&vector[idx]), DirectX::XMLoadFloat3(&vector[idx + 1]), interpolationValue);
}

DirectX::XMVECTOR Timeline::slerp(float currentTime) const {
    if (m_time == nullptr) return DEFAULT_ROTATION;
    auto vector = static_cast<DirectX::XMFLOAT4*>(m_vectors);
    if (m_last_entry == 0) return DirectX::XMLoadFloat4(&vector[0]);
    auto [idx, interpolationValue] = interpolationTime(currentTime);
    return DirectX::XMQuaternionSlerp(DirectX::XMLoadFloat4(&vector[idx]), DirectX::XMLoadFloat4(&vector[idx + 1]), interpolationValue);
}

Timeline::Timeline() : m_time(nullptr), m_last_entry(0), m_vectors(nullptr) {
}

float Timeline::getEndTime() const {
    return m_time ? m_time[m_last_entry] : 0;
}

bool Timeline::empty() const {
    return m_time == nullptr;
}
