#include "shader-loader.h"
#include "logger.h"

#define D3D_COMPILE_STANDARD_FILE_INCLUDE ((ID3DInclude*)(UINT_PTR)1)

ShaderBlob ShaderLoader::compile(const wchar_t* sourceFile, const D3D_SHADER_MACRO* defines, const char* target) {
    LOG_INFO("Compiling shader: %ls", sourceFile);
    ShaderBlob result;
    ComPtr<ID3DBlob> errorMsg;
    constexpr uint32_t COMMON_FLAGS = D3DCOMPILE_ALL_RESOURCES_BOUND;
#ifdef _DEBUG
    constexpr uint32_t FLAGS = COMMON_FLAGS | D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
    constexpr uint32_t FLAGS = COMMON_FLAGS;
#endif
    HRESULT hr = D3DCompileFromFile(
            sourceFile,
            defines,
            D3D_COMPILE_STANDARD_FILE_INCLUDE,
            "main",
            target,
            FLAGS,
            0,
            &result,
            &errorMsg);
    if (errorMsg) {
        LOG_ERROR("Cannot compile shader:\n%s", errorMsg->GetBufferPointer());
    }
    if (FAILED(hr)) {
        throw std::exception("Shader failure");
    }
    LOG_TRACE("Compiling shader: %ls DONE", sourceFile);
    return result;
}

ShaderBlob ShaderLoader::compileString(const std::string& source, const D3D_SHADER_MACRO* defines, const char* target) {
    LOG_INFO("Compiling shader from source");
    ShaderBlob result;
    ComPtr<ID3DBlob> errorMsg;
    constexpr uint32_t COMMON_FLAGS = D3DCOMPILE_ALL_RESOURCES_BOUND;
#ifdef _DEBUG
    constexpr uint32_t FLAGS = COMMON_FLAGS | D3DCOMPILE_DEBUG;
#else
    constexpr uint32_t FLAGS = COMMON_FLAGS;
#endif
    HRESULT hr = D3DCompile(
            source.c_str(),
            source.size(),
            nullptr,
            defines,
            D3D_COMPILE_STANDARD_FILE_INCLUDE,
            "main",
            target,
            FLAGS,
            0,
            &result,
            &errorMsg);
    if (errorMsg) {
        LOG_ERROR("Cannot compile shader:\n%s", errorMsg->GetBufferPointer());
    }
    if (FAILED(hr)) {
        throw std::exception("Shader failure");
    }
    LOG_TRACE("Compiling shader DONE");
    return result;
}
