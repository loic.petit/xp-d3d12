#pragma once

#include <cassert>
#include <base/base-thread.h>

template<typename T>
class Service {
    Service() = delete;

public:
    template<typename... Args>
    static void build(Args&&... args) {
        provide(new T(std::forward<Args>(args)...));
    }
    template<typename... Args>
    static void build(const ThreadId affinity, Args&&... args) {
        provide(new T(std::forward<Args>(args)...));
        setAffinity(affinity);
    }

    static void provide(T* ptr, const ThreadId affinity = _THREAD_ID_COUNT) {
        auto& service = storage();
#ifdef _DEBUG
        assert(!service && "Trying to remplace a service");
#endif
        service = ptr;
        setAffinity(affinity);
    }

    static T& get() {
        auto& service = storage();
#ifdef _DEBUG
        assert(service && "Trying to get a service which was not built");
        assert(isCurrentThreadValid() && "Trying to use a service in the wrong thread");
#endif
        return *service;
    }

    static void destroy() {
        auto& service = storage();
#ifdef _DEBUG
        assert(service && "Trying to destroy while service was not built");
#endif
        delete service;
        service = nullptr;
    }

    static void clear() {
        auto& service = storage();
#ifdef _DEBUG
        assert(service && "Trying to clear while service was not present");
#endif
        service = nullptr;
    }

    static void setAffinity(const ThreadId id) {
#ifdef _DEBUG
        threadAffinity() = id;
#endif
    }
private:
    static T*& storage() {
        static T* instance = nullptr;
        return instance;
    }
#ifdef _DEBUG
    static ThreadId& threadAffinity() {
        static ThreadId threadAffinity = _THREAD_ID_COUNT;
        return threadAffinity;
    }
    static bool isCurrentThreadValid() {
        const auto& affinity = threadAffinity();
        return affinity == _THREAD_ID_COUNT || affinity == CurrentThreadId();
    }
#endif
};

template<typename T>
class Provider {
public:
    explicit Provider(T* pointer, const ThreadId affinity = _THREAD_ID_COUNT) {
        Service<T>::provide(pointer);
        Service<T>::setAffinity(affinity);
    }
    ~Provider() {
        Service<T>::clear();
    }
};

template<typename T>
static T& Get() {
    return Service<T>::get();
}