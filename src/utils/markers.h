#pragma once

// reflex (if available)
#ifdef HAS_REFLEX_MARKERS
    #include "pclstats.h"
    #include "nvapi.h"

    #define LATENCY_MARKER(marker) \
        PCLSTATS_MARKER(marker, m_frameId); \
        {                          \
            NvAPI_Status status = NVAPI_OK; \
            NV_LATENCY_MARKER_PARAMS_V1 params = {}; \
            params.version = NV_LATENCY_MARKER_PARAMS_VER1; \
            params.frameID = m_frameId; \
            params.markerType = marker; \
            status = NvAPI_D3D_SetLatencyMarker(Get<Device>().ptr(), &params); \
        }
#else
    #define LATENCY_MARKER(marker)
#endif

// pix (if available)
#ifdef HAS_PIX
#include "pix3.h"
#else
#define PIX_COLOR_INDEX(_) 0
    #define PIXScopedEvent(...)
#endif
