#include "logger.h"

#include <base/base-thread.h>

#include "utils.h"

#ifdef _DEBUG
static LogLevel g_level = LogLevel::DEBUG;
#else
static LogLevel g_level = LogLevel::DEBUG;
#endif
static std::unique_ptr<std::ofstream> g_file;
static std::mutex g_logger_lock;

static void logger_write(const char* prefix, const char* file, int line, const char* msg) {
    const auto now = std::chrono::system_clock::now();
    auto epoch_seconds = std::chrono::system_clock::to_time_t(now);
    auto truncated = std::chrono::system_clock::from_time_t(epoch_seconds);
    auto delta_us = std::chrono::duration_cast<std::chrono::milliseconds>(now - truncated).count();

    {
        const char* const slash = strrchr(file, '/');
        if (slash != nullptr) {
            file = slash + 1;
        } else {
            const char* const backslash = strrchr(file, '\\');
            if (backslash != nullptr) {
                file = backslash + 1;
            }
        }
    }

    std::stringstream buffer;
    struct tm t {};
    gmtime_s(&t, &epoch_seconds);
    buffer << std::put_time(&t, "%FT%T") << "." << std::fixed << std::setw(3) << std::setfill('0') << delta_us;
    buffer << " [" << wstringToUtf8(THREAD_INFOS[CurrentThreadId()].threadName) << "] ";
    buffer << file << ":" << std::setw(3) << std::setfill(' ') << std::left << line << " ";
    buffer << std::setw(7) << prefix << " " << msg;

    {
        // ensure that we stream-line writes and protect g_file
        std::lock_guard<std::mutex> lock(g_logger_lock);
        if (g_file) {
            *g_file << buffer.str() << std::endl;
        }
    }
    std::cout << buffer.str() << std::endl;
}

void logger_out(LogLevel level, const char* file, int line, const char* msg) {
    const char* prefix = nullptr;
    switch (level) {
        case LogLevel::TRACE:
            prefix = "[TRACE]";
            break;
        case LogLevel::DEBUG:
            prefix = "[DEBUG]";
            break;
        case LogLevel::INFO:
            prefix = "[INFO]";
            break;
        case LogLevel::WARN:
            prefix = "[WARN]";
            break;
        case LogLevel::CRIT:
            prefix = "[ERROR]";
            break;
        default:
            break;
    }
    logger_write(prefix, file, line, msg);
}

void logger_printf(LogLevel level, const char* file, int line, const char* fmt, ...) {
    if (level < g_level) return;
    std::va_list al;
            va_start(al, fmt);
    char buffer[1024];
    vsnprintf(buffer, 1024, fmt, al);
            va_end(al);
    logger_out(level, file, line, buffer);
}

void logger_set_level(LogLevel level) {
    g_level = level;
}

void logger_set_file_output(const char* filename) {
    std::lock_guard<std::mutex> lock(g_logger_lock);
    g_file = std::make_unique<std::ofstream>();
    g_file->open(filename, std::ios_base::out | std::ios_base::app);
    if (!g_file->is_open()) {
        std::cerr << "Cannot open log file " << filename << ", ignoring" << std::endl;
        g_file = nullptr;
    }
}
