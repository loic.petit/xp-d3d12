#pragma once

#include <imgui/imgui.h>
#include <imgui_node_editor.h>

#include "core/event.h"
#include "core/rendertarget.h"

namespace ed = ax::NodeEditor;

class RenderTarget;

class CommandList;

class Window;

class Resource;

class GUI {
public:
    explicit GUI(Window& w);

    ~GUI();

    void winMsg(UINT msg, WPARAM wParam, LPARAM lParam, UINT& result);

    void load(const RenderTarget::Desc& desc);

    /**
     * Begin a new ImGui frame. Do this before calling any ImGui functions that modifies ImGui's render context.
     */
    void newFrame(const std::shared_ptr<CommandList>& commandList);

    /**
     * Render ImgGui to the given render target.
     */
    void render(RenderTarget& renderTarget);

    /**
     * Stage a texture in the descriptor heap
     */
    D3D12_GPU_DESCRIPTOR_HANDLE stageTexture(Resource& texture);

    /**
     * Set the font scaling for ImGui (this should be called when the window's DPI scaling changes.
     */
    void setScaling(float scale);

    bool capturingKeyboard();

    bool capturingMouse();

    CommandList& stagingList() const {
        return *m_stagedCommandList;
    }

    void image(Resource& resource, const ImVec2& size, const ImVec2& uv0 = ImVec2(0, 0), const ImVec2& uv1 = ImVec2(1, 1), const ImVec4& tint_col = ImVec4(1, 1, 1, 1), const ImVec4& border_col = ImVec4(0, 0, 0, 0)) {
        ImGui::Image(reinterpret_cast<ImTextureID>(stageTexture(resource).ptr), size, uv0, uv1, tint_col, border_col);
    }

    ImFont* getLargeFont();
    ImFont* getPrettyFont();

private:
    HWND m_hWnd;
    ImGuiContext* m_imguiCtx;
    ImFont* m_largeFont;
    ax::NodeEditor::EditorContext* m_nodesCtx;
    ComPtr<ID3D12DescriptorHeap> m_srvDescHeap;
    uint8_t m_stagedTextureId = 0;
    std::shared_ptr<CommandList> m_stagedCommandList;
    std::vector<Resource*> m_stagedTextures;
    DelegateHandle m_msgHandle;
    ImFont* m_prettyFont;
};
