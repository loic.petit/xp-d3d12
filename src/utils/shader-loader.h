#pragma once

class ShaderBlob : public ComPtr<ID3DBlob> {
public:
    operator D3D12_SHADER_BYTECODE() const { // NOLINT(google-explicit-constructor)
        return {
            Get()->GetBufferPointer(),
            Get()->GetBufferSize()
        };
    }
};

class ShaderLoader {
public:
    static ShaderBlob compile(const wchar_t* sourceFile, const D3D_SHADER_MACRO *defines, const char* target);
    static ShaderBlob compileString(const std::string& source, const D3D_SHADER_MACRO *defines, const char* target);
};

#define SHADER_FILE(source) SHADERS_DIR L##source

#define COMPILE_PS(source, defines) ShaderLoader::compile(SHADERS_DIR L##source, defines, "ps_5_1")
#define COMPILE_VS(source, defines) ShaderLoader::compile(SHADERS_DIR L##source, defines, "vs_5_1")
#define COMPILE_CS(source, defines) ShaderLoader::compile(SHADERS_DIR L##source, defines, "cs_5_1")

#define COMPILE_STR_PS(source, defines) ShaderLoader::compileString(source, defines, "ps_5_1")
#define COMPILE_STR_VS(source, defines) ShaderLoader::compileString(source, defines, "vs_5_1")
#define COMPILE_STR_CS(source, defines) ShaderLoader::compileString(source, defines, "cs_5_1")
