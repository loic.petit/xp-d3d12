#pragma once

#include <memory>

/**
 * \brief No-move vector. Doesn't support remove, doesn't support reallocation
 * \tparam T Content type
 */
template<class T>
class NmVector {
private:
    using StorageType = std::aligned_storage_t<sizeof(T), alignof(T)>;
    static_assert(sizeof(StorageType) == sizeof(T));

public:
    NmVector(NmVector const&) = delete;

    NmVector& operator=(NmVector const&) = delete;

    explicit NmVector(size_t capacity): capacity_{capacity},
                                        data_{std::make_unique<StorageType[]>(capacity)} {
    }

    ~NmVector() {
        for (size_t i = 0; i < size_; i++)
            reinterpret_cast<T&>(data_[i]).~T();
    }

    template<class... Args>
    T& emplace_back(Args&&... args) {
        if (size_ == capacity_)
            throw std::bad_alloc{};
        new(&data_[size_]) T{std::forward<Args>(args)...};
        return reinterpret_cast<T&>(data_[size_++]);
    }

    T& operator[](size_t i) { return reinterpret_cast<T&>(data_[i]); }
    T const& operator[](size_t i) const { return reinterpret_cast<T const&>(data_[i]); }
    [[nodiscard]] size_t size() const { return size_; }
    [[nodiscard]] size_t capacity() const { return capacity_; }
    [[nodiscard]] bool empty() const { return size_ == 0; }
    [[nodiscard]] T* data() { return reinterpret_cast<T*>(data_.get()); }
    [[nodiscard]] T const* data() const { return reinterpret_cast<T const*>(data_.get()); }
    T* begin() const {
        return reinterpret_cast<T*>(&data_[0]);
    }
    T* end() const {
        return reinterpret_cast<T*>(&data_[size()]);
    }

private:
    size_t const capacity_;
    std::unique_ptr<StorageType[]> const data_;
    size_t size_{0};
};
