#include "utils/utils.h"

static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wstringConverter;

std::string wstringToUtf8(const std::wstring& str) {
    return wstringConverter.to_bytes(str.c_str());
}

std::wstring utf8ToWstring(const std::string& str) {
    return wstringConverter.from_bytes(str.c_str());
}
