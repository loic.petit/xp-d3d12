#include "mips.h"

#include "service.h"
#include "core/device.h"
#include "core/commandlist.h"

#include "utils/shader-loader.h"

struct Args {
    uint32_t mipLevel;
    uint32_t dimension;
    DirectX::XMFLOAT2 texel;
    uint32_t isSRGB;
};

constexpr uint32_t GROUP_SIZE = 8;

void Mips::load() {
    CD3DX12_DESCRIPTOR_RANGE1 src(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0,
                                  D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE);
    CD3DX12_DESCRIPTOR_RANGE1 output(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0, 0,
                                     D3D12_DESCRIPTOR_RANGE_FLAG_DATA_VOLATILE);

    CD3DX12_ROOT_PARAMETER1 rootParameters[3];
    rootParameters[0].InitAsConstants(sizeof(Args) / 4, 0);
    rootParameters[1].InitAsDescriptorTable(1, &src);
    rootParameters[2].InitAsDescriptorTable(1, &output);

    CD3DX12_STATIC_SAMPLER_DESC linearClampSampler(0, D3D12_FILTER_MIN_MAG_MIP_LINEAR,
                                                   D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP,
                                                   D3D12_TEXTURE_ADDRESS_MODE_CLAMP);

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(_countof(rootParameters), rootParameters, 1,
                                                            &linearClampSampler);

    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE rootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_CS CS;
    } stream;

    // Shaders
    ShaderBlob cs = COMPILE_CS("mips/compute.hlsl", nullptr);
    stream.CS = cs;

    init(rootSignatureDesc, stream, stream.rootSignature);
}

template<typename T>
inline T divide(T value, size_t alignment) {
    return (T) ((value + alignment - 1) / alignment);
}

void Mips::execute(CommandList& list, Resource& texture) {
    // Texture state is
    // * mip 0: COPY_DEST
    // * other mips: COMMON
    stageDescriptors(list, 1, texture.srv());

    const D3D12_RESOURCE_DESC& resourceDesc = texture->GetDesc();
    const int lastMip = resourceDesc.MipLevels - 1;
    std::vector<D3D12_RESOURCE_BARRIER> barriers(lastMip+1);
    auto unormFormat = Resource::getUnormFromSRGB(resourceDesc.Format);
    bool isSRGB = unormFormat != resourceDesc.Format;
    for (uint32_t mip = 0; mip < lastMip; mip++) {
        uint32_t srcWidth = resourceDesc.Width >> mip;
        uint32_t srcHeight = resourceDesc.Height >> mip;
        uint32_t dstWidth = srcWidth >> 1;
        uint32_t dstHeight = srcHeight >> 1;

        dstWidth = std::max<DWORD>(1, dstWidth);
        dstHeight = std::max<DWORD>(1, dstHeight);

        struct Args args = {
                mip,
                ((srcHeight & 1) << 1) | (srcWidth & 1),
                {1.0f / (float) dstWidth, 1.0f / (float) dstHeight},
                isSRGB
        };
        list->SetComputeRoot32BitConstants(0, sizeof(Args) / 4, &args, 0);
        {
            D3D12_UNORDERED_ACCESS_VIEW_DESC desc = {
                    unormFormat, D3D12_UAV_DIMENSION_TEXTURE2D
            };
            desc.Texture2D.MipSlice = mip + 1;

            Get<Device>()->CreateUnorderedAccessView(texture.ptr(), nullptr, &desc, stageAllocated(list, 2));
        }
        barriers[0] = CD3DX12_RESOURCE_BARRIER::Transition(texture.ptr(), mip != 0 ? D3D12_RESOURCE_STATE_UNORDERED_ACCESS : D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, mip);
        barriers[1] = CD3DX12_RESOURCE_BARRIER::Transition(texture.ptr(), mip != 0 ? D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE : D3D12_RESOURCE_STATE_COMMON, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, mip+1);
        list->ResourceBarrier(2, barriers.data());
        commitDescriptors(list);
        list->Dispatch(divide(dstWidth, GROUP_SIZE), divide(dstHeight, GROUP_SIZE), 1);
    }
    for (uint32_t mip = 0; mip < lastMip; mip++) {
        barriers[mip] = CD3DX12_RESOURCE_BARRIER::Transition(texture.ptr(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COMMON, mip);
    }
    barriers[lastMip] = CD3DX12_RESOURCE_BARRIER::Transition(texture.ptr(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COMMON, lastMip);
    list->ResourceBarrier(lastMip+1, barriers.data());
    // New state is COMMON
    // But it will get promoted to PIXEL_SHADER_RESOURCE when it is used for the first time
}
