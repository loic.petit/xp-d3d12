#include "gui.h"

#include "core/window.h"
#include "core/rendertarget.h"
#include "core/commandlist.h"
#include "core/resource.h"

#include <imgui/imgui_impl_dx12.h>
#include <imgui/imgui_impl_win32.h>

#include "utils/service.h"
#include "utils/utils.h"

#include <imgui_internal.h>

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

GUI::GUI(Window& w) {
    m_msgHandle = w.onMsg.connect(this, &GUI::winMsg);
    m_hWnd = w.hwnd();

    m_imguiCtx = ImGui::CreateContext();
    ed::Config config;
    config.SettingsFile = nullptr;
    config.CanvasSizeMode = ed::CanvasSizeMode::FitHorizontalView;
    config.NavigateButtonIndex = 2;
    config.ContextMenuButtonIndex = 1;
    m_nodesCtx = ed::CreateEditor(&config);
    ed::SetCurrentEditor(m_nodesCtx);
    ImGui::SetCurrentContext(m_imguiCtx);
    if (!ImGui_ImplWin32_Init(m_hWnd)) {
        throw std::exception("Failed to initialize ImGui");
    }
}

void GUI::load(const RenderTarget::Desc& desc) {
    D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
    heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    heapDesc.NumDescriptors = 256;
    heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    ThrowIfFailed(Get<Device>()->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&m_srvDescHeap)));
    ImGui_ImplDX12_Init(Get<Device>().ptr(), 3,
                        desc.pixelFormats[0], m_srvDescHeap.Get(),
                        m_srvDescHeap->GetCPUDescriptorHandleForHeapStart(),
                        m_srvDescHeap->GetGPUDescriptorHandleForHeapStart());


    ImGuiIO& io = ImGui::GetIO();

    io.FontGlobalScale = ::GetDpiForWindow(m_hWnd) / 96.0f;
    // Allow user UI scaling using CTRL+Mouse Wheel scrolling
    io.FontAllowUserScaling = true;

    ImFontConfig config = ImFontConfig();
    config.SizePixels = 21.0f;
    io.Fonts->AddFontDefault();
    m_largeFont = io.Fonts->AddFontDefault(&config);

    ImFontConfig prettyConfig = ImFontConfig();
    prettyConfig.OversampleH = 8;
    prettyConfig.OversampleV = 4;
    m_prettyFont = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\DejaVuSansMono.ttf", 14, &prettyConfig);
}

void GUI::winMsg(UINT msg, WPARAM wParam, LPARAM lParam, UINT& result) {
    assert(CurrentThreadId() == WINDOW && "Should have been called from the main thread");
    result = ImGui_ImplWin32_WndProcHandler(m_hWnd, msg, wParam, lParam);
}

void GUI::newFrame(const std::shared_ptr<CommandList>& commandList) {
    ImGui::SetCurrentContext(m_imguiCtx);
    ImGui_ImplDX12_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
    ed::SetCurrentEditor(m_nodesCtx);

    m_stagedTextureId = 1;
    m_stagedTextures.clear();
    m_stagedCommandList = commandList;
}

void GUI::render(RenderTarget& renderTarget) {
    ImGui::Render();

    auto& list = *m_stagedCommandList;
    renderTarget.use(list);
    ID3D12DescriptorHeap* heaps = m_srvDescHeap.Get();
    list->SetDescriptorHeaps(1, &heaps);
    ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), list.ptr());
    m_stagedCommandList.reset();
}

D3D12_GPU_DESCRIPTOR_HANDLE GUI::stageTexture(Resource& texture) {
    uint8_t offset = m_stagedTextureId++;
    const UINT size = Get<Device>()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    D3D12_CPU_DESCRIPTOR_HANDLE dest = CD3DX12_CPU_DESCRIPTOR_HANDLE(m_srvDescHeap->GetCPUDescriptorHandleForHeapStart(), offset, size);
    Get<Device>()->CopyDescriptorsSimple(1, dest, texture.srv(), D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    m_stagedTextures.emplace_back(&texture);
    return CD3DX12_GPU_DESCRIPTOR_HANDLE(m_srvDescHeap->GetGPUDescriptorHandleForHeapStart(), offset, size);
}

GUI::~GUI() {
    if (m_srvDescHeap) {
        ImGui::EndFrame();
        ImGui_ImplDX12_Shutdown();
    }
    ImGui_ImplWin32_Shutdown();
    ed::DestroyEditor(m_nodesCtx);
    ImGui::DestroyContext(m_imguiCtx);
    m_nodesCtx = nullptr;
    m_imguiCtx = nullptr;
    m_srvDescHeap = nullptr;
}

void GUI::setScaling(float scale) {
    ImGuiIO& io = ImGui::GetIO();
    io.FontGlobalScale = scale;
}

bool GUI::capturingKeyboard() {
    ImGuiIO& io = ImGui::GetIO();
    return io.WantCaptureKeyboard;
}

bool GUI::capturingMouse() {
    ImGuiIO& io = ImGui::GetIO();
    return io.WantCaptureMouse;
}

ImFont* GUI::getLargeFont() {
    return m_largeFont;
}

ImFont* GUI::getPrettyFont() {
    return m_prettyFont;
}
