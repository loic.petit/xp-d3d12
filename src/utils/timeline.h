#pragma once

class Timeline {
public:
    Timeline();

    Timeline(float* time, size_t lastEntry, void* vectors);

    [[nodiscard]] DirectX::XMVECTOR lerp(float currentTime) const;

    [[nodiscard]] DirectX::XMVECTOR slerp(float currentTime) const;

    [[nodiscard]] float getEndTime() const;

    [[nodiscard]] bool empty() const;

protected:
    [[nodiscard]] std::pair<size_t, float> interpolationTime(float currentTime) const;

    float* m_time;
    size_t m_last_entry;
    void* m_vectors;
};
