#pragma once
#include <codecvt>

using namespace Microsoft::WRL;

#ifndef _DEBUG
    #ifndef assert
        #define assert(youpi)
    #endif
#endif

// From DXSampleHelper.h 
// Source: https://github.com/Microsoft/DirectX-Graphics-Samples
inline void ThrowIfFailed(HRESULT hr) {
    if (FAILED(hr)) {
        throw std::exception();
    }
}

std::string wstringToUtf8(const std::wstring& str);
std::wstring utf8ToWstring(const std::string& str);
