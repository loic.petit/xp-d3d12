#pragma once

#include "core/pipeline.h"

class Resource;

class Mips : public Pipeline {
public:
    explicit Mips() : Pipeline(true) {};

    void load();

    void execute(CommandList& list, Resource& texture);

private:
};

