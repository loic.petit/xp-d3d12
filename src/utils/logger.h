#pragma once

enum LogLevel {
    TRACE = 0,
    DEBUG = 1,
    INFO = 2,
    WARN = 3,
    CRIT = 4
};

extern "C" {

void logger_printf(LogLevel level, const char* file, int line, const char* fmt, ...);
void logger_set_level(LogLevel level);
void logger_set_file_output(const char* filename);

}
#define BOOL_STR(b) ((b) ? "yes" : "no")

#define LOG_TRACE(...) logger_printf(LogLevel::TRACE, __FILE__, __LINE__, __VA_ARGS__)
#define LOG_DEBUG(...) logger_printf(LogLevel::DEBUG, __FILE__, __LINE__, __VA_ARGS__)
#define LOG_INFO(...) logger_printf(LogLevel::INFO, __FILE__, __LINE__, __VA_ARGS__)
#define LOG_WARN(...) logger_printf(LogLevel::WARN, __FILE__, __LINE__, __VA_ARGS__)
#define LOG_ERROR(...) logger_printf(LogLevel::CRIT, __FILE__, __LINE__, __VA_ARGS__)
