#pragma once

#include <Windows.h>

// common stl calls

// c-standard
#include <cstdarg>
#include <cstddef>
#include <cstdint>

// core containers and utils
#include <array>
#include <chrono>
#include <functional>
#include <unordered_map>
#include <utility>
#include <map>
#include <optional>
#include <queue>
#include <random>
#include <memory>
#include <vector>

// string
#include <string>
// io
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
// except
#include <exception>
#include <stdexcept>
// threads
#include <thread>
#include <mutex>
#include <shared_mutex>

// comptr
#include <wrl/client.h>
#include <comdef.h>

// we're ok to open all WRL stuff to everyone
using namespace Microsoft::WRL;

// d3d12
#include <d3d12.h>
#include <d3dx12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
