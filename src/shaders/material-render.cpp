#include "material-render.h"

#include <core/device.h>
#include <utils/shader-loader.h>
#include <scenes/nodes/graph.h>
#include <utils/logger.h>
#include <utils/service.h>

enum MaterialRender::Parameters : uint32_t {
    MATRICES,
    NODE_MATRICES,
    TEXTURES,
    _COUNT
};

struct MaterialRender::Mat {
    DirectX::XMMATRIX modelMatrix;
    DirectX::XMMATRIX modelViewProjectionMatrix;
    DirectX::XMFLOAT4 viewPosition;
};

constexpr DXGI_FORMAT DEPTH_FORMAT = DXGI_FORMAT_D32_FLOAT;
constexpr DXGI_FORMAT G_BUFFER_FORMAT = DXGI_FORMAT_R16G16B16A16_FLOAT;

void MaterialRender::load(const Graph& graph) {
    D3D12_INPUT_ELEMENT_DESC layout[] = {
        {
            "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
        {
            "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
        {
            "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 2, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
        {
            "COLOR", 0, DXGI_FORMAT_R8G8B8A8_UINT, 3, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
        {
            "JOINTS", 0, DXGI_FORMAT_R16G16B16A16_UINT, 4, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
        {
            "WEIGHTS", 0, DXGI_FORMAT_R8G8B8A8_UINT, 5, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
        {
            "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 6, D3D12_APPEND_ALIGNED_ELEMENT,
            D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
        },
    };

    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
        CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC BlendDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    CD3DX12_DESCRIPTOR_RANGE1 textureDescriptorRange[1] = {
        // t0-t4
        {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 0, 0, 0},
    };
    CD3DX12_DESCRIPTOR_RANGE1 nodesDescriptorRange[1] = {
        // t0, space1
        {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 1},
    };
    // b0
    rootParameters[Parameters::MATRICES].InitAsConstantBufferView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
                                                                  D3D12_SHADER_VISIBILITY_VERTEX);
    // t0, space1
    rootParameters[Parameters::NODE_MATRICES].InitAsDescriptorTable(
        _countof(nodesDescriptorRange), nodesDescriptorRange, D3D12_SHADER_VISIBILITY_VERTEX);
    // t0, space0
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(
        _countof(textureDescriptorRange), textureDescriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

    std::vector<CD3DX12_STATIC_SAMPLER_DESC> samplers({
        // s0
        {0, D3D12_FILTER_ANISOTROPIC},
    });

    ShaderBlob vs;
    ShaderBlob ps; {
        ShaderBuilder builder;
        if (!graph.refresh(builder)) {
            LOG_ERROR("Material is not ready!");
            return;
        }

        std::stringstream finalPs;
        {
            std::ifstream psFile(SHADER_FILE("training/material-render-ps.hlsl"));
            std::string line;
            while(std::getline(psFile, line))
            {
                if (line.find("SHADER_BODY") != std::string::npos) {
                    finalPs << builder.m_shaderBody.str();
                } else if (line.find("SHADER_DECLARATION") != std::string::npos) {
                    finalPs << builder.m_shaderDeclaration.str();
                } else {
                    finalPs << line;
                }
                finalPs << "\n";
            }
        }
        std::string pixelShader = finalPs.str();
        D3D_SHADER_MACRO vertexShaderMacros[2] = {
            {"HAS_SKIN", "1"},
            {}
        };

        printf("COMPILE\n%s", pixelShader.c_str());

        // Shaders
        vs = COMPILE_VS("training/material-render-vs.hlsl", vertexShaderMacros);
        ps = COMPILE_STR_PS(pixelShader, nullptr);
        pipeline.VS = vs;
        pipeline.PS = ps;

        textureDescriptorRange[0].NumDescriptors = builder.m_uvs;
    }

    // input layout
    pipeline.InputLayout = {layout, _countof(layout)};
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Formats
    pipeline.DSVFormat = DEPTH_FORMAT;
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
        {
            DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_R16G16B16A16_FLOAT,
            DXGI_FORMAT_R16G16B16A16_FLOAT
        },
        4
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC{1, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT{});
    pipeline.RasterizerDesc = rasterizerDesc;


    CD3DX12_BLEND_DESC blendDesc = CD3DX12_BLEND_DESC(CD3DX12_DEFAULT{});
    blendDesc.IndependentBlendEnable = true;
    D3D12_RENDER_TARGET_BLEND_DESC& blendDescRT = blendDesc.RenderTarget[0];

    blendDescRT.BlendEnable = true;
    blendDescRT.BlendOp = D3D12_BLEND_OP_ADD;
    blendDescRT.SrcBlend = D3D12_BLEND_SRC_ALPHA;
    blendDescRT.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
    blendDescRT.SrcBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.DestBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.BlendOpAlpha = D3D12_BLEND_OP_ADD;
    blendDescRT.LogicOp = D3D12_LOGIC_OP_NOOP;
    blendDescRT.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

    blendDesc.RenderTarget[1].BlendEnable = false;
    blendDesc.RenderTarget[2].BlendEnable = false;
    blendDesc.RenderTarget[3].BlendEnable = false;
    pipeline.BlendDesc = blendDesc;

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
        _countof(rootParameters), rootParameters,
        samplers.size(), samplers.data(),
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}

void MaterialRender::setPoV(DirectX::XMVECTOR viewPosition, DirectX::XMMATRIX viewMatrix,
                            DirectX::XMMATRIX projectionMatrix) {
    DirectX::XMStoreFloat4(&m_viewPosition, viewPosition);
    m_viewProjectionMatrix = viewMatrix * projectionMatrix;
}

void MaterialRender::execute(CommandList& list) {
}

void MaterialRender::submitModelMatrices(CommandList& list, const DirectX::XMMATRIX& modelMatrix) {
    Mat mat {
            modelMatrix,
            modelMatrix * m_viewProjectionMatrix,
            m_viewPosition
    };
    list->SetGraphicsRootConstantBufferView(Parameters::MATRICES, list.uploadTemporary(mat));
}

void MaterialRender::submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address,
uint32_t elements) {
    auto& heap = list.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    uint32_t srvOffset = heap.allocate(1);
    auto& buffer = list.getCurrentBuffer();
    UINT64 offset = address - buffer->GetGPUVirtualAddress();
    D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
    srvDesc.Format = DXGI_FORMAT_UNKNOWN;
    srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
    srvDesc.Buffer.FirstElement = offset / sizeof(DirectX::XMMATRIX);
    srvDesc.Buffer.NumElements = elements;
    srvDesc.Buffer.StructureByteStride = sizeof(DirectX::XMMATRIX);
    srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
    srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    Get<Device>()->CreateShaderResourceView(buffer.ptr(), &srvDesc, heap.cpu(srvOffset));
    markStaged(Parameters::NODE_MATRICES, srvOffset);
}

void MaterialRender::submitTexture(CommandList& list, const D3D12_CPU_DESCRIPTOR_HANDLE& address, uint32_t offset) {
    stageDescriptors(list, Parameters::TEXTURES, address, 1, offset);
}
