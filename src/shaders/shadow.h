#pragma once

#include "core/pipeline.h"

class CommandList;

class ShadowPipeline : public Pipeline {
public:
    explicit ShadowPipeline();

    void load(DXGI_FORMAT dsvFormat);

    void setPoV(DirectX::XMMATRIX mvpMatrix);

    void submitNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices);

    D3D12_GPU_VIRTUAL_ADDRESS prepareNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices);

    void submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address, uint32_t elements);

    void submitMatrices(CommandList& list, DirectX::XMMATRIX modelMatrix);

private:
    struct Mat;
    enum Parameters;
    DirectX::XMMATRIX m_viewProjectionMatrix = DirectX::XMMatrixIdentity();
};