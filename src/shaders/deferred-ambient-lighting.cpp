#include "deferred-ambient-lighting.h"
#include "core/device.h"
#include "core/commandlist.h"

#include "utils/shader-loader.h"

using namespace Microsoft::WRL;

enum DeferredAmbientLighting::Parameters {
    PARAMS,
    TEXTURES,
    _COUNT
};

DeferredAmbientLighting::DeferredAmbientLighting() : Pipeline(false) {
}

void DeferredAmbientLighting::load() {
    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
        CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC BlendDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    // b0
    rootParameters[Parameters::PARAMS].InitAsConstants(6, 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRange[1] = {
            // t0-t1
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 3, 0, 0},
    };
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(_countof(descriptorRange), descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

    // input layout
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/screenspace-vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_PS("training/ambient-light-ps.hlsl", nullptr);
    pipeline.VS = vs;
    pipeline.PS = ps;

    // Formats
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
            {DXGI_FORMAT_R16G16B16A16_FLOAT}, 1
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC {1, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    pipeline.RasterizerDesc = rasterizerDesc;


    CD3DX12_BLEND_DESC blendDesc = CD3DX12_BLEND_DESC(CD3DX12_DEFAULT {});
    D3D12_RENDER_TARGET_BLEND_DESC& blendDescRT = blendDesc.RenderTarget[0];
    blendDescRT.BlendEnable = true;
    blendDescRT.BlendOp = D3D12_BLEND_OP_ADD;
    blendDescRT.SrcBlend = D3D12_BLEND_ONE;
    blendDescRT.DestBlend = D3D12_BLEND_ONE;
    blendDescRT.SrcBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.DestBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.BlendOpAlpha = D3D12_BLEND_OP_MAX;
    blendDescRT.LogicOp = D3D12_LOGIC_OP_NOOP;
    blendDescRT.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
    pipeline.BlendDesc = blendDesc;

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    0, nullptr,
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}

void DeferredAmbientLighting::execute(CommandList& list, const DirectX::XMVECTOR& color, bool enableAO, bool enableAOMapping, float power) {
    uint32_t flags = ((uint32_t)enableAO) | (((uint32_t)enableAOMapping) << 1);
    list->SetGraphicsRoot32BitConstants(Parameters::PARAMS, 4, &color, 0);
    list->SetGraphicsRoot32BitConstants(Parameters::PARAMS, 1, &flags, 4);
    list->SetGraphicsRoot32BitConstants(Parameters::PARAMS, 1, &power, 5);
    commitDescriptors(list);
    list->DrawInstanced(3, 1, 0, 0);
}

void DeferredAmbientLighting::submitGBuffers(CommandList& list, RenderTarget& gBuffers, Resource& resource) {
    D3D12_CPU_DESCRIPTOR_HANDLE handles[] {
            gBuffers.getBuffer(0).srv(),
            gBuffers.getBuffer(1).srv(),
            resource.srv()
    };
    stageDescriptors(list, Parameters::TEXTURES, handles, _countof(handles), 0);
}
