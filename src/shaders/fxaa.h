#pragma once

#include "core/pipeline.h"
#include "core/rendertarget.h"

class FxaaPipeline : public Pipeline {
public:
    explicit FxaaPipeline();

    void load();

    void execute(CommandList& list, Resource& source, RenderTarget& target);
private:
    enum Parameters;
    struct FxaaParams;
};