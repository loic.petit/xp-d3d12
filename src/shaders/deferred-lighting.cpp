#include "deferred-lighting.h"
#include "core/device.h"
#include "core/commandlist.h"
#include "utils/utils.h"
#include "scenes/lights/spotlight.h"

#include "utils/shader-loader.h"

using namespace Microsoft::WRL;

struct DeferredLighting::Params {
    DirectX::XMMATRIX invViewProjectionMatrix;
    DirectX::XMFLOAT4 viewPosition;
    struct {
        DirectX::XMFLOAT4 position;
        DirectX::XMFLOAT4 color;
        DirectX::XMMATRIX viewProjectionMatrix;
    } light;
};

enum DeferredLighting::Parameters {
    FEATURE_MASK,
    PARAMS,
    MATRIX,
    TEXTURES,
    SHADOW,
    _COUNT
};

DeferredLighting::DeferredLighting() : Pipeline(false) {
}

void DeferredLighting::load() {
    D3D12_INPUT_ELEMENT_DESC layout[] = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
    };

    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL DepthStencil;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
        CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC BlendDesc;
    };

    PipelineStateStream pipeline = {};

    CD3DX12_DEPTH_STENCIL_DESC depthStencil = CD3DX12_DEPTH_STENCIL_DESC(CD3DX12_DEFAULT());
    depthStencil.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
    depthStencil.DepthEnable = TRUE;
    depthStencil.DepthFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
    depthStencil.StencilEnable = FALSE;
    pipeline.DepthStencil = depthStencil;
    pipeline.DSVFormat = DXGI_FORMAT_D32_FLOAT;
    
    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    // b0
    rootParameters[Parameters::FEATURE_MASK].InitAsConstants(1, 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
    // b1
    rootParameters[Parameters::PARAMS].InitAsConstantBufferView(1, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
    // b2
    rootParameters[Parameters::MATRIX].InitAsConstantBufferView(2, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_VERTEX);
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRange[1] = {
            // t0-t4
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 5, 0, 0},
    };
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(_countof(descriptorRange), descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRangeShadow[1] = {
            // t0 space1
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 1},
    };
    rootParameters[Parameters::SHADOW].InitAsDescriptorTable(_countof(descriptorRangeShadow), descriptorRangeShadow, D3D12_SHADER_VISIBILITY_PIXEL);
    CD3DX12_STATIC_SAMPLER_DESC samplers[] = {
            // s0
            {0, D3D12_FILTER_ANISOTROPIC},
            // s1
            {1, D3D12_FILTER_COMPARISON_ANISOTROPIC}
    };
    samplers[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    samplers[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    samplers[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    samplers[1].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS;

    // input layout
    pipeline.InputLayout = {layout, _countof(layout)};
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/volume-light-vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_PS("training/gbuffer-light-ps.hlsl", nullptr);
    pipeline.VS = vs;
    pipeline.PS = ps;

    // Formats
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
            {DXGI_FORMAT_R16G16B16A16_FLOAT}, 1
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC {1, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    rasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
    pipeline.RasterizerDesc = rasterizerDesc;


    CD3DX12_BLEND_DESC blendDesc = CD3DX12_BLEND_DESC(CD3DX12_DEFAULT {});
    D3D12_RENDER_TARGET_BLEND_DESC& blendDescRT = blendDesc.RenderTarget[0];
    blendDescRT.BlendEnable = true;
    blendDescRT.BlendOp = D3D12_BLEND_OP_ADD;
    blendDescRT.SrcBlend = D3D12_BLEND_ONE;
    blendDescRT.DestBlend = D3D12_BLEND_ONE;
    blendDescRT.SrcBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.DestBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.BlendOpAlpha = D3D12_BLEND_OP_MAX;
    blendDescRT.LogicOp = D3D12_LOGIC_OP_NOOP;
    blendDescRT.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
    pipeline.BlendDesc = blendDesc;

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    _countof(samplers), samplers,
                    D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}

void DeferredLighting::setPoV(const DirectX::XMVECTOR& viewPosition, const DirectX::XMMATRIX& invViewMatrix, const DirectX::XMMATRIX& invProjectionMatrix, const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix) {
    XMStoreFloat4(&m_viewPosition, viewPosition);
    m_invViewProjectionMatrix = invProjectionMatrix * invViewMatrix;
    m_viewProjectionMatrix = viewMatrix * projectionMatrix;
}

void DeferredLighting::setFeatures(CommandList& list, uint32_t features) {
    list->SetGraphicsRoot32BitConstant(Parameters::FEATURE_MASK, features, 0);
}

void DeferredLighting::createBuffers(RenderTarget& gBuffers) {
    gBuffers.createBuffer(L"LightMain", DXGI_FORMAT_R16G16B16A16_FLOAT, true);
}

void DeferredLighting::submitGBuffers(CommandList& list, RenderTarget& gBuffers) {
    D3D12_CPU_DESCRIPTOR_HANDLE handles[] {
            gBuffers.getBuffer(0).srv(),
            gBuffers.getBuffer(1).srv(),
            gBuffers.getBuffer(2).srv(),
            gBuffers.getBuffer(3).srv(),
            gBuffers.getDepthStencil().srv(),
    };
    stageDescriptors(list, Parameters::TEXTURES, handles, _countof(handles), 0);
}

void DeferredLighting::execute(CommandList& list, Spotlight& light) {
    Params params {
            m_invViewProjectionMatrix,
            m_viewPosition,
            {
                light.m_position,
                light.m_color,
                light.m_mvpMatrix
            }
    };
    list->SetGraphicsRootConstantBufferView(Parameters::PARAMS, list.uploadTemporary(params));
    list->SetGraphicsRootConstantBufferView(Parameters::MATRIX, list.uploadTemporary(light.m_modelMatrix * m_viewProjectionMatrix));
    D3D12_CPU_DESCRIPTOR_HANDLE handle = light.m_shadow.getDepthStencil().srv();
    stageDescriptors(list, Parameters::SHADOW, handle, 1, 0);
    commitDescriptors(list);
    light.drawMesh(list);
}

