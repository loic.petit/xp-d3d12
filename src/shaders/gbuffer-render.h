#pragma once

#include "core/pipeline.h"
#include "core/rendertarget.h"

class CommandList;
struct LegacyMaterial;
struct FadeSelect;

class GBufferRenderPipeline : public Pipeline {
public:
    explicit GBufferRenderPipeline();

    void load();

    static void createBuffers(RenderTarget& gBuffers);

    const std::vector<std::string>& layout() {
        return m_layout;
    }

    void setPoV(DirectX::XMVECTOR viewPosition, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);

    D3D12_GPU_VIRTUAL_ADDRESS prepareNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices);

    inline void submitNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices) {
        submitNodeMatrices(list, prepareNodeMatrices(list, matrices), matrices.size());
    }

    void submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address, uint32_t elements);

    void submitModelMatrices(CommandList& list, DirectX::XMMATRIX modelMatrix);

    void submitTextures(CommandList& list, D3D12_CPU_DESCRIPTOR_HANDLE* srvs, uint32_t count);
    
    void submitMaterial(CommandList& list, const LegacyMaterial& material, const FadeSelect& fadeSelect);

private:
    struct Mat;
    struct MaterialParameters;
    enum Parameters;
    std::vector<std::string> m_layout;
    uint32_t m_features = 0;
    DirectX::XMFLOAT4 m_viewPosition = {};
    DirectX::XMMATRIX m_viewProjectionMatrix = DirectX::XMMatrixIdentity();
};