#include "forward-render.h"
#include "core/commandlist.h"
#include "gltf/load.h"

#include "utils/shader-loader.h"

using namespace Microsoft::WRL;

struct ForwardRenderPipeline::Mat {
    DirectX::XMMATRIX modelMatrix;
    DirectX::XMMATRIX modelViewProjectionMatrix;
    DirectX::XMFLOAT4 viewPosition;
};

struct ForwardRenderPipeline::LightParameter {
    ForwardRenderPipeline::Light lights[8];
    uint32_t numLights;
};

enum ForwardRenderPipeline::Parameters {
    FEATURE_MASK,
    MATRICES,
    MATERIAL,
    LIGHTS,
    NODE_MATRICES,
    TEXTURES,
    SHADOWS,
    _COUNT
};

ForwardRenderPipeline::ForwardRenderPipeline() : Pipeline(false) {
}

void ForwardRenderPipeline::load(DXGI_FORMAT pixelFormat, DXGI_FORMAT depthFormat, uint32_t samples) {
    D3D12_INPUT_ELEMENT_DESC layout[] = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,    1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"TANGENT",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 2, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"COLOR",    0, DXGI_FORMAT_R8G8B8A8_UINT,      3, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"JOINTS",   0, DXGI_FORMAT_R16G16B16A16_UINT,  4, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"WEIGHTS",  0, DXGI_FORMAT_R8G8B8A8_UINT,      5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       6, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
    };
    // store the layout for gltf loading
    m_layout.reserve(_countof(layout));
    for (auto& elt: layout) {
        m_layout.emplace_back(elt.SemanticName);
    }

    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
        CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC BlendDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRange[1] = {
            // t0-t4
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 5, 0, 0},
    };
    CD3DX12_DESCRIPTOR_RANGE1 shadowRange[1] = {
            // t0+, space1
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 3, 0, 1},
    };
    // b0
    rootParameters[Parameters::FEATURE_MASK].InitAsConstants(1, 0, 0, D3D12_SHADER_VISIBILITY_ALL);
    // b1
    rootParameters[Parameters::MATRICES].InitAsConstantBufferView(1, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_VERTEX);
    // b2
    rootParameters[Parameters::MATERIAL].InitAsConstantBufferView(2, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
    // b3
    rootParameters[Parameters::LIGHTS].InitAsConstantBufferView(3, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_ALL);
    // b4
    rootParameters[Parameters::NODE_MATRICES].InitAsConstantBufferView(4, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_VERTEX);
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(_countof(descriptorRange), descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);
    rootParameters[Parameters::SHADOWS].InitAsDescriptorTable(_countof(shadowRange), shadowRange, D3D12_SHADER_VISIBILITY_PIXEL);

    CD3DX12_STATIC_SAMPLER_DESC samplers[] = {
            // s0
            {0, D3D12_FILTER_ANISOTROPIC},
            // s1
            {1, D3D12_FILTER_COMPARISON_ANISOTROPIC},
            {2, D3D12_FILTER_MIN_MAG_MIP_LINEAR}
    };
//    samplers[0].MaxLOD = 1;
//    samplers[0].MinLOD = 1;
    samplers[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    samplers[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    samplers[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
    samplers[1].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS;
    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    _countof(samplers), samplers,
                    D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    // input layout
    pipeline.InputLayout = {layout, _countof(layout)};
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_PS("training/ps.hlsl", nullptr);
    pipeline.VS = vs;
    pipeline.PS = ps;

    // Formats
    pipeline.DSVFormat = depthFormat;
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
            {pixelFormat}, 1
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC {samples, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    pipeline.RasterizerDesc = rasterizerDesc;

    CD3DX12_BLEND_DESC blendDesc = CD3DX12_BLEND_DESC(CD3DX12_DEFAULT {});
    D3D12_RENDER_TARGET_BLEND_DESC& blendDescRT = blendDesc.RenderTarget[0];
    blendDescRT.BlendEnable = true;
    blendDescRT.BlendOp = D3D12_BLEND_OP_ADD;
    blendDescRT.SrcBlend = D3D12_BLEND_SRC_ALPHA;
    blendDescRT.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
    blendDescRT.SrcBlendAlpha = D3D12_BLEND_ONE;
    blendDescRT.DestBlendAlpha = D3D12_BLEND_ZERO;
    blendDescRT.BlendOpAlpha = D3D12_BLEND_OP_ADD;
    blendDescRT.LogicOp = D3D12_LOGIC_OP_NOOP;
    blendDescRT.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
    pipeline.BlendDesc = blendDesc;

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}

void ForwardRenderPipeline::setPoV(DirectX::XMVECTOR viewPosition, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) {
    XMStoreFloat4(&m_viewPosition, viewPosition);
    m_viewProjectionMatrix = viewMatrix * projectionMatrix;
}

void ForwardRenderPipeline::submitModelMatrices(CommandList& list, DirectX::XMMATRIX modelMatrix) {
    Mat mat {
            modelMatrix,
            modelMatrix * m_viewProjectionMatrix,
            m_viewPosition
    };
    list->SetGraphicsRootConstantBufferView(Parameters::MATRICES, list.uploadTemporary(mat));
}

D3D12_GPU_VIRTUAL_ADDRESS ForwardRenderPipeline::prepareNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices) {
    return list.uploadTemporary(matrices.data(), matrices.size() * sizeof(DirectX::XMMATRIX));
}

void ForwardRenderPipeline::submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address) {
    list->SetGraphicsRootConstantBufferView(Parameters::NODE_MATRICES, address);
}

void ForwardRenderPipeline::submitLights(CommandList& list, const std::vector<Light>& lights) {
    struct LightParameter param;
    memcpy_s(param.lights, sizeof(param.lights), lights.data(), lights.size() * sizeof(Light));
    param.numLights = lights.size();
    list->SetGraphicsRootConstantBufferView(Parameters::LIGHTS, list.uploadTemporary(param));
}

void ForwardRenderPipeline::submitMaterial(CommandList& list, LegacyMaterial& material) {
    list->SetGraphicsRootConstantBufferView(Parameters::MATERIAL, list.uploadTemporary(material));
    list->SetGraphicsRoot32BitConstant(Parameters::FEATURE_MASK, material.features, 0);
}

void ForwardRenderPipeline::submitTextures(CommandList& list, D3D12_CPU_DESCRIPTOR_HANDLE baseSRV) {
    stageDescriptors(list, Parameters::TEXTURES, baseSRV);
}

void ForwardRenderPipeline::submitShadows(CommandList& list, D3D12_CPU_DESCRIPTOR_HANDLE* targets, uint32_t numTargets) {
    if (numTargets == 0) {
        return;
    }
    stageDescriptors(list, Parameters::SHADOWS, targets, numTargets);
}
