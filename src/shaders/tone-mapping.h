#pragma once

#include "core/pipeline.h"
#include "core/rendertarget.h"

class CommandList;

class ToneMapping : public Pipeline {
public:
    explicit ToneMapping();

    void load();

    void execute(CommandList& list, float exposure, Resource& input, RenderTarget& target);
private:
    enum Parameters;
};