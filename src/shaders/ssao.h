#pragma once

#include "core/pipeline.h"
#include "core/commandlist.h"
#include "utils/camera.h"
#include "core/rendertarget.h"

class SSAOPipeline : public Pipeline {
public:
    explicit SSAOPipeline();

    void load();

    void setParameters(CommandList& list, const Camera& camera, uint32_t sampleCount, float radius);

    void execute(CommandList& list, Resource& depth, Resource& normal, RenderTarget& output);
private:
    enum Parameters;
    struct Params;
    uint32_t m_sampleCount = 0;
    Resource m_samples;
    Resource m_randomRotations;
    D3D12_CPU_DESCRIPTOR_HANDLE m_randomRotationsDescriptor;
    D3D12_CPU_DESCRIPTOR_HANDLE m_samplesDescriptor;
};