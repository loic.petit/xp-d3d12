#pragma once

#include "core/pipeline.h"

class CommandList;
struct LegacyMaterial;

class ForwardRenderPipeline : public Pipeline {
public:
    typedef struct Light {
        DirectX::XMFLOAT4 lightPosition;
        DirectX::XMFLOAT4 lightColor;
        DirectX::XMMATRIX viewProjectionMatrix;
    } Light;

    explicit ForwardRenderPipeline();

    void load(DXGI_FORMAT pixelFormat, DXGI_FORMAT depthFormat, uint32_t samples);

    const std::vector<std::string>& layout() {
        return m_layout;
    }

    void setPoV(DirectX::XMVECTOR viewPosition, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);

    void submitLights(CommandList& list, const std::vector<Light>& lights);

    D3D12_GPU_VIRTUAL_ADDRESS prepareNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices);

    inline void submitNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices) {
        submitNodeMatrices(list, prepareNodeMatrices(list, matrices));
    }

    void submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address);

    void submitModelMatrices(CommandList& list, DirectX::XMMATRIX modelMatrix);

    void submitTextures(CommandList& list, D3D12_CPU_DESCRIPTOR_HANDLE baseSRV);

    void submitShadows(CommandList& list, D3D12_CPU_DESCRIPTOR_HANDLE* targets, uint32_t numTargets);

    void submitMaterial(CommandList& list, LegacyMaterial& material);

private:
    struct Mat;
    struct LightParameter;
    enum Parameters;
    std::vector<std::string> m_layout;
    DirectX::XMFLOAT4 m_viewPosition = {};
    DirectX::XMMATRIX m_viewProjectionMatrix = DirectX::XMMatrixIdentity();
};