#include "tone-mapping.h"
#include "core/device.h"
#include "core/commandlist.h"

#include "utils/shader-loader.h"

using namespace Microsoft::WRL;

enum ToneMapping::Parameters {
    PARAMS,
    TEXTURES,
    _COUNT
};

ToneMapping::ToneMapping() : Pipeline(false) {
}

void ToneMapping::load() {
    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    // b0
    rootParameters[Parameters::PARAMS].InitAsConstants(1, 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRange[1] = {
            // t0
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0},
    };
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(_countof(descriptorRange), descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

    // input layout
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/screenspace-vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_PS("training/tone-mapping-ps.hlsl", nullptr);
    pipeline.VS = vs;
    pipeline.PS = ps;

    // Formats
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
            {DXGI_FORMAT_R8G8B8A8_UNORM}, 1
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC {1, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    pipeline.RasterizerDesc = rasterizerDesc;

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    0, nullptr,
                    D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}


void ToneMapping::execute(CommandList& list, float exposure, Resource& input, RenderTarget& target) {
    list.state(input, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE)
        .state(target.getBuffer(0), D3D12_RESOURCE_STATE_RENDER_TARGET).commitTransitions();
    target.use(list);
    list->SetGraphicsRoot32BitConstants(Parameters::PARAMS, 1, &exposure, 0);
    stageDescriptors(list, Parameters::TEXTURES, input.srv(), 1, 0);
    commitDescriptors(list);
    list->DrawInstanced(3, 1, 0, 0);
}

