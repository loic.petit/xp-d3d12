#pragma once

#include "core/pipeline.h"
#include "core/rendertarget.h"

class CommandList;
class Spotlight;

class DeferredLighting : public Pipeline {
public:
    explicit DeferredLighting();

    void load();

    static void createBuffers(RenderTarget& output);

    void setPoV(const DirectX::XMVECTOR& viewPosition, const DirectX::XMMATRIX& invViewMatrix, const DirectX::XMMATRIX& invProjectionMatrix, const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix);

    void setFeatures(CommandList& list, uint32_t features);

    void submitGBuffers(CommandList& list, RenderTarget& gBuffers);

    void execute(CommandList& list, Spotlight& light);
private:
    struct Params;
    enum Parameters;
    DirectX::XMFLOAT4 m_viewPosition = {};
    DirectX::XMMATRIX m_invViewProjectionMatrix = DirectX::XMMatrixIdentity();
    DirectX::XMMATRIX m_viewProjectionMatrix = DirectX::XMMatrixIdentity();
};