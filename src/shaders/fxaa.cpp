#include "fxaa.h"
#include "core/commandlist.h"

#include "utils/shader-loader.h"

enum FxaaPipeline::Parameters {
    PARAMS,
    TEXTURES,
    _COUNT
};

struct FxaaPipeline::FxaaParams {
    DirectX::XMFLOAT4 consoleRcpFrameOpt;
    DirectX::XMFLOAT4 consoleRcpFrameOpt2;
    DirectX::XMFLOAT4 console360RcpFrameOpt2;
    DirectX::XMFLOAT4 console360ConstDir;
    DirectX::XMFLOAT2 halfTexel;
    DirectX::XMFLOAT2 qualityRcpFrame;
    float qualitySubpix;
    float qualityEdgeThreshold;
    float qualityEdgeThresholdMin;
    float consoleEdgeSharpness;
    float consoleEdgeThreshold;
    float consoleEdgeThresholdMin;
};

constexpr float QUALITY_RCP_FRAME = 0.50;
constexpr float QUALITY_SUBPIX = 0.75;
constexpr float QUALITY_EDGE_THRESHOLD = 0.166;
constexpr float QUALITY_EDGE_THRESHOLD_MIN = 0.0833;
constexpr float CONSOLE_EDGE_SHARPNESS = 8.0;
constexpr float CONSOLE_EDGE_THRESHOLD = 0.125;
constexpr float CONSOLE_EDGE_THRESHOLD_MIN = 0.05;

constexpr DXGI_FORMAT BUFFER_FORMAT = DXGI_FORMAT_R8G8B8A8_UNORM;

FxaaPipeline::FxaaPipeline() : Pipeline(false) {
}

void FxaaPipeline::load() {
    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    // b0
    rootParameters[Parameters::PARAMS].InitAsConstants(sizeof(FxaaParams) / 4, 0, 0, D3D12_SHADER_VISIBILITY_PIXEL);
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRange[1] = {
            // t0
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0},
    };
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(_countof(descriptorRange), descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

    // input layout
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/screenspace-vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_PS("training/fxaa-ps.hlsl", nullptr);
    pipeline.VS = vs;
    pipeline.PS = ps;

    // Formats
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
            {BUFFER_FORMAT}, 1
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC {1, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    pipeline.RasterizerDesc = rasterizerDesc;

    CD3DX12_STATIC_SAMPLER_DESC samplers[] = {
            // s0
            {0, D3D12_FILTER_MIN_MAG_MIP_LINEAR},
    };

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    _countof(samplers), samplers,
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}

void FxaaPipeline::execute(CommandList& list, Resource& input, RenderTarget& target) {
    list.state(input, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE)
        .state(target.getBuffer(0), D3D12_RESOURCE_STATE_RENDER_TARGET).commitTransitions();
    target.use(list);
    const D3D12_RESOURCE_DESC& desc = input->GetDesc();
    float texelX = 1.0f / desc.Width;
    float texelY = 1.0f / desc.Height;
    FxaaParams params{
        /* consoleRcpFrameOpt */
        {-QUALITY_RCP_FRAME * texelX, -QUALITY_RCP_FRAME * texelY, QUALITY_RCP_FRAME * texelX, QUALITY_RCP_FRAME * texelY},
        /* consoleRcpFrameOpt2 */
        {-2 * texelX, -2 * texelY, 2 * texelX, 2 * texelY},
        /* console360RcpFrameOpt2 */
        {8 * texelX, 8 * texelY, -4 * texelX, -4 * texelY},
        /* console360ConstDir */
        {1.0, -1.0, 0.25, -0.25},
        /* halfTexel */
        {texelX / 2, texelY / 2},
        /* qualityRcpFrame */
        {texelX, texelY},
        /* qualitySubpix */
        QUALITY_SUBPIX,
        /* qualityEdgeThreshold */
        QUALITY_EDGE_THRESHOLD,
        /* qualityEdgeThresholdMin */
        QUALITY_EDGE_THRESHOLD_MIN,
        /* consoleEdgeSharpness */
        CONSOLE_EDGE_SHARPNESS,
        /* consoleEdgeThreshold */
        CONSOLE_EDGE_THRESHOLD,
        /* consoleEdgeThresholdMin */
        CONSOLE_EDGE_THRESHOLD_MIN,
    };
    list->SetGraphicsRoot32BitConstants(Parameters::PARAMS, sizeof(FxaaParams) / 4, &params, 0);
    stageDescriptors(list, Parameters::TEXTURES, input.srv(), 1, 0);
    commitDescriptors(list);
    list->DrawInstanced(3, 1, 0, 0);
}
