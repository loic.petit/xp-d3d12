#include "shadow.h"
#include "core/device.h"
#include "core/commandlist.h"
#include "utils/service.h"

#include "utils/shader-loader.h"

using namespace Microsoft::WRL;

struct ShadowPipeline::Mat {
    DirectX::XMMATRIX modelViewProjectionMatrix;
};

enum ShadowPipeline::Parameters {
    MATRICES,
    NODE_MATRICES,
    _COUNT
};

ShadowPipeline::ShadowPipeline() : Pipeline(false) {
}

void ShadowPipeline::load(DXGI_FORMAT dsvFormat) {
    D3D12_INPUT_ELEMENT_DESC layout[] = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,    1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"TANGENT",  0, DXGI_FORMAT_R32G32B32A32_FLOAT, 2, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"COLOR",    0, DXGI_FORMAT_R8G8B8A8_UINT,      3, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"JOINTS",   0, DXGI_FORMAT_R16G16B16A16_UINT,  4, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"WEIGHTS",  0, DXGI_FORMAT_R8G8B8A8_UINT,      5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
            {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,       6, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
    };

    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    // b1
    rootParameters[Parameters::MATRICES].InitAsConstants(sizeof(Mat) / 4, 1, 0, D3D12_SHADER_VISIBILITY_VERTEX);
    CD3DX12_DESCRIPTOR_RANGE1 nodesDescriptorRange[1] = {
        // t0, space1
        {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 1},
};
    rootParameters[Parameters::NODE_MATRICES].InitAsDescriptorTable(_countof(nodesDescriptorRange), nodesDescriptorRange, D3D12_SHADER_VISIBILITY_VERTEX);

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    0, nullptr,
                    D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );

    // input layout
    pipeline.InputLayout = {layout, _countof(layout)};
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/shadow-vs.hlsl", nullptr);
    pipeline.VS = vs;
    ShaderBlob ps = COMPILE_PS("training/shadow-ps.hlsl", nullptr);
    pipeline.PS = ps;

    // Formats
    pipeline.DSVFormat = dsvFormat;

    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    // rasterizerDesc.DepthBias = 1000000;
    // rasterizerDesc.DepthBiasClamp = 0.0f;
    // rasterizerDesc.SlopeScaledDepthBias = 1.5f;
    rasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
    pipeline.RasterizerDesc = rasterizerDesc;

    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}

void ShadowPipeline::setPoV(DirectX::XMMATRIX mvpMatrix) {
    m_viewProjectionMatrix = mvpMatrix;
}

void ShadowPipeline::submitMatrices(CommandList& list, DirectX::XMMATRIX modelMatrix) {
    Mat mat {
            modelMatrix * m_viewProjectionMatrix
    };
    list->SetGraphicsRoot32BitConstants(Parameters::MATRICES, sizeof(Mat) / 4, &mat, 0);
}

void ShadowPipeline::submitNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices) {
    submitNodeMatrices(list, ShadowPipeline::prepareNodeMatrices(list, matrices), matrices.size());
}

D3D12_GPU_VIRTUAL_ADDRESS ShadowPipeline::prepareNodeMatrices(CommandList& list, const std::vector<DirectX::XMMATRIX>& matrices) {
    return list.uploadTemporary(matrices.data(), matrices.size() * sizeof(DirectX::XMMATRIX));
}

void ShadowPipeline::submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address, uint32_t elements) {
    auto& heap = list.getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    uint32_t srvOffset = heap.allocate(1);
    auto& buffer = list.getCurrentBuffer();
    UINT64 offset = address - buffer->GetGPUVirtualAddress();
    D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
    srvDesc.Format = DXGI_FORMAT_UNKNOWN;
    srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
    srvDesc.Buffer.FirstElement = offset / sizeof(DirectX::XMMATRIX);
    srvDesc.Buffer.NumElements = elements;
    srvDesc.Buffer.StructureByteStride = sizeof(DirectX::XMMATRIX);
    srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
    srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    Get<Device>()->CreateShaderResourceView(buffer.ptr(), &srvDesc, heap.cpu(srvOffset));
    markStaged(Parameters::NODE_MATRICES, srvOffset);
}