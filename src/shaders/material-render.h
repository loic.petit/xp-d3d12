#pragma once

#include <core/pipeline.h>

class Graph;

class MaterialRender : public Pipeline {
public:
    explicit MaterialRender()
        : Pipeline(false) {
    }

    void load(const Graph& graph);

    void setPoV(DirectX::XMVECTOR viewPosition, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);

    void execute(CommandList& list);

    void submitModelMatrices(CommandList& list, const DirectX::XMMATRIX& xmmatrix);

    void submitNodeMatrices(CommandList& list, const D3D12_GPU_VIRTUAL_ADDRESS& address, uint32_t elements);

    void submitTexture(CommandList& list, const D3D12_CPU_DESCRIPTOR_HANDLE& address, uint32_t offset);

private:
    enum Parameters : uint32_t;
    struct Mat;
    DirectX::XMMATRIX m_viewProjectionMatrix;
    DirectX::XMFLOAT4 m_viewPosition;
};
