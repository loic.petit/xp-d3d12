#pragma once

#include "core/pipeline.h"
#include "core/rendertarget.h"

class CommandList;

class DeferredAmbientLighting : public Pipeline {
public:
    explicit DeferredAmbientLighting();

    void load();

    void submitGBuffers(CommandList& list, RenderTarget& gBuffers, Resource& resource);

    void execute(CommandList& list, const DirectX::XMVECTOR& color, bool enableAO, bool enableAOMapping, float power);
private:
    enum Parameters;
};