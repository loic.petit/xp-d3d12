#include "ssao.h"
#include "core/device.h"
#include "utils/service.h"

#include "utils/shader-loader.h"
#include "utils/utils.h"

struct SSAOPipeline::Params {
    DirectX::XMMATRIX viewProjectionMatrix;
    DirectX::XMMATRIX invViewProjectionMatrix;
    uint32_t sampleCount;
    float radius;
};

enum SSAOPipeline::Parameters {
    PARAMETERS,
    RANDOM_VECTORS,
    TEXTURES,
    _COUNT
};

constexpr uint32_t ROTATION_TEXTURE_SIZE = 4;

SSAOPipeline::SSAOPipeline() : Pipeline(false) {
}

void SSAOPipeline::load() {
    struct PipelineStateStream {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE RootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC SampleDesc;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER RasterizerDesc;
    };

    PipelineStateStream pipeline = {};

    // root signature
    CD3DX12_ROOT_PARAMETER1 rootParameters[Parameters::_COUNT] = {};
    // b0
    rootParameters[Parameters::PARAMETERS].InitAsConstantBufferView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
    rootParameters[Parameters::RANDOM_VECTORS].InitAsConstantBufferView(1, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, D3D12_SHADER_VISIBILITY_PIXEL);
    CD3DX12_DESCRIPTOR_RANGE1 descriptorRange[1] = {
            // t0-t2
            {D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 3, 0, 0},
    };
    rootParameters[Parameters::TEXTURES].InitAsDescriptorTable(_countof(descriptorRange), descriptorRange, D3D12_SHADER_VISIBILITY_PIXEL);

    // input layout
    pipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

    // Shaders
    ShaderBlob vs = COMPILE_VS("training/screenspace-vs.hlsl", nullptr);
    ShaderBlob ps = COMPILE_PS("training/ssao-ps.hlsl", nullptr);
    pipeline.VS = vs;
    pipeline.PS = ps;

    // Formats
    D3D12_RT_FORMAT_ARRAY rtvFormats = {
            {DXGI_FORMAT_R32_FLOAT}, 1
    };
    pipeline.RTVFormats = rtvFormats;
    pipeline.SampleDesc = DXGI_SAMPLE_DESC {1, 0};

    // Other metas
    CD3DX12_RASTERIZER_DESC rasterizerDesc = CD3DX12_RASTERIZER_DESC(CD3DX12_DEFAULT {});
    pipeline.RasterizerDesc = rasterizerDesc;

    CD3DX12_STATIC_SAMPLER_DESC samplers[] = {
            // s0
            {0, D3D12_FILTER_MIN_MAG_MIP_LINEAR},
    };

    CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc(
                    _countof(rootParameters), rootParameters,
                    _countof(samplers), samplers,
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                    D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
    );
    DescriptorHeap& heap = Get<Device>().getHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    m_randomRotationsDescriptor = heap.cpu(heap.allocate());
    m_samplesDescriptor = heap.cpu(heap.allocate());
    init(rootSignatureDesc, pipeline, pipeline.RootSignature);
}


void SSAOPipeline::setParameters(CommandList& list, const Camera& camera, uint32_t sampleCount, float radius) {
    if (sampleCount != m_sampleCount) {
        std::vector<DirectX::XMFLOAT4> samples;
        samples.resize(sampleCount);
        std::uniform_real_distribution<float> randomFloats(0.0, 1.0);
        std::default_random_engine generator;
        for (uint32_t i = 0 ; i < sampleCount ; i++) {
            DirectX::XMVECTOR sample = DirectX::XMVectorSet(
                    static_cast<float>(randomFloats(generator) * 2.0 - 1.0),
                    static_cast<float>(randomFloats(generator) * 2.0 - 1.0),
                    randomFloats(generator),
                    0
            );
            DirectX::XMVector3Normalize(sample);
            sample = DirectX::XMVectorScale(sample, randomFloats(generator));
            DirectX::XMStoreFloat4(&samples[i], sample);
        }
        auto& device = Get<Device>();
        m_samples = device.uploadBuffer(list, samples.data(), samples.size() * sizeof(DirectX::XMFLOAT4));
        ThrowIfFailed(m_samples->SetName(L"SSAO Sample Kernel"));
        m_samples.reset(D3D12_RESOURCE_STATE_COPY_DEST);
        list.state(m_samples, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
        samples.resize(ROTATION_TEXTURE_SIZE*ROTATION_TEXTURE_SIZE);
        for (uint32_t i = 0 ; i < ROTATION_TEXTURE_SIZE*ROTATION_TEXTURE_SIZE ; i++) {
            samples[i] = {
                    static_cast<float>(randomFloats(generator) * 2.0 - 1.0),
                    static_cast<float>(randomFloats(generator) * 2.0 - 1.0),
                    0,
                    1
            };
        }
        m_randomRotations = device.uploadTexture(list, CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_R32G32B32A32_FLOAT, ROTATION_TEXTURE_SIZE, ROTATION_TEXTURE_SIZE), samples.data(), samples.size() * sizeof(DirectX::XMFLOAT4));
        ThrowIfFailed(m_randomRotations->SetName(L"SSAO Rotations"));
        m_randomRotations.reset(D3D12_RESOURCE_STATE_COPY_DEST);
        device->CreateShaderResourceView(m_randomRotations.ptr(), nullptr, m_randomRotationsDescriptor);
        m_randomRotations.srv(m_randomRotationsDescriptor);
        list.state(m_randomRotations, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
        m_sampleCount = sampleCount;
    }
    Params const parameters {
        camera.get_ViewMatrix() * camera.get_ProjectionMatrix(),
        camera.get_InverseProjectionMatrix() * camera.get_InverseViewMatrix(),
        sampleCount,
        radius
    };
    list.commitTransitions();
    list->SetGraphicsRootConstantBufferView(Parameters::PARAMETERS, list.uploadTemporary(parameters));
}

void SSAOPipeline::execute(CommandList& list, Resource& depth, Resource& normal, RenderTarget& output) {
    output.use(list);
    D3D12_CPU_DESCRIPTOR_HANDLE handles[] {
            m_randomRotations.srv(),
            normal.srv(),
            depth.srv()
    };
    stageDescriptors(list, Parameters::TEXTURES, handles, _countof(handles), 0);
    list->SetGraphicsRootConstantBufferView(Parameters::RANDOM_VECTORS, m_samples->GetGPUVirtualAddress());
    commitDescriptors(list);
    list->DrawInstanced(3, 1, 0, 0);
}
