#include "stdafx.h"
#include "core/window.h"
#include "engine.h"
#include "scenes/materials/resource-manager.h"
#include "utils/service.h"

int main(int argc, char* argv[]) {
    // Windows 10 Creators update adds Per Monitor V2 DPI awareness context.
    // Using this awareness context allows the client area of the window
    // to achieve 100% scaling while still allowing non-client window content to
    // be rendered in a DPI sensitive fashion.
    SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);
    ThreadGuard mainThread(WINDOW);

    Service<Device>::build();
    Service<ResourceManager>::build();
    Service<Engine>::build(GetModuleHandle(NULL));

    Get<Device>().init();
    Get<ResourceManager>().init();
    Get<Engine>().init();

    Get<Engine>().start();
    Window::winRun();

    Service<Engine>::destroy();
    Service<ResourceManager>::destroy();
    Service<Device>::destroy();

    Device::reportLiveObjects();
    return 0;
}
