#pragma once
#include "thread-config.h"

extern thread_local ThreadId g_currentThreadId;

inline ThreadId CurrentThreadId() {
    return g_currentThreadId;
}

/**
 * \brief Will perform CoInit/SetThreadName/SetThreadId in the current thread
 */
class ThreadGuard {
public:
    ThreadGuard(ThreadId id): m_id(id) {
        initCurrentThread(id);
    }
    ~ThreadGuard() {
        stopCurrentThread(m_id);
    }
private:
    static void initCurrentThread(ThreadId id);
    static void stopCurrentThread(ThreadId id);
    ThreadId m_id;
};

class BaseThread {
public:
    explicit BaseThread(ThreadId id, std::function<void()> exec);

    template<typename C>
    explicit BaseThread(ThreadId id, C* instance, void (C::*exec)()) :
        BaseThread(id, [=] { (instance->*exec)(); }) {
    }

    virtual ~BaseThread();

    virtual void start();

    void join();

private:
    std::thread m_thread;
    ThreadId m_id;
    std::function<void()> m_exec;
};

class StoppableThread : public BaseThread {
public:
    explicit StoppableThread(ThreadId id, std::function<void()> tick);

    template<typename C>
    explicit StoppableThread(ThreadId id, C* instance, void (C::*tick)()) :
        StoppableThread(id, [=] { (instance->*tick)(); }) {
    }

    ~StoppableThread() override;

    void stop();

    [[nodiscard]] bool isAlive() const {
        return m_alive;
    }

private:
    std::atomic_bool m_alive;
};
