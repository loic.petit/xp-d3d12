#include "entity.h"

Entity World::create(const std::string& name) {
    return {m_entityCounter++, name, this};
}
