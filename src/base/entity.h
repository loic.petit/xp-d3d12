#pragma once

#include <utility>

#include "type_id.h"

template<typename T>
class MutableRef;
using Destructor = void (*)(void* pointer);
using Constructor = void* (*)();

using EntityId = uint64_t;

enum TermFlag : uint32_t {
    None = 0,
    In = 1,
    Out = 2,
    Optional = 4,
};

DEFINE_ENUM_FLAG_OPERATORS(TermFlag)

class Table {
public:
    struct Row {
        EntityId id = 0;
        bool isDirty = false;
        void* data = nullptr;
        Destructor dtor;

        Row(const Row& other) = delete;

        Row(Row&& other) noexcept {
            id = other.id;
            isDirty = other.isDirty;
            data = other.data;
            dtor = other.dtor;
            other.data = nullptr;
        }

        Row& operator=(const Row& other) = delete;

        Row& operator=(Row&& other) noexcept {
            id = other.id;
            isDirty = other.isDirty;
            data = other.data;
            dtor = other.dtor;
            other.data = nullptr;
            return *this;
        }

        Row(EntityId id, bool is_dirty, void* data, Destructor dtor)
            : id(id),
              isDirty(is_dirty),
              data(data),
              dtor(dtor) {
        }

        ~Row() {
            if (data) {
                dtor(data);
                data = nullptr;
            }
        }
    };

    Table(const Constructor ctor, const Destructor dtor)
        : m_ctor(ctor),
          m_dtor(dtor) {
    }

    template<typename T>
    const T& get(const EntityId id) {
        return *static_cast<T*>(m_data[id].data);
    }

    Row* row(const EntityId id) {
        const auto it = m_data.find(id);
        if (it == m_data.end()) {
            return nullptr;
        }
        return &it->second;
    }

    Row& add(EntityId id) {
        const auto it = m_data.find(id);
        if (it != m_data.end()) {
            throw std::exception("Entity id already exists");
        }
        return m_data.emplace_hint(it, id, Row(id, true, m_ctor(), m_dtor))->second;
    }

    void remove(const EntityId id) {
        m_data.erase(id);
    }

    [[nodiscard]] uint32_t count() const {
        return m_data.size();
    }

    std::unordered_map<EntityId, Row>::iterator begin() {
        return m_data.begin();
    }

    std::unordered_map<EntityId, Row>::iterator end() {
        return m_data.end();
    }

    std::unordered_map<EntityId, Row>& data() {
        return m_data;
    }

private:
    // dummy implementation for now
    std::unordered_map<EntityId, Row> m_data;
    Constructor m_ctor;
    Destructor m_dtor;
};

template<typename T>
class TableRef {
public:
    explicit TableRef(Table* m_table)
        : m_table(m_table) {
    }

    const T& get(const EntityId id) {
        return *static_cast<T*>(m_table->data().at(id).data);
    }

    MutableRef<T> mutation(EntityId id) {
        return MutableRef<T>(&m_table->data().at(id));
    }

    MutableRef<T> add(EntityId id) {
        return MutableRef<T>(&m_table->add(id));
    }
private:
    Table* m_table;
};

class Query {
public:
    struct Term {
        type_id_t m_type;
        TermFlag m_flags;
    };

    template<typename T>
    void add(const TermFlag flags = TermFlag::In | TermFlag::Out) {
        m_terms.emplace_back(Term{type_id<T>(), flags});
    }

    [[nodiscard]] std::vector<Term>& terms() {
        return m_terms;
    }

private:
    std::vector<Term> m_terms;
};

class QueryEntity {
private:
};

class Entity;

class World {
public:
    World() = default;

    template<typename T>
    TableRef<T> table() {
        return TableRef<T>(&table(type_id<T>()));
    }

    Table& table(const type_id_t type_id) {
        return m_tables.at(type_id);
    }

    Entity create(const std::string& name = "");

    template<typename T>
    void declare() {
        auto typeId = type_id<T>();
        auto it = m_tables.find(typeId);
        if (it != m_tables.end()) return;
        m_tables.emplace_hint(
            it,
            typeId,
            Table(
                [] {
                    return static_cast<void*>(new T());
                },
                [](void* x) {
                    delete reinterpret_cast<T*>(x);
                }
            )
        );
    }

    uint32_t query(Query& query, const std::function<void(EntityId id, Table::Row**)>& callback) {
        if (query.terms().empty()) return 0;
        uint32_t minSize = 0xffffffff;
        Table* minTable = nullptr;
        type_id_t minType = nullptr;
        for (const auto& term: query.terms()) {
            auto& currentTable = table(term.m_type);
            if (currentTable.count() == 0) return 0;
            if (currentTable.count() < minSize) {
                minTable = &currentTable;
                minType = term.m_type;
                minSize = currentTable.count();
            }
        }
        if (minTable == nullptr) {
            throw std::exception("Shouldnt happen");
        }

        uint32_t count = 0;
        std::vector<Table::Row*> data(query.terms().size(), nullptr);
        for (auto& it: *minTable) {
            const auto entityId = it.first;
            bool resolved = true;
            uint32_t i = 0;
            for (const auto& term: query.terms()) {
                if (term.m_type == minType) {
                    data[i] = &it.second;
                } else {
                    Table::Row* row = table(term.m_type).row(entityId);
                    if (row == nullptr) {
                        resolved = false;
                        break;
                    }
                    data[i] = row;
                }
                i++;
            }
            if (!resolved) {
                continue;
            }
            count++;
            callback(entityId, data.data());
        }
        return count;
    }

    template<typename Callable>
    uint32_t query(Callable&& userCallback) {
        // forces query_trait to be callable and resolve underlying types
        using QueryCallable = query_trait<Callable>;
        // get underlying types
        using ArgComponents = typename QueryCallable::component_type;
        using ArgSequence = typename ArgComponents::sequence_type;

        // forwarding to the actual implementation with the proper types resolved
        return queryImpl(std::forward<Callable>(userCallback), ArgComponents{}, ArgSequence{});
    }

private:
    /* META PROGRAMMING MADNESS */

    // ComponentTypes is a struct which will force type resolution of all components
    // Important part: its size is null and therefore has no stack allocation
    template<typename... Components>
    struct ComponentTypes {
        // contains a sequence type which will serve as a sequence for iteration over components when necessary
        using sequence_type = std::index_sequence_for<Components...>;
    };

    // query_trait is there to extract the tuple-type used in the search-implementation from a Callable
    // This will help the type resolver to ensure that we call with something that makes sense

    // base declaration
    template <typename T>
    struct query_trait : query_trait<decltype(&T::operator())> {};

    // specialization for function types
    template <typename ReturnType, typename... Args>
    struct query_trait<ReturnType(*)(EntityId, Args...)> {
        using component_type = ComponentTypes<std::remove_reference_t<Args>...>;
    };

    // specialization for member function types (lambdas)
    template <typename ClassType, typename ReturnType, typename... Args>
    struct query_trait<ReturnType(ClassType::*)(EntityId, Args...) const> {
        using component_type = ComponentTypes<std::remove_reference_t<Args>...>;
    };

    // Actual implementation with components and indices resolved
    template<typename Callable, typename... Components, std::size_t... Is>
    uint32_t queryImpl(Callable&& userCallback, ComponentTypes<Components...>, std::index_sequence<Is...>) {
        Query q;

        // Add terms to the query based on the component types and const-ness
        (q.add<std::remove_const_t<Components>>(std::is_const_v<Components> ? TermFlag::In : (TermFlag::In | TermFlag::Out)), ...);

        // Call the existing query function with the constructed query and wrapper callback
        return query(q, [&userCallback](EntityId id, Table::Row** rows) {
            userCallback(id, *static_cast<Components*>(rows[Is]->data)...);
        });
    }

    std::unordered_map<type_id_t, Table> m_tables;
    EntityId m_entityCounter = 0;
};

template<typename T>
class MutableRef {
public:
    explicit MutableRef(Table::Row* m_row)
        : m_row(m_row) {
    }

    ~MutableRef() {
        m_row->isDirty = true;
    }

    MutableRef& operator=(const MutableRef& o) noexcept {
        *m_row->data = *o.m_row->data;
        m_row->isDirty = true;
        return *this;
    }

    MutableRef& operator=(MutableRef&& o) noexcept {
        *m_row->data = *o.m_row->data;
        m_row->isDirty = true;
        return *this;
    }

    MutableRef& operator=(T&& o) noexcept {
        *m_row->data = o;
        m_row->isDirty = true;
        return *this;
    }

    MutableRef& operator=(const T& o) noexcept {
        *m_row->data = o;
        m_row->isDirty = true;
        return *this;
    }

    T* operator->() const {
        return (T*) m_row->data;
    }

private:
    Table::Row* m_row;
};

class Entity {
public:
    Entity(EntityId id, std::string name, World* world) : m_id(id), m_name(std::move(name)), m_world(world) {
    }

    template<typename T>
    MutableRef<T> mutation() {
        return m_world->table<T>().mutation(m_id);
    }

    template<typename T>
    const T& get() {
        return m_world->table<T>().get(m_id);
    }

    template<typename T>
    Entity& add() {
        addAndGet<T>();
        return *this;
    }

    template<typename T>
    MutableRef<T> addAndGet() {
        return m_world->table<T>().add(m_id);
    }

    [[nodiscard]] EntityId id() const { return m_id; };

    [[nodiscard]] const std::string& name() const { return m_name; };

private:
    std::string m_name;
    EntityId m_id;
    World* m_world;
};
