#pragma once

#include <objbase.h>

enum ThreadId {
    WINDOW = 0,
    RENDER,
    RESOURCE_MANAGER_CPU,
    RESOURCE_MANAGER_GPU,
    _THREAD_ID_COUNT
};

struct ThreadInfo {
    const wchar_t* threadName;
    COINIT coInit;
    int priority;
};

#ifdef __cplusplus
extern "C" {
#endif
extern const struct ThreadInfo THREAD_INFOS[_THREAD_ID_COUNT+1];
#ifdef __cplusplus
}
#endif
