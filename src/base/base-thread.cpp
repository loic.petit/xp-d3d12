#include "base-thread.h"

#include "thread-name.h"

thread_local ThreadId g_currentThreadId;

void ThreadGuard::initCurrentThread(ThreadId id) {
    g_currentThreadId = id;
    const auto& info = THREAD_INFOS[id];
    if (info.coInit)
        ThrowIfFailed(CoInitializeEx(nullptr, info.coInit));
    SetThreadName(info.threadName);
    SetThreadPriority(GetCurrentThread(), info.priority);
}

void ThreadGuard::stopCurrentThread(ThreadId id) {
    const auto& info = THREAD_INFOS[id];
    if (info.coInit)
        CoUninitialize();
}

BaseThread::BaseThread(ThreadId id, std::function<void()> exec): m_id(id), m_exec(std::move(exec)) {
}

BaseThread::~BaseThread() {
    join();
}

void BaseThread::start() {
    m_thread = std::thread([&]() {
        ThreadGuard tg(m_id);
        m_exec();
    });
}

void BaseThread::join() {
    if (m_thread.joinable()) {
        m_thread.join();
    }
}

StoppableThread::StoppableThread(ThreadId id, std::function<void()> tick) : //
    BaseThread(id, [this, tick_ = std::move(tick)] {
        m_alive = true;
        while (m_alive) {
            tick_();
        }
    }), m_alive(false) {
}

StoppableThread::~StoppableThread() {
    stop();
}

void StoppableThread::stop() {
    m_alive = false;
}
