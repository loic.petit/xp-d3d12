#include "thread-config.h"

const struct ThreadInfo THREAD_INFOS[] = {
    [WINDOW] = {L"Main", COINIT_APARTMENTTHREADED, THREAD_PRIORITY_ABOVE_NORMAL},
    [RENDER] = {L"Render", COINIT_MULTITHREADED, THREAD_PRIORITY_ABOVE_NORMAL},
    [RESOURCE_MANAGER_CPU] = {L"ResourceManager CPU", 0},
    [RESOURCE_MANAGER_GPU] = {L"ResourceManager GPU", 0},
    [_THREAD_ID_COUNT] = {L"UNKNOWN"},
};
