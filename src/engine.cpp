#include "engine.h"
#include "utils/logger.h"
#include "utils/markers.h"

#include <XInput.h>

#include "base/entity.h"
#include "scenes/materials/resource-manager.h"
#include "utils/service.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb_image_write.h"

constexpr int MSAA_LEVEL = 8;

Engine::Engine(HINSTANCE hInstance) : //
        Provider<EngineQueues>(this),
        Provider<EngineInput>(&m_inputs, RENDER),
        // create the queues
        m_directCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT),
        m_computeCommandQueue(D3D12_COMMAND_LIST_TYPE_COMPUTE),
        m_copyCommandQueue(D3D12_COMMAND_LIST_TYPE_COPY),
        m_renderThread(RENDER, this, &Engine::onUpdate),
        m_window(hInstance, L"XP D3D12", 1920, 1080),
        // msaa render target
        m_msaaRenderTarget(L"MSAA") {

    struct Test {
        uint32_t a;
    };
    World w;
    w.declare<Test>();
    TableRef<Test> t = w.table<Test>();
    t.add(1);
    auto mutable_ref = t.mutation(1);

    m_msgHandle = m_window.onMsg.connect(this, &Engine::onWindowsMsg);

    // m_device.setLowLatencyMode(m_latencyMode.enabled, m_latencyMode.boost, m_latencyMode.maxFps, m_latencyMode.useMarkers);
}

Engine::~Engine() {
    m_renderThread.stop();
    m_renderThread.join();
    Get<Device>().beginFlush(true);
    Service<GUI>::destroy();
}

void Engine::init() {
    Service<GUI>::build(RENDER, m_window);
    m_computeCommandQueue.init(L"Queue: Engine Compute");
    m_directCommandQueue.init(L"Queue: Engine Direct");
    m_copyCommandQueue.init(L"Queue: Engine Copy");
    LOG_INFO("Created command queues");

    SwapChain& swapChain = m_window.emplaceSwapChain();
    auto& desc = swapChain.getBackBuffer().desc();
    m_msaaRenderTarget.init(desc.width, desc.height, MSAA_LEVEL);
    m_msaaRenderTarget.createBuffer(L"MSAA Render", desc.pixelFormats[0]);
    m_msaaRenderTarget.createDepthStencil(L"MSAA DepthStencil", DXGI_FORMAT_D32_FLOAT);
    m_resizeHandle = swapChain.onResize.connect([&](uint32_t width, uint32_t height) {
        m_msaaRenderTarget.resize(width, height);
        m_scene.resize(width, height);
    });
    // EngineQueues shouldnt be used outside the render thread
    Service<EngineQueues>::setAffinity(RENDER);
}

bool Engine::start() {
    m_renderThread.start();
    m_window.show();
    return true;
}

void Engine::flush() {
    auto& device = Get<Device>();
    device.beginFlush(false);
    device.endFlush();
}

void Engine::onWindowsMsg(UINT message, WPARAM wParam, LPARAM lParam, UINT& result) {
    PIXScopedEvent(PIX_COLOR_INDEX(1), "Engine::onWindowsMsg()");
#ifdef HAS_REFLEX_MARKERS
    if (PCLSTATS_IS_PING_MSG_ID(message)) {
        m_needsPing = true;
        result = 0;
        return;
    }
#endif
    std::lock_guard lock(m_mutex);
    switch (message) {
        case WM_ACTIVATE:
            if (!wParam) {
                ZeroMemory(m_localInputs.wasd, sizeof(m_localInputs.wasd));
                m_localInputs.mouseButton = false;
            }
            break;
        case WM_KEYDOWN:
            if (m_localInputs.guiIsCapturingKeyboard) break;
            if (wParam == 0x57) // W
                m_localInputs.wasd[0] = true;
            else if (wParam == 0x41) // A
                m_localInputs.wasd[1] = true;
            else if (wParam == 0x53) // S
                m_localInputs.wasd[2] = true;
            else if (wParam == 0x44) // D
                m_localInputs.wasd[3] = true;
            result = 0;
            break;
        case WM_KEYUP:
            if (m_localInputs.guiIsCapturingKeyboard) break;
            if (wParam == 0x57) // W
                m_localInputs.wasd[0] = false;
            else if (wParam == 0x41) // A
                m_localInputs.wasd[1] = false;
            else if (wParam == 0x53) // S
                m_localInputs.wasd[2] = false;
            else if (wParam == 0x44) // D
                m_localInputs.wasd[3] = false;
            result = 0;
            break;
        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
            if (m_localInputs.guiIsCapturingMouse) break;
            m_localInputs.mouseButton = message == WM_LBUTTONDOWN;
            result = 0;
            break;
        case WM_MOUSEMOVE:
            if (m_localInputs.guiIsCapturingMouse) break;
            int32_t xPos = LOWORD(lParam);
            int32_t yPos = HIWORD(lParam);
            m_localInputs.mouseX = xPos;
            m_localInputs.mouseY = yPos;
            result = 0;
            break;
    }
}

void Engine::onRender() {
    PIXScopedEvent(PIX_COLOR_INDEX(0), "Engine::onRender()");
    SwapChain& swapChain = m_window.getSwapChain();


    LATENCY_MARKER(RENDERSUBMIT_START);

    Get<ResourceManager>().executeOnGpu();

    bool screenshot = ImGui::IsKeyPressed(ImGuiKey_P);
    std::shared_ptr<CommandList> list = m_directCommandQueue.createCommandList();
    {
        PIXScopedEvent(list->ptr(), PIX_COLOR_INDEX(0), "Engine::directCommandQueueSubmit()");
        RenderTarget& dest = swapChain.getBackBuffer();
        auto& gui = Get<GUI>();
        // do something
        if (!m_isLoaded) {
            m_scene.load(dest.desc());
            gui.load(dest.desc());
            m_isLoaded = true;
        }
        gui.newFrame(list);
        static bool materialEditorView = false;
        static bool gbufferView = false;
        static bool mainControls = true;
        static bool reflexSdkView = false;
        static bool resourceManagerView = false;
        static bool fadeSelectorView = true;
        static bool demoWindowShow = false;
        if (!screenshot && ImGui::BeginMainMenuBar()) {
            if (ImGui::BeginMenu("Window")) {
                ImGui::MenuItem("Material Editor", nullptr, &materialEditorView);
                ImGui::Separator();
                ImGui::MenuItem("Main Controls", nullptr, &mainControls);
                ImGui::MenuItem("GBuffer Viewer", nullptr, &gbufferView);
                ImGui::MenuItem("Reflex SDK", nullptr, &reflexSdkView);
                ImGui::MenuItem("Resource Manager", nullptr, &resourceManagerView);
                ImGui::MenuItem("Fade Selector", nullptr, &fadeSelectorView);
                ImGui::Separator();
                ImGui::MenuItem("Demo Window", nullptr, &demoWindowShow);
                ImGui::MenuItem("Take screenshot");
                ImGui::EndMenu();
            }
            ImGui::EndMainMenuBar();
        }
        if (demoWindowShow) {
            ImGui::ShowDemoWindow();
        }

        if (!materialEditorView) {
            m_scene.render(*list, dest);
        }

        //        m_msaaRenderTarget.resolveInto(*list, dest);
        m_scene.gui(mainControls && !screenshot, materialEditorView && !screenshot, gbufferView && !screenshot, fadeSelectorView && !screenshot);
        if (resourceManagerView) {
            Get<ResourceManager>().gui();
        }
        if (reflexSdkView) {
            reflexSdkGui();
        }
        gui.render(dest);

        dest.prepareToPresent(*list, 0);
    }
    Resource screenshotResource;
    D3D12_SUBRESOURCE_FOOTPRINT footprint;
    if (screenshot) {
        auto& device = Get<Device>();
        screenshotResource = device.downloadTextureInto(*list, swapChain.getBackBuffer().getBuffer(0), 0, footprint);
    }
    m_directCommandQueue.execute(list);
    LATENCY_MARKER(RENDERSUBMIT_END);
    if (screenshot) {
        m_directCommandQueue.waitForFence(m_directCommandQueue.latestFenceValue());

        void* mem;
        screenshotResource->Map(0, nullptr, &mem);
        stbi_write_png("screenshot.png", footprint.Width, footprint.Height, 4, mem, footprint.RowPitch);
        screenshotResource->Unmap(0, nullptr);
    }
    {
        PIXScopedEvent(PIX_COLOR_INDEX(0), "Engine::swapChainPresent()");
        LATENCY_MARKER(PRESENT_START);
        swapChain.present();
        LATENCY_MARKER(PRESENT_END);
    }
}

void Engine::reflexSdkGui() {
#ifdef HAS_REFLEX_MARKERS
    ImGui::Begin("Reflex SDK");
    ImGui::Checkbox("Enabled", &m_latencyMode.enabled);
    ImGui::Checkbox("Boost", &m_latencyMode.boost);
    ImGui::SliderInt("Max FPS", (int*) &m_latencyMode.maxFps, 0, 240);
    ImGui::Checkbox("Use markers", &m_latencyMode.useMarkers);
    if (ImGui::Button("Apply")) {
        Get<Device>().setLowLatencyMode(m_latencyMode.enabled, m_latencyMode.boost, m_latencyMode.maxFps, m_latencyMode.useMarkers);
    }
    ImGui::Separator();
    {
        NvAPI_Status status = NVAPI_OK;
        NV_GET_SLEEP_STATUS_PARAMS_V1 params = {};
        params.version = NV_GET_SLEEP_STATUS_PARAMS_VER1;

        status = NvAPI_D3D_GetSleepStatus(Get<Device>().ptr(), &params);
        if (status == NVAPI_OK) {
            ImGui::Text("Low latency mode: %d", params.bLowLatencyMode);
            ImGui::Text("Control panel vsync overriden: %d", params.bCplVsyncOn);
            ImGui::Text("Fullscreen VRR: %d", params.bFsVrr);
        } else {
            ImGui::Text("Failed to fetch sleep status %d", status);
        }
    }
    ImGui::Separator();
    ImGui::PushItemWidth(75);

    {
        if (m_reflexMeasures.status == NVAPI_OK && m_reflexMeasures.gpuRender != 0) {
            ImGui::LabelText("Status", "Success");
            ImGui::LabelText("Frame Time", "%d µs", (uint32_t) m_reflexMeasures.frameTime);
            ImGui::LabelText("Swap Chain Wait", "%d µs", (uint32_t) m_reflexMeasures.swapChainWait);
            ImGui::LabelText("Reflex Sleep", "%d µs", (uint32_t) m_reflexMeasures.reflexSleep);
            ImGui::LabelText("Game To Render", "%d µs", m_reflexMeasures.gameToRender);
            ImGui::LabelText("Simulation", "%d µs", m_reflexMeasures.sim);
            ImGui::LabelText("Render Submit", "%d µs", m_reflexMeasures.renderSubmit);
            ImGui::LabelText("Present", "%d µs", m_reflexMeasures.present);
            ImGui::LabelText("Driver", "%d µs", m_reflexMeasures.driver);
            ImGui::LabelText("Render Queue", "%d µs", m_reflexMeasures.osRenderQueue);
            ImGui::LabelText("Render", "%d µs", m_reflexMeasures.gpuRender);
        } else if (m_reflexMeasures.status == NVAPI_OK) {
            ImGui::LabelText("Status", "Waiting...");
        } else {
            ImGui::LabelText("Status", "Failed %d", m_reflexMeasures.status);
        }
    }
    ImGui::PopItemWidth();
    ImGui::End();
#endif
}

void Engine::updateFpsCounter() {
    static auto t0 = std::chrono::high_resolution_clock::now();

    auto t1 = std::chrono::high_resolution_clock::now();
    auto deltaTime = t1 - t0;
    t0 = t1;
    m_frameTimes[m_frameId % 64] = std::chrono::duration_cast<std::chrono::microseconds>(deltaTime).count();

#ifdef PIX_REFLEX_COUNTERS
    {
        NV_LATENCY_RESULT_PARAMS_V1 params = {};
        params.version = NV_LATENCY_RESULT_PARAMS_VER1;
        if (NvAPI_D3D_GetLatency(m_device.ptr(), &params) == NVAPI_OK && params.frameReport[63].gpuRenderEndTime != 0) {
            auto& report = params.frameReport[63];
            PIXReportCounter(L"Reflex::sim", report.simEndTime - report.simStartTime);
            PIXReportCounter(L"Reflex::renderSubmit", report.renderSubmitEndTime - report.renderSubmitStartTime);
            PIXReportCounter(L"Reflex::present", report.presentEndTime - report.presentStartTime);
            PIXReportCounter(L"Reflex::driver", report.driverEndTime - report.driverStartTime);
            PIXReportCounter(L"Reflex::osRenderQueue", report.osRenderQueueEndTime - report.osRenderQueueStartTime);
            PIXReportCounter(L"Reflex::gpuRender", report.gpuRenderEndTime - report.gpuRenderStartTime);
            PIXReportCounter(L"Reflex::gpuActiveRender", report.gpuActiveRenderTimeUs);
            PIXReportCounter(L"Reflex::frameTime", report.gpuFrameTimeUs);
        }
    }
#endif
    if ((m_frameId % 64) == 0) {
#ifdef HAS_REFLEX_MARKERS
        {
            NV_LATENCY_RESULT_PARAMS_V1 params = {};
            params.version = NV_LATENCY_RESULT_PARAMS_VER1;
            m_reflexMeasures.status = NvAPI_D3D_GetLatency(Get<Device>().ptr(), &params);
            if (m_reflexMeasures.status == NVAPI_OK && params.frameReport[63].gpuRenderEndTime != 0) {
                uint64_t gameToRender = 0;
                uint64_t sim = 0;
                uint64_t renderSubmit = 0;
                uint64_t present = 0;
                uint64_t driver = 0;
                uint64_t osRenderQueue = 0;
                uint64_t gpuRender = 0;
                uint32_t count = 0;
                for (int i = 0; i < 64; i++) {
                    auto& report = params.frameReport[i];
                    if (report.gpuRenderEndTime == 0 || report.driverStartTime == 0) continue;
                    count++;
                    gameToRender += report.gpuRenderEndTime - report.simStartTime;
                    sim += report.simEndTime - report.simStartTime;
                    renderSubmit += report.renderSubmitEndTime - report.renderSubmitStartTime;
                    present += report.presentEndTime - report.presentStartTime;
                    driver += report.driverEndTime - report.driverStartTime;
                    osRenderQueue += report.gpuRenderStartTime - report.osRenderQueueStartTime;
                    gpuRender += report.gpuRenderEndTime - report.gpuRenderStartTime;
                }
                m_reflexMeasures.gameToRender = gameToRender / count;
                m_reflexMeasures.sim = sim / count;
                m_reflexMeasures.renderSubmit = renderSubmit / count;
                m_reflexMeasures.present = present / count;
                m_reflexMeasures.driver = driver / count;
                m_reflexMeasures.osRenderQueue = osRenderQueue / count;
                m_reflexMeasures.gpuRender = gpuRender / count;
            }
        }
#endif
        uint64_t swapChainWait = 0;
        uint64_t reflexSleep = 0;
        uint64_t frameTime = 0;
        for (int i = 0; i < 64; i++) {
            frameTime += m_frameTimes[i];
            swapChainWait += m_swapChainWait[i];
            reflexSleep += m_reflexSleep[i];
        }
        m_reflexMeasures.frameTime = frameTime / 64;
        m_reflexMeasures.swapChainWait = swapChainWait / 64;
        m_reflexMeasures.reflexSleep = reflexSleep / 64;
        {
            wchar_t buffer[64];
            swprintf_s(buffer, 64, L"FPS: %f", 1000000.0 / m_reflexMeasures.frameTime);
            m_window.setTitle(buffer);
        }
    }
}

void Engine::onUpdate() {
    PIXScopedEvent(PIX_COLOR_INDEX(0), "Engine::onUpdate()");
    if (m_frameId == 0) {
    }
    m_frameId++;
    auto swapChainBefore = std::chrono::high_resolution_clock::now();
    m_window.getSwapChain().waitForBackBufferToBeReady();
    auto swapChainAfter = std::chrono::high_resolution_clock::now();
    m_swapChainWait[m_frameId % 64] = std::chrono::duration_cast<std::chrono::microseconds>(
            swapChainAfter - swapChainBefore).count();
    {
        PIXScopedEvent(PIX_COLOR_INDEX(0), "Engine::stateUpdate()");
#ifdef HAS_REFLEX_MARKERS
        auto reflexBefore = std::chrono::high_resolution_clock::now();
        {
            PIXScopedEvent(PIX_COLOR_INDEX(0), "NvAPI_D3D_Sleep");
            NvAPI_D3D_Sleep(Get<Device>().ptr());
        }
        auto reflexAfter = std::chrono::high_resolution_clock::now();
        m_reflexSleep[m_frameId % 64] = std::chrono::duration_cast<std::chrono::microseconds>(reflexAfter - reflexBefore).count();

        if (m_needsPing.exchange(false)) {
            LATENCY_MARKER(PC_LATENCY_PING);
        }
        LATENCY_MARKER(SIMULATION_START);
#endif
        updateFpsCounter();
        updateInputs();
        m_scene.update();
        LATENCY_MARKER(SIMULATION_END);
    }
    onRender();
}

void updateButtonPress(uint8_t& target, uint8_t input) {
    target &= INPUT_PRESSED;
    if (target && !input) {
        target = INPUT_NEGATIVE_EDGE | input;
    } else if (!target && input) {
        target = INPUT_POSITIVE_EDGE | input;
    }
}

void Engine::updateInputs() {
    PIXScopedEvent(PIX_COLOR_INDEX(0), "Engine::updateInputs()");
    XINPUT_STATE state;
    bool trigger = false;
    for (int i = 0; i < 4; i++) {
        if (XInputGetState(i, &state) == ERROR_SUCCESS) {
            if (state.Gamepad.wButtons) {
                trigger = true;
                break;
            }
        }
    }
    std::lock_guard lock(m_mutex);
    m_localInputs.guiIsCapturingKeyboard = Get<GUI>().capturingKeyboard();
    m_localInputs.guiIsCapturingMouse = Get<GUI>().capturingMouse();
    updateButtonPress(m_inputs.wasd[0], m_localInputs.wasd[0]);
    updateButtonPress(m_inputs.wasd[1], m_localInputs.wasd[1]);
    updateButtonPress(m_inputs.wasd[2], m_localInputs.wasd[2]);
    updateButtonPress(m_inputs.wasd[3], m_localInputs.wasd[3]);
    updateButtonPress(m_inputs.mouseButton, m_localInputs.mouseButton);
    updateButtonPress(m_inputs.trigger, trigger || m_localInputs.mouseButton);
    if (m_inputs.trigger & INPUT_POSITIVE_EDGE) {
        LATENCY_MARKER(TRIGGER_FLASH);
    }
    m_inputs.mouseDeltaX = m_localInputs.mouseX - m_inputs.mouseX;
    m_inputs.mouseDeltaY = m_localInputs.mouseY - m_inputs.mouseY;
    m_inputs.mouseX = m_localInputs.mouseX;
    m_inputs.mouseY = m_localInputs.mouseY;
}
