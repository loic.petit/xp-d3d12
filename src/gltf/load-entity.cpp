#include "./load-entity.h"

#include <utils/service.h>

#include "./load.h"

using namespace DirectX;


struct MeshData {
    std::vector<D3D12_VERTEX_BUFFER_VIEW> vertexViews;
    D3D12_INDEX_BUFFER_VIEW indexViews {};
    size_t indexCount {};
    size_t vertexCount {};
};

struct GpuStorage {
    std::vector<Resource> buffers;
};

#define offset(attr, base) (uint32_t)(reinterpret_cast<char const volatile*>(&(attr))-reinterpret_cast<const volatile char*>(&(base)))

EntityId createGrid(World& world, CommandList& copy, float w, float h, float d) {
    auto entity = world.create("Grid");
    struct Grid {
        XMFLOAT3 vertices[20];
        XMFLOAT3 normals[20];
        XMFLOAT4 tangents[20];
        uint8_t color[20][4];
        uint16_t joints[20][4];
        uint8_t weights[20][4];
        XMFLOAT2 texCoord[20];
    };

    Grid rawData = {
        {
            {-w, 0, d}, // 0 back
            {+w, 0, d},
            {-w, h, d},
            {+w, h, d},

            {-w, 0, -d}, // 4 left
            {-w, 0, +d},
            {-w, h, -d},
            {-w, h, +d},

            {-w, 0, -d}, // 8 floor
            {+w, 0, -d},
            {-w, 0, +d},
            {+w, 0, +d},

            {+w, 0, +d}, // 12 right
            {+w, 0, -d},
            {+w, h, +d},
            {+w, h, -d},

            {-w, h, +d}, // 16 ceiling
            {+w, h, +d},
            {-w, h, -d},
            {+w, h, -d},
        },
        // normals
        {
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},
            {0, 0, -1},

            {1, 0, 0},
            {1, 0, 0},
            {1, 0, 0},
            {1, 0, 0},

            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0},

            {-1, 0, 0},
            {-1, 0, 0},
            {-1, 0, 0},
            {-1, 0, 0},

            {0, -1, 0},
            {0, -1, 0},
            {0, -1, 0},
            {0, -1, 0},
        },
        // tangents
        {
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},

            {0, 0, -1, 1},
            {0, 0, -1, 1},
            {0, 0, -1, 1},
            {0, 0, -1, 1},

            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},

            {0, 0, 1, 1},
            {0, 0, 1, 1},
            {0, 0, 1, 1},
            {0, 0, 1, 1},

            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},
            {0, 1, 0, 1},
        },
        // colors
        {
            {255, 255, 255, 255},
            {127, 255, 127, 255},
            {127, 255, 127, 255},
            {0, 255, 0, 255},

            {255, 255, 255, 255},
            {127, 127, 255, 255},
            {127, 127, 255, 255},
            {0, 0, 255, 255},

            {255, 255, 255, 255},
            {255, 127, 127, 255},
            {255, 127, 127, 255},
            {255, 0, 0, 255},

            {255, 255, 255, 255},
            {127, 255, 255, 255},
            {127, 255, 255, 255},
            {0, 255, 255, 255},

            {255, 255, 255, 255},
            {0, 127, 255, 255},
            {0, 127, 255, 255},
            {255, 0, 255, 255},
        },
        // joints
        {},
        // weights
        {},
        // tex coords
        {
            {-w, 0}, // 0 back
            {+w, 0},
            {-w, h},
            {+w, h},

            {0, -d}, // 4 left
            {0, +d},
            {h, -d},
            {h, +d},

            {-w, -d}, // 8 floor
            {+w, -d},
            {-w, +d},
            {+w, +d},

            {0, +d}, // 12 right
            {0, -d},
            {h, +d},
            {h, -d},

            {-w, +d}, // 16 ceiling
            {+w, +d},
            {-w, -d},
            {+w, -d},
        }
    };
    MutableRef<GpuStorage> storage = entity.addAndGet<GpuStorage>();

    auto& device = Get<Device>();
    D3D12_GPU_VIRTUAL_ADDRESS gpuPtr = storage->buffers.emplace_back(
        device.uploadBuffer(copy, &rawData, sizeof(rawData))
    )->GetGPUVirtualAddress();

    auto mesh = entity.addAndGet<MeshData>();
    mesh->vertexViews = {
        {gpuPtr + offset(rawData.vertices, rawData), sizeof(rawData.vertices), sizeof(rawData.vertices[0])},
        {gpuPtr + offset(rawData.normals, rawData), sizeof(rawData.normals), sizeof(rawData.normals[0])},
        {gpuPtr + offset(rawData.tangents, rawData), sizeof(rawData.tangents), sizeof(rawData.tangents[0])},
        {gpuPtr + offset(rawData.color, rawData), sizeof(rawData.color), sizeof(rawData.color[0])},
        {gpuPtr + offset(rawData.joints, rawData), sizeof(rawData.joints), sizeof(rawData.joints[0])},
        {gpuPtr + offset(rawData.weights, rawData), sizeof(rawData.weights), sizeof(rawData.weights[0])},
        {gpuPtr + offset(rawData.texCoord, rawData), sizeof(rawData.texCoord), sizeof(rawData.texCoord[0])},
    };
    mesh->vertexCount = _countof(rawData.vertices);

    uint16_t indices[30] = {
        0, 2, 1,
        1, 2, 3,

        4, 6, 5,
        5, 6, 7,

        8, 10, 9,
        9, 10, 11,

        12, 14, 13,
        13, 14, 15,

        16, 18, 17,
        17, 18, 19,
    };
    gpuPtr = storage->buffers.emplace_back(
        device.uploadBuffer(copy, &indices, sizeof(indices))
    )->GetGPUVirtualAddress();

    mesh->indexViews = {gpuPtr, sizeof(indices), DXGI_FORMAT_R16_UINT};
    mesh->indexCount = _countof(indices);
    // mesh->materialId = 0;

    return entity.id();
}

/*
module materials
using flecs.components.graphics

Prefab BlueStone {
- Specular{specular_power: 0.8, shininess: 6.0}
- Rgb{0.05, 0.2117, 0.58}
}
Prefab Water {
- Specular{specular_power: 1.5, shininess: 6.0}
- Rgb{0.025, 0.4, 0.6}
}
Prefab Wood {
- Rgb{0.15, 0.1, 0.05}
}
Prefab BeaconMaterial {
- Emissive{1.0}
- Rgb{0.5, 1, 2.5}
}
*/