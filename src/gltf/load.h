#pragma once

#include "../core/resource.h"
#include "../core/device.h"
#include "utils/timeline.h"

struct MeshPrimitive {
    std::vector<D3D12_VERTEX_BUFFER_VIEW> vertexViews;
    D3D12_INDEX_BUFFER_VIEW indexViews {};
    size_t indexCount {};
    size_t vertexCount {};
    uint16_t materialId;

    inline void draw(ID3D12GraphicsCommandList* list) {
        list->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        list->IASetVertexBuffers(0, vertexViews.size(), vertexViews.data());
        list->IASetIndexBuffer(&indexViews);
        list->DrawIndexedInstanced(indexCount, 1, 0, 0, 0);
    }
};

struct NodeAnimation {
    Timeline translation {};
    Timeline rotation {};
};

struct Animation {
    std::string name;
    std::vector<NodeAnimation> nodes;
    float time;
};

struct Node {
    std::string name;
    DirectX::XMMATRIX localBindMatrix;
    DirectX::XMMATRIX bindMatrix;
    DirectX::XMMATRIX inverseBindMatrix;
    uint32_t parent;
    uint32_t joint;
};

typedef enum MaterialFeature {
    MATERIAL_NONE = 0,
    LIGHTING_ALBEDO_MAPPING = 0x1,
    LIGHTING_NORMAL_MAPPING = 0x2,
    LIGHTING_SRMA_MAPPING = 0x4,
    LIGHTING_SSS_MAPPING = 0x8,
    LIGHTING_MASK_MAPPING = 0x10,
    LIGHTING_ENABLE_SSS = 0x20,
    LIGHTING_SATURATE_DIFFUSE = 0x40,
    MATERIAL_HAS_SKELETON = 0x80,
    LIGHTING_HAS_SHADOWS = 0x100,
} MaterialFeature;

DEFINE_ENUM_FLAG_OPERATORS(MaterialFeature);

struct LegacyMaterial {
    DirectX::XMFLOAT4 albedo;
    DirectX::XMFLOAT4 srma;
    DirectX::XMFLOAT4 sss;
    DirectX::XMFLOAT4 customColor[3];
    float alphaClip;
    uint32_t features;
};

struct Object {
    std::vector<Resource> buffers;
    std::vector<std::string> buffersData;
    std::vector<MeshPrimitive> primitives;
    std::vector<Animation> animations;
    std::vector<Node> nodes;
    std::vector<uint32_t> joints;
    std::vector<LegacyMaterial> materials;
};

struct FadeSelect {
    DirectX::XMFLOAT4 innerColor;
    DirectX::XMFLOAT4 outerColor;
    float params[4];
};

void load_gltf(CommandList& copy, const std::vector<std::string>& inputLayout, const std::wstring& filename, Object& result);
