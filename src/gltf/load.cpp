#include "load.h"
#include "utils/logger.h"
#include "scenes/training.h"
#include "core/commandlist.h"
#include "utils/timeline.h"
#include "utils/utils.h"

#include <json.hpp>

#include <codecvt>

using namespace DirectX;

enum GL_ENUM {
    GL_BYTE = 5120,
    GL_UNSIGNED_BYTE = 5121,
    GL_SHORT = 5122,
    GL_UNSIGNED_SHORT = 5123,
    GL_INT = 5124,
    GL_UNSIGNED_INT = 5125,
    GL_FLOAT = 5126,
};

void patch_winding(uint16_t* index, size_t count);

void patch_quaternions(float* vectors, size_t count);

void gltf_buffers(nlohmann::json& gltf, int i, size_t& count, uint32_t& buffer, size_t& offset, uint32_t& size, DXGI_FORMAT& componentType) {
    auto& accessor = gltf["accessors"][i];
    auto& bufferView = gltf["bufferViews"][accessor["bufferView"].get<uint32_t>()];

    count = accessor["count"].get<uint32_t>();
    buffer = bufferView["buffer"].get<uint32_t>();
    offset = bufferView["byteOffset"].get<uint32_t>();
    size = bufferView["byteLength"].get<uint32_t>();

    switch (accessor["componentType"].get<int32_t>()) {
        case GL_BYTE:
            componentType = DXGI_FORMAT_R8_SINT;
            break;
        case GL_UNSIGNED_BYTE:
            componentType = DXGI_FORMAT_R8_UINT;
            break;
        case GL_SHORT:
            componentType = DXGI_FORMAT_R16_SINT;
            break;
        case GL_UNSIGNED_SHORT:
            componentType = DXGI_FORMAT_R16_UINT;
            break;
        case GL_INT:
            componentType = DXGI_FORMAT_R32_SINT;
            break;
        case GL_UNSIGNED_INT:
            componentType = DXGI_FORMAT_R32_UINT;
            break;
        case GL_FLOAT:
            componentType = DXGI_FORMAT_R32_FLOAT;
            break;
    }
}

void patch_winding(uint16_t* index, size_t count) {
    for (int i = 0; i < count; i += 3) {
        std::swap(index[i + 1], index[i + 2]);
    }
}

void patch_positions(float* positions, size_t count, int splice) {
    count *= splice;
    for (int i = 0; i < count; i += splice) {
        positions[i + 2] = -positions[i + 2];
    }
}

void patch_bitangent(float* tangent, size_t count) {
    for (int i = 0; i < count * 4; i += 4) {
        if (tangent[i + 3] != 1) {
            return;
        }
    }
    // we know all entries are 1, so we change them to -1 because why the f. not
    for (int i = 0; i < count * 4; i += 4) {
        tangent[i + 3] = -1;
    }
}

Animation load_animation(nlohmann::json& gltf, nlohmann::json& json, std::vector<std::string>& buffers) {
    Animation anim;
    float max_time = 0;
    for (auto& channel: json["channels"]) {
        auto& sampler = json["samplers"][channel["sampler"].get<int>()];
        auto input = sampler["input"].get<int>();
        auto output = sampler["output"].get<int>();
        auto node = channel["target"]["node"].get<int>();
        auto path = channel["target"]["path"].get<std::string>();
        if (node >= anim.nodes.size())
            anim.nodes.resize(node + 1);

        size_t count;
        uint32_t size;
        uint32_t buffer;
        size_t offset;
        DXGI_FORMAT format;

        gltf_buffers(gltf, input, count, buffer, offset, size, format);
        if (format != DXGI_FORMAT_R32_FLOAT) {
            throw std::exception();
        }
        auto* time = reinterpret_cast<float*>(buffers[buffer].data() + offset);
        if (max_time < time[count - 1]) {
            max_time = time[count - 1];
        }
        gltf_buffers(gltf, output, count, buffer, offset, size, format);
        if (format != DXGI_FORMAT_R32_FLOAT) {
            throw std::exception();
        }
        void* vectors = buffers[buffer].data() + offset;

        if (path == "translation") {
            anim.nodes[node].translation = Timeline(time, count - 1, vectors);
        } else {
            anim.nodes[node].rotation = Timeline(time, count - 1, vectors);
        }
    }
    anim.name = json["name"].get<std::string>();
    anim.time = max_time;
    return anim;
}

void load_node(nlohmann::json& json, int nodeId, std::vector<Node>& vector, XMMATRIX& transform) {
    auto& node = json[nodeId];
    auto& result = vector[nodeId];

    char buffer[5] = {};
    sprintf(buffer, "%04d", nodeId);
    result.name = std::string(buffer) + " " + node["name"].get<std::string>();
    result.localBindMatrix = XMMatrixIdentity();
    auto& rotation = node["rotation"];
    if (!rotation.is_null()) {
        auto float4 = XMFLOAT4(rotation[0].get<float>(), rotation[1].get<float>(), rotation[2].get<float>(), rotation[3].get<float>());
        result.localBindMatrix *= XMMatrixRotationQuaternion(XMLoadFloat4(&float4));
    }
    auto& translation = node["translation"];
    if (!translation.is_null()) {
        XMMATRIX matrixTranslation = XMMatrixTranslation(translation[0].get<float>(), translation[1].get<float>(), translation[2].get<float>());
        result.localBindMatrix *= matrixTranslation;
    }
    result.bindMatrix = result.localBindMatrix * transform;
    result.inverseBindMatrix = XMMatrixInverse(nullptr, result.bindMatrix);
    if (!node["children"].is_null()) {
        for (auto& child: node["children"]) {
            int id = child.get<int>();
            load_node(json, id, vector, result.bindMatrix);
            vector[id].parent = nodeId;
        }
    }
}

void load_gltf(CommandList& copy, const std::vector<std::string>& inputLayout, const std::wstring& filename, Object& result) {
    static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    nlohmann::json gltf;
    {
        std::ifstream f(filename);
        std::stringstream buffer;
        buffer << f.rdbuf();
        gltf = nlohmann::json::parse(buffer.str());
    }

    auto& bufferData = result.buffersData;
    for (auto& buf: gltf["buffers"]) {
        auto bufferFilename = filename.substr(0, filename.find_last_of(L'/') + 1) + converter.from_bytes(buf["uri"].get<std::string>());
        std::ifstream f(bufferFilename, std::ios_base::in | std::ios_base::binary);
        if (!f.is_open()) {
            // continue
            LOG_ERROR("Buffer %ls cannot be loaded", bufferFilename.data());
            return;
        }
        f.seekg(0, std::ios::end);
        int64_t size = f.tellg();

        std::string& buffer = bufferData.emplace_back(size, 0);
        f.seekg(0);
        f.read(&buffer[0], size);
        UINT64 uploadAllocatedSize = (size + 255) & (~255);
        result.buffers.emplace_back(CD3DX12_RESOURCE_DESC::Buffer(uploadAllocatedSize));
    }

    for (auto& primitive: gltf["meshes"][0]["primitives"]) {
        MeshPrimitive& mp = result.primitives.emplace_back();

        uint32_t buffer;
        gltf_buffers(gltf, primitive["indices"].get<int>(), mp.indexCount, buffer, mp.indexViews.BufferLocation, mp.indexViews.SizeInBytes, mp.indexViews.Format);
        patch_winding((uint16_t*) &bufferData[buffer][mp.indexViews.BufferLocation], mp.indexCount);
        mp.indexViews.BufferLocation += result.buffers[buffer]->GetGPUVirtualAddress();

        {
            mp.materialId = primitive["material"].get<uint16_t>();

            result.materials.resize(std::max(result.materials.size(), (size_t) mp.materialId + 1));
            auto& mat = result.materials[mp.materialId];
            mat.features = LIGHTING_ALBEDO_MAPPING |
                           LIGHTING_NORMAL_MAPPING |
                           LIGHTING_SRMA_MAPPING |
                           LIGHTING_SSS_MAPPING |
                           LIGHTING_MASK_MAPPING |
                           LIGHTING_ENABLE_SSS |
                           MATERIAL_HAS_SKELETON |
                           LIGHTING_SATURATE_DIFFUSE |
                           LIGHTING_HAS_SHADOWS;
            mat.alphaClip = 0;
            auto& pbr = gltf["materials"][mp.materialId]["pbrMetallicRoughness"];
            auto& baseColor = pbr["baseColorFactor"];
            mat.albedo = {baseColor[0].get<float>(), baseColor[1].get<float>(), baseColor[2].get<float>(), baseColor[3].get<float>()};
            mat.srma.y = pbr["roughnessFactor"].get<float>();
            mat.srma.z = pbr["metallicFactor"].get<float>();
            mat.srma.w = 1.0;
        }

        auto& attributes = primitive["attributes"];
        mp.vertexViews.resize(inputLayout.size());
        uint32_t slot = 0;
        for (const auto& input: inputLayout) {
            auto attr = attributes[input];
            if (attributes[input].is_null()) {
                attr = attributes[input + "_0"];
            }

            D3D12_VERTEX_BUFFER_VIEW& view = mp.vertexViews[slot++];
            DXGI_FORMAT format;
            gltf_buffers(gltf, attr.get<int>(), mp.vertexCount, buffer, view.BufferLocation, view.SizeInBytes, format);
            view.StrideInBytes = view.SizeInBytes / mp.vertexCount;
            if (input.compare(0, 7, "TANGENT") == 0) {
                patch_bitangent((float*) &bufferData[buffer][view.BufferLocation], mp.vertexCount);
            }
            view.BufferLocation += result.buffers[buffer]->GetGPUVirtualAddress();
        }
    }

    result.nodes.resize(gltf["nodes"].size());
    for (auto& skin: gltf["skins"]) {
        XMMATRIX identity = XMMatrixIdentity();
        for (auto& joint: skin["joints"]) {
            uint32_t j = joint.get<uint32_t>();
            result.nodes[j].joint = result.joints.size();
            result.joints.emplace_back(j);
        }
        load_node(gltf["nodes"], skin["skeleton"].get<int>(), result.nodes, identity);
    }

    for (auto& animation: gltf["animations"]) {
        result.animations.emplace_back(load_animation(gltf, animation, bufferData));
    }
    /*
    {
        std::vector<bool> test(result.nodes.size());
        for (auto & anim : result.animations) {
            int i = 0;
            if (anim.name.ends_with("RefPose")) continue;
            for (auto & node : anim.nodes) {
                if (!node.rotation.empty() || !node.translation.empty()) {
                    test[i] = true;
                }
                i++;
            }
        }
        for (int i = 0 ; i < test.size() ; i++) {
            if (!test[i]) {
                LOG_INFO("NO ANIMATION FOR %s", result.nodes[i].name.c_str());
            }
        }
    }
    */
    for (int i = 0; i < bufferData.size(); i++) {
        Resource& r = result.buffers[i];
        const UINT64 allocated = r->GetDesc().Width;
        Resource upload(CD3DX12_RESOURCE_DESC::Buffer(allocated), D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, D3D12_HEAP_TYPE_UPLOAD);
        ThrowIfFailed(upload->SetName(L"Upload buffer"));
        void* mem;
        ThrowIfFailed(upload->Map(0, nullptr, &mem));
        auto& buffer = bufferData[i];
        memcpy(mem, buffer.data(), buffer.size());
        upload->Unmap(0, nullptr);

        copy->CopyBufferRegion(r.ptr(), 0, upload.ptr(), 0, allocated);

        copy.track(upload.d3d12());
    }
}
