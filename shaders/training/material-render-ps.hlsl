struct PixelShaderInput
{
    float3 normalWS : NORMAL;
    float4 tangentWS : TANGENT;
    float2 texCoord : TEXCOORD;
};

struct GBuffer
{
    float4 albedo : SV_Target0;
    float4 srma : SV_Target1;
    float4 sss : SV_Target2;
    float4 normal : SV_Target3;
};

Texture2D DiffuseTexture : register(t0);
Texture2D MaskTexture    : register(t1);
Texture2D NormalTexture  : register(t2);
Texture2D SRMATexture    : register(t3);
Texture2D SSSTexture     : register(t4);

SamplerState SAMPLER : register(s0);

SHADER_DECLARATION

void main(PixelShaderInput IN, out GBuffer OUT) {
SHADER_BODY
}
