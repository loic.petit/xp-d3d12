struct Vertex
{
    uint id : SV_VertexID;
};

struct VertexShaderOutput
{
    float2 uv : UV;
    float2 screenSpace : SCREEN_SPACE;
    float4 position : SV_Position;
};

/*
(-1,1)
0  ---	1  (3,1)
|   /
2
(-1,-3)

0,1
1,1
0,0

(0, 0)	(2, 0)

(0, -2)
*/

VertexShaderOutput main(Vertex IN)
{
	VertexShaderOutput OUT;
    float2 positionXY = float2(-1.0, -3.0) + float2(IN.id % 2, IN.id <= 1) * 4.0;
    OUT.position = float4(positionXY, 0, 1.0);
    OUT.uv = float2(IN.id % 2, IN.id >> 1) * 2.0;
    OUT.screenSpace = positionXY;
    return OUT;
}