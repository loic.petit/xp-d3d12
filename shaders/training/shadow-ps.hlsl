struct PixelShaderInput
{
    float linearDepth : DEPTH;
};

struct PixelShaderOutput
{
    float depth : SV_Depth;
};

void main(PixelShaderInput IN, out PixelShaderOutput OUT)
{
	OUT.depth = IN.linearDepth;
}
