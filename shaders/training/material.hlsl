#define LIGHTING_NONE 0
#define LIGHTING_ALBEDO_MAPPING 0x1
#define LIGHTING_NORMAL_MAPPING 0x2
#define LIGHTING_SRMA_MAPPING 0x4
#define LIGHTING_SSS_MAPPING 0x8
#define LIGHTING_MASK_MAPPING 0x10
#define LIGHTING_ENABLE_SSS 0x20
#define LIGHTING_SATURATE_DIFFUSE 0x40
#define MATERIAL_HAS_SKELETON 0x80
#define LIGHTING_HAS_SHADOWS 0x100

#define SHADOWMAP_TEXEL_WIDTH 4096
#define SHADOWMAP_TEXEL_HEIGHT 4096

#define MAX_LIGHTS 8

struct Material
{
    float4 albedo;
    float4 srma;
    float4 sss;
    float4 customColor[3];
    float4 fadeInner;
    float4 fadeOuter;
    float4 fadeParams;
    float alphaClip;
};

struct LightSource {
    float4 position;
    float4 color;
    matrix viewProjection;
};

struct Lights {
    LightSource lights[MAX_LIGHTS];
    uint numLights;
};

struct Feature {
	uint f;
};
