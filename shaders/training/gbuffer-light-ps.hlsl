#include "material.hlsl"
#include "light.hlsl"

struct PixelShaderInput
{
    float4 position : SV_Position;
};

struct Params
{
    matrix invViewProjectionMatrix;
    float4 viewPosition;
    LightSource light;
};

Texture2D<float4> AlbedoGBuf : register(t0);
Texture2D<float4> SRMAGBuf : register(t1);
Texture2D<float4> SSSGBuf : register(t2);
Texture2D<float4> NormalGBuf : register(t3);
Texture2D<float1> DepthGBuf : register(t4);
Texture2D<float1> ShadowMap : register(t0, space1);

ConstantBuffer<Feature> FEATURES : register(b0);
ConstantBuffer<Params> PARAMS : register(b1);

SamplerState SAMPLER : register(s0);
SamplerComparisonState SHADOW_SAMPLER : register(s1);

#define DISCARD discard
//#define DISCARD return float4(0,0,0.2,1)

float computeShadows(float2 coords, float linearDepth) {
    coords.x = coords.x/2 + 0.5;
    coords.y = coords.y/-2 + 0.5;
    float occlusion = 0;
    float2 shift;

    for (float i = -1.5 ; i <= 1.51 ; i += 1.0) {
        for (float j = -1.5 ; j <= 1.51 ; j += 1.0) {
            shift = float2(i/SHADOWMAP_TEXEL_WIDTH, j/SHADOWMAP_TEXEL_HEIGHT);
            occlusion += ShadowMap.SampleCmpLevelZero(SHADOW_SAMPLER, coords.xy + shift, linearDepth).r;
        }
    }

    return occlusion / 16.0f;
}
/*
float4 main(PixelShaderInput IN) : SV_Target {
	return float4(0.1, 0.1, 0.1, 1.0);
}
*/
float4 main(PixelShaderInput IN) : SV_Target {
    // position in world space
//    OUT.uv = ;
//    OUT.uv.y = 1-OUT.uv.y;

	uint2 texSize;
    DepthGBuf.GetDimensions(texSize.x, texSize.y);
    uint2 coordinates = IN.position.xy;
    float2 screenSpace = (IN.position.xy/texSize)*float2(2,-2) + float2(-1,1);

    float depth = DepthGBuf[coordinates].r;
    float4 position = float4(screenSpace, depth, 1.0);
    position = mul(PARAMS.invViewProjectionMatrix, position);

    // discard based on shadow
    position /= position.w;
	float4 positionLS = mul(PARAMS.light.viewProjection, position);
    float linearDepth = positionLS.z;
	positionLS /= positionLS.w;
	if (linearDepth < 0.0 || linearDepth > 1.0)
		DISCARD;
	float r = (positionLS.x * positionLS.x + positionLS.y * positionLS.y);
	if (r >= 1)
		DISCARD;
	float occlusion = 1;
	if (FEATURES.f & LIGHTING_HAS_SHADOWS) {
		occlusion = computeShadows(positionLS.xy, linearDepth);
		if (occlusion == 0)
			DISCARD;
	}

    float3 albedo = AlbedoGBuf[coordinates].rgb;
    float4 sss = SSSGBuf[coordinates];
    float4 srma = SRMAGBuf[coordinates];
    float4 normal = NormalGBuf[coordinates];

    float spec = srma.r;
    float roughness = max(0.04f, srma.g);
    float metallic = srma.b;
    float ao = srma.a;

    float3 view = PARAMS.viewPosition.xyz - position.xyz;

    float3 V = normalize(view);
    float3 N = normalize(normal.xyz * 2 - 1);
    float NdotV = max(dot(N, V), 0.0);

    // Calculate F0 based on specular strength
    float3 F0 = lerp(0.08 * spec, albedo, metallic);

	float3 light = PARAMS.light.position.xyz - position.xyz;
	float D = length(light);
	float3 L = normalize(light);
	float3 H = normalize(L+V);

	float attenuation = (1 - r * r) / (1+0.01*D*D);
	float3 radiance = PARAMS.light.color.rgb * attenuation * occlusion;

	float NdotL = max(dot(N, L), 0.0);
	float HdotN = max(dot(H, N), 0.0);
	float HdotV = max(dot(H, V), 0.0);

	// cook-torrance brdf
	float NDF = DistributionGGX(HdotN, roughness);
	float G = GeometrySmith(NdotV, NdotL, roughness);
	float3 F = FresnelSchlick(HdotV, F0);

	float3 numerator = NDF * G * F;
	float denominator = 4.0 * NdotV * NdotL + 0.0001;
	float3 specular = numerator / denominator;
	//		float3 specular = EnvBRDFApprox( SpecularColor, roughness, NdotV );

	// removing kD actually... seems to not be part of the style of SFV
	float3 kD = 1.0;
	if (!(FEATURES.f & LIGHTING_SATURATE_DIFFUSE)) {
		float3 kS = F;
		kD -= kS;
		kD *= 1.0 - metallic;
	}
	float3 diffuse = kD*albedo / PI;

	float3 subSurface = 0;
	// adding subSurface
	if (FEATURES.f & LIGHTING_ENABLE_SSS) {
		subSurface = unrealSubsurface(sss.rgb, sss.a, ao, L, V, N);
//        			subSurface = disneySubsurface(NdotL, NdotV, HdotV, roughness, sss.rgb);
//		subSurface *= sss.a;
	}
	float3 Lo = (diffuse + specular) * NdotL + subSurface;

    return float4(Lo * radiance, 1.0);
}
