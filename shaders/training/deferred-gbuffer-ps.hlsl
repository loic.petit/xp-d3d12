#include "material.hlsl"
#include "light.hlsl"

struct Mat
{
    matrix modelMatrix;
    matrix modelViewProjectionMatrix;
    float4 viewPosition;
};

struct PixelShaderInput
{
    float3 positionWS : POSITION;
    float3 normalWS : NORMAL;
    float4 tangentWS : TANGENT;
    float2 texCoord : TEXCOORD;
};

struct GBuffer
{
    float4 albedo : SV_Target0;
    float4 srma : SV_Target1;
    float4 sss : SV_Target2;
    float4 normal : SV_Target3;
    float4 emission : SV_Target4;
};

ConstantBuffer<Feature> FEATURES : register(b0);
ConstantBuffer<Mat> MATRICES : register(b1);
ConstantBuffer<Material> MATERIAL : register(b3);

Texture2D DiffuseTexture : register(t0);
Texture2D MaskTexture    : register(t1);
Texture2D NormalTexture  : register(t2);
Texture2D SRMATexture    : register(t3);
Texture2D SSSTexture     : register(t4);

SamplerState SAMPLER : register(s0);

static inline float3 applyMaskingColors(float3 texColor, float2 texCoord) {
	float3 result = texColor;
	float4 maskVector = MaskTexture.Sample(SAMPLER, texCoord);

	result = lerp(result, texColor * MATERIAL.customColor[0].rgb, maskVector.r);
	result = lerp(result, texColor * MATERIAL.customColor[1].rgb, maskVector.g);
	result = lerp(result, texColor * MATERIAL.customColor[2].rgb, maskVector.b);

	return result;
}

void main(PixelShaderInput IN, out GBuffer OUT) {
    //if (true) return phong(IN);
	float4 texColor = MATERIAL.albedo;
	if (FEATURES.f & LIGHTING_ALBEDO_MAPPING) {
		texColor = DiffuseTexture.Sample(SAMPLER, IN.texCoord);
	}
	float alpha = texColor.a;
    if (alpha < MATERIAL.alphaClip)
        discard;
    float3 N = normalize(IN.normalWS);
	float3 albedo = texColor.rgb;;
	if (FEATURES.f & LIGHTING_MASK_MAPPING && MATERIAL.customColor[0].a != 0) {
		albedo = applyMaskingColors(albedo, IN.texCoord).rgb;
	}

	// lighting
    OUT.srma = MATERIAL.srma;
    OUT.sss = MATERIAL.sss;
	if (FEATURES.f & LIGHTING_SSS_MAPPING) {
        OUT.sss = SSSTexture.Sample(SAMPLER, IN.texCoord);
	}
	if (FEATURES.f & LIGHTING_SRMA_MAPPING) {
        OUT.srma = SRMATexture.Sample(SAMPLER, IN.texCoord);
	}

	// prepare vectors for PBR
    if (FEATURES.f & LIGHTING_NORMAL_MAPPING) {
        float3 normalTS = float3(NormalTexture.Sample(SAMPLER, IN.texCoord).xy, 1);
        normalTS = normalTS * 2.0 - 1.0;
        normalTS.z = sqrt(1 - normalTS.x * normalTS.x + normalTS.y * normalTS.y);

        // create TBN
        float3 T = IN.tangentWS.xyz;
        // re-orthogonalize T
        T = normalize(T - cross(dot(T, N), N));
        float3 B = cross(N, T) * IN.tangentWS.w;
        float3x3 tbn = {
                T.x, B.x, N.x,
                T.y, B.y, N.y,
                T.z, B.z, N.z,
        };

        N = mul(tbn, normalTS);
    }
    OUT.normal = float4((N + 1) / 2, 1.0);

	float3 V = normalize(IN.positionWS - MATRICES.viewPosition.xyz);
	float VdotN = dot(V, N);
	// fresnel
	albedo = lerp(albedo, albedo*albedo, clamp(pow(1.0 + VdotN, 2.0), 0.0, 1.0));
	float3 emissionFade = 0;
	float emissiveScale = 0;
	if (FEATURES.f & LIGHTING_MASK_MAPPING && MATERIAL.customColor[0].a != 0) {
		// default
		float4 inner = MATERIAL.fadeInner;
		float4 outer = MATERIAL.fadeOuter;
		float4 params = MATERIAL.fadeParams;
		// test
//		float4 inner = float4(1, 0, 1, 0.5);
//		float4 outer = float4(0, 1, 0, 1);
//		float4 params = float4(2, 1, 1, 1);
		// ex
//		float4 inner = 0;
//		float4 outer = float4(1, 0.0448045, 0, 14.1738);
//		float4 params = float4(2, 1, 1, 1);
		// fang
//		float4 inner = float4(0.629325, 0.455, 3.5, 0.125);
//		float4 outer = float4(-0.05, -0.1, 0.02, 0.6);
//		float4 params = float4(2, 0.5, 1, 1);
		// psyko
//		float4 inner = float4(-0.2, -0.6, 0.2, 0.3);
//		float4 outer = float4(2.00001, 0.600003, 30, 0.1);
//		float4 params = float4(0, 3, 1, 1);
		// blanka electricity
//		float4 inner = float4(0.889896, 0.483515, -0.21787, 0.933334);
//		float4 outer = float4(13.242, 3.90477, 0.105216, 1.24333);
//		float4 params = float4(2, 2.2, 1, 1);
		// p1
//		float4 inner = float4(0.64, 0.048, 0.048, 0.85);
//		float4 outer = float4(0.64, 0.048, 0.048, 0.85);
//		float4 params = float4(2, 2.2, 1, 1);
		// p2
//		float4 inner = float4(0, 0.211821, 1, 0.85);
//		float4 outer = float4(0, 0.211821, 1, 0.85);
//		float4 params = float4(2, 2.2, 1, 1);
		// ryu electricity
//		float4 inner = float4(0.682632, 0.451396, -0.870413, 0.983333);
//		float4 outer = float4(9.67104, 2.05003, 0.245785, 1.47222);
//		float4 params = float4(2, 2.2, 1, 1);
		// ken fire
//		float4 inner = float4(-0.6, -0.6, -0.3, 0.26);
//		float4 outer = float4(60, 8, 1, 0.5);
//		float4 params = float4(0, 2.2, 1, 1);

		float fresnel = clamp(pow(1.0 + VdotN, params.y), 0.0, 1.0);
		if (params.r == 2) {
			albedo = lerp(
				lerp(albedo, inner.rgb, clamp(inner.a, 0, 1)),
				lerp(albedo, outer.rgb, clamp(outer.a, 0, 1)),
				fresnel
			);
			emissionFade = outer.rgb * outer.a * fresnel * params.b;
		} else {
			albedo = albedo + lerp(
				inner.rgb * inner.a,
				outer.rgb * outer.a,
				fresnel
			);
			emissionFade = outer.rgb * fresnel * params.b;
		}
		OUT.srma = float4(clamp(OUT.srma.r - params.a, 0, 1), OUT.srma.g, OUT.srma.b, OUT.srma.a);
//		emissiveScale = 0.5;
//		emissionFade = 0;
	}

    OUT.albedo = float4(albedo, alpha);
    OUT.emission = float4(albedo * alpha * emissiveScale + emissionFade, 1);
}
