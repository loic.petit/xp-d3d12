#include "material.hlsl"
#include "light.hlsl"

float3 uncharted2_tonemap_partial(float3 x)
{
    float A = 0.15f;
    float B = 0.50f;
    float C = 0.10f;
    float D = 0.20f;
    float E = 0.02f;
    float F = 0.30f;
    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

float3 unreal_tonemap(float3 x) {
	float blackClip = 0.0270874;
	float whiteClip = 0.259612;
	float slope = 0.773196;
	float shoulder = 0.491064;
	float toe = 0.112864;
	float saturation = 16;

	float offset = blackClip * blackClip / (blackClip + toe);
	float slopeBlack = (1 - slope * (saturation - whiteClip) / (saturation + shoulder))/(whiteClip - offset);

	float3 blackClippedColor = max(blackClip - x, 0);
	float3 whiteClippedColor = max(x, whiteClip);
	float3 color = slope * (whiteClippedColor - whiteClip) / (whiteClippedColor + shoulder);
	color += slopeBlack * clamp(x, blackClip, whiteClip);
	color -= slopeBlack * toe * blackClippedColor / (blackClippedColor + toe);
	color -= slopeBlack * offset;
	return color;
}

float3 uncharted2_filmic(float3 v)
{
    return uncharted2_tonemap_partial(v) / uncharted2_tonemap_partial(11.2f);
}

struct PixelShaderInput
{
    float2 uv : UV;
    float2 screenSpace : SCREEN_SPACE;
};

struct Params
{
    float exposure;
};

Texture2D<float4> Source : register(t0);

ConstantBuffer<Params> PARAMS : register(b0);

float4 main(PixelShaderInput IN) : SV_Target {
	uint2 coordinates;
    Source.GetDimensions(coordinates.x, coordinates.y);
    coordinates *= IN.uv;
    float3 color = Source[coordinates].rgb;

	// Tone mapping
	// Using Uncharted 2 tone mapping
	// si > whiteclip
	// color = slope * (r0 - whiteClip) / (r0 + shoulder) + 1.03 * whiteclip - 0.0054;
	// color = slope * (r0 - whiteClip) / (r0 + shoulder) + 1.03 * whiteclip - 0.0054;
	// si < blackclip
	// color = 1.03 * blackclip - 1.03 * toe * (blackClip-r0) / (blackClip-r0 + toe) - 0.0054
	// 0 = X * blackclip - X * toe * blackClip / (blackClip + toe) - Y
	// Y = X * blackClip * blackClip / (blackClip + toe)
	// Y = 1.03 * blackClip * blackClip / (blackClip + toe)
	// si infty
	// slope * (whiteClippedColor - whiteClip) / (whiteClippedColor + shoulder) + slopeBlack * (clamp(r0, blackClip, whiteClip) - toe * blackClippedColor / (blackClippedColor + toe) - blackClip * blackClip / (blackClip + toe));
	// slope + slopeBlack * (whiteClip - blackClip * blackClip / (blackClip + toe))



//	color = (max(r0, 0.259612) * 0.773196 - 0.200731) / (max(r0, 0.259612) + 0.259612 * 2 - 0.0270874) + clamp(r0, 0.0270874, 0.259612) * 1.03 - 0.11625 * b / (b + 0.112864) - 0.0054;
	// (max(a1, 0.259612) * 0.773196 - 0.20731) / (max(a1, 0.259612) + 0.491064) + clamp(a1, 0.0270874, 0.259612) * 1.03 - 0.11625 * max(0.0270874-a1, 0) / (max(0.0270874-a1, 0) + 0.112864) - 0.0054;
	color = unreal_tonemap(color * PARAMS.exposure);
    color = toSRGB(color);

    return float4(color, dot(color, float3(0.299, 0.587, 0.114)));
}
