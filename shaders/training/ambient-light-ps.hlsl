#include "material.hlsl"
#include "light.hlsl"


#define AO_COMPUTED 1
#define AO_MAPPED 2

struct PixelShaderInput
{
    float2 uv : UV;
    float2 screenSpace : SCREEN_SPACE;
};

struct Params
{
    float4 color;
    uint flags;
    float power;
};

Texture2D<float4> AlbedoGBuf : register(t0);
Texture2D<float4> SRMAGBuf : register(t1);
Texture2D<float1> AOBuf : register(t2);

ConstantBuffer<Params> PARAMS : register(b0);

float4 main(PixelShaderInput IN) : SV_Target {
    // position in world space
	uint2 dimensions;
    AlbedoGBuf.GetDimensions(dimensions.x, dimensions.y);
    int2 coordinates = IN.uv * dimensions;

    float3 albedo = AlbedoGBuf[coordinates].rgb;

	float ao = 0;
	if (PARAMS.flags & AO_COMPUTED) {
		uint c = 0;
		uint min_x = max(coordinates.x-2, 0);
		uint min_y = max(coordinates.y-2, 0);
		uint max_x = min(coordinates.x+3, dimensions.x);
		uint max_y = min(coordinates.y+3, dimensions.y);
		for (uint x = min_x ; x < max_x ; x++) {
			for (uint y = min_y ; y < max_y ; y++) {
				ao += AOBuf[uint2(x,y)].r;
			}
		}
		ao /= (max_x - min_x) * (max_y - min_y);
	} else {
		ao = 1.0;
	}
	if (PARAMS.flags & AO_MAPPED) {
    	float4 srma = SRMAGBuf[coordinates];
		ao *= srma.a;
	}
	ao = pow(abs(ao), PARAMS.power);

	// ambient
    return float4(PARAMS.color.rgb * albedo * ao, 1.0);
}
