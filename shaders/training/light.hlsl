#define PI 3.1415926535

float DistributionGGX(float NdotH, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH2 = NdotH*NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float VisibilitySmith(float NoV, float NoL, float roughness)
{
    float a  = roughness*roughness;
    float a2 = a * a;
    float GGXV = NoL * sqrt(NoV * NoV * (1.0 - a2) + a2);
    float GGXL = NoV * sqrt(NoL * NoL * (1.0 - a2) + a2);
    return 0.5 / (GGXV + GGXL);
}

float GeometrySmith(float NdotV, float NdotL, float roughness)
{
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

float SchlickWeight(float cosTheta) {
    float m = clamp(1. - cosTheta, 0., 1.);
    return (m * m) * (m * m) * m;
}

float3 FresnelSchlick(float cosTheta, float3 F0)
{
    return F0 + (1.0 - F0) * SchlickWeight(cosTheta);
}

float3 unrealSubsurface(float3 baseColor, float opacity, float ao, float3 L, float3 V, float3 N)
{
    float3 H = normalize(V + L);

    // to get an effect when you see through the material
    // hard coded pow constant
    float InScatter = pow(saturate(dot(L, -V)), 12) * lerp(3, .1f, opacity);
    // wrap around lighting, /(PI*2) to be energy consistent (hack do get some view dependnt and light dependent effect)
    float OpacityFactor = opacity;
    // Opacity of 0 gives no normal dependent lighting, Opacity of 1 gives strong normal contribution
    float NormalContribution = saturate(dot(N, H) * OpacityFactor + 1 - OpacityFactor);
    float BackScatter = ao * NormalContribution / (PI * 2);

    // lerp to never exceed 1 (energy conserving)
    return baseColor * lerp(BackScatter, 1, InScatter);
}

float3 disneySubsurface(float NdotL, float NdotV, float LdotH, float roughness, float3 baseColor) {
    float FL = SchlickWeight(NdotL), FV = SchlickWeight(NdotV);
    float Fss90 = LdotH*LdotH*roughness;
    float Fss = lerp(1.0, Fss90, FL) * lerp(1.0, Fss90, FV);
    float ss = 1.25 * (Fss * (1. / (NdotL + NdotV + 0.00001) - .5) + .5);

    return (1./PI) * ss * baseColor;
}

float3 ACESFilmic( float3 x, float A, float B, float C, float D, float E, float F )
{
    return ( ( x * ( A * x + C * B ) + D * E ) / ( x * ( A * x + B ) + D * F ) ) - ( E / F );
}

float3 toLinear(float3 x)
{
    return x < 0.04045f ? x / 12.92 : pow(abs(x + 0.055) / 1.055, 2.4);
}

float3 toSRGB(float3 x)
{
    return x < 0.0031308 ? 12.92 * x : 1.055 * pow(abs(x), 1.0 / 2.4) - 0.055;
}