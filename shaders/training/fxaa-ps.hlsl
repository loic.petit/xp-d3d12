#define FXAA_PC 1
#define FXAA_HLSL_5 1
#define FXAA_QUALITY__PRESET 13

#pragma warning(disable: 4000)
#include "../extern/fxaa.hlsl"

struct PixelShaderInput
{
    float2 uv : UV;
    float2 screenSpace : SCREEN_SPACE;
};

struct FxaaParams {
	float4 consoleRcpFrameOpt;
	float4 consoleRcpFrameOpt2;
	float4 console360RcpFrameOpt2;
	float4 console360ConstDir;
	float2 halfTexel;
	float2 qualityRcpFrame;
	float qualitySubpix;
	float qualityEdgeThreshold;
	float qualityEdgeThresholdMin;
	float consoleEdgeSharpness;
	float consoleEdgeThreshold;
	float consoleEdgeThresholdMin;
};

ConstantBuffer<FxaaParams> PARAMS : register(b0);
Texture2D<float4> Source : register(t0);
SamplerState SAMPLER : register(s0);

float4 main(PixelShaderInput IN) : SV_Target {
    float2 pos = IN.uv;
	FxaaTex tex;
	tex.smpl = SAMPLER;
	tex.tex = Source;

	float4 result = FxaaPixelShader(
		pos,
		float4(pos.x-PARAMS.halfTexel.x, pos.y-PARAMS.halfTexel.y, pos.x+PARAMS.halfTexel.x, pos.y+PARAMS.halfTexel.y),
		tex,
		tex,
		tex,
		PARAMS.qualityRcpFrame,
		PARAMS.consoleRcpFrameOpt,
		PARAMS.consoleRcpFrameOpt2,
		PARAMS.console360RcpFrameOpt2,
		PARAMS.qualitySubpix,
		PARAMS.qualityEdgeThreshold,
		PARAMS.qualityEdgeThresholdMin,
		PARAMS.consoleEdgeSharpness,
		PARAMS.consoleEdgeThreshold,
		PARAMS.consoleEdgeThresholdMin,
		PARAMS.console360ConstDir
	);
	return float4(result.rgb, 1);
}