struct Mat
{
    matrix modelViewProjectionMatrix;
};

ConstantBuffer<Mat> MATRICES : register(b2);

struct VertexPosition
{
    float3 position : POSITION;
};

struct VertexShaderOutput
{
//    float2 uv : UV;
//    float2 screenSpace : SCREEN_SPACE;
    float4 position : SV_Position;
};

VertexShaderOutput main(VertexPosition IN)
{
    VertexShaderOutput OUT;
	float4 pos = float4(IN.position, 1.0f);
    OUT.position = mul(MATRICES.modelViewProjectionMatrix, pos);
//	pos = OUT.position / OUT.position.w;
//    OUT.screenSpace = pos.xy;
//    OUT.uv = (OUT.screenSpace + 1) / 2;
//    OUT.uv.y = 1-OUT.uv.y;
//
    return OUT;
}