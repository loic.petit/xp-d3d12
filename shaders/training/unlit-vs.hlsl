#include "material.hlsl"

struct Mat
{
    matrix modelViewProjectionMatrix;
};

struct Transform {
	matrix transform;
};

ConstantBuffer<Mat> MATRICES : register(b1);
StructuredBuffer<Transform> NODES : register(t0, space1);

struct VertexPositionNormalTexture
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float4 tangent : TANGENT;
    uint4 color : COLOR;
    uint4 joints : JOINTS;
    uint4 weights : WEIGHTS;
    float2 texCoord : TEXCOORD;
};

struct VertexShaderOutput
{
    float4 position : SV_Position;
};

VertexShaderOutput main(VertexPositionNormalTexture IN)
{
    VertexShaderOutput OUT;
	float4 pos = float4(IN.position, 1.0f);
	float4 weights = IN.weights / 255.0;
    if (weights.x || weights.y || weights.z || weights.w) {
        matrix skinMat = (
                weights.x * NODES[int(IN.joints.x)].transform +
                weights.y * NODES[int(IN.joints.y)].transform +
                weights.z * NODES[int(IN.joints.z)].transform +
                weights.w * NODES[int(IN.joints.w)].transform
        );
        pos = mul(skinMat, pos);
    }
    OUT.position = mul(MATRICES.modelViewProjectionMatrix, pos);

    return OUT;
}