#include "material.hlsl"

struct Mat
{
    matrix modelMatrix;
    matrix modelViewProjectionMatrix;
    float4 viewPosition;
};

struct Transform {
	matrix transform[1000];
};

ConstantBuffer<Feature> FEATURES : register(b0);
ConstantBuffer<Mat> MATRICES : register(b1);
ConstantBuffer<Lights> LIGHTS : register(b3);
ConstantBuffer<Transform> NODES : register(b4);

struct VertexPositionNormalTexture
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float4 tangent : TANGENT;
    uint4 color : COLOR;
    uint4 joints : JOINTS;
    uint4 weights : WEIGHTS;
    float2 texCoord : TEXCOORD;
};

struct VertexShaderOutput
{
    float3 normalVS : NORMAL;
    float2 texCoord : TEXCOORD;
    float4 color : COLOR;
    float4 shadowPosition[MAX_LIGHTS] : T_SHADOW_POS;
    float3 tangentLightPos[MAX_LIGHTS] : T_LIGHT_POS;
    float3 tangentViewPos : T_VIEW_POS;
    float3 tangentFragPos : T_FRAG_POS;
    float4 position : SV_Position;
};

#define IDENTITY_MATRIX float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)

float3x3 extractSubMatrix(matrix m) {
    return float3x3(m[0].xyz, m[1].xyz, m[2].xyz);
}

VertexShaderOutput main(VertexPositionNormalTexture IN)
{
    VertexShaderOutput OUT;
    uint l = 0;
	float4 pos = float4(IN.position, 1.0f);
	float4 lightColor = float4(1.0, 1.0, 1.0, 1.0);
    float3x3 normalMapMat = extractSubMatrix(MATRICES.modelMatrix);

	if (FEATURES.f & MATERIAL_HAS_SKELETON) {
		float4 weights = IN.weights / 255.0;
		matrix skinMat = (
			weights.x * NODES.transform[int(IN.joints.x)] +
			weights.y * NODES.transform[int(IN.joints.y)] +
			weights.z * NODES.transform[int(IN.joints.z)] +
			weights.w * NODES.transform[int(IN.joints.w)]
		);
		pos = mul(skinMat, pos);
		// remove translation from transform matrix
        normalMapMat = mul(normalMapMat, extractSubMatrix(skinMat));
	}
    float4 posWS = mul(MATRICES.modelMatrix, pos);
	[unroll(MAX_LIGHTS)] for (l = 0 ; l < MAX_LIGHTS ; l++) {
        OUT.tangentLightPos[l] = LIGHTS.lights[l].position.xyz;
        OUT.shadowPosition[l] = mul(LIGHTS.lights[l].viewProjection, posWS);
	}

    OUT.tangentViewPos = MATRICES.viewPosition.xyz;
    OUT.tangentFragPos = posWS.xyz;
    OUT.normalVS = normalize(mul(normalMapMat, IN.normal));

	if (FEATURES.f & LIGHTING_NORMAL_MAPPING) {
        float3 normal = OUT.normalVS;
        float3 tangent = normalize(mul(normalMapMat, IN.tangent.xyz));
        float3 bitangent = cross(normal, tangent) * IN.tangent.w;
        // float3x3 is row based so it's already tbn = transpose([T,B,N])
        float3x3 tbn = float3x3(tangent, bitangent, normal);

        OUT.tangentViewPos = mul(tbn, OUT.tangentViewPos);
        [unroll(MAX_LIGHTS)] for (l = 0; l < LIGHTS.numLights; l++) {
            OUT.tangentLightPos[l] = mul(tbn, OUT.tangentLightPos[l]);
        }
        OUT.tangentFragPos = mul(tbn, OUT.tangentFragPos);
    }

    OUT.position = mul(MATRICES.modelViewProjectionMatrix, pos);
    OUT.texCoord = IN.texCoord;
    OUT.color = IN.color / 255.0f;

    return OUT;
}