#include "light.hlsl"
#include "material.hlsl"

struct PixelShaderInput
{
    float3 normalVS   : NORMAL;
    float2 texCoord   : TEXCOORD;
    float4 color      : COLOR;
    float4 shadowPosition[MAX_LIGHTS] : T_SHADOW_POS;
    float3 tangentLightPos[MAX_LIGHTS] : T_LIGHT_POS;
    float3 tangentViewPos : T_VIEW_POS;
    float3 tangentFragPos : T_FRAG_POS;
};

ConstantBuffer<Feature> FEATURES : register(b0);
ConstantBuffer<Material> MATERIAL : register(b2);
ConstantBuffer<Lights> LIGHTS : register(b3);

Texture2D DiffuseTexture : register(t0);
Texture2D MaskTexture    : register(t1);
Texture2D NormalTexture  : register(t2);
Texture2D SRMATexture    : register(t3);
Texture2D SSSTexture     : register(t4);
Texture2D ShadowMap[3]   : register(t0, space1);

SamplerState SAMPLER : register(s0);
SamplerComparisonState SHADOW_SAMPLER : register(s1);
SamplerState POINT_SAMPLER : register(s2);

float3 applyMaskingColors(float3 texColor, float2 texCoord) {
	float3 result = texColor;
    if (MATERIAL.customColor[0].a != 0) {
		float4 maskVector = MaskTexture.Sample(SAMPLER, texCoord);
		if (maskVector[0] > 0.5) {
			int maskId = 3;
			if (maskVector[1] <= 0.5 && maskVector[2] <= 0.5) {
				// red
				maskId = 0;
			} else if (maskVector[1] > 0.5 && maskVector[2] <= 0.5) {
				// yellow
				maskId = 1;
			} else {
				// purple / white
				maskId = 2;
			}
			result = sqrt(texColor * MATERIAL.customColor[maskId].rgb);
		}
	}
	return result;
}

float3 EnvBRDFApprox( float3 SpecularColor, float Roughness, float NoV )
{
    // [ Lazarov 2013, "Getting More Physical in Call of Duty: Black Ops II" ]
    // Adaptation to fit our G term.
    const float4 c0 = { -1, -0.0275, -0.572, 0.022 };
    const float4 c1 = { 1, 0.0425, 1.04, -0.04 };
    float4 r = Roughness * c0 + c1;
    float a004 = min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
    float2 AB = float2( -1.04, 1.04 ) * a004 + r.zw;

    // Anything less than 2% is physically impossible and is instead considered to be shadowing
    // In ES2 this is skipped for performance as the impact can be small
    // Note: this is needed for the 'specular' show flag to work, since it uses a SpecularColor of 0
    AB.y *= saturate( 50.0 * SpecularColor.g );

    return SpecularColor * AB.x + AB.y;
}

float3 disneyDiffuse(float NdotL, float NdotV, float LdotH, float roughness, float3 baseColor) {
    float FL = SchlickWeight(NdotL), FV = SchlickWeight(NdotV);
    float Fd90 = 0.5 + 2. * LdotH*LdotH * roughness;
    float Fd = lerp(1.0, Fd90, FL) * lerp(1.0, Fd90, FV);

    return (1./PI) * Fd * baseColor;
}

float computeShadows(float4 coords, uint l) {
    coords.x = coords.x/2 + 0.5;
    coords.y = coords.y/-2 + 0.5;
    float occlusion = 0;
    float2 shift;

    for (float i = -1.5 ; i <= 1.51 ; i += 1.0) {
        for (float j = -1.5 ; j <= 1.51 ; j += 1.0) {
            shift = float2(i/SHADOWMAP_TEXEL_WIDTH, j/SHADOWMAP_TEXEL_HEIGHT);
            occlusion += ShadowMap[l].SampleCmpLevelZero(SHADOW_SAMPLER, coords.xy + shift, coords.z).r;
        }
    }

    return occlusion / 16.0f;
}


float4 phong(PixelShaderInput IN)
{
    float ambiant = 0.1;
    float4 texColor = MATERIAL.albedo;
    if (FEATURES.f & LIGHTING_ALBEDO_MAPPING) {
        texColor = DiffuseTexture.Sample(SAMPLER, IN.texCoord);
    }
    float alpha = texColor.a;
    float3 albedo = texColor.rgb;
    if (FEATURES.f & LIGHTING_MASK_MAPPING) {
        albedo = applyMaskingColors(albedo, IN.texCoord).rgb;
    }
//    albedo = toLinear(albedo);

    // lighting
    float4 srma = MATERIAL.srma;
    float3 normal = 0;
    float4 sss = MATERIAL.sss;
    if (!(FEATURES.f & LIGHTING_NORMAL_MAPPING)) {
        normal = normalize(IN.normalVS);
    } else {
        normal = NormalTexture.Sample(SAMPLER, IN.texCoord).rgb * 255.0 / 254.0;
        normal = normalize(normal * 2.0 - 1.0);
    }
    if (FEATURES.f & LIGHTING_SSS_MAPPING) {
        sss = SSSTexture.Sample(SAMPLER, IN.texCoord);
    }
    if (FEATURES.f & LIGHTING_SRMA_MAPPING) {
        srma = SRMATexture.Sample(SAMPLER, IN.texCoord);
    }
    // lighting
    float3 viewDir = normalize(IN.tangentViewPos - IN.tangentFragPos);
    float3 Lo = 0;
    for (uint l = 0 ; l < LIGHTS.numLights ; l++) {
        float4 shadow = IN.shadowPosition[l];
        shadow = shadow / shadow.w;
        if (shadow.z < 0.0 || shadow.z > 1.0)
            continue;
        float r = (shadow.x * shadow.x + shadow.y * shadow.y);
        if (r >= 1)
            continue;

        float occlusion = 1.0;
        if (FEATURES.f & LIGHTING_HAS_SHADOWS) {
            occlusion = computeShadows(shadow, l);
        }
        if (occlusion == 0) {
            continue;
        }
        // diffuse
        float3 lightDir = normalize(IN.tangentLightPos[l] - IN.tangentFragPos);
        float distance = length(IN.tangentLightPos[l] - IN.tangentFragPos);
        float attenuation = (1 - r * r) / (1+0.01*distance*distance);
        float diff = max(dot(lightDir, normal), 0.0);
        float3 lightColor = LIGHTS.lights[l].color.rgb* attenuation;
        float3 diffuse = 0.8 * diff * lightColor;

        // specular
        float3 reflectDir = reflect(-lightDir, normal);
        float3 halfwayDir = normalize(lightDir + viewDir);
        float spec = pow(max(dot(reflectDir, viewDir), 0.0), 32.0);
        float3 specular = 0.2 * spec * lightColor;

        Lo += (diffuse + specular) * albedo;
    }
    Lo += ambiant * albedo;
    float3 color = Lo;
    // Tone mapping
    // Using Uncharted 2 tone mapping
//    color = ACESFilmic(color, 0.220, 0.300, 0.100, 0.200, 0.010, 0.300) / ACESFilmic(11.200, 0.220, 0.300, 0.100, 0.200, 0.010, 0.300);
//    color = toSRGB(color);

    return float4(color, 1.0);
}

float4 main(PixelShaderInput IN) : SV_Target {
    //if (true) return phong(IN);
	float4 texColor = MATERIAL.albedo;
	if (FEATURES.f & LIGHTING_ALBEDO_MAPPING) {
		texColor = DiffuseTexture.Sample(SAMPLER, IN.texCoord);
	}
	float alpha = texColor.a;
	float3 albedo = texColor.rgb;
	if (FEATURES.f & LIGHTING_MASK_MAPPING) {
		albedo = applyMaskingColors(albedo, IN.texCoord).rgb;
	}
	albedo = toLinear(albedo);

	// lighting
	float4 srma = MATERIAL.srma;
	float4 sss = MATERIAL.sss;
	if (FEATURES.f & LIGHTING_SSS_MAPPING) {
		sss = SSSTexture.Sample(SAMPLER, IN.texCoord);
	}
	if (FEATURES.f & LIGHTING_SRMA_MAPPING) {
		srma = SRMATexture.Sample(SAMPLER, IN.texCoord);
	}
	float spec = srma.r;
	float roughness = max(0.04f, srma.g);
	float metallic = srma.b;
	float ao = srma.a;
	float3 Lo = 0.0;

//    float DielectricSpecular = 0.08 * spec;
//    float3 DiffuseColor = albedo - albedo * metallic;	// 1 mad
//    float3 SpecularColor = (DielectricSpecular - DielectricSpecular * metallic) + albedo * metallic;	// 2 mad

    // Calculate F0 based on specular strength
    float3 F0 = lerp(0.08 * spec, albedo, metallic);

	// prepare vectors for PBR
    float3 V = normalize(IN.tangentViewPos - IN.tangentFragPos);

    float3 N;
    if (!(FEATURES.f & LIGHTING_NORMAL_MAPPING)) {
        N = normalize(IN.normalVS);
    } else {
        N = NormalTexture.Sample(SAMPLER, IN.texCoord).rgb * 255.0 / 254.0;
        N = normalize(N * 2.0 - 1.0);
    }

    for (uint l = 0 ; l < LIGHTS.numLights ; l++) {
    	float4 shadow = IN.shadowPosition[l];
    	shadow = shadow / shadow.w;
    	if (shadow.z < 0.0 || shadow.z > 1.0)
    		continue;
        float r = (shadow.x * shadow.x + shadow.y * shadow.y);
        if (r >= 1)
            continue;
        float occlusion = 1.0;
        if (FEATURES.f & LIGHTING_HAS_SHADOWS) {
            occlusion = computeShadows(shadow, l);
        }
        if (occlusion == 0) {
            continue;
        }
		float3 lightColor = LIGHTS.lights[l].color.rgb;

		float3 L = normalize(IN.tangentLightPos[l] - IN.tangentFragPos);
		float3 H = normalize(V + L);
		float distance = length(IN.tangentLightPos[l] - IN.tangentFragPos);
		float attenuation = (1 - r * r) / (1+0.01*distance*distance);
		float3 radiance = lightColor * attenuation * occlusion;

		float NdotL = max(dot(N, L), 0.0);
		float NdotV = max(dot(N, V), 0.0);
		float HdotN = max(dot(H, N), 0.0);
		float HdotV = max(dot(H, V), 0.0);

		// cook-torrance brdf
		float NDF = DistributionGGX(HdotN, roughness);
		float G = GeometrySmith(NdotV, NdotL, roughness);
		float3 F = FresnelSchlick(HdotV, F0);


		float3 numerator = NDF * G * F;
		float denominator = 4.0 * NdotV * NdotL + 0.0001;
        float3 specular = numerator / denominator;
//		float3 specular = EnvBRDFApprox( SpecularColor, roughness, NdotV );

		// removing kD actually... seems to not be part of the style of SFV
		float3 kD = 1.0;
		if (!(FEATURES.f & LIGHTING_SATURATE_DIFFUSE)) {
			float3 kS = F;
			kD -= kS;
			kD *= 1.0 - metallic;
		}
        /*
		// add to outgoing radiance Lo
		float3 diffuse = disneyDiffuse(NdotL, NdotV, HdotV, roughness, albedo);
        if (!(FEATURES.f & LIGHTING_SATURATE_DIFFUSE)) {
            diffuse *= 1.0 - metallic;
        }
        */
        float3 diffuse = kD*albedo / PI;

        float3 subSurface = 0;
		// adding subSurface
		if (FEATURES.f & LIGHTING_ENABLE_SSS) {
			subSurface = unrealSubsurface(sss.rgb, sss.a, ao, L, V, N);
//			subSurface = disneySubsurface(NdotL, NdotV, HdotV, roughness, sss.rgb);
//            diffuse += subSurface * (sss.a);
		}

		Lo += (diffuse + specular) * radiance * NdotL + subSurface * radiance;
	}
	// ambient
    float3 ambient = 0.05 * albedo * ao;

    float3 color = ambient + Lo;

	// Tone mapping
	// Using Uncharted 2 tone mapping
	color = ACESFilmic(color, 0.220, 0.300, 0.100, 0.200, 0.010, 0.300) / ACESFilmic(11.200, 0.220, 0.300, 0.100, 0.200, 0.010, 0.300);
    color = toSRGB(color);

    return float4(color, alpha);
}
