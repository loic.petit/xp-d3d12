struct Mat
{
    matrix modelMatrix;
    matrix modelViewProjectionMatrix;
    float4 viewPosition;
};

ConstantBuffer<Mat> MATRICES : register(b0);

#ifdef HAS_SKIN

struct Transform {
	matrix transform;
};
StructuredBuffer<Transform> NODES : register(t0, space1);

#endif

struct VertexPositionNormalTexture
{
    float3 position : POSITION;
    float3 normal : NORMAL;
    float4 tangent : TANGENT;
    uint4 color : COLOR;
    uint4 joints : JOINTS;
    uint4 weights : WEIGHTS;
    float2 texCoord : TEXCOORD;
};

struct VertexShaderOutput
{
    float3 normalWS : NORMAL;
    float4 tangentWS : TANGENT;
    float2 texCoord : TEXCOORD;
    float4 position : SV_Position;
};

#define IDENTITY_MATRIX float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)

float3x3 extractSubMatrix(matrix m) {
    return float3x3(m[0].xyz, m[1].xyz, m[2].xyz);
}

VertexShaderOutput main(VertexPositionNormalTexture IN)
{
    VertexShaderOutput OUT;
    uint l = 0;
	float4 pos = float4(IN.position, 1.0f);
    float3x3 normalMapMat = extractSubMatrix(MATRICES.modelMatrix);

#ifdef HAS_SKIN
	float4 weights = IN.weights / 255.0;
	matrix skinMat = (
		weights.x * NODES[int(IN.joints.x)].transform +
		weights.y * NODES[int(IN.joints.y)].transform +
		weights.z * NODES[int(IN.joints.z)].transform +
		weights.w * NODES[int(IN.joints.w)].transform
	);
	pos = mul(skinMat, pos);
	// remove translation from transform matrix
	normalMapMat = mul(normalMapMat, extractSubMatrix(skinMat));
#endif

    OUT.normalWS = normalize(mul(normalMapMat, IN.normal));
    OUT.tangentWS = float4(normalize(mul(normalMapMat, IN.tangent.xyz)), IN.tangent.w);

    OUT.position = mul(MATRICES.modelViewProjectionMatrix, pos);
    OUT.texCoord = IN.texCoord;

    return OUT;
}