struct PixelShaderInput {
    float2 uv : UV;
    float2 screenSpace : SCREEN_SPACE;
    float4 position : SV_Position;
};

struct RandomVectors {
	float4 samples[1024];
};

struct Parameters {
    matrix viewProjectionMatrix;
    matrix invViewProjectionMatrix;
	uint sampleCount;
	float radius;
};

ConstantBuffer<Parameters> PARAMETERS : register(b0);
ConstantBuffer<RandomVectors> SAMPLES : register(b1);

Texture2D<float4> RandomRotation : register(t0);
Texture2D<float4> NormalGBuf     : register(t1);
Texture2D<float1> DepthGBuf      : register(t2);

SamplerState DEPTH_SAMPLER : register(s0);

float3x3 randomTBN(uint2 pixel, float3 N) {
	uint2 modulo;
    RandomRotation.GetDimensions(modulo.x, modulo.y);
	pixel %= modulo;
	float3 random = RandomRotation[pixel].xyz;
	float3 T = normalize(random - N * dot(random, N));
	float3 B = cross(N, T);
	float3x3 invTBN = {
			T.x, B.x, N.x,
			T.y, B.y, N.y,
			T.z, B.z, N.z,
	};
	return invTBN;
}

float main(PixelShaderInput IN) : SV_Target {
	uint2 pixel = (uint2)IN.position.xy;

    float depth = DepthGBuf[pixel].r;
    float4 position = float4(IN.screenSpace, depth, 1.0);
    position = mul(PARAMETERS.invViewProjectionMatrix, position);
    position /= position.w;

	float3 normal = NormalGBuf[pixel].xyz * 2 - 1;
	float3x3 invTBN = randomTBN(pixel, normal);
	float occlusion = 0;
	for (uint i = 0 ; i < PARAMETERS.sampleCount ; i++) {
		float3 randomRay = mul(invTBN, SAMPLES.samples[i].xyz) * PARAMETERS.radius;
		float4 ray = float4(position.xyz + randomRay, 1.0f);
		float4 rayProjected = mul(PARAMETERS.viewProjectionMatrix, ray);
		rayProjected /= rayProjected.w;
		float2 rayUV = rayProjected.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
       	float collisionDepth = DepthGBuf.Sample(DEPTH_SAMPLER, rayUV.xy).r;
       	if (collisionDepth <= rayProjected.z) {
			float4 collisionPosition = mul(PARAMETERS.invViewProjectionMatrix, float4(IN.screenSpace, collisionDepth, 1.0));
			collisionPosition /= collisionPosition.w;
			float distance = length(collisionPosition.xyz - position.xyz) / PARAMETERS.radius;
			float rangeCheck = smoothstep(0, 1, 1/distance);
			occlusion += rangeCheck;
    	}
	}
	return 1 - occlusion / PARAMETERS.sampleCount;
}