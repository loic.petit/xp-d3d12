
#define BLOCK_SIZE 8

#define WIDTH_HEIGHT_EVEN 0
#define WIDTH_ODD_HEIGHT_EVEN 1
#define WIDTH_EVEN_HEIGHT_ODD 2
#define WIDTH_HEIGHT_ODD 3

struct ComputeShaderInput
{
    uint3 groupId          : SV_GroupID;
    uint3 groupThreadId    : SV_GroupThreadID;
    uint3 dispatchThreadId : SV_DispatchThreadID;
    uint  groupIndex       : SV_GroupIndex;
};

struct Args
{
    uint mipLevel;
    uint dimension;
    float2 texel;
    uint isSRGB;
};

ConstantBuffer<Args> args: register(b0);
Texture2D<float4> src: register(t0);
RWTexture2D<float4> output: register(u0);

SamplerState samplr: register(s0);

float3 toSRGB(float3 x)
{
    return x < 0.0031308 ? 12.92 * x : 1.055 * pow(abs(x), 1.0 / 2.4) - 0.055;
}

[numthreads(BLOCK_SIZE, BLOCK_SIZE, 1)]
void main(ComputeShaderInput IN) {
	float4 result;
	float2 uv, offset;

	switch(args.dimension) {
		case WIDTH_HEIGHT_EVEN:
            uv = args.texel * (IN.dispatchThreadId.xy + 0.5);
			result = src.SampleLevel(samplr, uv, args.mipLevel);
			break;
		case WIDTH_ODD_HEIGHT_EVEN:
//            uv = args.texel * (IN.dispatchThreadId.xy + float2(1.0/3, 0.5));
//            offset = args.texel * float2(1, 0) / 3;
            uv = args.texel * (IN.dispatchThreadId.xy + float2(0.25, 0.5));
            offset = args.texel * float2(0.5, 0);

            result = src.SampleLevel(samplr, uv, args.mipLevel);
			result += src.SampleLevel(samplr, uv + offset, args.mipLevel);
			result *= 0.5;
			break;
		case WIDTH_EVEN_HEIGHT_ODD:
//            uv = args.texel * (IN.dispatchThreadId.xy + float2(0.5, 1.0/3));
//            offset = args.texel * float2(0, 1) / 3;
            uv = args.texel * (IN.dispatchThreadId.xy + float2(0.5, 0.25));
            offset = args.texel * float2(0, 0.5);

			result = src.SampleLevel(samplr, uv, args.mipLevel);
			result += src.SampleLevel(samplr, uv + offset, args.mipLevel);
			result *= 0.5;
			break;
		case WIDTH_HEIGHT_ODD:
//            uv = args.texel * (IN.dispatchThreadId.xy + float2(1, 1)/3);
//            offset = args.texel / 3;
            uv = args.texel * (IN.dispatchThreadId.xy + float2(0.25, 0.25));
            offset = args.texel * 0.5;

			result = src.SampleLevel(samplr, uv, args.mipLevel);
			result += src.SampleLevel(samplr, uv + float2(offset.x, 0), args.mipLevel);
			result += src.SampleLevel(samplr, uv + float2(0, offset.y), args.mipLevel);
			result += src.SampleLevel(samplr, uv + offset, args.mipLevel);
			result *= 0.25;
			break;
		default:
			result = 0;
			break;
	}
	if (args.isSRGB) {
		result = float4(toSRGB(result.rgb), result.a);
	}
	output[IN.dispatchThreadId.xy] = result;
}