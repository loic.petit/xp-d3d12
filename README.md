# D3D12 Experiments

This is a D3D12 experiment repository for me. I have written a detailed article on this [available here](https://petitl.fr/articles/creating-a-directx12-3d-engine-when-you-know-nothing).

## Assets
To get the assets, I assume that you know how to export data from Street Fighter V. The paths are hardcoded with Cammy's
jumpsuit found in `StreetFighterV\Content\Chara\CMY\SkelMesh\17`. Export the gltf from Unreal Viewer in
`assets\model\Z10_17.{gltf,bin}`, and copy the exported textures in `assets\textures` and it should work.

## Build
This is a standard CMake build prepared to work with VS.

## Feature set
Look at the [TODO list](./TODO.md), you will have the list of things implemented and things I want to explore eventually.

## Licence & Dependencies
This project is published under [MIT licence](./LICENSE) for education purposes mainly. I strongly encourage anyone to NOT copy 
anything from here, if you do, it's not my problem.

As of dependencies:
* [nlohmann json](https://github.com/nlohmann/json): MIT
* [stb](https://github.com/nothings/stb): public licence / MIT
* [Dear Imgui](https://github.com/ocornut/imgui/): MIT
* [d3dx12](https://github.com/microsoft/DirectX-Headers/blob/main/include/directx/d3dx12.h): MIT
